<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bracket extends Model
{
    public function clan1() {
        return $this->belongsTo('App\Clan','clan_1');
    }

    public function clan2() {
        return $this->belongsTo('App\Clan','clan_2');
    }
}
