<?php namespace App;

use Illuminate\Database\Eloquent\Model;
/**
 * @mixin \Illuminate\Database\Eloquent\Builder
 * @mixin \Eloquent
 */
class Clan extends Model
{
    protected $table = 'clans';

    public function user() {
        return $this->hasMany('App\User');
    }

    public function tournamentRegistrations() {
        return $this->hasMany('App\TournamentRegistrations','id');
    }

    public function tournamentRegisteredPlayers() {
        return $this->hasMany('App\tournamentRegisteredPlayers','clan_id');
    }

    public function clanMembers() {
        return $this->hasMany('App\ClanMembers','clan_id');
    }

    public function standing() {
        return $this->hasMany('App\Standing','clan_id');
    }
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'clan_name', 'clan_email', 'clan_description' , 'clan_tel', 'clan_status', 'clan_email', 'clan_logo','clan_owner_id', 'secondary_captain'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
