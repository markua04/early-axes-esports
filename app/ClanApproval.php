<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClanApproval extends Model
{
    protected $table = 'clans';

    public function user() {
        return $this->hasMany('App\User');
    }
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'clan_id', 'user_clan_status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
