<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class ClanMembers extends Model
{

    public function user() {
        return $this->belongsTo('App\User','user_id');
    }

    public function clan() {
        return $this->belongsTo('App\Clan','clan_id');
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'clan_id','user_id','approval_id'
    ];
}
