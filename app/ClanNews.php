<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class ClanNews extends Model
{

    public function clan() {
        return $this->belongsTo('App\Clan','clan_id');
    }

    public function user() {
        return $this->belongsTo('App\User','user_id');
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'clan_id','user_id','title','content','banner'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'remember_token',
    ];
}
