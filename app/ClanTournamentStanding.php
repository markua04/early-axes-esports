<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class ClanTournamentStanding extends Model
{

    public function tournament() {
        return $this->hasMany('App\Tournament','tournament_id');
    }

    public function clan() {
        return $this->hasMany('App\Clan','id', 'clan_id');
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'clan_id','tournament_id', 'played', 'points', 'wins', 'losses', 'draws'
    ];
}