<?php namespace App\Console\Commands;

use App\Fixture;
use Carbon\Carbon;
use Illuminate\Console\Command;

class FixtureTime extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fix:times';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fixes times for time field in fixtures';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $fixtures = Fixture::all();
        foreach($fixtures as $fixture){
            $fixtureInstance = Fixture::find($fixture->id);
            $fixtureOldTime = $fixtureInstance->time;
            $fixture->time = date('H:i:s',strtotime($fixtureOldTime));
            $fixture->save();
        }
    }
}
