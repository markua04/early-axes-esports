<?php namespace App\Console\Commands;

use App\Http\Controllers\StandingsController;
use Illuminate\Console\Command;

class ForfeitOldFixtures extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fixtures:forfeit';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Updates all fixtures that are 7 days or older to forfeit';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $standingsController = new StandingsController();
        return $standingsController->forfeitFixtures();
    }
}
