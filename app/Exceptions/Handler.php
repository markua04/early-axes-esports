<?php

namespace App\Exceptions;

use App\Mail\ErrorsMail;
use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Request;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        if($exception->getMessage() !== ""){
            if($this->checkErrors($exception->getMessage()) == false){
                $this->emailErrors($exception,Request::url());
            }
        }
        parent::report($exception);
    }

    private function emailErrors($exception,$url){
        try{
            if(Auth::check()){
                Mail::to(env('DEV_EMAIL'))->send(new ErrorsMail($exception, $url, Auth::user()));
            }
        } catch(Exception $e){
            Log::error($e);
        }
    }

    private function checkErrors($message){
        $messages = [
            'The given data was invalid.'
        ];
        if(in_array($message,$messages)){
            return true;
        }
        return false;
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        return parent::render($request, $exception);
    }

}
