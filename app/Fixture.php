<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Fixture extends Model
{

    public function tournament() {
        return $this->belongsTo('App\Tournament', 'tournament_id','id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function fixtureResult() {
        return $this->hasMany('App\FixtureResult','fixture_id');
    }


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'date','time','tournament_id','clan_1','clan_2','clan_1_image','clan_2_image','bracket'
    ];
}
