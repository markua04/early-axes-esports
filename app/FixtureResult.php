<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class FixtureResult extends Model
{

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function fixture() {
        return $this->belongsTo('App\Fixture','fixture_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function clan1() {
        return $this->belongsTo('App\Clan','clan_1_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function clan2() {
        return $this->belongsTo('App\Clan','clan_2_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tournament() {
        return $this->belongsTo('App\Tournament','tournament_id');
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'fixture_id','tournament_id','clan_1','clan_2','clan_1_score','clan_2_score','clan_1_points','clan_2_points','image'
    ];
}
