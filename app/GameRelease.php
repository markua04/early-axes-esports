<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class GameRelease extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'start_countdown_date', 'start_countdown_time', 'end_countdown_date' , 'end_countdown_time', 'game', 'image'
    ];
}
