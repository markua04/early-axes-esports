<?php namespace App\Helpers;

class Lang
{
    public static function getStatus($key){
        $strings = [
            'contact_user_successful' => 'Thank you for contacting us! We will reply as soon as possible to your query!',
            'confirm_not_robot' => 'Please confirm that you are not a robot',
            'fixture_created' => 'Fixture has been created!',
            'fixtures_created' => 'Fixtures have been created!',
            'fixture_update' => 'Fixture has been updated.'
        ];
        $string = 'Success!';
        if(isset($strings[$key])){
            $string = $strings[$key];
        }
        return $string;
    }

    public static function errorStatus($key){
        $strings = [
            'no_access_rights' => 'You do not have rights to access that page.',
            'no_permission_edit_user' => 'You don\'t have permission to edit that user.',
            'fixture_not_created' => 'Error occurred, Fixture has not been created! Contact system admin.',
            'fixture_result_exists' => 'Error occurred, You have already submitted that result!',
            'invalid_request' => 'Invalid API request.'
        ];
        $string = 'An error has occured. Please contact an admin using the contact us form.';
        if(isset($strings[$key])){
            $string = $strings[$key];
        }
        return $string;
    }
}