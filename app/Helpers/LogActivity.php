<?php namespace App\Helpers;

use Request;
use App\LogActivity as LogActivityModel;
use Illuminate\Support\Facades\Auth;

class LogActivity
{
    public static function addToLog($subject,$data = null)
    {
        $log = [];
        $log['subject'] = $subject;
        $log['url'] = Request::fullUrl();
        $log['method'] = Request::method();
        $log['ip'] = Request::ip();
        $log['agent'] = Request::header('user-agent');
        $log['user_id'] = Auth::check() ? Auth::user()->id : 1;
        $log['user_name'] = Auth::check() ? Auth::user()->name : 1;
        $log['user_surname'] = Auth::check() ? Auth::user()->surname : 1;
        $log['data'] = $data;
        LogActivityModel::create($log);
    }

    public static function logActivityLists()
    {
        return LogActivityModel::latest()->paginate(50);
    }

}