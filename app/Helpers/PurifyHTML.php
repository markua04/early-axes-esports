<?php namespace App\Helpers;

class PurifyHTML
{
    public static function purify($content)
    {
        $strippedContent = str_replace('&lt;script&gt;','',$content);
        $cleanedContent = str_replace('&lt;/script&gt;','',$strippedContent);
        return $cleanedContent;
    }

}