<?php namespace App\Http\Controllers\API;

use App\Clan;
use App\Fixture;
use App\FixtureResult;
use App\Helpers\Lang;
use App\Http\Controllers\DiscordController;
use App\Http\Controllers\StandingsController;
use App\Http\Repositories\ClanTournamentStandingRepository;
use App\Http\Repositories\UserRepository;
use App\Http\Requests\FixtureRequest;
use App\Http\Repositories\FixtureRepository;
use App\IndividualFixture;
use App\Mail\FixtureResultsMail;
use App\Tournament;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Illuminate\View\View;
use ZipArchive;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;
use Exception;

class FixturesController extends Controller
{

    /**
     * @param FixtureRequest $request
     * @return Factory|RedirectResponse|View
     */
    public function create(FixtureRequest $request)
    {
        $tournaments = Tournament::where('tournament_type_id',2)->get();
        $clans = Clan::orderBy('clan_name')->get();
        if ($request->isMethod('post')) {
            $fixtureService = new FixtureRepository();
            $saveFixture = $fixtureService->create($request);
            if($saveFixture == true){
                return redirect()->back()->with('status', Lang::getStatus('fixture_created'));
            } else {
                return redirect()->back()->with('status', Lang::errorStatus('fixture_not_created'));
            }
        }
        return view('/admin/fixtures/createorupdate', compact('tournaments', 'clans'));
    }

    public function update(FixtureRequest $request,$id)
    {
        $tournaments = Tournament::all();
        $clans = Clan::all();
        if ($request->isMethod('post')) {
            return redirect('view-tournament/'.$request->get('tournament_id').'/')->with('status', Lang::getStatus(''));
        }
        return view('/admin/fixtures/createorupdate', compact('tournaments', 'clans'));
    }

    /**
     * @param $clanName
     * @return mixed
     */
    public function getClanImage($clanName)
    {
        $clan = Clan::where('clan_name',$clanName)->first();
        $clanImage = $clan->clan_logo;
        return $clanImage;
    }

//    public function createFixtureResult(Request $request)
//    {
//        $fixtureRepo = new FixtureRepository();
//        $fixture = $fixtureRepo->getById($request->fixture_id);
//        $tournament = Tournament::find($fixture->tournament_id);
//        if($request->get('clan_1') !== null){
//            $clan1 = Clan::where('clan_name',$fixture->clan_1)->first();
//            $clan2 = Clan::where('clan_name',$fixture->clan_2)->first();
//        } else {
//            $clan1 = Clan::where('id',$fixture->clan_1_id)->first();
//            $clan2 = Clan::where('id',$fixture->clan_2_id)->first();
//        }
//        $x = 1;
//        if(Auth::user()->id == $clan1->clan_owner_id || Auth::user()->id == $clan2->clan_owner_id || Auth::user()->id == $clan1->secondary_captain || Auth::user()->id == $clan2->secondary_captain  || Auth::user()->user_level_id == 1) {
//            if ($request->isMethod('post')) {
//                if(count($request->rounds) < 1){
//                    return redirect()->back()->with('status','Please upload at least 3 screenshots.');
//                } elseif (count($request->rounds) < 1){
//                    return redirect()->back()->with('status','Please upload at least 1 screenshot.');
//                }
//                $fixtureResultExists = FixtureResult::where('clan_1',$clan1->id)
//                    ->where('clan_2',$clan2->id)
//                    ->where('tournament_id',$tournament->id)
//                    ->where('fixture_id',$fixture->id)->first();
//                $destinationPath = public_path('/uploads/fixture-results/' . $fixture->id . '/');
//                File::makeDirectory($destinationPath, $mode = 0777, true, true);
//                if($fixtureResultExists == null){
//                    $fixtureResult = new FixtureResult();
//                    if($request->rounds !== null){
//                        foreach ($request->rounds as $round) {
//                            $extension = $round->getClientOriginalExtension();// getting image extension
//                            $fileName = 'round_' . $x . '.' . $extension; // renaming image
//                            $round->move($destinationPath, $fileName); // uploading file to given path
//                            $x++;
//                        }
//                    }
//                    $fixtureResult->fixture_id = $fixture->id;
//                    $fixtureResult->tournament_id = $fixture->tournament_id;
//                    $fixtureResult->clan_1 = $clan1->clan_name;
//                    $fixtureResult->clan_2 = $clan2->clan_name;
//                    $fixtureResult->clan_1_id = $clan1->id;
//                    $fixtureResult->clan_2_id = $clan2->id;
//                    $fixtureResult->clan_1_score = $request->clan_1_score;
//                    $fixtureResult->clan_2_score = $request->clan_2_score;
//                    $fixtureResult->forfeit = 0;
//                    $fixtureResult->save();
//                    $fixture->result = 1;
//                    $fixture->save();
//                    $standingsController = new StandingsController();
//                    $clan1WinsLossesDraws = $standingsController->getClanWinsLossesDrawsForTournament($clan1,$tournament);
//                    $clan2WinsLossesDraws = $standingsController->getClanWinsLossesDrawsForTournament($clan2,$tournament);
//                    $calculatedClanPointsForFixture = $standingsController->getClanPoints($tournament,$request->clan_1_score,$request->clan_2_score);
//                    $this->saveClanStanding($clan1->id, $tournament->id,$calculatedClanPointsForFixture[0],$clan1WinsLossesDraws);
//                    $this->saveClanStanding($clan2->id, $tournament->id,$calculatedClanPointsForFixture[1],$clan2WinsLossesDraws);
//                    if(isset($tournament->discord_url) && $tournament !== null) {
//                        $discordController = new DiscordController();
//                        $discordController->fixtureResultsPerTournament($request, $clan1, $clan2, $fixture->tournament_id);
//                    }
//                    if($request->get('dynamic')){
//                        return redirect("/dashboard")->with('status','Fixture result submitted!');
//                    } else {
//                        return redirect()->back()->with('status','Fixture result submitted!');
//                    }
//                } else {
//                    return redirect()->back()->with('status',Lang::errorStatus('fixture_result_exists'));
//                }
//            }
//            return view('frontend.fixtures.submit-results',compact('clan1','clan2','fixture','clanId'));
//        } else {
//            return redirect()->back()->with('status','You do not have permissions to do that.');
//        }
//    }

    public function createFixtureResult(Request $request)
    {
        $fixtureRepo = new FixtureRepository();
        $fixture = $fixtureRepo->getById($request->fixture_id);
        $tournament = Tournament::find($fixture->tournament_id);
        $tournamentPlatform = isset($tournament->tournament_platform) ? $tournament->tournament_platform : null;
        if($request->get('clan_1') !== null){
            $clan1 = Clan::where('clan_name',$fixture->clan_1)->first();
            $clan2 = Clan::where('clan_name',$fixture->clan_2)->first();
        } else {
            $clan1 = Clan::where('id',$fixture->clan_1_id)->first();
            $clan2 = Clan::where('id',$fixture->clan_2_id)->first();
        }
        $x = 1;
        if ($request->isMethod('post')) {
            if(count($request->get('files')) < 1) {
                return redirect()->back()->with('status', 'Please upload at least 1 screenshot.');
            }
            $fixtureResultExists = FixtureResult::where('clan_1',$clan1->id)
                ->where('clan_2',$clan2->id)
                ->where('tournament_id',$tournament->id)
                ->where('fixture_id',$fixture->id)->first();
            $destinationPath = public_path('/uploads/fixture-results/' . $fixture->id . '/');
            File::makeDirectory($destinationPath, $mode = 0777, true, true);

            if($fixtureResultExists == null){
                $fixtureResult = new FixtureResult();
                $object = json_decode(json_encode($request->get('files')), FALSE);
                foreach ($object as $round) {
                    $extension = $round->getClientOriginalExtension();// getting image extension
                    $fileName = 'round_' . $x . '.' . $extension; // renaming image
                    File::move($destinationPath, $fileName); // uploading file to given path
                    $x++;
                }
                $fixtureResult->fixture_id = $fixture->id;
                $fixtureResult->tournament_id = $fixture->tournament_id;
                $fixtureResult->clan_1 = $clan1->clan_name;
                $fixtureResult->clan_2 = $clan2->clan_name;
                $fixtureResult->clan_1_id = $clan1->id;
                $fixtureResult->clan_2_id = $clan2->id;
                $fixtureResult->clan_1_score = $request->get('clan_1_score');
                $fixtureResult->clan_2_score = $request->get('clan_2_score');
                $fixtureResult->forfeit = 0;
                $fixtureResult->save();
                $fixture->result = 1;
                $fixture->save();
                $standingsController = new StandingsController();
                $clan1WinsLossesDraws = $standingsController->getClanWinsLossesDrawsForTournament($clan1,$tournament);
                $clan2WinsLossesDraws = $standingsController->getClanWinsLossesDrawsForTournament($clan2,$tournament);
                $calculatedClanPointsForFixture = $standingsController->getClanPoints($tournament,$request->get('clan_1_score'),$request->get('clan_2_score'));
                $this->saveClanStanding($clan1->id, $tournament->id,$calculatedClanPointsForFixture[0],$clan1WinsLossesDraws, $tournamentPlatform);
                $this->saveClanStanding($clan2->id, $tournament->id,$calculatedClanPointsForFixture[1],$clan2WinsLossesDraws, $tournamentPlatform);
                if(isset($tournament->discord_url) && $tournament !== null) {
                    $discordController = new DiscordController();
                    $discordController->fixtureResultsPerTournament($request, $clan1, $clan2, $fixture->tournament_id);
                }
                return response(['status' => true],200);
            }
        } else {
            return response(['status' => false],404);
        }
    }

    /**
     * @param $id
     * @return Factory|View
     */
    public function listFixtureResults($id){
        $fixtureResults = FixtureResult::where('tournament_id',$id)->get();
        $tournament = Tournament::find($id);
        return view('admin.fixtures.list',compact('fixtureResults','tournament'));
    }


    /**
     * @param $id
     * @return Factory|View
     */
    public function listFixtures($id){
        $tournament = Tournament::find($id);
        if($tournament->tournament_type_id == 2){
            $fixtures = Fixture::orderBy('created_at', 'desc')->where('tournament_id',$id)->get();
            return view('admin.fixtures.list-tournament-fixtures',compact('fixtures','tournament'));
        } else {
            $fixtures = IndividualFixture::orderBy('created_at', 'desc')->where('tournament_id',$id)->get();
            return view('admin.fixtures.list-individual-tournament-fixtures',compact('fixtures','tournament'));
        }
    }

    /**
     * @param $id
     * @return Factory|View
     */
    public function remindFixtureResults($id,Request $request){
        $fixture = Fixture::find($id);
        $clan1 = Clan::where('clan_name',$fixture->clan_1)->first();
        $clan2 = Clan::where('clan_name',$fixture->clan_2)->first();
        if($clan1->clan_email){
            try {
                Mail::to($clan1->clan_email)->send(new FixtureResultsMail($clan1));
            } catch(Exception $e){
                Log::error($e);
            }
        }
        if($clan2->clan_email){
            try {
                Mail::to($clan2->clan_email)->send(new FixtureResultsMail($clan2));
            } catch(Exception $e){
                Log::error($e);
            }
        }
        $fixture->reminded = 1;
        $fixture->save();
        return redirect()->back()->with('status', 'Reminder sent to clan leaders!');
    }

    /**
     * @param $id
     * @return Factory|View
     */
    public function deleteFixture($id){
        $fixture = Fixture::find($id);
        $fixture->delete();
        return redirect()->back()->with('status','Fixture has been deleted.');
    }

    public function downloadAllScreenshots($id){
        $fixture = Fixture::find($id);
        $clan1 = str_slug($fixture->clan_1);
        $clan2 = str_slug($fixture->clan_2);
        // Define Dir Folder
        $public_dir = public_path()."/uploads/fixture-results/$id/";
        $files = $files = File::allFiles(public_path()."/uploads/fixture-results/$id/");
        // Zip File Name
        $zipFileName = $clan1.'VS'.$clan2.'.zip';
        // Create ZipArchive Obj
        $zip = new ZipArchive;
        if ($zip->open($public_dir . '/' . $zipFileName, ZipArchive::CREATE) === TRUE) {
            // Add Multiple file
            foreach($files as $file) {
                $zip->addFile($file->getPathname(), $file->getBasename());
            }
            $zip->close();
        }
        // Set Header
        $headers = array(
            'Content-Type' => 'application/octet-stream',
        );
        $filetopath=$public_dir.'/'.$zipFileName;
        // Create Download Response
        if(file_exists($filetopath)){
            return response()->download($filetopath,$zipFileName,$headers);
        }
    }

    public function saveClanScoreByTournament($tournament,$fixtureId,$clan1Score,$clan2Score){
        $winPoints = $tournament->win_points;
        $lossPoints = $tournament->loss_points;
        $drawPoints = $tournament->draw_points;
        $fixtureResult = FixtureResult::where('fixture_id',$fixtureId)->first();
        if($clan1Score > $clan2Score){
            $fixtureResult->clan_1_points = $winPoints;
            $fixtureResult->clan_2_points = $lossPoints;
        } elseif ($clan2Score > $clan1Score){
            $fixtureResult->clan_1_points = $lossPoints;
            $fixtureResult->clan_2_points = $winPoints;
        } elseif($clan2Score == $clan1Score){
            $fixtureResult->clan_1_points = $drawPoints;
            $fixtureResult->clan_2_points = $drawPoints;
        } else {
            $fixtureResult->clan_1_points = $lossPoints;
            $fixtureResult->clan_2_points = $lossPoints;
        }
        $fixtureResult->save();
    }

    public function saveClanStanding($clan,$tournamentId,$points,$clanWinsLossesDraws, $tournamentPlatform){
        $clanTournamentStandingRepo = new ClanTournamentStandingRepository();
        $clanStandings = $clanTournamentStandingRepo->getStandingsByClanAndTournamentId($clan,$tournamentId);
        if($clanStandings !== null){
            return $clanTournamentStandingRepo->edit($clan,$clanStandings,$clanWinsLossesDraws ,$points, $tournamentPlatform);
        }
        return $clanTournamentStandingRepo->create($clan,$tournamentId,$clanWinsLossesDraws,$points, $tournamentPlatform);
    }

    public function getClanFixtures(Request $request){
        $userRepo = new UserRepository();
        $user = $userRepo->getUserByTokenData($request);
        $fixtureRepo = new FixtureRepository();
        $fixtures = $fixtureRepo->getByClanId($user->clan_id);
        if(!empty($fixtures)){
            return response()->json($fixtures,200);
        } else {
            return response()->json(['message' => 'Fixtures not found'],401);
        }
    }
}
