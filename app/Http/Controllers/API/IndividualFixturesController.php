<?php namespace App\Http\Controllers\API;

use App\Http\Controllers\StandingsController;
use App\Http\Repositories\IndividualFixtureResultRepository;
use App\Http\Repositories\IndividualFixtureRepository;
use App\Http\Repositories\IndividualTournamentStandingRepository;
use App\Http\Repositories\TournamentRepository;
use App\Http\Repositories\UserRepository;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndividualFixturesController extends Controller
{
    const FIFA = 15;

    /**
     * @param Request $request
     * @return Factory
     */
    public function getUserFixtures(Request $request)
    {
        $userController = new UserController();
        $user = $userController->getUserByToken($request);
        $individualFixtureRepo = new IndividualFixtureRepository();
        $fixturesData = $individualFixtureRepo->getByUser($user);
        foreach($fixturesData as $fixture){
            if($fixture->player_1_gt == null || $fixture->player_2_gt == null){
                $individualFixtureRepo->addMissingGamerTagsForFixture($fixture);
            }
            if($fixture->resulted !== 1){
                $this->addMissingResultedFixtures($fixture);
                $fixture->match_result = null;
            } else {
                $fixture->match_result = $this->getResultOfFixture($fixture, $fixture->player_1_gt, $fixture->player_2_gt);
            }
            $fixture->player_1_gamer_pic = $this->getGamerPic($fixture->player_1_id);
            $fixture->player_2_gamer_pic = $this->getGamerPic($fixture->player_2_id);
        }
        if($fixturesData !== null) {
            return response()->json($fixturesData, 200);
        } else {
            return response()->json([], 500);
        }
    }

    public function addMissingResultedFixtures($fixture){
        $individualFixtureRepo = new IndividualFixtureRepository();
        $individualFixtureResultRepo = new IndividualFixtureResultRepository();
        $individualFixtureResult = $individualFixtureResultRepo->findByFixtureId($fixture->id);
        if($individualFixtureResult !== null){
            $individualFixture = $individualFixtureRepo->findById($fixture->id);
            $individualFixture->resulted = 1;
            $individualFixture->save();
        } else {
            $individualFixture = $individualFixtureRepo->findById($fixture->id);
            $individualFixture->resulted = null;
            $individualFixture->save();
        }
    }

    public function getGamerPic($userId){
        $userRepo = new UserRepository();
        $user = $userRepo->getById($userId);
        return isset($user->gamer_pic) ? $user->gamer_pic : null;
    }

    public function getResultOfFixture($fixture, $player1, $player2){
        $fixtureResult = new IndividualFixtureResultRepository();
        $result = $fixtureResult->findByFixtureId($fixture->id);
        if($result !== null){
            if($result->player_1_score > $result->player_2_score){
                return $player1;
            } elseif($result->player_1_score < $result->player_2_score) {
                return $player2;
            } elseif($result->player_1_score == $result->player_2_score) {
                return "Draw";
            }
        }
        return null;
    }

    public function createIndividualFixtureResult(Request $request)
    {
        $x = 1;
        $individualFixtureRepo = new IndividualFixtureRepository();
        $fixture = $individualFixtureRepo->findById($request->fixture_id);
        $userRepo = new UserRepository();
        $player1 = $userRepo->getById($fixture->player_1_id);
        $player2 = $userRepo->getById($fixture->player_2_id);
        $tournamentRepo = new TournamentRepository();
        $tournament = $tournamentRepo->findById($fixture->tournament_id);
        if ($request->player_1_id == $fixture->player_1_id || $request->player_1_id == $fixture->player_2_id || $request->player_2_id == $fixture->player_1_id || $request->player_2_id == $fixture->player_2_id) {
            if ($request->isMethod('post')) {
                $individualFixtureResultRepo = new IndividualFixtureResultRepository();
                $fixtureResultExists = $individualFixtureResultRepo->getFixtureResultsByPlayersAndTournament($player1, $player2, $tournament, $fixture);
                $destinationPath = public_path('/uploads/fixture-results-individual/' . $fixture->id . '/');
                if ($fixtureResultExists == null) {
                    if (isset($request->rounds)) {
                        foreach ($request->rounds as $round) {
                            $extension = $round->getClientOriginalExtension();
                            $fileName = 'round_' . $x . '.' . $extension;
                            $round->move($destinationPath, $fileName);
                            $x++;
                        }
                    }
                    $individualFixtureResultRepo->create($request, $fixture->id, $fixture->tournament_id, $player1->id, $player2->id);
                    $standingsController = new StandingsController();
                    $player1WinsLossesDraws = $standingsController->getUserWinsLossesDrawsForTournament($player1, $tournament);
                    $player2WinsLossesDraws = $standingsController->getUserWinsLossesDrawsForTournament($player2, $tournament);
                    $player1GoalsForAndAgainst = $standingsController->goalsForAndAgainst($player1, $tournament);
                    $player2GoalsForAndAgainst = $standingsController->goalsForAndAgainst($player2, $tournament);
                    $calculatedUserPointsForFixture = $standingsController->getClanPoints($tournament, $request->player_1_score, $request->player_2_score);
                    $this->saveIndividualStanding($player1->id, $tournament->id, $calculatedUserPointsForFixture[0], $player1WinsLossesDraws, $player1GoalsForAndAgainst);
                    $this->saveIndividualStanding($player2->id, $tournament->id, $calculatedUserPointsForFixture[1], $player2WinsLossesDraws, $player2GoalsForAndAgainst);
                    $individualFixtureRepo->resultFixture($fixture->id);
                    return response()->json(['status' => true, 'message' => 'success'], 200);
                } else {
                    return response()->json(['status' =>  false, 'message' => 'fixture result already exists'], 200);
                }
            }
        }
        return response()->json(['message' => 'failed'], 200);
    }


    /**
     * @param $player
     * @param $tournamentId
     * @param $points
     * @param $playerWinsLossesDraws
     * @param $playerGoals
     */
    public function saveIndividualStanding($player, $tournamentId, $points, $playerWinsLossesDraws, $playerGoals)
    {
        $tournamentRepo = new TournamentRepository();
        $tournament = $tournamentRepo->findById($tournamentId);
        $individualTournamentStandingRepo = new IndividualTournamentStandingRepository();
        $playerStandings = $individualTournamentStandingRepo->getByTournamentPlayerAndId($player, $tournamentId);
        $individualTournamentStandingRepo->create($playerWinsLossesDraws, $playerStandings, $tournament, $points, $playerGoals, $player);
    }

}
