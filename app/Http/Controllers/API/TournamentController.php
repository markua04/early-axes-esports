<?php namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Repositories\TournamentRegisteredPlayersRepository;
use App\TournamentRegisteredPlayers;
use Illuminate\Http\Request;

class TournamentController extends Controller
{
    public function removePlayerFromTournament(Request $request){
        $tournamentRegisteredPlayersRepo = new TournamentRegisteredPlayersRepository();
        $verdict = $tournamentRegisteredPlayersRepo->deleteByPlayerAndTournamentId($request->get('playerId'),$request->get('tournamentId'));
        if($verdict == true){
            return response()->json(['status' => $verdict, 'message' => 'success'], 200);
        }
        return response()->json(['status' => false, 'message' => 'failed'], 200);
    }

    public function addPlayerToTournament(Request $request){
        $playerId = $request->get('playerId');
        $tournamentId = $request->get('tournamentId');
        $clanId = $request->get('clanId');
        $checkIfPlayerRegistered = TournamentRegisteredPlayers::where('tournament_id',$tournamentId)->where('clan_member_id',$playerId)->first();
        if($checkIfPlayerRegistered == null){
            $tournamentRegisteredPlayersRepo = new TournamentRegisteredPlayersRepository();
            $verdict = $tournamentRegisteredPlayersRepo->create($playerId,$tournamentId,$clanId);
            if($verdict == true){
                return response()->json(['status' => $verdict, 'message' => 'success'], 200);
            }
            return response()->json(['status' => false, 'message' => 'An error has occured'], 200);
        } else {
            return response()->json(['status' => false, 'message' => 'Already registered in this tournament!'], 200);
        }
    }
}
