<?php namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;
use App\Http\Repositories\ClanRepository;
use App\Http\Repositories\UserRepository;

class UserController extends Controller
{
    public function getUserByToken($request){
        $userService = new UserRepository();
        $user = $userService->getUserByTokenData($request);
        $clanRepo = new ClanRepository();
        $clan = $clanRepo->getById($user->clan_id);
        $clanOwner = false;
        if($clan->clan_owner_id == $user->id){
            $clanOwner = true;
        }
        if($user !== null) {
            $userData = [
                'id' => $user->id,
                'email'        => $user->email,
                'name'        => $user->name,
                'surname'        => $user->surname,
                'gamer_tag'        => $user->gamer_tag,
                'psn_id'        => $user->psn_id,
                'steam_id'        => $user->steam_id,
                'description'        => $user->description,
                'selected_source'        => $user->selected_source,
                'gamer_pic'        => $user->gamer_pic,
                'clan_id' => $user->clan_id,
                'clan_owner' => $clanOwner,
                'message' => 'success'
            ];
            return $userData;
        }
    }

    public function getUserById($id){
        $userService = new UserRepository();
        $user = $userService->getUserById($id);
        if($user !== null) {
            $userData = [
                'id' => $user->id,
                'email'        => $user->email,
                'name'        => $user->name,
                'surname'        => $user->surname,
                'gamer_tag'        => $user->gamer_tag,
                'clan_id'        => $user->clan_id,
                'psn_id'        => $user->psn_id,
                'steam_id'        => $user->steam_id,
                'description'        => $user->description,
                'selected_source'        => $user->selected_source,
                'gamer_pic'        => $user->gamer_pic,
                'message' => 'success'
            ];
            return $userData;
        }
    }
}