<?php namespace App\Http\Controllers;

use App\Clan;
use App\Contact;
use App\Fixture;
use App\IndividualFixture;
use App\Mail\ContactMail;
use App\News;
use App\Tournament;
use App\TournamentFpsStandings;
use App\TournamentIndividualRegisteredPlayers;
use App\TournamentRules;
use App\TournamentStandings;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Mail;

class ApiController extends Controller
{

    /**
     * @param $gameId
     * @param $gamerTag
     * @return mixed
     */
    public function getRocketLeagueStats($platform,$gamerTag)
    {
        $xuid = $this->getXuid($gamerTag);
        $url = "https://api.rocketleaguestats.com/v1/player?unique_id=$xuid&platform_id=$platform";
        $ch = curl_init();
        curl_setopt(
            $ch,
            CURLOPT_URL,
            $url
        );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "Authorization: JJ4MV2V89IEKLWNN3UP7JRP2J66B9LZ9"
        ));
        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }

    public function displayRocketLeagueStats($platform,$gamerTag)
    {
        if($platform == 'xbl'){
            $platform = 3;
        } else {
            $platform = 1;
        }
        if (Cache::has('RLStats-' . $gamerTag)) {
            $rlStats = Cache::get('RLStats-' . $gamerTag);
        } else {
            $stats = $this->getRocketLeagueStats($platform,$gamerTag);
            Cache::put('RLStats-' . $gamerTag, $stats, 1440);
            $rlStats = Cache::get('RLStats-' . $gamerTag);
        }
        $decodedStats = json_decode($rlStats);
        return response()->json(['rlStats' => $decodedStats]);
    }

    /**
     * @param $gameId
     * @param $gamerTag
     * @return mixed
     */
    public function getXboxGameStatsByGameId($gameId, $gamerTag)
    {
        $xuid = $this->getXuid($gamerTag);
        $gameId = 950328474;
        $url = "https://xboxapi.com/v2/$xuid/game-stats/$gameId";
        $ch = curl_init();
        curl_setopt(
            $ch,
            CURLOPT_URL,
            $url
        );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "X-Auth: 7b048c5f56e34c1c9b5e6934ee636f729ca89c8b"
        ));
        $response = curl_exec($ch);
        dump($response);die;
        curl_close($ch);
        return $response;
    }

    /**
     * @param $url
     * @return mixed
     */
    public function getStats($url)
    {
        $ch = curl_init();
        curl_setopt(
            $ch,
            CURLOPT_URL,
            $url
        );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }

    /**
     * @param $url
     * @return mixed
     */
    public function getXboxActivityFeed($url)
    {
        $ch = curl_init();
        curl_setopt(
            $ch,
            CURLOPT_URL,
            $url
        );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "X-Auth: 7b048c5f56e34c1c9b5e6934ee636f729ca89c8b"
        ));
        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }

    /**
     * @param $platform
     * @return mixed
     */
    public function getSiegeOperatorStats($platform,$gamerTag)
    {
        $selectedPlatform = "xone";
        if($platform == "steam"){
            $selectedPlatform = "uplay";
        }
        $game = 'siege';
        $operatorsUrl = "https://api.r6stats.com/api/v1/players/$gamerTag/operators?platform=$selectedPlatform";
        $operatorStats = $this->getStats($operatorsUrl, $game);
        $operatorStatsDecoded = json_decode($operatorStats);
        return $operatorStatsDecoded;
    }

    public function getSiegeStats($platform,$gamerTag){
        $selectedPlatform = "xone";
        if($platform == "steam"){
            $selectedPlatform = "uplay";
        }
        $game = 'siege';
        $url = "https://api.r6stats.com/api/v1/players/$gamerTag?platform=$selectedPlatform";
        $stats = $this->getStats($url, $game);
        $decodedStats = json_decode($stats);
        return $decodedStats;
    }

    /**
     * @param $gamerTag
     * @return mixed4
     */
    public function getXboxLiveAccount($gamerGt)
    {
        $gamerTag = str_replace(' ','%20',$gamerGt);
        $user = User::where('gamer_tag', $gamerTag)->first();
        if($user == null){
            $xuid = $this->getXuid($gamerTag);
        } else {
            if($user['xuid'] == null){
                $xuid = $this->getXuid($gamerTag);
                $user->xuid = $xuid;
                $user->save();
            }
            $xuid = $user->xuid;
        }
        $url = "https://xboxapi.com/v2/$xuid/profile";
        $ch = curl_init();
        curl_setopt(
            $ch,
            CURLOPT_URL,
            $url
        );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "X-Auth: 69d958d5040cc01bc0c7e5c62b55cdea3eaafdd0"
        ));
        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }

    public function getXboxLiveColours($url)
    {
        $ch = curl_init();
        curl_setopt(
            $ch,
            CURLOPT_URL,
            $url
        );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "X-Auth: 7b048c5f56e34c1c9b5e6934ee636f729ca89c8b"
        ));
        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }


    public function getXuid($gamerTag)
    {
        $urlXuid = "https://xboxapi.com/v2/xuid/$gamerTag";
        $ch = curl_init();
        curl_setopt(
            $ch,
            CURLOPT_URL,
            $urlXuid
        );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "X-Auth: 69d958d5040cc01bc0c7e5c62b55cdea3eaafdd0"
        ));
        $response = curl_exec($ch);
        curl_close($ch);

        return $response;
    }

    public function getMostUsedOperator($operatorStats)
    {
        $test = array();
        foreach($operatorStats->operator_records as $operator){
            $test[] = $operator->stats->playtime;
        }
        $getValue = max($test);
        $test2 = array();
        foreach($operatorStats->operator_records as $operator){
            $test2[$operator->operator->name] = $operator->stats->playtime;
        }
        $operatorAdvancedStat = '';
        $operatorSimpleStat = '';
        $mostUsedOperator = array_search($getValue, $test2);
        foreach($operatorStats->operator_records as $operator_record){
            if($operator_record->operator->name == $mostUsedOperator){
                $operatorSimpleStat = $operator_record->operator;
                $operatorAdvancedStat = $operator_record->stats;
            }
        }
        $operatorMostUsed = array(
            'operatorSimpleStat' => $operatorSimpleStat,
            'operatorAdvancedStat' => $operatorAdvancedStat
        );
        return $operatorMostUsed;
    }


    /**
     * @param $gamerTag
     * @return mixed
     */
    public function getSteamId($gamerTag)
    {
        $steamId = "http://api.steampowered.com/ISteamUser/ResolveVanityURL/v0001/?key=69DFE59F1B02D2A8D3831BBB638F8DF6&vanityurl=$gamerTag";
        $ch = curl_init();
        curl_setopt(
            $ch,
            CURLOPT_URL,
            $steamId
        );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        $response = curl_exec($ch);
        curl_close($ch);
        $level1 = json_decode($response);
        if($level1->response->success == 42){
            return $level1->response->success;
        } else {
            $level2 = $level1->response->steamid;
            $level3 = json_decode($level2);
            return $level3;
        }
    }

    /**
     * @param $steamId
     * @return mixed
     */
    public function getSteamProfile($steamId)
    {
        $profileUrl = "http://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/?key=69DFE59F1B02D2A8D3831BBB638F8DF6&steamids=$steamId";
        $ch = curl_init();
        curl_setopt(
            $ch,
            CURLOPT_URL,
            $profileUrl
        );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }

    /**
     * @return mixed
     */
    public function getSteamAchievements($steamId){

        $profileUrl = "http://api.steampowered.com/IPlayerService/GetRecentlyPlayedGames/v0001/?key=69DFE59F1B02D2A8D3831BBB638F8DF6&steamid=$steamId&format=json";
        $ch = curl_init();
        curl_setopt(
            $ch,
            CURLOPT_URL,
            $profileUrl
        );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }

    /**
     * @param $steamId
     * @return mixed
     */
    public function getCSGOStats($steamId)
    {
        $url = "http://api.steampowered.com/ISteamUserStats/GetUserStatsForGame/v0002/?appid=730&key=69DFE59F1B02D2A8D3831BBB638F8DF6&steamid=$steamId";
        $profileUrl = "http://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/?key=69DFE59F1B02D2A8D3831BBB638F8DF6&steamids=$steamId";
        $ch = curl_init();
        curl_setopt(
            $ch,
            CURLOPT_URL,
            $url
        );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }

    public function hashValues(){
        $dateTime = str_replace(' ','',Carbon::now()->toDateTimeString());
        $dateTimeCorrect = str_replace('-','',$dateTime);
        $md5Hash = '2678'.'createsession'.'BCE590AFC1134F619BAC139F887B2A44'.Carbon::now()->toDateTimeString();
        dump($dateTimeCorrect);
    }

    public function getTournamentRules($id)
    {
        $rules = TournamentRules::where('tournament_id',$id)->first();
        return response()->json(['rules' => $rules]);
    }

    public function getTournamentStandings($id)
    {
        $standings = TournamentStandings::where('tournament_id',$id)->first();
        return response()->json(['standings' => $standings]);
    }

    public function getTournamentFixtures($id){
        $tournament = Tournament::find($id);
        if (Cache::has('allFixtures'.$id)) {
            $tournaments = Cache::get('allFixtures'.$id);
        } else {
            if($tournament->tournament_type_id){
                $tournamentData = Fixture::where('tournament_id',$id)->where('date', '>=', Carbon::now()->subDays(10))->orderBy('date','asc')->get();
            } else {
                $tournamentData = IndividualFixture::where('tournament_id',$id)->where('date', '>=', Carbon::now()->subDays(10))->orderBy('date','asc')->get();
            }
            Cache::put('allFixtures'.$id,$tournamentData, 30);
            $tournaments = Cache::get('allFixtures'.$id);
        }
        if($tournaments !== null) {
            return response()->json($tournaments, 200);
        } else {
            return response()->json([], 500);
        }
    }

    public function mobiappContact(Request $request){
        if($request->all() !== null){
            $contact = new Contact();
            $contact->name = $request['name'];
            $contact->surname = $request['surname'];
            $contact->email = $request['email'];
            $contact->contact_number = $request['contact_number'];
            $contact->description = $request['description'];
            $contact->save();
            Mail::to(env('ADMIN_EMAIL_ADDRESS'))->send(new ContactMail($request));
            return response()->json(true, '200');
        } else {
            return response()->json(false, '400');
        }
    }

    public function getStandingsPage($id){
        $standings = TournamentStandings::where('tournament_id',$id)->first();
        $tournament = Tournament::find($id);
        $standingsController = new StandingsController();
        $autoGeneratedStandings = $standingsController->getClanStandingsByTournament($id);
        return view('frontend.standings.standingsPage',compact('standings', 'tournament','autoGeneratedStandings'));
    }

    public function getClansEntered($id){
        $tournamentController = new TournamentController();
        $tournament = Tournament::find($id);
        $registeredClanMembers = $tournamentController->getClanMembersRegistered($id);
        return view('frontend.clans.app.clansPage',compact('registeredClanMembers','tournament'));
    }

    public function getPlayersEntered($id){
        $tournament = Tournament::find($id);
        $tournamentRegisteredIndividuals = TournamentIndividualRegisteredPlayers::where('tournament_id',$id)->get();
        return view('frontend.tournaments.app.registeredIndividuals',compact('tournamentRegisteredIndividuals','tournament'));
    }

    public function getLatestNews($count){
        if (Cache::has('news')) {
            $news = Cache::get('news');
        } else {
            $getNews = News::orderBy('created_at', 'desc')->take($count)->get();
            Cache::put('news', $getNews, 800);
            $news = Cache::get('news');
        }
        return response()->json($news, 200);
    }

    public function getUser($email){
        $clan = 0;
        $user = null;
        if (Cache::has($email)) {
            $user = Cache::get($email);
            if($user->clan_id !== 0){
                $clan = Cache::get('clan_'.$user->clan_id);
            }
        } else {
            $findUser = User::where('email',$email)->first();
            $findClan = Clan::find($findUser->clan_id);
            if($findUser->clan_id !== 0){
                Cache::put('clan_', $findClan->id, 750);
                $clan = Cache::get('clan_'.$findClan->id);
            }
            Cache::put($findUser->email, $findUser, 750);
            $user = Cache::get($findUser->email);
        }
        $data = [
            'user' => $user,
            'clan' => $clan
        ];

        return response()->json($data,200);
    }

    public function getTest(){
        return response()->json(['test' => 'MOOI'],200);
    }

}
