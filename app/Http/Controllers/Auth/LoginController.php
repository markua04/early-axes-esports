<?php namespace App\Http\Controllers\Auth;

use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\Controller;
use App\Http\Repositories\UserRepository;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    protected $redirectTo = '/dashboard';

    /**
     * Where to redirect users after login.
     *
     * @var string
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function authenticated(Request $request)
    {
        $user = User::where('email',$request->get('email'))->first();
        if (!$user->verified) {
            auth()->logout();
            return back()->with('warning', 'You need to confirm your account. We have sent you an activation code, please check your email.');
        }
        if ($user->user_status_id == 2) {

            $message = 'Your account has been deactivated. Please contact us to query this via : info@earlyaxes.co.za';

            // Log the user out.
            $this->logout($request);

            // Return them to the log in form.
            return redirect(session('link'))
                ->withInput($request->only($this->username(), 'remember'))
                ->withErrors([
                    // This is where we are providing the error message.
                    $this->username() => $message,
                ]);
        }
    }

    public function showLoginForm()
    {
        return view('auth.login');
    }

    private function updateAdminToken(){
        $authController = new AuthController();
        $userRepo = new UserRepository();
        $userRepo->updateAdminToken($authController->generateToken());
    }

    private function deleteAdminToken(){
        $userRepo = new UserRepository();
        $userRepo->removeAdminToken(Auth::user()->id);
    }

    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        if(Auth::user()->user_level_id == 1){
            $this->deleteAdminToken();
        }
        $this->guard()->logout();
        $request->session()->invalidate();
        return redirect('/');
    }
}
