<?php namespace App\Http\Controllers\Auth;

use App\Clan;
use App\ClanMembers;
use App\Http\Repositories\UserRepository;
use App\Mail\UserRegisteredForClanMail;
use App\Mail\VerifyMail;
use App\Platform;
use App\Http\Controllers\Controller;
use App\VerifyUser;
use http\Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    const ACTIVE_USER = 1;
    const VERIFIED_USER = 1;
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        $clans = Clan::all();
        $platforms = Platform::all();
        return view('auth.register', compact('clans','platforms'));
    }


    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $messages = array(
            'gamer_tag.required_if' => 'This is required if you have selected Xbox.',
            'steam_id.required_if' => 'This is required if you have selected Steam.',
            'psn_id.required_if' => 'This is required if you have selected Playstation.',
        );

        $rules = array(
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'gamer_tag' => 'required_if:selected_source,==,1',
            'psn_id' => 'required_if:selected_source,==,2',
            'steam_id' => 'required_if:selected_source,==,3',
            'uplay_id' => 'required_if:selected_source,==,4',
            'selected_source' => 'required|string',
            'phone_number' => 'required|numeric',
            'password' => 'required|string|min:6|confirmed',
        );

        return Validator::make($data, $rules, $messages);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $destinationPath = 'uploads'; // upload path
        if (Input::file('image') !== null) {
            $extension = Input::file('image')->getClientOriginalExtension();// getting image extension
            $fileName = rand(11111, 99999) . '.' . $extension; // renaming image
            Input::file('image')->move($destinationPath, $fileName); // uploading file to given path
            $data['gamer_pic'] = "/uploads/" . $fileName;
        } else {
            $data['gamer_pic'] = null;
        }
        if($data['clan_id'] == null){
            $data['clan_id'] = 0;
        }
        $userService = new UserRepository();
        $verdict = $userService->create($data);
        if($verdict == true){
            $user = $userService->getByEmail($data['email']);
            $clan = Clan::find($data['clan_id']);
            if($clan !== null && $clan->clan_email !== null && $user->gamer_tag !== null && $clan->id !== 0){
                try {
                    Mail::to($clan->clan_email)->send(new UserRegisteredForClanMail($user));
                } catch(\Exception $e){
                    Log::error($e);
                }
            }
            $clanMembers = new ClanMembers;
            $clanMembers->user_id = $user->id;
            $clanMembers->clan_id = $user->clan_id;
            $clanMembers->approval_id = 0;
            $clanMembers->save();

            VerifyUser::create([
                'user_id' => $user->id,
                'token' => str_random(40)
            ]);
            try {
                Mail::to(strtolower($user->email))->send(new VerifyMail($user));
            } catch(\Exception $e){
                Log::error($e);
            }
            return $user;
        }
    }

    public function verifyUser($token)
    {
        $verifyUser = VerifyUser::where('token', $token)->first();
        if(isset($verifyUser) ){
            $user = $verifyUser->user;
            if(!$user->verified) {
                $verifyUser->user->verified = self::VERIFIED_USER;
                $verifyUser->user->save();
                $status = "Your e-mail is verified. You can now login.";
            }else{
                $status = "Your e-mail is already verified. You can now login.";
            }
        }else{
            return redirect('/login')->with('warning', "Sorry your email cannot be verified.");
        }

        return redirect('/login')->with('status', $status);
    }

    /**
     * @param Request $request
     * @param $user
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function registered(Request $request, $user)
    {
        $this->guard()->logout();
        return redirect('/login')->with('status', 'We sent you an activation code. Check your email and click on the link to verify.');
    }
}
