<?php

namespace App\Http\Controllers;

use App\Clan;
use App\Helpers\Lang;
use App\Http\Repositories\BracketRepository;
use App\Http\Repositories\FixtureRepository;
use App\Tournament;
use Illuminate\Http\Request;

class BracketsController extends Controller
{
    public function create(Request $request){
        $tournaments = Tournament::where('tournament_type_id',2)->get();
        $clans = Clan::orderBy('clan_name')->get();
        $clansArray = [];
        if ($request->isMethod('post')) {
            $seperateDateTime = explode( ',', $request->date_time );
            $count = count($request->clans);
            $halfCount = $count / 2;
            $newCount = $halfCount - 1;
            $pieces = array_chunk($request['clans'], ceil(count($request['clans']) / 2));
            $matchUps = [];
            for($x = 0; $x <= $newCount; $x++){
                $matchUps[$x] = [$pieces[0][$x] => $pieces[1][$x]];
            }
            $bracketRepo = new BracketRepository();
            foreach ($matchUps as $matchUp ){
                foreach ($matchUp as $key => $value) {
                    $timeString = str_replace(' ','',$seperateDateTime[1]);
                    $request->date = date_format(date_create($seperateDateTime[0]),"Y-m-d");
                    $request->time = date("H:i", strtotime($timeString));
                    $request->clan_1 = $key;
                    $request->clan_2 = (int)$value;
                    $fixtureRepo = new FixtureRepository();
                    $fixtureId = $fixtureRepo->create($request);
                    $bothClans = [$key,(int)$value];
                    $bracketRepo->create($fixtureId,$request['tournament_id'],1,$bothClans,0);
                }
            }
            return redirect()->back()->with('status', Lang::getStatus('fixtures_created'));
        }
        $x = 0;
        foreach ($clans as $clan){
            $clansArray[$x]['id'] = $clan->id;
            $clansArray[$x]['name'] = $clan->clan_name;
            $x++;
        }
        $clansObj = json_encode($clansArray);
        return view('/admin/brackets/createorupdate', compact('tournaments', 'clansObj'));
    }
}
