<?php namespace App\Http\Controllers;

use App\Clan;
use App\ClanMembers;
use App\ClanNews;
use App\Fixture;
use App\Helpers\LogActivity;
use App\Helpers\PurifyHTML;
use App\Http\Repositories\ClanMemberRepository;
use App\Http\Repositories\ClanRepository;
use App\Http\Repositories\FixtureResultRepository;
use App\Http\Repositories\TournamentRepository;
use App\Http\Repositories\UserRepository;
use App\Http\Requests\ClanRequest;
use App\Mail\ClanRegisteredMail;
use App\Mail\UserRegisteredForClanMail;
use App\Tournament;
use App\TournamentRegisteredPlayers;
use App\TournamentRegistrations;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;

class ClanController extends Controller
{

    /**
     * @param Request $request
     * @return $this
     */
    public function create(Request $request)
    {
        $clanOwner = Clan::where('clan_owner_id',Auth::user()->getAttributes()['id'])->first();
        if($clanOwner == null) {
            if ($request->isMethod('post')) {
                $request->validate([
                    'clan_name' => 'required|unique:clans,clan_name,'.Auth::user()->clan_id,
                    'clan_email' => 'required|email|unique:clans,clan_email,'.Auth::user()->clan_id,
                    'clan_description' => 'required',
                    'clan_tel' => 'required',
                ]);
                $userId = Auth::user()->getAttributes()['id'];
                $clan = new Clan();
                $description = $request->get('clan_description');
                $destinationPath = 'uploads'; // upload path
                if (Input::file('image') !== null) {
                    $extension = Input::file('image')->getClientOriginalExtension();// getting image extension
                    $fileName = rand(11111, 99999) . '.' . $extension; // renaming image
                    Input::file('image')->move($destinationPath, $fileName); // uploading file to given path
                    $clan->clan_logo = "/uploads/" . $fileName;
                }
                $clan->clan_name = $request->get('clan_name');
                $clan->clan_email = $request->get('clan_email');
                $clan->clan_tel = $request->get('clan_tel');
                $clan->clan_description = PurifyHTML::purify($description);
                $clan->clan_owner_id = $userId;
                if ($request->get('clan_status') == '' || $request->get('clan_status') == null) {
                    $clan->clan_status = 0;
                }
                $clan->save();
                $userClan = Clan::where('clan_owner_id', '=', $userId)->first();
                User::where('id', $userId)->update(['clan_id' => $userClan->id]);
                $clanMember = new ClanMembers();
                $clanMember->clan_id = $userClan->id;
                $clanMember->user_id = $userId;
                $clanMember->approval_id = 1;
                $clanMember->save();
                $emails = ['markua04@gmail.com','cronjr88@gmail.com','jpdupreez1704@gmail.com'];
                $clanNameToModerate = $request->get('clan_name');
                Mail::to($emails)->send(new ClanRegisteredMail($clanNameToModerate));
                return redirect('/view-clans')->with('status', 'Clan has been created and is awaiting approval by admins');
            }
            return view('/auth/register-clan');
        }
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function updateClan($id,Request $request)
    {
        if($request->isMethod('post')) {
            $request->validate([
                'clan_name' => 'required|unique:clans,clan_name,'.$id,
                'clan_email' => 'required|email|unique:clans,clan_email,'.$id,
                'clan_description' => 'required',
                'clan_tel' => 'required',
            ]);
            $clan = Clan::find($id);
            if(Auth::user()->id == $clan->clan_owner_id || Auth::user()->user_level_id == 1){
                $clan_image = $clan->clan_logo;
                $description = PurifyHTML::purify($request->get('clan_description'));
                $clanOwner = $clan->clan_owner_id;
                $secondaryCaptain = $clan->secondary_captain;
                $clan->clan_name = $request->get('clan_name');
                $clan->clan_email = $request->get('clan_email');
                $clan->clan_tel = $request->get('clan_tel');
                $clan->clan_description = $description;
                if($request->get('clan_owner_id') !== null){
                    $clan->clan_owner_id = $request->get('clan_owner_id');
                } else {
                    $clan->clan_owner_id = $clanOwner;
                }
                if($request->get('secondary_captain') !== null){
                    $clan->secondary_captain = $request->get('secondary_captain');
                } else {
                    $clan->secondary_captain = $secondaryCaptain;
                }
                $destinationPath = 'uploads'; // upload path
                if(Input::file('image') !== null){
                    $extension = Input::file('image')->getClientOriginalExtension();// getting image extension
                    $fileName = rand(11111,99999).'.'.$extension; // renaming image
                    Input::file('image')->move($destinationPath, $fileName); // uploading file to given path
                    $clan->clan_logo = "/uploads/".$fileName;
                } else {
                    $clan->clan_logo = $clan_image;
                }
                $clan->save();
                LogActivity::addToLog('Clan Edited by'.Auth::user()->name .' '.Auth::user()->surname);
                return redirect("/view-clans")->with('status','Clan updated!');
            } else {
                return redirect("/view-clans")->with('status','You do not have permissions to edit that clan!');
            }
        } else {
            $clan = Clan::find($id);
            $clanOwner = $clan->clan_owner_id;
            $secondaryCaptain = $clan->secondary_captain;
            $clanMembers = ClanMembers::where('clan_id',$clan->id)->get();
            return view('admin.manage-clans.edit-clan-profile', compact('clan','clanMembers','clanOwner','secondaryCaptain'));
        }
    }

    public function listClans()
    {
        $clans = Clan::where('clan_status', 1)->paginate(10);
        return view('/admin/manage-clans/list-clans', compact('clans'));
    }

    public function getClanById($id)
    {
        $clan = Clan::find($id);
        $clanData = $clan->getAttributes();
        $members = $this->clanMembers($id);
        if($clan->clan_status == 0 && Auth::user()->user_level_id !== 1){
            return redirect('/headquarters')->with('status','Your clan has not been approved by the moderators yet.');
        } else {
            return view('/admin/manage-clans/clan-profile', compact('clanData', 'members'));
        }
    }

    /**
     * @param $clanId
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function clanMembers($clanId) {
        $users = ClanMembers::where('clan_id', $clanId)->where('approval_id',1)->paginate(15);
        return $users;
    }


    /**
     * @param $clanId
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function deleteClan($clanId) {
        $clan = Clan::find($clanId);
        $tournamentStatus = false;
        if(Auth::user()->user_level_id == 1 || Auth::user()->id == $clan->clan_owner_id){
            $tournamentRegistrations = TournamentRegistrations::where('clan_id',$clanId)->get();
            foreach ($tournamentRegistrations as $registration){
                $tournamentRepo = new TournamentRepository();
                $tournament = $tournamentRepo->getById($registration->tournament_id);
                if(isset($tournament) && $tournament->tournament_status == 1){
                    $tournamentStatus = true;
                    break;
                }
            }
            if($tournamentStatus == true){
                return redirect('/')->with('status','This clan cannot be deleted as it is still part of an active tournament. Please contact an admin.');
            }
            $clanMembers = ClanMembers::where('clan_id',$clanId)->get();
            $tournamentRegisteredPlayers = TournamentRegisteredPlayers::where('clan_id',$clanId)->get();
            foreach($clanMembers as $member){
                User::where('clan_id',$clanId)->update(['clan_id' => 0]);
                $member->delete();
            }
            foreach($tournamentRegistrations as $registration){
                $registration->delete();
            }
            foreach($tournamentRegisteredPlayers as $player){
                $player->delete();
            }
            $clan->delete();
            $owner = User::find($clan->clan_owner_id);
            $owner->clan_id = 0;
            $owner->save();
            return redirect('/view-clans')->with('status', 'Clan has been removed!');
        }
    }


    /**
     * @param ClanRequest $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function createClanNews(ClanRequest $request)
    {
        if ($request->isMethod('post')) {
            if(isOwnerOfClan() == "Yes") {
                $user = Auth::user()->getAttributes();
                $clanNews = new ClanNews();
                $destinationPath = 'uploads/clan-news'; // upload path
                if (Input::file('image') !== null) {
                    $extension = Input::file('image')->getClientOriginalExtension();// getting image extension
                    $fileName = rand(11111, 99999) . '.' . $extension; // renaming image
                    Input::file('image')->move($destinationPath, $fileName); // uploading file to given path
                    $clanNews->banner = "/uploads/clan-news/" . $fileName;
                }
                $clanNews->clan_id = $user['clan_id'];
                $clanNews->user_id = $user['id'];
                $clanNews->title = $request->get('title');
                $clanNews->content = $this->truncateText($request->get('content'),500);
                $clanNews->save();
                return redirect('/create-clan-news')->with('status', 'Clan News has been created!');
            } else {
                return redirect('/headquarters')->with('status', 'You are not the owner of this clan.');
            }
        } else {
            return view('/frontend/clans/create-clan-news');
        }
    }


    /**
     * @param $id
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function editClanNews($id, Request $request)
    {
        if ($request->isMethod('post')) {
            if(isOwnerOfClan() == "Yes") {
                $user = Auth::user()->getAttributes();
                $clanNews = new ClanNews();
                $destinationPath = 'uploads/clan-news'; // upload path
                if (Input::file('image') !== null) {
                    $extension = Input::file('image')->getClientOriginalExtension();// getting image extension
                    $fileName = rand(11111, 99999) . '.' . $extension; // renaming image
                    Input::file('image')->move($destinationPath, $fileName); // uploading file to given path
                    $clanNews->banner = "/uploads/clan-news/" . $fileName;
                }
                $clanNews->clan_id = $user['clan_id'];
                $clanNews->user_id = $user['id'];
                $clanNews->title = $request->get('title');
                $clanNews->content = $this->truncateText($request->get('content'),500);
                $clanNews->save();
                return redirect('/create-clan-news')->with('status', 'Clan News has been created!');
            } else {
                return redirect('/headquarters')->with('status', 'You are not the owner of this clan.');
            }
        } else {
            $newsArticle = ClanNews::find($id);
            return view('/frontend/clans/create-clan-news');
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function listFixturesByTournament($clanId){
        $myDate = Carbon::now()->format('Y/m/d');
        $todaysDate = str_replace('/','-',$myDate);
        $fixtures = Fixture::where('clan_1_id',$clanId)->orWhere('clan_2_id',$clanId)->get();
        return view('frontend.fixtures.list-tournament-fixtures',compact('fixtures','todaysDate','clanId'));
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function listFixtures($id,$clanId){
        $clanOwner = Clan::find($clanId);
        if(Auth::user()->id == $clanOwner->clan_owner_id || Auth::user()->id == $clanOwner->secondary_captain || Auth::user()->user_level_id == 1){
            $myDate = Carbon::now()->format('Y/m/d');
            $todaysDate = str_replace('/','-',$myDate);
            $clanName = Clan::find($clanId)->clan_name;
            $tournament = Tournament::find($id);
            $fixtures = Fixture::where('clan_1',$clanName)->orWhere('clan_2',$clanName)->get();
            return view('frontend.fixtures.list-tournament-fixtures',compact('fixtures','tournament','todaysDate','clanId'));
        } else {
            return redirect()->back()->with('status','You do not own that clan!');
        }
    }

    /**
     * @param $text
     * @param $limit
     * @return string
     */
    public function truncateText($text, $limit) {
        if (str_word_count($text, 0) > $limit) {
            $words = str_word_count($text, 2);
            $pos = array_keys($words);
            $text = substr($text, 0, $pos[$limit]) . '...';
        }
        return $text;
    }

    public function updateClanNames($clan){
        $fixtures = Fixture::where('clan_1', $clan->clan_name)
            ->where('clan_2', $clan->clan_name)
            ->get();
        return $fixtures;
    }

    public function clanResultsFeed($clanId){
        $fixtureResultsRepo = new FixtureResultRepository();
        $fixtureResults = $fixtureResultsRepo->getByClanId($clanId);
        return $fixtureResults;
    }

    public function removeClanMember(Request $request, $clanId, $userId){
        $clan = Clan::find($clanId);
        if(Auth::user()->id == $clan->clan_owner_id){
            $member = ClanMembers::where('user_id',$userId)->first();
            $member->delete();
            $user = User::where('id',$userId)->first();
            $user->clan_id = 0;
            $user->save();
            return redirect()->back()->with('status', "$user->gamer_tag has been removed from the clan.");
        } else {
            return redirect()->back()->with('status','You do not own that clan!');
        }
    }

    public function joinClan(Request $request){
        $clanMemberRepo = new ClanMemberRepository();
        if($request->get('confirm') == "true"){
            $clanName = $request->get('clan');
        } else {
            session_start();
            unset($_SESSION["joinUrl"]);
            return redirect('/')->with('status','Join clan request cancelled!');
        }
        $user = User::find(Auth::user()->id);
        $clan = Clan::where('clan_name',$clanName)->first();
        if($clanName !== null && $user->clan_id !== $clan->id && $user !== null) {
            if(isset($user->clan_id)){
                $userOldClan = ClanMembers::where('clan_id',$user->clan_id)->where('user_id',$user->id)->first();
                if($userOldClan !== null){
                    $userOldClan->delete();
                }
                $clanMemberRepo->create($user,$clan);
            }
            if ($clan !== null && $clan->clan_email !== null && $clan->id !== 0) {
                $user->clan_id = $clan->id;
                $user->save();
                if(isset($clan->clan_email)){
                    Mail::to($clan->clan_email)->send(new UserRegisteredForClanMail($user));
                }
                $userTournamentRegistrations = TournamentRegisteredPlayers::where('clan_member_id', Auth::user()->id)->get();
                if(!empty($userTournamentRegistrations)){
                    foreach ($userTournamentRegistrations as $registration) {
                        $registration->destroy($registration->id);
                    }
                }
                session_start();
                unset($_SESSION["joinUrl"]);
            }
            return redirect('/')->with('status', 'Successfully registered for clan ' . $clan->clan_name);
        }
        session_start();
        unset($_SESSION["joinUrl"]);
        return redirect('/dashboard')->with('status','You already belong to the clan. '.$clan->clan_name);
    }

    public function joinClanQuestion($clanName, Request $request){
        return view('frontend.partials.clans.doYouWantToJoin',compact('clanName'));
    }

}

