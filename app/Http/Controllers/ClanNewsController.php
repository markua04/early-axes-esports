<?php namespace App\Http\Controllers;

use App\News;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class ClanNewsController extends Controller
{
    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param Request $request
     * @return $this
     */
    public function create(Request $request)
    {
        if($request->isMethod('post')){
            $userId = Auth::user()->getAttributes()['id'];
            $news = new News();
            $destinationPath = 'uploads'; // upload path
            if(Input::file('image') !== null){
                $extension = Input::file('image')->getClientOriginalExtension();// getting image extension
                $fileName = rand(11111,99999).'.'.$extension; // renaming image
                Input::file('image')->move($destinationPath, $fileName); // uploading file to given path
                $news->banner = "/uploads/".$fileName;
            }
            $news->title = $request->get('title');
            $news->news_body = $request->get('news_body');
            $news->content_creator = $userId;
            if($request->get('article_status') == '' || $request->get('article_status') == null){
                $news->article_status = 0;
            }
            $news->save();
            return redirect('/view-clans')->with('status','Clan has been created and is awaiting approval by admins');
        } else {
            return view('/auth/create-news');
        }
    }

    public function listNews()
    {
        $news = News::where('article_status', 1)->get();

        return view('/news', ['clans' => $news]);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function updateClan($id,Request $request)
    {
        if($request->isMethod('post')) {
            $clan = Clan::find($id);
            $clan_image = $clan->clan_logo;
            $clan->clan_name = $request->get('clan_name');
            $clan->clan_email = $request->get('clan_email');
            $clan->clan_tel = $request->get('clan_tel');
            $clan->clan_description = $request->get('clan_description');
            $clan->clan_owner_id = Auth::user()->getAttributes()['id'];;
            $destinationPath = 'uploads'; // upload path
            if(Input::file('image') !== null){
                $extension = Input::file('image')->getClientOriginalExtension();// getting image extension
                $fileName = rand(11111,99999).'.'.$extension; // renaming image
                Input::file('image')->move($destinationPath, $fileName); // uploading file to given path
                $clan->clan_logo = "/uploads/".$fileName;
            } else {
                $clan->clan_logo = $clan_image;
            }
            $clan->save();
            return redirect("/view-clans");
        } else {
            $clan = Clan::find($id);
            return view('admin.manage-clans.edit-clan-profile', compact('clan'));
        }
    }
}
