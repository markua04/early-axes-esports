<?php namespace App\Http\Controllers;

use App\Tournament;
use Illuminate\Http\Request;

class DiscordController extends Controller
{

    public function fixtureResultsAdmin($request, $clan1, $clan2, $tournamentId)
    {
        $tournament = Tournament::find($tournamentId);
        $url = "https://discordapp.com/api/webhooks/559992607778799626/ouBadOE83dJ1_20VC8vVy8kackpezxnxvpGOEccFYetFmaPDjP5dkw8Sf0IgGg2YK6sf";
        if ($request->clan_1_score < $request->clan_2_score) {
            $result = $clan2->clan_name . " beats " . $clan1->clan_name . " and the score was : " . $request->clan_2_score . " - " . $request->get('clan_1_score');
        } elseif ($request->get('clan_1_score') == $request->clan_2_score) {
            $result = "Match was a draw and the score was : " . $request->clan_2_score . " - " . $request->get('clan_1_score');
        } else {
            $result = $clan1->clan_name . " beats " . $clan2->clan_name . " and the score was : " . $request->get('clan_1_score') . " - " . $request->clan_2_score;
        }
        $hookObject = json_encode([
            /*
             * The general "message" shown above your embeds
             */
            "content" => $result,

            /*
             * Whether or not to read the message in Text-to-speech
             */
            "tts" => false,
            /*
             * File contents to send to upload a file
             */
            // "file" => "",
            /*
             * An array of Embeds
             */
            "embeds" => [
                /*
                 * Our first embed
                 */
                [
                    // Set the title for your embed
                    "title" => "Tournament : " . $tournament->tournament_title,

                    // The type of your embed, will ALWAYS be "rich"
                    "type" => "rich",

                    // Field array of objects
                    "fields" => [
                        // Field 1
                        [
                            "name" => "Screenshots",
                            "value" => "https://earlyaxes-esports.co.za/admin/list-fixture-results-clan/$tournamentId/",
                            "inline" => false
                        ],
                    ]

                    // A description for your embed
                    // Image object
                ]
            ]

        ], JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);


        $ch = curl_init();

        curl_setopt_array($ch, [
            CURLOPT_URL => $url,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $hookObject,
            CURLOPT_HTTPHEADER => [
                "Length" => strlen($hookObject),
                "Content-Type" => "application/json"
            ]
        ]);

        curl_exec($ch);
        return curl_close($ch);
    }

    public function fixtureResultsPerTournament($request, $clan1, $clan2, $tournamentId)
    {
        $tournament = Tournament::find($tournamentId);
        if ($request->get('clan_1_score') < $request->get('clan_2_score')) {
            $result = $clan2->clan_name . " defeated " . $clan1->clan_name . " and the score was : " . $request->get('clan_2_score') . " - " . $request->get('clan_1_score');
        } elseif ($request->get('clan_1_score') == $request->get('clan_2_score')) {
            $result = "Match was a draw and the score was : " . $request->get('clan_2_score') . " - " . $request->get('clan_1_score');
        } else {
            $result = $clan1->clan_name . " defeated " . $clan2->clan_name . " and the score was : " . $request->get('clan_1_score') . " - " . $request->get('clan_2_score');
        }
        $hookObject = json_encode([
            "content" => $result,
            "tts" => false,
            /*
             * File contents to send to upload a file
             */
            // "file" => "",
            /*
             * An array of Embeds
             */
            "embeds" => [
                [
                    // Set the title for your embed
                    "title" => "Tournament : " . $tournament->tournament_title,

                    // The type of your embed, will ALWAYS be "rich"
                    "type" => "rich",

                    // A description for your embed
                    // Image object
                ]
            ]

        ], JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);


        $ch = curl_init();

        curl_setopt_array($ch, [
            CURLOPT_URL => $tournament->discord_url,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $hookObject,
            CURLOPT_HTTPHEADER => [
                "Length" => strlen($hookObject),
                "Content-Type" => "application/json"
            ]
        ]);

        curl_exec($ch);
        return curl_close($ch);
    }

}
