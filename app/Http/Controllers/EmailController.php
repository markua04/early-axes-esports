<?php

namespace App\Http\Controllers;

use App\Unsubscribe;
use Illuminate\Http\Request;

class EmailController extends Controller
{
    public function unsubscribe(Request $request){
        if($request->isMethod('post')){
            $request->validate([
                'email' => 'required|email',
            ]);
            $checkExisting = Unsubscribe::where('email',$request->email)->first();
            if($checkExisting == null){
                $email = new Unsubscribe();
                $email->email = $request->email;
                $email->reason = $request->reason;
                $email->save();
                return redirect()->back()->with('status','You have successfully unsubscribed from our mailing list!');
            } else {
                return redirect()->back()->with('status','You have already unsubscribed.');
            }
        }
        return view('emails.unsubscribe');
    }
}
