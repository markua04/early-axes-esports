<?php namespace App\Http\Controllers;
use App\Event;
use App\GamesList;
use App\Http\Requests\EventRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class EventController extends Controller
{

    /**
     * @param EventRequest $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function create(EventRequest $request)
    {
        $games = GamesList::all();
        if ($request->isMethod('post')) {
            $weekendEvent = new Event();
            $destinationPath = 'uploads'; // upload path
            if (Input::file('image') !== null) {
                $extension = Input::file('image')->getClientOriginalExtension();// getting image extension
                $fileName = rand(11111, 99999) . '.' . $extension; // renaming image
                Input::file('image')->move($destinationPath, $fileName); // uploading file to given path
                $weekendEvent->banner = "/uploads/" . $fileName;
            }
            $weekendEvent->title = $request->get('title');
            $weekendEvent->description = $request->get('description');
            $weekendEvent->game_id = $request->get('game_id');
            $weekendEvent->date = $request->get('date');
            $weekendEvent->time = $request->get('time');
            $weekendEvent->save();

            return redirect('/list-events')->with('status', 'Event has been created.');
        }
        return view('/admin/events/createorupdate',compact('games'));
    }

    /**
     * @param $id
     * @param EventRequest $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function editEvent($id, EventRequest $request)
    {
        $games = GamesList::all();
        $event = Event::find($id);
        if($request->isMethod('post')) {
            $event = Event::find($id);
            $eventBanner = $event->banner;
            $event->title = $request->get('title');
            $event->description = $request->get('description');
            $event->game_id = $request->get('game_id');
            $event->date = $request->get('date');
            $event->time = $request->get('time');
            $destinationPath = 'uploads'; // upload path
            if(Input::file('image') !== null){
                $extension = Input::file('image')->getClientOriginalExtension();// getting image extension
                $fileName = rand(11111,99999).'.'.$extension; // renaming image
                Input::file('image')->move($destinationPath, $fileName); // uploading file to given path
                $event->banner = "/uploads/".$fileName;
            } else {
                $event->banner = $eventBanner;
            }
            $event->save();
            return redirect("/list-events");
        }
        return view('admin.events.createorupdate', compact('event','games'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function listEvents()
    {
        $events = Event::where('id','>','0')->orderBy('created_at', 'desc')->paginate(5);
        if(Auth::user()['user_level_id'] == 1){
            return view('admin.events.list', compact('events'));
        } else {
            if($events->total() == 0){
                return $this->comingSoon('Events');
            } else {
                return view('admin.events.list', compact('events'));
            }
        }
    }

    /**
     * @param $id
     * @param $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function viewEvent($id, $slug)
    {
        $event = Event::find($id);
        $todaysDate = Carbon::now()->format('Y-m-d');
        return view("admin.events.view", compact('event','todaysDate'));
    }

    /**
     * @param $eventId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function deleteEvent($eventId) {
        if(Auth::user()->user_level_id == 1){
            $clan = Event::find($eventId);
            $clan->delete();
            return redirect('/list-events')->with('status', 'Event has been removed!');
        }
    }

    public function registerForEvent(Request $request){

    }
}
