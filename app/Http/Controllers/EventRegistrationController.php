<?php namespace App\Http\Controllers;

use App\Event;
use App\EventRegistration;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EventRegistrationController extends Controller
{
    /**
     * @param Request $request
     * @return $this
     */
    public function create(Request $request)
    {
        try {
            $eventRegistration = new EventRegistration();
            $eventRegistration->name = $request->get('name');
            $eventRegistration->surname = $request->get('surname');
            $eventRegistration->email = $request->get('email');
            $eventRegistration->gamer_tag = $request->get('gamer_tag');
            $eventRegistration->event_id = (int)$request->get('event_id');
            $eventRegistration->save();
            return response()->json($eventRegistration);
        } catch (\Exception $e) {
            return response()->json($e->getMessage());
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function generateEventEntries(Request $request)
    {
        $events = Event::all();
        if ($request->isMethod('post')) {
            $this->generateExcel($request->get('event_id'), $request->get('csv_name'));
        }
        return view('admin/events/export-event-registration-data', compact('events'));
    }

    /**
     * @param $eventRegistrationId
     * @param $csvName
     */
    public function generateExcel($eventRegistrationId, $csvName)
    {
        $sqlQuery = "
        SELECT
            events.title,
            event_registrations.name,
            event_registrations.surname,
            event_registrations.email,
            event_registrations.gamer_tag
        FROM event_registrations
        INNER JOIN events ON events.id = event_registrations.event_id
        WHERE event_registrations.event_id = $eventRegistrationId";

        $query = DB::select(DB::raw($sqlQuery));
        $records = collect($query)->map(function ($x) {
            return (array)$x;
        })->toArray();

        $formattedDate = str_replace('-', '_', Carbon::now()->toDateString());
        $filename = $csvName . "_" . $formattedDate . ".csv";
        $delimiter = ";";

        // open raw memory as file so no temp files needed, you might run out of memory though
        $f = fopen('php://memory', 'w');
        // loop over the input array
        $headers = [];
        $headers[] = 'Event';
        $headers[] = 'Name';
        $headers[] = 'Surname';
        $headers[] = 'Email';
        $headers[] = 'Gamer Tag';
        fputcsv($f, $headers);
        foreach ($records as $line) {
            // generate csv lines from the inner arrays
            fputcsv($f, $line, $delimiter);
        }
        // reset the file pointer to the start of the file
        fseek($f, 0);
        // tell the browser it's going to be a csv file
        header('Content-Type: application/csv');
        // tell the browser we want to save it instead of displaying it
        ob_end_clean();
        header('Content-Disposition: attachment; filename="' . $filename . '";');
        // make php send the generated csv lines to the browser
        fpassthru($f);
        exit;
    }
}
