<?php namespace App\Http\Controllers;

use App\Clan;
use App\Fixture;
use App\IndividualFixture;
use App\Tournament;
use App\TournamentSiegeIndividualStats;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\File;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\DB;
use App\User;

class ExcelImporterController extends Controller
{
    const CLAN_TOURNAMENT = 2;
    const INDIVIDUAL_TOURNAMENT = 3;

    public function index()
    {
        $tournaments = Tournament::all();
        return view('frontend.importer',compact('tournaments'));
    }

    public function deleteIndividualStats($id){
        TournamentSiegeIndividualStats::where('tournament_id', $id)->delete();
    }

    public function import(Request $request){
        //validate the xls file
        $this->validate($request, array(
            'file'      => 'required'
        ));

        if($request->hasFile('file')){
            $tournamentIndividualStats = TournamentSiegeIndividualStats::where('tournament_id', $request->get('tournament'))->first();
            if($tournamentIndividualStats !== null){
                $this->deleteIndividualStats($request->get('tournament'));
            }
            $tournament = $request->get('tournament');
            $year = $request->get('year');
            $date = $request->get('date');
            $extension = File::extension($request->file->getClientOriginalName());
            if ($extension == "xlsx" || $extension == "xls" || $extension == "csv") {

                $path = $request->file->getRealPath();
                $data = Excel::load($path, function($reader) {
                })->get();
                if(!empty($data) && $data->count()){

                    foreach ($data as $key => $value) {
                        $insert[] = [
                            'gamer_tag' => $value->gamer_tag,
                            'clan_name' => $value->clan_name,
                            'matches_played' => $value->matches_played,
                            'avg_score' => floatval($value->avg_score),
                            'scores' => $value->scores,
                            'kills' => $value->kills,
                            'deaths' => $value->deaths,
                            'kill_death_ratio' => floatval($value->kill_death_ratio),
                            'assists' => $value->assists,
                            'year' => $year,
                            'tournament_id' => $tournament,
                            'date_added' => $date,
                        ];
                    }

                    if(!empty($insert)){

                        $insertData = DB::table('tournament_siege_individual_stats')->insert($insert);
                        if ($insertData) {
                            Session::flash('success', 'Your Data has successfully imported');
                        }else {
                            Session::flash('error', 'Error inserting the data..');
                            return back();
                        }
                    }
                }

                return back();

            }else {
                Session::flash('error', 'File is a '.$extension.' file.!! Please upload a valid xls/csv file..!!');
                return back();
            }
        }
    }

    public function individual()
    {
        $tournaments = Tournament::where('tournament_type_id',self::INDIVIDUAL_TOURNAMENT)->get();
        return view('frontend.importer-individuals',compact('tournaments'));
    }

    public function importIndividualFixtures(Request $request){
        //validate the xls file
        $this->validate($request, array(
            'file'      => 'required'
        ));

        if($request->hasFile('file')){
            $tournament = $request->get('tournament');
            $extension = File::extension($request->file->getClientOriginalName());
            if ($extension == "xlsx" || $extension == "xls" || $extension == "csv") {
                $path = $request->file->getRealPath();
                $data = Excel::load($path, function($reader) {})->get();
                if(!empty($data) && $data->count()){

                    foreach ($data as $key => $value) {
                        $user = User::where('email',$value->player_1_id)->first();
                        $user2 = User::where('email',$value->player_2_id)->first();
                        if($user !== null && $user2 !== null){
                            $insert[] = [
                                'date' => $value->date,
                                'time' => $value->time,
                                'tournament_id' => $tournament,
                                'player_1_id' => $user->id,
                                'player_2_id' => $user2->id,
                                'pool' => $request->get('pool'),
                                'created_at' => Carbon::now()
                            ];
                        }
                    }

                    if(!empty($insert)){

                        $insertData = DB::table('individual_fixtures')->insert($insert);
                        if ($insertData) {
                            Session::flash('success', 'Your Data has successfully imported');
                        }else {
                            Session::flash('error', 'Error inserting the data..');
                            return back();
                        }
                    }
                }

                return back();

            }else {
                Session::flash('error', 'File is a '.$extension.' file.!! Please upload a valid xls/csv file..!!');
                return back();
            }
        }
    }

    public function clansFixtureImporter()
    {
        $tournaments = Tournament::where('tournament_type_id',self::CLAN_TOURNAMENT)->get();
        return view('frontend.importer-clan-fixtures',compact('tournaments'));
    }

    public function importClanFixtures(Request $request){
        //validate the xls file
        $this->validate($request, array(
            'file'      => 'required'
        ));

        if($request->hasFile('file')){
            $tournamentId = $request->get('tournament');
            $extension = File::extension($request->file->getClientOriginalName());
            if ($extension == "xlsx" || $extension == "xls" || $extension == "csv") {
                $path = $request->file->getRealPath();
                $data = Excel::load($path, function($reader) {})->get();
                if(!empty($data) && $data->count()){

                    foreach ($data as $key => $value) {
                        $clan1 = Clan::where('clan_email',$value->clan_1_email)->first();
                        $clan2 = Clan::where('clan_email',$value->clan_2_email)->first();
                        if($clan1 !== null && $clan2 !== null) {
                            $insert[] = [
                                'date' => $value->date,
                                'time' => $value->time,
                                'tournament_id' => $tournamentId,
                                'clan_1' => $clan1->clan_name,
                                'clan_2' => $clan2->clan_name,
                                'clan_1_id' => $clan1->id,
                                'clan_2_id' => $clan2->id,
                                'clan_1_image' => $clan1->clan_logo,
                                'clan_2_image' => $clan2->clan_logo,
                                'pool' => $request->get('pool'),
                                'created_at' => Carbon::now()
                            ];
                        }
                    }

                    if(!empty($insert)){

                        $insertData = DB::table('fixtures')->insert($insert);
                        if ($insertData) {
                            Session::flash('success', 'Your Data has successfully imported');
                        }else {
                            Session::flash('error', 'Error inserting the data..');
                            return back();
                        }
                    }
                }

                return back();

            }else {
                Session::flash('error', 'File is a '.$extension.' file.!! Please upload a valid xls/csv file..!!');
                return back();
            }
        }
    }
}
