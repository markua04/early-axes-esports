<?php namespace App\Http\Controllers;

use App\EventRegistration;
use App\TournamentRegisteredPlayers;
use Illuminate\Http\Request;
use App\User;

class ExperiencePointsController extends Controller
{
    public function generateXPForUser($id){
        $user = User::find($id);
        $eventPoints = 0;
        $tournamentPoints = 0;
        $userTournamentRegistrations = TournamentRegisteredPlayers::where('clan_member_id',$id)->get();
        $userEventRegistrations = EventRegistration::where('gamer_tag',$user->gamer_tag)->get();
        foreach($userTournamentRegistrations as $registration){
            $tournamentPoints += 100;
        }
        foreach($userEventRegistrations as $registration){
            $eventPoints += 50;
        }
        $xp = $eventPoints + $tournamentPoints;
        return $xp;
    }
}
