<?php namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Cache;

class FetchGameStatsController extends Controller
{

    public function fetchApexUser($request){
        $client = new Client(['base_uri' => 'https://apextab.com/']);
        $platform = $this->structurePlatformForApi($request);
        $apexUser = [];
        // Fetch Apex user id
        if(Cache::has('apexUserId-'.$request->gamertag)){
            $response = Cache::get('apexUserId-'.$request->gamertag);
        } else {
            $jsonData = $client->request('GET', 'api/search.php?platform='.$platform.'&search='.$request->gamertag);
            $response = json_decode($jsonData->getBody());
        }
        if($response->totalresults == 0){
            return false;
        }
        $apexUserId = $response->results[0]->aid;
        //Fetch Apex user stats with Apex User Id
        if(Cache::has('apexStats-'.$request->gamertag)){
            $apexUser['apex'] = Cache::get('apexStats-'.$request->gamertag);
        } else {
            $statsDataJson = $client->request('GET', 'https://apextab.com/api/player.php?aid='.$apexUserId);
            $apexUser['apex'] = json_decode($statsDataJson->getBody());
        }
        $apexUser['specialist_stats'] = $this->getApexSpecialistStats($apexUser['apex']);
        $apexUser['xboxData'] = $this->getXboxProfile($request->gamertag);
        return $apexUser;
    }

    public function fetchSiegeUser($request){
        $platform = $this->structurePlatformForApi($request);
        if (Cache::has('uplayId-'.$request->gamertag)) {
            $siegeId = Cache::get('uplayId-'.$request->gamertag);
        } else {
            $client = new Client(['base_uri' => 'https://r6tab.com/api/']);
            $response = $client->request('GET', 'search.php?platform='.$platform.'&search='.$request->gamertag);
            $siegeId = json_decode($response->getBody());
            Cache::put('uplayId-'.$request->gamertag,$siegeId, 10080);
            $siegeId = Cache::get('uplayId-'.$request->gamertag,$siegeId);
        }
        if($siegeId->totalresults == 0){
            return false;
        }
        $pId = $siegeId->results['0']->p_id;
        $siegeStats = [];
        if (Cache::has('siegeStats-'.$request->gamertag)) {
            $siegeStats = Cache::get('siegeStats-'.$request->gamertag);
            $siegeStats['xboxData'] = $this->getXboxProfile($request->gamertag);
        } else {
            $client = new Client(['base_uri' => 'https://r6tab.com/api/']);
            $data = $client->request('GET', 'player.php?p_id='.$pId);
            $siegeData['siege'] = json_decode($data->getBody());
            $siegeData['siege']->KD = $this->calculateSiegeKD($siegeData['siege']->kd);
            Cache::put('siegeStats-'.$request->gamertag,$siegeData, 1440);
            $siegeStats['xboxData'] = $this->getXboxProfile($request->gamertag);
            $siegeStats = Cache::get('siegeStats-'.$request->gamertag,$siegeData);
        }
        $siegeStats['operators'] = $this->mapSiegeOperators($siegeStats['siege']->operators);
        $siegeStats['thunt'] = json_decode($siegeStats['siege']->thunt);
//        dump($siegeStats);die;
        return $siegeStats;
    }

    public function structurePlatformForApi($request){
        if($request->game == 'Rainbow Six Siege' || $request->game == 'Apex Legends'){
            if($request->platform == 1){
                $platform = 'xbl';
            } elseif($request->platform == 2){
                $platform = 'psn';
            } else {
                if($request->game == 'Apex Legends'){
                    $platform = 'pc';
                } else {
                    $platform = 'uplay';
                }
            }
        }
        return $platform;
    }

    public function calculateSiegeKD($kd){
        if($kd > 99){
            $firstNumber = substr($kd,0,1);
            $nextNumber = substr($kd,1,1);
            $thirdNumber = substr($kd,2,1);
            $killDeathRatio = "$firstNumber.$nextNumber$thirdNumber";
        } else {
            $firstNumber = 0;
            $killDeathRatio = "$firstNumber.$kd";
        }
        return $killDeathRatio;
    }

    public function getXboxProfile($gamerTag){
        if (Cache::has('xboxProfile-'.$gamerTag)) {
            $xboxProfileData = Cache::get('xboxProfile-'.$gamerTag);
        } else {
            $xboxApi = new ApiController();
            $xboxProfile = $xboxApi->getXboxLiveAccount($gamerTag);
            $xboxData = json_decode($xboxProfile);
            Cache::put('xboxProfile-'.$gamerTag,$xboxData, 1440);
            $xboxProfileData = Cache::get('xboxProfile-'.$gamerTag);
        }
        return $xboxProfileData;
    }

    public function mapSiegeOperators($operators){
       $mappedOperators = [];
       $decodedData = json_decode($operators);
       foreach($decodedData as $operators){
           foreach($operators as $key => $value){
               switch ($key) {
                   case "4:3":
                       $mappedOperators['4:3'] = 'twitch';
                       break;
                   case "3:E":
                       $mappedOperators['3:E'] = 'lion';
                       break;
                   case "3:C":
                       $mappedOperators['3:C'] = 'zofia';
                       break;
                   case "2:6":
                       $mappedOperators['2:6'] = 'buck';
                       break;
                   case "5:2":
                       $mappedOperators['5:2'] = 'thermite';
                       break;
                   case "2:9":
                       $mappedOperators['2:9'] = 'hibana';
                       break;
                   case "2:5":
                       $mappedOperators['2:5'] = 'blitz';
                       break;
                   case "3:4":
                       $mappedOperators['3:4'] = 'fuze';
                       break;
                   case "3:2":
                       $mappedOperators['3:2'] = 'ash';
                       break;
                   case "4:E":
                       $mappedOperators['4:E'] = 'finka';
                       break;
                   case "2:4":
                       $mappedOperators['2:4'] = 'glaz';
                       break;
                   case "5:1":
                       $mappedOperators['5:1'] = 'thatcher';
                       break;
                   case "2:A":
                       $mappedOperators['2:A'] = 'jackal';
                       break;
                   case "2:10":
                       $mappedOperators['2:10'] = 'maverick';
                       break;
                   case "2:D":
                       $mappedOperators['2:D'] = 'dokkaebi';
                       break;
                   case "4:1":
                       $mappedOperators['4:1'] = 'sledge';
                       break;
                   case "2:11":
                       $mappedOperators['2:11'] = 'nomad';
                       break;
                   case "5:3":
                       $mappedOperators['5:3'] = 'montagne';
                       break;
                   case "2:7":
                       $mappedOperators['2:7'] = 'blackbeard';
                       break;
                   case "3:5":
                       $mappedOperators['3:5'] = 'IQ';
                       break;
                   case "4:4":
                       $mappedOperators['4:4'] = 'kapkan';
                       break;
                   case "3:6":
                       $mappedOperators['3:6'] = 'frost';
                       break;
                   case "2:1":
                       $mappedOperators['2:1'] = 'smoke';
                       break;
                   case "3:3":
                       $mappedOperators['3:3'] = 'rook';
                       break;
                   case "3:B":
                       $mappedOperators['3:B'] = 'lesion';
                       break;
                   case "3:1":
                       $mappedOperators['3:1'] = 'mute';
                       break;
                   case "5:5":
                       $mappedOperators['5:5'] = 'bandit';
                       break;
                   case "4:5":
                       $mappedOperators['4:5'] = 'jager';
                       break;
                   case "2:C":
                       $mappedOperators['2:C'] = 'ela';
                       break;
                   case "4:2":
                       $mappedOperators['4:2'] = 'pulse';
                       break;
                   case "2:2":
                       $mappedOperators['2:2'] = 'castle';
                       break;
                   case "2:3":
                       $mappedOperators['2:3'] = 'doc';
                       break;
                   case "3:F":
                       $mappedOperators['3:F'] = 'alibi';
                       break;
                   case "3:A":
                       $mappedOperators['3:A'] = 'mira';
                       break;
                   case "2:F":
                       $mappedOperators['2:F'] = 'maestro';
                       break;
                   case "3:9":
                       $mappedOperators['3:9'] = 'echo';
                       break;
                   case "5:4":
                       $mappedOperators['5:4'] = 'tachanka';
                       break;
                   case "3:12":
                       $mappedOperators['3:12'] = 'gridlock';
                       break;
                   case "2:12":
                       $mappedOperators['2:12'] = 'mozzie';
                       break;
                   case "3:11":
                       $mappedOperators['3:11'] = 'kaid';
                       break;
                   case "2:13":
                       $mappedOperators['2:13'] = 'nokk';
                       break;
                   case "2:14":
                       $mappedOperators['2:14'] = 'warden';
                       break;
                   case "3:7":
                       $mappedOperators['3:7'] = 'valkyrie';
                       break;
                   default :
                       $mappedOperators['NA'] = 'N/A';
                       break;
               }
           }
       }
       return $mappedOperators;
    }

    public function getApexSpecialistStats($stats){
        $specialists = [];
        $specialistStats = [];
        foreach($stats as $key => $value){
            if(strpos($key, 'kills_') !== false){
                $specialists[] = str_replace('kills_','',$key);
            }
        }
        foreach($specialists as $specialist){
            foreach($stats as $name => $stat){
                if(strpos($name, $specialist) !== false){
                    if($name == "kills_".$specialist){
                        $specialistStats[$specialist]['kills'] = $stat;
                    }
                    if($name == "headshots_".$specialist){
                        $specialistStats[$specialist]['headshots'] = $stat;
                    }
                    if($name == "damage_".$specialist){
                        $specialistStats[$specialist]['damage'] = $stat;
                    }
                    if($name == "matches_".$specialist){
                        $specialistStats[$specialist]['matches'] = $stat;
                    }
                }
            }

        }
        return $specialistStats;
    }

}
