<?php namespace App\Http\Controllers;

use App\Clan;
use App\ClanTournamentStanding;
use App\Fixture;
use App\FixtureResult;
use App\Helpers\Lang;
use App\Http\Repositories\BracketRepository;
use App\Http\Repositories\ClanRepository;
use App\Http\Repositories\ClanTournamentStandingRepository;
use App\Http\Repositories\FixtureResultRepository;
use App\Http\Repositories\TournamentRepository;
use App\Http\Requests\FixtureRequest;
use App\Http\Repositories\FixtureRepository;
use App\IndividualFixture;
use App\Mail\FixtureResultsMail;
use App\Tournament;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\View\View;
use ZipArchive;
use Illuminate\Support\Facades\Mail;

class FixturesController extends Controller
{

    /**
     * @param FixtureRequest $request
     * @return Factory|RedirectResponse|View
     */
    public function create(Request $request)
    {
        $tournaments = Tournament::where('tournament_type_id',2)->get();
        $clans = Clan::orderBy('clan_name')->get();
        $clansArray = [];
        $x = 0;
        if ($request->isMethod('post')) {
            $seperateDateTime = explode( ',', $request->date_time );
            $fixtureService = new FixtureRepository();
            $count = count($request->clans_1);
            for($y = 0; $y < $count; $y++){
                $timeString = str_replace(' ','',$seperateDateTime[1]);
                $request->date = date_format(date_create($seperateDateTime[0]),"Y-m-d");
                $request->time = date("H:i", strtotime($timeString));
                $request->clan_1 = $request->clans_1[$y];
                $request->clan_2 = $request->clans_2[$y];
                $saveFixture = $fixtureService->create($request);
            }
            if($saveFixture == true){
                return redirect()->back()->with('status', Lang::getStatus('fixtures_created'));
            } else {
                return redirect()->back()->with('status', Lang::errorStatus('fixture_not_created'));
            }
        }
        foreach ($clans as $clan){
            $clansArray[$x]['id'] = $clan->id;
            $clansArray[$x]['name'] = $clan->clan_name;
            $x++;
        }
        $clansObj = json_encode($clansArray);
        return view('/admin/fixtures/createorupdate', compact('tournaments', 'clansObj'));
    }

    public function update(FixtureRequest $request,$id)
    {
        $tournaments = Tournament::all();
        $clans = Clan::all();
        if ($request->isMethod('post')) {
            return redirect('view-tournament/'.$request->get('tournament_id').'/')->with('status', Lang::getStatus(''));
        }
        return view('/admin/fixtures/createorupdate', compact('tournaments', 'clans'));
    }

    /**
     * @param $clanName
     * @return mixed
     */
    public function getClanImage($clanName)
    {
        $clan = Clan::where('clan_name',$clanName)->first();
        $clanImage = $clan->clan_logo;
        return $clanImage;
    }

    public function createFixtureResult($id,$clanId,Request $request)
    {
        $fixture = Fixture::find($id);
        $tournament = Tournament::find($fixture->tournament_id);
        $tournamentPlatform = $tournament->tournament_platform;
        if(!isset($fixture->clan_1_id) || !isset($fixture->clan_2_id)){
            $clan1 = Clan::where('clan_name',$fixture->clan_1)->first();
            $clan2 = Clan::where('clan_name',$fixture->clan_2)->first();
        } else {
            $clan1 = Clan::find($fixture->clan_1_id);
            $clan2 = Clan::find($fixture->clan_2_id);
        }
        $x = 1;
        $bracketsRepo = new BracketRepository();
        if(isset($clan1->clan_owner_id) && (Auth::user()->id == $clan1->clan_owner_id) || isset($clan2->clan_owner_id) && (Auth::user()->id == $clan2->clan_owner_id) || isset($clan1->secondary_captain) && (Auth::user()->id == $clan1->secondary_captain) || isset($clan2->secondary_captain) && (Auth::user()->id == $clan2->secondary_captain)  || Auth::user()->user_level_id == 1) {
            if ($request->isMethod('post')) {
                if(count($request->rounds) < 1){
                    return redirect()->back()->with('status','Please upload at least 3 screenshots.');
                } elseif (count($request->rounds) < 1){
                    return redirect()->back()->with('status','Please upload at least 1 screenshot.');
                }
                $calculateWinLossDraw = $this->calculateScores($request->clan_1_score,$request->clan_2_score,$tournament);
                $fixtureResultExists = FixtureResult::where('fixture_id',$fixture->id)->first();
                $destinationPath = public_path('/uploads/fixture-results/' . $fixture->id . '/');
                File::makeDirectory($destinationPath, $mode = 0777, true, true);
                if($fixtureResultExists == null){
                    $fixtureResult = new FixtureResult();
                    if($request->rounds !== null){
                        foreach ($request->rounds as $round) {
                            $extension = $round->getClientOriginalExtension();// getting image extension
                            $fileName = 'round_' . $x . '.' . $extension; // renaming image
                            $round->move($destinationPath, $fileName); // uploading file to given path
                            $x++;
                        }
                    }
                    $fixtureResult->fixture_id = $fixture->id;
                    $fixtureResult->tournament_id = $fixture->tournament_id;
                    $fixtureResult->clan_1 = isset($clan1->clan_name) ? $clan1->clan_name : 'No Name';
                    $fixtureResult->clan_2 = isset($clan2->clan_name) ? $clan2->clan_name : 'No Name';
                    $fixtureResult->clan_1_id = isset($clan1->id) ? $clan1->id : 0;
                    $fixtureResult->clan_2_id = isset($clan2->id) ? $clan2->id : 0;
                    $fixtureResult->clan_1_score = $request->clan_1_score;
                    $fixtureResult->clan_2_score = $request->clan_2_score;
                    $fixtureResult->clan_1_points = $calculateWinLossDraw['clan_1'];
                    $fixtureResult->clan_2_points = $calculateWinLossDraw['clan_2'];
                    $fixtureResult->save();
                    $fixture->result = 1;
                    $fixture->save();
                    $standingsController = new StandingsController();
                    $clan1WinsLossesDraws = $standingsController->getClanWinsLossesDrawsForTournament($clan1,$tournament);
                    $clan2WinsLossesDraws = $standingsController->getClanWinsLossesDrawsForTournament($clan2,$tournament);
                    $this->saveClanStanding($clan1->id, $tournament->id,$this->calculateFixtureResultPoints($clan1->id,$tournament->id),$clan1WinsLossesDraws, $tournamentPlatform);
                    $this->saveClanStanding($clan2->id, $tournament->id,$this->calculateFixtureResultPoints($clan2->id,$tournament->id),$clan2WinsLossesDraws, $tournamentPlatform);
                    if(isset($tournament->discord_url) && $tournament !== null) {
                        $discordController = new DiscordController();
                        $discordController->fixtureResultsPerTournament($request, $clan1, $clan2, $fixture->tournament_id);
                    }
                    if($fixture->bracket == 1){
                        $bracketsRepo->update($fixture->id,$tournament->id);
                    }
                    return redirect("/dashboard")->with('status','Fixture result submitted!');
                } else {
                    return redirect("/dashboard")->with('status',Lang::errorStatus('fixture_result_exists'));
                }
            }
            return view('frontend.fixtures.submit-results',compact('clan1','clan2','fixture','clanId'));
        } else {
            return redirect()->back()->with('status','You do not have permissions to do that.');
        }
    }

    /**
     * @param $id
     * @return Factory|View
     */
    public function listFixtureResults($id){
        $fixtureResultRepo = new FixtureResultRepository();
        $tournamentRepo = new TournamentRepository();
        $fixtureResults = $fixtureResultRepo->getByTournamentId($id);
        $tournament = $tournamentRepo->getById($id);
        return view('admin.fixtures.list',compact('fixtureResults','tournament'));
    }


    /**
     * @param $id
     * @return Factory|View
     */
    public function listFixtures($id){
        $tournamentRepo = new TournamentRepository();
        $tournament = $tournamentRepo->getById($id);
        if($tournament->tournament_type_id == 2){
            $fixtures = Fixture::orderBy('created_at', 'desc')->where('tournament_id',$id)->get();
            return view('admin.fixtures.list-tournament-fixtures',compact('fixtures','tournament'));
        } else {
            $fixtures = IndividualFixture::orderBy('created_at', 'desc')->where('tournament_id',$id)->get();
            return view('admin.fixtures.list-individual-tournament-fixtures',compact('fixtures','tournament'));
        }
    }

    /**
     * @param $id
     * @return Factory|View
     */
    public function remindFixtureResults($id,Request $request){
        $fixtureRepo = new FixtureRepository();
        $clanRepo = new ClanRepository();
        $fixture = $fixtureRepo->getById($id);
        $clan1 = $clanRepo->getByName($fixture->clan_1);
        $clan2 = $clanRepo->getByName($fixture->clan_2);
        if($clan1->clan_email){
            Mail::to($clan1->clan_email)->send(new FixtureResultsMail($clan1));
        }
        if($clan2->clan_email){
            Mail::to($clan2->clan_email)->send(new FixtureResultsMail($clan2));
        }
        $fixture->reminded = 1;
        $fixture->save();
        return redirect()->back()->with('status', 'Reminder sent to clan leaders!');
    }

    /**
     * @param $id
     * @return Factory|View
     */
    public function deleteFixture($id){
        $fixtureRepo = new FixtureRepository();
        $fixtureRepo->delete($id);
        return redirect()->back()->with('status','Fixture has been deleted.');
    }

    public function downloadAllScreenshots($id){
        $fixture = Fixture::find($id);
        $clan1 = str_slug($fixture->clan_1);
        $clan2 = str_slug($fixture->clan_2);
        // Define Dir Folder
        $public_dir = public_path()."/uploads/fixture-results/$id/";
        $files = $files = File::allFiles(public_path()."/uploads/fixture-results/$id/");
        // Zip File Name
        $zipFileName = $clan1.'VS'.$clan2.'.zip';
        // Create ZipArchive Obj
        $zip = new ZipArchive;
        if ($zip->open($public_dir . '/' . $zipFileName, ZipArchive::CREATE) === TRUE) {
            // Add Multiple file
            foreach($files as $file) {
                $zip->addFile($file->getPathname(), $file->getBasename());
            }
            $zip->close();
        }
        // Set Header
        $headers = array(
            'Content-Type' => 'application/octet-stream',
        );
        $filetopath=$public_dir.'/'.$zipFileName;
        // Create Download Response
        if(file_exists($filetopath)){
            return response()->download($filetopath,$zipFileName,$headers);
        }
    }

    public function saveClanScoreByTournament($tournament,$fixtureId,$clan1Score,$clan2Score){
        $winPoints = $tournament->win_points;
        $lossPoints = $tournament->loss_points;
        $drawPoints = $tournament->draw_points;
        $fixtureResult = FixtureResult::where('fixture_id',$fixtureId)->first();
        if($clan1Score > $clan2Score){
            $fixtureResult->clan_1_points = $winPoints;
            $fixtureResult->clan_2_points = $lossPoints;
        } elseif ($clan2Score > $clan1Score){
            $fixtureResult->clan_1_points = $lossPoints;
            $fixtureResult->clan_2_points = $winPoints;
        } elseif($clan2Score == $clan1Score){
            $fixtureResult->clan_1_points = $drawPoints;
            $fixtureResult->clan_2_points = $drawPoints;
        } else {
            $fixtureResult->clan_1_points = $lossPoints;
            $fixtureResult->clan_2_points = $lossPoints;
        }
        $fixtureResult->save();
    }

    public function saveClanStanding($clan,$tournamentId,$points,$clanWinsLossesDraws, $tournamentPlatform){
        $clanTournamentStandingRepo = new ClanTournamentStandingRepository();
        $clanStandings = $clanTournamentStandingRepo->getByClanAndTournament($clan,$tournamentId);
        if($clanStandings !== null){
            $clanTournamentStandingRepo->edit($clanStandings->id,$clanStandings,$clanWinsLossesDraws,$points, $tournamentPlatform);
        } else {
            $clanTournamentStandingRepo->create($clan,$tournamentId,$clanWinsLossesDraws,$points, $tournamentPlatform);
        }
    }

    public function saveForfeitedClanStanding($clan,$tournamentId,$points,$clanWinsLossesDraws){
        $clanStandings = ClanTournamentStanding::where('clan_id',$clan)->where('tournament_id',$tournamentId)->first();
        if($clanStandings !== null){
            $clanPoints = $clanStandings->points;
            $gamesPlayed = $clanStandings->played;
            $clanStandings->points = $clanPoints + $points;
            $clanStandings->wins = $clanWinsLossesDraws['wins'];
            $clanStandings->losses = $clanWinsLossesDraws['losses'];
            $clanStandings->draws = $clanWinsLossesDraws['draws'];
            $clanStandings->played = $gamesPlayed;
            $clanStandings->forfeit = 1;
            $clanStandings->save();
        } else {
            $clanStandings = new ClanTournamentStanding();
            $clanStandings->clan_id = $clan;
            $clanStandings->tournament_id = $tournamentId;
            $clanStandings->wins = $clanWinsLossesDraws['wins'];
            $clanStandings->losses = $clanWinsLossesDraws['losses'];
            $clanStandings->draws = $clanWinsLossesDraws['draws'];
            $clanStandings->points = $points;
            $clanStandings->played = 0;
            $clanStandings->forfeit = 1;
            $clanStandings->save();
        }
    }

    public function calculateFixtureResultPoints($clanId,$tournamentId){
        $fixtureResultsRepo = new FixtureResultRepository();
        $results = $fixtureResultsRepo->getByClanAndTournamentId($clanId,$tournamentId);
        $clansPoints = 0;
        foreach($results as $result){
            if($result->clan_1_id == $clanId){
                $clansPoints += $result->clan_1_points;
            } else {
                $clansPoints += $result->clan_2_points;
            }
        }
        return $clansPoints;
    }

    public function calculateScores($clan1Score,$clan2Score,$tournament){
        if($clan1Score > $clan2Score){
            $result = ['clan_1' => $tournament->win_points, 'clan_2' => $tournament->loss_points];
        } elseif($clan2Score > $clan1Score){
            $result = ['clan_1' => $tournament->loss_points, 'clan_2' => $tournament->win_points];
        } elseif($clan1Score !== 0 && $clan2Score !== 0 && $clan1Score == $clan2Score){
            $result = ['clan_1' => $tournament->draw_points, 'clan_2' => $tournament->draw_points];
        } else {
            $result = ['clan_1' => 0, 'clan_2' => 0];
        }
        return $result;
    }

    public function deleteAutoFixtures(Request $request){
        $tournamentRepo = new TournamentRepository();
        $tournaments = $tournamentRepo->getAll();
        if($request->isMethod('post')){
            $status = 'Fixtures not deleted! Please delete manually.';
            $fixtureRepo = new FixtureRepository();
            $verdict = $fixtureRepo->deleteAutoFixturesByPoolAndTournament($request->tournament_id,$request->pool);
            if($verdict == true){
                $status = 'Fixtures deleted!';
            }
            return redirect()->back()->with('status',$status);
        }
        return view('admin.tournaments.delete-auto-fixtures',compact('tournaments'));
    }
}
