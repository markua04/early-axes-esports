<?php namespace App\Http\Controllers;

use App\Platform;
use Illuminate\Http\Request;

class GameApiStatsController extends Controller
{
    public function playerStats(Request $request){
        $noStats = 'No stats found for that user.';
        $games = [
            0 => 'Apex Legends',
            1 => 'Rainbow Six Siege'
        ];
        $platforms = Platform::all();
        if($request->isMethod('post')){
            if($request->game !== null && $request->platform !== null){
                if($request->game == 'Apex Legends'){
                    if($this->getApexStatsByUser($request) == false){
                        return redirect()->back()->with('status',$noStats);
                    } else {
                        return $this->getApexStatsByUser($request);
                    }
                } elseif($request->game == 'Rainbow Six Siege'){
                   if($this->getSiegeStatsByUser($request) == false){
                       return redirect()->back()->with('status',$noStats);
                   } else {
                       return $this->getSiegeStatsByUser($request);
                   }
                } else {
                    return redirect()->back()->with('status','Sorry, that selection is not correct!');
                }
            } elseif ($request->game == null || $request->platform == null){
                return redirect()->back()->with('status','Please select a game and platform.');
            }
        }
        return view('frontend.gamer_stats.player-stats',compact('games','platforms'));
    }

    public function getApexStatsByUser(Request $request){
        $getStats = new FetchGameStatsController();
        $platform = $request->platform;
        $userStats = $getStats->fetchApexUser($request);
        if($userStats == false){
            return false;
        }
        return view('frontend.gamer_stats.apex',compact('userStats','platform'));
    }

    public function getSiegeStatsByUser(Request $request){
        $getStats = new FetchGameStatsController();
        $platform = $request->platform;
        $userStats = $getStats->fetchSiegeUser($request);
        if($userStats == false){
            return false;
        }
        return view('frontend.gamer_stats.siege',compact('userStats','platform'));
    }
}
