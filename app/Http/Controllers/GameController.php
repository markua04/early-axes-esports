<?php namespace App\Http\Controllers;
use App\GamesList;
use App\Tournament;
use App\TournamentTypes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class GameController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function create(Request $request)
    {
        if (Auth::user()->getAttributes()['user_level_id'] == 1) {
            $games = new GamesList;
            if($request->isMethod('post')){
                $request->validate([
                    'game_name' => 'required',
                ]);
                $destinationPath = 'uploads/games'; // upload path
                if(Input::file('image') !== null){
                    $extension = Input::file('image')->getClientOriginalExtension();// getting image extension
                    $fileName = rand(11111,99999).'.'.$extension; // renaming image
                    Input::file('image')->move($destinationPath, $fileName); // uploading file to given path
                    $games->game_banner = "/uploads/games/".$fileName;
                }
                $games->game_name = $request->get('game_name');
                $games->save();
                return redirect('/admin/list-games')->with('status','Game '.$request->get('game_name').' has been created successfully');
            }
            return view('/admin/manage-games/create-games');
        }
        return redirect()->back()->with('status', 'You are not allowed to create tournaments');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function edit(Request $request, $id)
    {
        $game = GamesList::find($id);
        if($request->isMethod('post')){
            $request->validate([
                'game_name' => 'required',
            ]);
            $destinationPath = 'uploads/games'; // upload path
            if(Input::file('image') !== null){
                $extension = Input::file('image')->getClientOriginalExtension();// getting image extension
                $fileName = rand(11111,99999).'.'.$extension; // renaming image
                Input::file('image')->move($destinationPath, $fileName); // uploading file to given path
                $game->game_banner = "/uploads/games/".$fileName;
            }
            $game->game_name = $request->get('game_name');
            $game->save();
            return redirect('/admin/list-games')->with('status','Game '.$request->get('game_name').' has been created successfully');
        }
        return view('/admin/manage-games/edit',compact('game'));
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function updateTournament($id,Request $request)
    {
        $tournament = Tournament::find($id);
        $games = GamesList::all();
        $tournamentTypes = TournamentTypes::all();
        if($request->isMethod('post')) {
            $request->validate([
                'tournament_title' => 'required',
                'tournament_prize' => 'required',
                'tournament_info' => 'required',
            ]);
            $tournament = Tournament::find($id);
            $tournament_banner = $tournament->tournament_banner;
            $tournament->tournament_title = $request->get('tournament_title');
            $tournament->tournament_info = $request->get('tournament_info');
            $tournament->tournament_rules = $request->get('tournament_rules');
            $tournament->tournament_type_id = $request->get('tournament_type_id');
            $destinationPath = 'uploads/tournaments'; // upload path
            if(Input::file('image') !== null){
                $extension = Input::file('image')->getClientOriginalExtension();// getting image extension
                $fileName = rand(11111,99999).'.'.$extension; // renaming image
                Input::file('image')->move($destinationPath, $fileName); // uploading file to given path
                $tournament->tournament_banner = "/uploads/tournaments/".$fileName;
            } else {
                $tournament->tournament_banner = $tournament_banner;
            }
            $tournament->save();
            return redirect("/admin/list-tournaments")->with('status','Tournament Updated!');
        }
        return view("admin/tournaments/edit-tournament", compact('tournament','games', 'tournamentTypes'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function listGames()
    {
        if(Auth::user()->user_level_id == 1){
            $games = GamesList::all();
            return view('/admin/manage-games/list-games', ['games' => $games]);
        }
        return redirect()->back()->with('status','You lack permissions to do that.');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function delete($id)
    {
        if(Auth::user()->user_level_id == 1){
            $games = GamesList::all();
            $game = GamesList::find($id);
            $game->delete();
            return view('/admin/manage-games/list-games', ['games' => $games]);
        }
        return redirect()->back()->with('status','You lack permissions to do that.');
    }
}
