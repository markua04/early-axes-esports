<?php namespace App\Http\Controllers;

use App\Clan;
use App\ClanMembers;
use App\ClanNews;
use App\Event;
use App\GameRelease;
use App\Helpers\Lang;
use App\Http\Repositories\ClanMemberRepository;
use App\Http\Repositories\ClanNewsRepository;
use App\Http\Repositories\ClanRepository;
use App\Http\Repositories\ContactRepository;
use App\Http\Repositories\FixtureRepository;
use App\Http\Repositories\FixtureResultRepository;
use App\Http\Repositories\TournamentRepository;
use App\User;
use App\EventRegistration;
use App\Fixture;
use App\FixtureResult;
use App\Http\Requests\ContactRequest;
use App\Mail\ContactMail;
use App\News;
use App\Tournament;
use App\TournamentRegisteredPlayers;
use App\TournamentSiegeIndividualStats;
use http\Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;
use Illuminate\View\View;

class HomeController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function home()
    {
        return view('welcome');
    }

    /**
     * @param ContactRequest $request
     * @return Factory|View
     */
    public function contact(ContactRequest $request)
    {
        if ($request->isMethod('post')) {
            if ($request->{'g-recaptcha-response'} !== null) {
                $contactService = new ContactRepository();
                $saveContact = $contactService->create($request);
                if($saveContact == true){
                    try {
                        Mail::to(env('ADMIN_EMAIL_ADDRESS'))->send(new ContactMail($request));
                    } catch(Exception $e){
                        Log::error($e->getMessage());
                    }
                }
                return redirect()->back()->with('status', Lang::getStatus('contact_user_successful'));
            } else {
                return redirect()->back()->with('status', Lang::getStatus('confirm_not_robot'));
            }
        }
        return view('frontend.contact');
    }

    /**
     * @return Factory|View
     */
    public function help()
    {
        return view('frontend.help');
    }

    /**
     * @return bool|string
     */
    public function getPosts()
    {
        $profileUrl = "https://earlyaxes.co.za/wp-json/wp/v2/posts?per_page=6";
        $ch = curl_init();
        curl_setopt(
            $ch,
            CURLOPT_URL,
            $profileUrl
        );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }

    /**
     * Show the application dashboard.get latest by created_at laravel
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $latestTournaments = Tournament::where('tournament_status',1)->orderBy('id', 'desc')->take(2)->get();
        $systemNews = News::orderBy('id', 'desc')->take(5)->get();
        $fixtures = FixtureResult::orderBy('id','desc')->take(10)->get();
        $gameRelease = GameRelease::latest()->first();
        $fixturesArray = [];
        $x = 0;
        foreach ($fixtures as $fixture){
            $fixtureTime = "";
            $fixtureDate = "";
            if(isset($fixture->fixture->time) && isset($fixture->fixture->date)){
                $fixtureTime = $fixture->fixture->time;
                $fixtureDate = $fixture->fixture->date;
            }
            $fixturesArray[$x]['clan_1'] = $fixture->clan_1;
            $fixturesArray[$x]['clan_2'] = $fixture->clan_2;
            $fixturesArray[$x]['clan_1_id'] = $fixture->clan_1_id;
            $fixturesArray[$x]['clan_2_id'] = $fixture->clan_2_id;
            $fixturesArray[$x]['clan_1_score'] = $fixture->clan_1_score;
            $fixturesArray[$x]['clan_2_score'] = $fixture->clan_2_score;
            $fixturesArray[$x]['date'] = $fixtureDate;
            $fixturesArray[$x]['time'] = $fixtureTime;
            $fixturesArray[$x]['tournament'] = getTournamentById($fixture->tournament_id)->tournament_title;
            $x++;
        }
        return view('home', compact('systemNews', 'earlyaxesLatestNews', 'results','gameRelease','latestTournaments','fixturesArray'));
    }

    public function getClanById($id)
    {
        $fixtureResults = new ClanController();
        $clanRepo = new ClanRepository();
        $results = $fixtureResults->clanResultsFeed($id);
        if (Auth::check()) {
            $clan = $clanRepo->getById($id);
            $members = $this->clanMembers($id);
            $clanNews = $this->getClanNews($id);
            if (isset($clan->clan_status) && $clan->clan_status == 0 && Auth::user()->user_level_id !== 1) {
                return redirect('/')->with('status', 'Your clan has not been approved by the moderators yet.');
            } else {
                return view('/admin/manage-clans/clan-profile', compact('clan', 'members', 'clanNews', 'status', 'results'));
            }
        } else {
            $clan = $clanRepo->getById($id);
            $members = $this->clanMembers($id);
            $status = "";
            return view('/admin/manage-clans/clan-profile', compact('clan', 'members', 'status', 'results'));
        }
    }


    /**
     * @param $id
     * @return mixed
     */
    public function clanMembers($id)
    {
        $clanMembersRepo = new ClanMemberRepository();
        return $clanMembersRepo->getApprovedClanMembersById($id);
    }

    public function listClans()
    {
        $clanRepo = new ClanRepository();
        $clans = $clanRepo->getByClanStatus(2);
        return view('/admin/manage-clans/list-clans', compact('clans'));
    }

    /**
     * @param $clanId
     * @return mixed
     */
    public function getClanNews($clanId)
    {
        $clanNews = ClanNews::where('clan_id', $clanId)->paginate(2);
        return $clanNews;
    }

    /**
     * @param $slug
     * @return Factory|View
     */
    public function getClanNewsArticle($slug)
    {
        $clanNewsRepo = new ClanNewsRepository();
        $clanNewsArticle = $clanNewsRepo->getBySlug($slug);
        return view('/frontend/clans/clan-news', compact('clanNewsArticle'));
    }

    public function cachePosts()
    {
        $profileUrl = "https://earlyaxes.co.za/wp-json/wp/v2/posts?per_page=6";
        $ch = curl_init();
        curl_setopt(
            $ch,
            CURLOPT_URL,
            $profileUrl
        );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        $response = curl_exec($ch);
        curl_close($ch);
        return response()->json($response);
    }

    public function getQuote(){
        $profileUrl = "http://quotes.rest/qod.json";
        $ch = curl_init();
        curl_setopt(
            $ch,
            CURLOPT_URL,
            $profileUrl
        );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        $response = curl_exec($ch);
        curl_close($ch);
        if (Cache::has('dailyQuote')) {
            $quote = Cache::get('dailyQuote');
        } else {
            Cache::put('dailyQuote',$response, 800);
            $quote = Cache::get('dailyQuote');
        }
        $decodedQuote = json_decode($quote);
        if(isset($decodedQuote->error->code)){
            if($decodedQuote->error->code == 429){
                $decodedQuote = null;
            }
        }
        return $decodedQuote;
    }

    /**
     * @return Factory|View
     */
    public function dashboard()
    {
        if (Auth::user() && Auth::user()['user_level_id'] == 1) {
            $clanRepo = new ClanRepository();
            $user = User::find(Auth::user()->id);
            $clanOwner = isOwnerOfClan();
            $clanJoinUrl = "";
            if($clanOwner == 'Yes'){
                $clanJoinUrl = "/join-clan/".str_replace(" ","%20",$user->clan->clan_name);
            }
            $fixturesNeedingResults = Fixture::whereDate('date', '<', Carbon::now())->where('result',null)->get();
            $quote = $this->getQuote();
            $userCount = User::count();
            $clanCount = $clanRepo->getAll()->count();
            $tournamentCount = Tournament::count();
            $clansNeedingModeration = Clan::where('clan_status', 0)->get();
            $now = Carbon::now();
            $newUsersThisMonth = User::whereMonth('created_at', '=', $now->month)->count();
            return view('admin.admin.dashboard', compact('userCount', 'clanCount', 'tournamentCount', 'clansNeedingModeration','newUsersThisMonth','quote','fixturesNeedingResults','clanJoinUrl','fixturesObj'));
        } else {
           return $this->userdash();
        }
    }

    public function userdash()
    {
        session_start();
        $user = Auth::user();
        if($_SESSION && $_SESSION['joinUrl'] !== null){
            return redirect($_SESSION['joinUrl']);
        }
        $gamerTag = getGamerTag($user);
        $myDate = Carbon::now()->format('Y/m/d');
        $todaysDate = str_replace('/','-',$myDate);
        $clan = Clan::find($user->clan_id);
        $clanJoinUrl = "";
        $clanOwner = isOwnerOfClan();
        if($clanOwner == 'Yes' && isset($user->clan->clan_name)){
            $clanJoinUrl = "/join-clan/".str_replace(" ","%20",$user->clan->clan_name);
            $fixtures = Fixture::where('clan_1_id', $user->clan_id)->orWhere('clan_2_id',$user->clan_id)->orderBy('id','DESC')->paginate(50);
            $fixturesArray = $this->fixturesObj($fixtures);
            $fixturesObj = json_encode($fixturesArray['fixturesArray']);
        }
        $xp = new ExperiencePointsController();
        $userXp = $xp->generateXPForUser($user->id);
        $event = Event::where('id','>','0')->orderBy('id','desc')->first();
        $latestTournament = Tournament::where('tournament_status',1)->orderBy('id','DESC')->first();
        $tweet = $this->getLatestTwitterPost();
        $siegeStats = TournamentSiegeIndividualStats::where('gamer_tag', $user->gamer_tag)->first();
        $eventRegistrations = EventRegistration::where('gamer_tag', $user->gamer_tag)->count();
        $tournamentsPlayed = TournamentRegisteredPlayers::where('clan_member_id', $user->id)->count();
        if (!is_null($user->clan)) {
            $upcomingGames = Fixture::where('clan_1', $user->clan->clan_name)->orWhere('clan_2', $user->clan->clan_name)->get();
        } else {
            $upcomingGames = null;
        }
        $upcomingGame = collect($upcomingGames)->last();
        if (!is_null($user->clan)) {
            $latestFixtureResults = FixtureResult::where('clan_1', $user->clan->clan_name)->orWhere('clan_2', $user->clan->clan_name)->get();
        } else {
            $latestFixtureResults = null;
        }
        $latestFixtureResult = collect($latestFixtureResults)->last();
        return view('frontend.dashboard.dashboard', compact('userXp','event','latestTournament','tweet','user', 'tournamentsPlayed', 'eventRegistrations', 'upcomingGame', 'latestFixtureResult', 'siegeStats','clanOwner','clan','fixtures','todaysDate','individualFixtures','clanJoinUrl','gamerTag','fixturesObj'));
    }

    public function getTitleAndDate($fixtureId){
        $fixtureRepository = new FixtureRepository();
        return $fixtureRepository->getByFixtureId($fixtureId);
    }

    /**
     * @return mixed
     */
    public function clanResultsFeed()
    {
        $fixtureResults = FixtureResult::orderBy('id','desc')->take(10)->get();
        foreach ($fixtureResults as $fixtureResult){
            $fixture = $this->getTitleAndDate($fixtureResult->fixture_id);
            if($fixture !== null){
                $tournament = getTournamentById($fixtureResult->tournament_id)->tournament_title;
                $fixtureResult->tournament = $tournament;
                $fixtureResult->time = $fixture->time;
                $fixtureResult->date = $fixture->date;
                $fixtureResult->syncOriginal();
            }
        }
        return $fixtureResults;
    }

    public function getLatestTwitterPost()
    {
        $twitter_timeline = "user_timeline";
        $request = array(
            'count' => '1',
            'screen_name' => env('SCREEN_NAME')
        );

        $oauth = array(
            'oauth_consumer_key' => env('CONSUMER_KEY'),
            'oauth_nonce' => time(),
            'oauth_signature_method' => 'HMAC-SHA1',
            'oauth_token' => env('OAUTH_ACCESS_TOKEN'),
            'oauth_timestamp' => time(),
            'oauth_version' => '1.0'
        );

        $oauth = array_merge($oauth, $request);
        $baseURI="https://api.twitter.com/1.1/statuses/$twitter_timeline.json";
        $method="GET";
        $params=$oauth;

        $r = array();
        ksort($params);
        foreach($params as $key=>$value){
            $r[] = "$key=" . rawurlencode($value);
        }
        $base_info = $method."&" . rawurlencode($baseURI) . '&' . rawurlencode(implode('&', $r));
        $composite_key = rawurlencode(env('CONSUMER_SECRET')) . '&' . rawurlencode(env('OAUTH_ACCESS_TOKEN_SECRET'));
        $oauth_signature = base64_encode(hash_hmac('sha1', $base_info, $composite_key, true));
        $oauth['oauth_signature'] = $oauth_signature;
        $r = 'Authorization: OAuth ';

        $values = array();
        foreach($oauth as $key=>$value){
            $values[] = "$key=\"" . rawurlencode($value) . "\"";
        }
        $r .= implode(', ', $values);
        $header = array($r, 'Expect:');
        $options = array(
            CURLOPT_HTTPHEADER => $header,
            CURLOPT_HEADER => false,
            CURLOPT_URL => "https://api.twitter.com/1.1/statuses/$twitter_timeline.json?". http_build_query($request),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYPEER => true
        );
        $feed = curl_init();
        curl_setopt_array($feed, $options);
        $json = curl_exec($feed);
        curl_close($feed);
        $tweet=json_decode($json, true);
        return $tweet;
    }

    public function logActivity()
    {
        if(Auth::user()->user_level_id == 1){
            $logs = \App\Helpers\LogActivity::logActivityLists();
            return view('logActivity',compact('logs'));
        }
        return redirect()->back()->with('status',Lang::errorStatus('no_access_rights'));
    }

    public function fixturesObj($fixtures){
        $fixturesArray = [];
        $x = 0;
        $tournamentRepo = new TournamentRepository();
        foreach($fixtures as $data){
            $tournament = $tournamentRepo->getById($data->tournament_id);
            $fixturesArray[$x]['id'] = $data->id;
            $fixturesArray[$x]['clan_1'] = $data->clan_1;
            $fixturesArray[$x]['clan_2'] = $data->clan_2;
            $fixturesArray[$x]['clan_1_id'] = $data->clan_1_id;
            $fixturesArray[$x]['clan_2_id'] = $data->clan_2_id;
            $fixturesArray[$x]['tournament_id'] = $data->tournament_id;
            $fixturesArray[$x]['tournament_title'] = isset($tournament->tournament_title) ? $tournament->tournament_title : '';
            $fixturesArray[$x]['date'] = isset($data->date) ? $data->date : '';
            $fixturesArray[$x]['time'] = isset($data->time) ? $data->time : '';
            $fixturesArray[$x]['result'] = isset($data->result) ? $data->result : '';
            $x++;
        }
        return ['fixturesArray' => $fixturesArray];
    }

}
