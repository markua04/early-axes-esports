<?php namespace App\Http\Controllers;

use App\Clan;
use App\Helpers\Lang;
use App\Http\Requests\FixtureRequest;
use App\Http\Requests\FixtureResultsRequest;
use App\Http\Repositories\FixtureRepository;
use App\Http\Repositories\IndividualFixtureResultRepository;
use App\Http\Repositories\IndividualFixtureRepository;
use App\Http\Repositories\IndividualTournamentStandingRepository;
use App\Http\Repositories\TournamentRepository;
use App\Http\Repositories\UserRepository;
use App\IndividualFixture;
use App\IndividualFixtureResult;
use App\Tournament;
use App\User;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\View\View;
use ZipArchive;
use Carbon\Carbon;

class IndividualFixturesController extends Controller
{
    const FIFA = 15;

    /**
     * @param Request $request
     * @return Factory|RedirectResponse|View
     */
    public function create(Request $request)
    {
        $tournaments = Tournament::where('tournament_type_id', 3)->get();
        $users = User::orderBy('name')->get();
        $usersArray = [];
        $x = 0;
        foreach($users as $user){
            $usersArray[$x]['id'] = $user->id;
            $usersArray[$x]['name'] = $this->getGamerTag($user);
            $x++;
        }
        if ($request->isMethod('post')) {
            $seperateDateTime = explode( ',', $request->date_time );
            $individualFixtureRepo = new IndividualFixtureRepository();
            $count = count($request->users_1);
            for($y = 0; $y < $count; $y++){
                $data = [];
                $timeString = str_replace(' ','',$seperateDateTime[1]);
                $request->request->add(['date' => date_format(date_create($seperateDateTime[0]),"Y-m-d")]);
                $request->request->add(['time' => date("H:i", strtotime($timeString))]);
                $data['player_1']  = $request->users_1[$y];
                $data['player_2'] = $request->users_2[$y];
                $individualFixtureRepo->create($request,$data);
            }
            return redirect()->back()->with('status', 'Fixtures have been created.');
        }
        $usersObj = json_encode($usersArray);
        return view('/admin/fixtures/individual-createorupdate', compact('tournaments', 'users','usersObj'));
    }

    public function update(FixtureRequest $request, $id)
    {
        $tournaments = Tournament::all();
        $clans = Clan::all();
        if ($request->isMethod('post')) {
            $tournamentService = new TournamentRepository();
            $tournament = $tournamentService->findById($request->get('tournament_id'));
            $destinationPath = 'uploads'; // upload path
            if (Input::file('image') !== null) {
                $fileName = $this->saveImage($destinationPath, "/uploads/fixture/");
                $resultImage = "/uploads/fixture/" . $fileName;
            }
            $fixtureService = new FixtureRepository();
            $fixtureService->update($id,$request, $resultImage);

            return redirect('view-tournament/' . $request->get('tournament_id') . '/' . $tournament->tournament_title)->with('status', 'Fixture has been created.');
        }
        return view('/admin/fixtures/createorupdate', compact('tournaments', 'clans'));
    }

    public function createIndividualFixtureResult($id, FixtureResultsRequest $request)
    {
        $x = 1;
        $individualFixtureRepo = new IndividualFixtureRepository();
        $fixture = $individualFixtureRepo->findById($id);
        $userService = new UserRepository();
        $player1 = $userService->getById($fixture->player_1_id);
        $player2 = $userService->getById($fixture->player_2_id);
        $tournamentService = new TournamentRepository();
        $tournament = $tournamentService->findById($fixture->tournament_id);
        if (Auth::user()->id == $fixture->player_1_id || Auth::user()->id == $fixture->player_2_id || Auth::user()['user_level_id'] == 1) {
            if ($request->isMethod('post')) {
                $individualFixtureResultRepo = new IndividualFixtureResultRepository();
                $fixtureResultExists = $individualFixtureResultRepo->getFixtureResultsByPlayersAndTournament($player1, $player2, $tournament, $fixture);
                $destinationPath = public_path('/uploads/fixture-results-individual/' . $fixture->id . '/');
                if ($fixtureResultExists == null) {
                    if ($request->rounds !== null) {
                        foreach ($request->rounds as $round) {
                            $extension = $round->getClientOriginalExtension();
                            $fileName = 'round_' . $x . '.' . $extension;
                            $round->move($destinationPath, $fileName);
                            $x++;
                        }
                    }
                    $individualFixtureResultRepo->create($request, $fixture->id, $fixture->tournament_id, $player1->id, $player2->id);
                    $standingsController = new StandingsController();
                    $player1WinsLossesDraws = $standingsController->getUserWinsLossesDrawsForTournament($player1, $tournament);
                    $player2WinsLossesDraws = $standingsController->getUserWinsLossesDrawsForTournament($player2, $tournament);
                    $player1GoalsForAndAgainst = $standingsController->goalsForAndAgainst($player1, $tournament);
                    $player2GoalsForAndAgainst = $standingsController->goalsForAndAgainst($player2, $tournament);
                    $calculatedUserPointsForFixture = $standingsController->getClanPoints($tournament, $request->player_1_score, $request->player_2_score);
                    $this->saveIndividualStanding($player1->id, $tournament->id, $calculatedUserPointsForFixture[0], $player1WinsLossesDraws, $player1GoalsForAndAgainst);
                    $this->saveIndividualStanding($player2->id, $tournament->id, $calculatedUserPointsForFixture[1], $player2WinsLossesDraws, $player2GoalsForAndAgainst);
                    $individualFixtureRepo->resultFixture($fixture->id);
                    return redirect("/individual-fixtures/" . Auth::user()->id);
                } else {
                    return redirect("/individual-fixtures/" . Auth::user()->id)->with('status',Lang::errorStatus('fixture_result_exists'));
                }
            }
            return view('frontend.fixtures.submit-individual-results', compact('player1', 'player2', 'fixture'));
        } else {
            return redirect()->back()->with('status', 'You do not have permissions to do that.');
        }
    }

    /**
     * @param $id
     * @return Factory|View
     */
    public function listFixtureResults($id)
    {
        $fixtureResults = IndividualFixtureResult::where('tournament_id', $id)->get();
        $tournament = Tournament::find($id);
        return view('admin.fixtures.list-individual-fixtures-results', compact('fixtureResults', 'tournament'));
    }

    /**
     * @param $id
     * @return Factory|View
     */
    public function listUserFixtures($id)
    {
        if (Auth::user()->id == $id || Auth::user()->user_level_id == 1) {
            $myDate = Carbon::now()->format('Y/m/d');
            $todaysDate = str_replace('/', '-', $myDate);
            $fixtures = IndividualFixture::where('player_1_id', $id)->orWhere('player_2_id', $id)->get();
            $user = User::find($id);
            return view('admin.fixtures.list-user-fixtures', compact('fixtures', 'user', 'todaysDate'));
        } else {
            return redirect('/')->with('status', 'You do not have permissions to access that.');
        }
    }

    /**
     * @param $id
     * @return Factory|View
     */
    public function listFixtures($id)
    {
        $tournamentService = new TournamentRepository();
        $tournament = $tournamentService->findById($id);
        $individualFixtureRepo = new IndividualFixtureRepository();
        $fixtures = $individualFixtureRepo->getByTournamentId($id);
        return view('admin.fixtures.list-individual-tournament-fixtures', compact('fixtures', 'tournament'));
    }

    /**
     * @param $id
     * @return Factory|View
     */
    public function deleteIndividualFixture($id)
    {
        $individualFixtureRepo = new IndividualFixtureRepository();
        $individualFixtureRepo->delete($id);
        return redirect()->back()->with('status', 'Fixture has been deleted.');
    }

    public function downloadAllScreenshots($id)
    {
        $fixture = IndividualFixture::find($id);
        $player1 = str_slug($fixture->player_1_gamer_tag);
        $player2 = str_slug($fixture->player_2_gamer_tag);
        $tournament = Tournament::find($fixture->tournament_id);
        // Define Dir Folder
        $public_dir = public_path() . "/uploads/fixture-results-individual/$id/";
        $files = $files = File::allFiles(public_path() . "/uploads/fixture-results-individual/$id/");
        // Zip File Name
        $zipFileName = $tournament->tournament_title . '_' . $player1 . 'VS' . $player2 . '.zip';
        // Create ZipArchive Obj
        $zip = new ZipArchive;
        if ($zip->open($public_dir . '/' . $zipFileName, ZipArchive::CREATE) === TRUE) {
            // Add Multiple file
            foreach ($files as $file) {
                $zip->addFile($file->getPathname(), $file->getBasename());
            }
            $zip->close();
        }
        // Set Header
        $headers = array(
            'Content-Type' => 'application/octet-stream',
        );
        $filetopath = $public_dir . '/' . $zipFileName;
        // Create Download Response
        if (file_exists($filetopath)) {
            return response()->download($filetopath, $zipFileName, $headers);
        }
    }

    public function getGamerTag($user)
    {
        $tag = null;
        if ($user->gamer_tag !== "") {
            $tag = $user->gamer_tag;
        } elseif ($user->psn_id !== "") {
            $tag = $user->psn_id;
        } elseif ($user->uplay_id == "") {
            $tag = $user->uplay_id;
        } elseif ($user->steam_id) {
            $tag = $user->steam_id;
        } else {
            $tag = "N'A";
        }
        return $tag;
    }

    public function getGamerTagByTournamentPlatform($tournament, $userId)
    {
        $userService = new UserRepository();
        $user = $userService->getById($userId);
        $tag = null;
        if ($tournament->tournament_platform == 1) {
            $tag = isset($user->gamer_tag) ? $user->gamer_tag : '';
        } elseif ($tournament->tournament_platform == 2) {
            $tag = isset($user->psn_id) ? $user->psn_id : '';
        } elseif ($tournament->tournament_platform == 3) {
            $tag = isset($user->steam_id) ? $user->steam_id : '';
        } elseif ($tournament->tournament_platform == 4) {
            $tag = isset($user->uplay_id) ? $user->uplay_id : '';
        } else {
            $tag = $this->getGamerTag($user);
        }
        return $tag;
    }

    public function saveIndividualScoreByTournament($tournament, $fixtureId, $player1Score, $player2Score)
    {
        $winPoints = $tournament->win_points;
        $lossPoints = $tournament->loss_points;
        $drawPoints = $tournament->draw_points;
        $fixtureResult = IndividualFixtureResult::where('fixture_id', $fixtureId)->first();
        if ($player1Score > $player2Score) {
            $fixtureResult->player_1_points = $winPoints;
            $fixtureResult->player_2_points = $lossPoints;
        } elseif ($player2Score > $player1Score) {
            $fixtureResult->player_1_points = $lossPoints;
            $fixtureResult->player_2_points = $winPoints;
        } elseif ($player2Score == $player1Score) {
            $fixtureResult->player_1_points = $drawPoints;
            $fixtureResult->player_2_points = $drawPoints;
        } else {
            $fixtureResult->player_1_points = $lossPoints;
            $fixtureResult->player_2_points = $lossPoints;
        }
        $fixtureResult->save();
    }

    /**
     * @param $player
     * @param $tournamentId
     * @param $points
     * @param $playerWinsLossesDraws
     * @param $playerGoals
     */
    public function saveIndividualStanding($player, $tournamentId, $points, $playerWinsLossesDraws, $playerGoals)
    {
        $tournamentService = new TournamentRepository();
        $tournament = $tournamentService->findById($tournamentId);
        $individualTournamentStandingService = new IndividualTournamentStandingRepository();
        $playerStandings = $individualTournamentStandingService->getByTournamentPlayerAndId($player, $tournamentId);
        $individualTournamentStandingService->create($playerWinsLossesDraws, $playerStandings, $tournament, $points, $playerGoals, $player);
    }

    public function saveImage($destinationPath, $pathToImage)
    {
        $extension = Input::file('image')->getClientOriginalExtension();// getting image extension
        $fileName = rand(11111, 99999) . '.' . $extension; // renaming image
        Input::file('image')->move($destinationPath, $fileName); // uploading file to given path
        $path = $pathToImage . $fileName;
        return $path;
    }

    public function getUserStandingsByTournament($tournamentId){
        $tournamentService = new TournamentRepository();
        $tournament = $tournamentService->findById($tournamentId);
        $individualTournamentStandingsService = new IndividualTournamentStandingRepository();
        $standings = $individualTournamentStandingsService->getByTournamentId($tournamentId);
        foreach($standings as $standing){
            $standing->gamer_name = $this->getGamerTagByTournamentPlatform($tournament,$standing->user_id);
        }
        if($standings !== null){
            return response()->json(['standings' => $standings, 'tournament' => $tournament],200);
        } else {
            return response()->json(['standings' => null],500);
        }
    }
}
