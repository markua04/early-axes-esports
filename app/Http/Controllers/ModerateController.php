<?php namespace App\Http\Controllers;

use App\Clan;
use App\ClanMembers;
use App\Helpers\LogActivity;
use App\Http\Repositories\ClanMemberRepository;
use App\Http\Repositories\ClanRepository;
use App\Http\Repositories\IndividualFixtureRepository;
use App\Http\Repositories\TournamentRegisteredPlayersRepository;
use App\Http\Repositories\UserRepository;
use App\Mail\VerifyMail;
use App\User;
use App\UserLevel;
use App\UserStatuses;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ModerateController extends Controller
{

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function listUsers() {
        $userRepo = new UserRepository();
        if(isset(Auth::user()->user_level_id) && Auth::user()->user_level_id == 1){
            $users = $userRepo->getAllPaginated(15);
            return view('admin/moderation/manage-users', compact('users'));
        } elseif(Auth::check()) {
            $users = $userRepo->getAllNormalUsersPaginated(15);
            return view('frontend/members/list-users', compact('users'));
        } else {
            return redirect()->back()->with('status','You do not have permission to view that page.');
        }
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function searchUsers() {
        if(isset(Auth::user()->user_level_id) && Auth::user()->user_level_id == 1){
            $q = Input::get ( 'q' );
            if($q == null){
                return $this->listUsers();
            }
            if($q != ""){
                $user = User::where ( 'name', 'LIKE', '%' . $q . '%' )->orWhere( 'gamer_tag', 'LIKE', '%' . $q . '%' )->orWhere('psn_id', 'LIKE', '%' . $q . '%')->orWhere('uplay_id', 'LIKE', '%' . $q . '%')->orWhere('steam_id', 'LIKE', '%' . $q . '%')->paginate (10)->setPath ( '' );
                $users = $user->appends ( array (
                    'q' => Input::get ( 'q' )
                ) );
                if (count ( $user ) > 0)
                    return view ( 'admin/moderation/manage-users',compact('users'))->withDetails($user)->withQuery($q);
            }
            return view('admin/moderation/manage-users', compact('users'))->with('status','No Details found. Try to search again !');
        } elseif(Auth::check()) {
            $q = Input::get ( 'q' );
            if($q == null){
                return $this->listUsers();
            }
            if($q != ""){
                $user = User::where ( 'name', 'LIKE', '%' . $q . '%' )->orWhere( 'gamer_tag', 'LIKE', '%' . $q . '%' )->orWhere('psn_id', 'LIKE', '%' . $q . '%')->orWhere('uplay_id', 'LIKE', '%' . $q . '%')->orWhere('steam_id', 'LIKE', '%' . $q . '%')->paginate (10)->setPath ( '' );
                $users = $user->appends ( array (
                    'q' => Input::get ( 'q' )
                ) );
                if (count ( $user ) > 0)
                    return view ( 'frontend/members/list-users',compact('users'))->withDetails($user)->withQuery($q);
            }
            return view('admin/moderation/manage-users', compact('users'))->with('status','No Details found. Try to search again !');
        }

    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function frontendListUsers()
    {
        $userRepo = new UserRepository();
        $users = $userRepo->getByUserLevelIdPaginated(3,15);
        return view('frontend.members.list-users', compact('users'));
    }


    /**
     * @param $id
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function manageUser($id = null, Request $request)
    {
        $clans = Clan::all();
        $userLevels = UserLevel::all();
        $userEntity = User::find($id);
        $userStatuses = UserStatuses::all();
        $userRepo = new UserRepository();
        if($request->isMethod('post')) {
            $user = $userRepo->getById($id);
            $phoneNumber = $user->phone_number;
            $clan_id = $user->clan_id;
            $destinationPath = 'uploads'; // upload path
            if (Input::file('image') !== null) {
                $extension = Input::file('image')->getClientOriginalExtension();// getting image extension
                $fileName = rand(11111, 99999) . '.' . $extension; // renaming image
                Input::file('image')->move($destinationPath, $fileName); // uploading file to given path
                $data['gamer_pic'] = "/uploads/" . $fileName;
            }
            $user->name = $request->get('name');
            $user->surname = $request->get('surname');
            $user->email = $request->get('email');
            $user->description = $userEntity->description;
            $user->gamer_tag = $request->get('gamer_tag');
            $user->psn_id = $request->get('psn_id');
            $user->uplay_id = $request->get('uplay_id');
            $user->steam_id = $request->get('steam_id');

            if($request->get('clan_id') !== null){
                $clanMembers = ClanMembers::firstOrNew(array('user_id' => $user->id));
                if($request->get('clan_id') == null){
                    $clanMembers->clan_id = $clan_id;
                } else {
                    $clanMembers->clan_id = $request->get('clan_id');
                }
                $clanMembers->user_id = $user->id;
                $clanMembers->approval_id = 0;
                $clanMembers->save();
            }
            if($request->get('clan_id') == null){
                $user->clan_id = $clan_id;
            } else {
                $user->clan_id = $request->get('clan_id');
            }
            if($request->get('phone_number') == null){
                $user->phone_number = $phoneNumber;
            } else {
                $user->phone_number = $request->get('phone_number');
            }
            $user->user_level_id = $request->get('user_level');
            $user->gamer_pic = $request->get('gamer_pic');
            $user->password = $userEntity->password;
            $user->user_clan_status = $userEntity->user_clan_status;
            if($request->get('user_status_id') == null){
                $user->user_status_id = $userEntity->user_status_id;
            } else {
                $user->user_status_id = $request->get('user_status_id');
            }

            $user->save();
            LogActivity::addToLog('Managed User - '.$user['name'] . ' ' .$user['surname']);
            return redirect('/manage-users');
        } else {
            $user = User::find($id);
            return view('admin/moderation/edit-user', compact('user', 'userLevels','clans','userStatuses'));
        }
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function listClans()
    {
        $clanArray = [];
        $clans = Clan::where('clan_status', 0)->get();
        foreach($clans as $clan){
            $clanArray[$clan->id] = $clan->clan_name;
        }
        return view('admin.moderate.moderate-clans', ['clans' => $clanArray]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function listUnapprovedClanMembers($clanId)
    {
        $clanRepo = new ClanRepository();
        $clanMembersRepo = new ClanMemberRepository();
        $clan = $clanRepo->getById($clanId);
        $x = 0;
        $clanMembersArray = [];
        if($clanId == 0){
            return redirect('/')->with('status','You do not have permissions to access that!');
        } else {
            if(Auth::user()->id == $clan->clan_owner_id){
                $clanMembersNeedingModeration = $this->moderateClanUserRegistrations($clanId);
                foreach($clanMembersNeedingModeration as $clanMember){
                    $clanMembersArray[$x]['id'] = $clanMember->user_id;
                    $clanMembersArray[$x]['name'] = $clanMember->user->name;
                    $clanMembersArray[$x]['surname'] = $clanMember->user->surname;
                    $clanMembersArray[$x]['gamer_tag'] = getGamerTag($clanMember->user_id);
                    $x++;
                }
                $clanMembers = $clanMembersRepo->getByClanId($clanId);
                return view('frontend.clans.list-unapproved-members', compact('clanMembersNeedingModeration','clanMembers','clanMembersArray'));
            } else {
                return redirect('/')->with('status','You do not have permissions to access that!');
            }
        }

    }

    /**
     * @param $clanId
     * @return mixed
     */
    public function moderateClanUserRegistrations($clanId){
        $clanService =  new ClanRepository();
        $clanMembersRepo = new ClanMemberRepository();
        $clan = $clanService->getById($clanId);
        if(Auth::user()->id == $clan->clan_owner_id) {
            $clanMembers = $clanMembersRepo->getUnApprovedClanMembersByClanIdPaginated($clanId,15);
            return $clanMembers;
        } else {
            return redirect('/')->with('status','You do not have permissions to access that!');
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateClanStatus(Request $request){
       try {
           $clanRepo = new ClanRepository();
           $clan = $clanRepo->updateClanStatus($request);
           return response()->json($clan);
       } catch (\Exception $e) {
           return response()->json($e->getMessage(), '400');
       }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateUserClanStatus(Request $request){
        $clanMembersRepo = new ClanMemberRepository();
        $message = "An error has occured";
        try {
            $message = "An error has occured";
            if($request->selected == 1){
                $message = "User approved!";
                $clanMembersRepo->approveClanMember($request);
                return response()->json(['message' => $message],200);
            } else {
                $message = "User not approved!";
                $clanMembersRepo->dontApproveClanMemberAndRemoveApplication($request);
                return response()->json(['message' => $message],200);
            }
        } catch (\Exception $e) {
            return response()->json(['message' => $message],500);
        }
    }

    /**
     * @param $userId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function deleteUser($userId) {
        if(Auth::user()->user_level_id == 1){
            $individualFixtureRepo = new IndividualFixtureRepository();
            $clanMemberRepo = new ClanMemberRepository();
            $tournamentRegisteredPlayersRepo = new TournamentRegisteredPlayersRepository();
            $userRepo = new UserRepository();
            $clanMember = $clanMemberRepo->getByUserId($userId);
            if(isset($clanMember) && $clanMember !== null){
                $clanMember->delete();
            }
            $tournamentRegisteredPlayers = $tournamentRegisteredPlayersRepo->getByClanMemberId($userId);
            foreach($tournamentRegisteredPlayers as $registration){
                $registration->delete();
            }
            $userFixtures = $individualFixtureRepo->getByUserId($userId);
            foreach($userFixtures as $fixture){
                $fixture->delete();
            }
            $user = $userRepo->getById($userId);
            $user->delete();
            return redirect('/manage-users')->with('status', 'User has been removed!');
        }
    }

    public function resendRegistrationEmail(Request $request){
        if(Auth::user()->user_level_id == 1){
            if($request->isMethod('post')){
                $user = User::where('email', $request->email)->first();
                if($user !== null){
                    Mail::to(strtolower($user->email))->send(new VerifyMail($user));
                    return redirect()->back()->with('status','The verification email has been resent!');
                } else {
                    return redirect()->back()->with('status','The email was not found. Please tell the user to re-register');
                }
            }
            return view('admin.registration.registration-email-resend');
        } else {
            return redirect()->back()->with('status','You do not have permissions to do that!');
        }
    }

    public function userGamerTagApprovals(Request $request){
        $userRepo = new UserRepository();
        if($request->isMethod('post')){
            $userRepo->updateUserGtApproval($request->user);
        }
        $users = $userRepo->getUsersByGamerTagApprovals();
        $usersArray = [];
        $x = 0;
        foreach ($users as $user) {
            $usersArray[$x]['id'] = $user->id;
            $usersArray[$x]['name'] = $user->name;
            $usersArray[$x]['surname'] = $user->surname;
            $usersArray[$x]['gamer_tag'] = $user->gamer_tag;
            $usersArray[$x]['psn_id'] = $user->psn_id;
            $usersArray[$x]['steam_id'] = $user->steam_id;
            $usersArray[$x]['uplay_id'] = $user->uplay_id;
            $usersArray[$x]['gt_change'] = $user->gt_change;
            $x++;
        }
        return view('admin.moderate.moderate-users',compact('usersArray'));
    }
}
