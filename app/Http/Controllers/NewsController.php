<?php namespace App\Http\Controllers;

use App\Http\Requests\NewsRequest;
use App\News;
use App\NewsCategories;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class NewsController extends Controller
{
    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * @param NewsRequest $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function create(NewsRequest $request)
    {
        $categories = NewsCategories::all();
        if($request->isMethod('post')){
            $userId = Auth::user()->getAttributes()['id'];
            $news = new News();
            $destinationPath = 'uploads'; // upload path
            if(Input::file('image') !== null){
                $extension = Input::file('image')->getClientOriginalExtension();// getting image extension
                $fileName = rand(11111,99999).'.'.$extension; // renaming image
                Input::file('image')->move($destinationPath, $fileName); // uploading file to given path
                $news->image = "/uploads/".$fileName;
            }
            $news->title = $request->get('title');
            $news->content = $request->get('content');
            $news->status = $request->get('status');
            $news->type = $request->get('type');
            $news->creator_id = $userId;
            if($request->get('article_status') == '' || $request->get('article_status') == null){
                $news->status = 0;
            }
            $news->save();
            return redirect('/view-clans')->with('status','System news has been created.');
        } else {
            return view('admin.news.create-news',compact('categories'));
        }
    }

    /**
     * @param Request $request
     * @return $this
     */
    public function edit(NewsRequest $request,$id)
    {
        $categories = NewsCategories::all();
        $news = News::find($id);
        if($request->isMethod('post')){
            $userId = Auth::user()->getAttributes()['id'];
            $newsType = $news->type;
            $newsBanner = $news->image;
            $destinationPath = 'uploads'; // upload path
            if(Input::file('image') !== null){
                $extension = Input::file('image')->getClientOriginalExtension();// getting image extension
                $fileName = rand(11111,99999).'.'.$extension; // renaming image
                Input::file('image')->move($destinationPath, $fileName); // uploading file to given path
                $news->image = "/uploads/".$fileName;
            } else {
                $news->image = $newsBanner;
            }
            if($news->type == null){
                $news->type = $newsType;
            } else {
                $news->type = $request->get('type');
            }
            $news->title = $request->get('title');
            $news->content = $request->get('content');
            $news->status = $request->get('status');
            $news->creator_id = $userId;
            if($request->get('article_status') == '' || $request->get('article_status') == null){
                $news->status = 0;
            }
            $news->save();
            return redirect('/admin/list-news')->with('status','System news has been updated.');
        } else {
            return view('admin.news.edit-news',compact('news','categories'));
        }
    }


    public function listNews()
    {
        $newsArticles = News::all();
        return view('/admin/news/list-news', compact('newsArticles'));
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id)
    {
        if (Auth::user()->getAttributes()['user_level_id'] == 1) {
            News::destroy($id);
            return redirect()->back()->with('status', 'News Deleted');
        } else {
            return redirect()->back()->with('status', 'Your permissions do not allow this.');
        }
    }

}
