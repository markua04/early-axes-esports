<?php

namespace App\Http\Controllers;

use App\ClanTournamentStanding;
use App\Http\Repositories\ClanRepository;
use App\Http\Repositories\GamesListRepository;
use App\Http\Repositories\PlatformRepository;
use Illuminate\Http\Request;

class OverallClanStandingsController extends Controller
{
    public function getOverallClanStandingsByGameAndPlatform(Request $request){
        $clanStandings = ClanTournamentStanding::where('game_id',$request->get('game_id'))->where('platform',$request->get('platform_id'))->get();
        $gamesListRepo = new GamesListRepository();
        $game = $gamesListRepo->getById($request->get('game_id'));
        $clans = [];
        $x = 0;
        $ordered = [];
        foreach($clanStandings as $clanStanding){
            $clanRepo = new ClanRepository();
            $clan = $clanRepo->getById($clanStanding->clan_id);
            $clans[$x]['clan_name'] = isset($clan->clan_name) ? $clan->clan_name : '';
            $clans[$x]['wins'] = $clanStanding->wins;
            $clans[$x]['losses'] = $clanStanding->losses;
            $clans[$x]['draws'] = $clanStanding->draws;
            $clans[$x]['played'] = $clanStanding->wins +  $clanStanding->draws + $clanStanding->losses;
            $clans[$x]['win_loss_ratio'] = ($clanStanding->wins !== 0 && $clans[$x]['played'] !== null) ? $clanStanding->wins / $clans[$x]['played'] : 0;
            $clans[$x]['clan_logo'] = isset($clan->clan_logo) ? $clan->clan_logo : '';
            $x++;
        }
        usort($clans, function($a, $b) {
            $retval = $b['win_loss_ratio'] <=> $a['win_loss_ratio'];
            return $retval;
        });
        return view('frontend.standings.overallStandings',compact('clans','game'));
    }

    public function getOverallTournamentGames(){
        $gamesListRepo = new GamesListRepository;
        $games = $gamesListRepo->getById(6);
        $platformRepo = new PlatformRepository();
        $allPlatforms = $platformRepo->getAll();
        $x = 0;
        foreach($allPlatforms as $platform){
            if($platform->platform == "Xbox"){
                $platforms[0]['id'] = $platform->id;
                $platforms[0]['name'] = $platform->platform;
            } elseif($platform->platform == "Uplay"){
                $platforms[1]['id'] = $platform->id;
                $platforms[1]['name'] = $platform->platform;
            }
            $x++;
        }
        return view('frontend.standings.overallStandingsGamesList',compact('games','platforms'));
    }
}
