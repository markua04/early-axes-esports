<?php namespace App\Http\Controllers;

use App\Http\Repositories\UserRepository;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Request;

class PlatformController extends Controller
{
    public function getXboxUserProfile($user) {
        $gamerTagCheck = gamerTagChecker($user->gamer_tag);
        $code = isset($gamerTagCheck->code);
        if ($code == false) {
            $xboxRecentActivity = $this->getXboxRecentActivity($user);
            $profileData = $this->getXboxProfile($user);
            $data = [
                'profileData' => $profileData,
                'xboxRecentActivity' => $xboxRecentActivity
            ];
            return $data;
        }
        return false;
    }

    public function getSteamUserProfile($user) {
        return false;
    }

    public function getPSUserProfile($user) {
        return false;
    }

    public function getXboxRecentActivity($user) {
        $xboxProfile = $this->getXboxProfile($user);
        $code = isset($xboxProfile->error_code);
        $codeSecond = isset($xboxProfile->code);
        if ($code == false && $xboxProfile !== null && $codeSecond == false) {
            $xboxData = new ApiController();
            $url = "https://xboxapi.com/v2/$xboxProfile->id/activity/recent";
            $xboxRecentActivityData = $xboxData->getXboxActivityFeed($url);
            Cache::put('xboxActivity-' . $xboxProfile->Gamertag, $xboxRecentActivityData, 2900);
            $xboxProfileData = Cache::get('xboxActivity-' . $xboxProfile->Gamertag);
            return json_decode($xboxProfileData);
        } else {
            return false;
        }
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getXboxProfile($id)
    {
        $userRepo = new UserRepository();
        if(isset($id->gamer_tag)){
            $gamerTag = $id->gamer_tag;
        } else {
            $gamerTag = $userRepo->getUserById($id);
        }
        if (Cache::has('xboxprofile-' . $gamerTag)) {
            $xboxProfileData = Cache::get('xboxprofile-' . $gamerTag);
        } else {
            $xboxData = new ApiController();
            $xboxProfileData = $xboxData->getXboxLiveAccount($gamerTag);
            Cache::put('xboxprofile-' . $gamerTag, $xboxProfileData, 1440);
            $xboxProfileData = Cache::get('xboxprofile-' . $gamerTag);
        }
        return json_decode($xboxProfileData);
    }
}
