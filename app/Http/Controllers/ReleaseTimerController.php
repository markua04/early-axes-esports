<?php namespace App\Http\Controllers;

use App\GameRelease;
use App\GamesList;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class ReleaseTimerController extends Controller
{
    public function create(Request $request){
        $gamesList = GamesList::all();
        if($request->isMethod('post')){
            $gameRelease = new GameRelease( );
            $gameRelease->start_countdown_date = $request->start_countdown_date;
            $gameRelease->start_countdown_time = $request->start_countdown_time;
            $gameRelease->end_countdown_date = $request->end_countdown_date;
            $gameRelease->end_countdown_time = $request->end_countdown_time;
            if(Input::file('image') !== null) {
                $imageDestination = $this->imageUpload('uploads/gamesRelease', Input::file('image'));
            }
            $gameRelease->image = $imageDestination;
            $gameRelease->game = $request->game;
            $gameRelease->save();
        }
        return view('admin.game-releases.createorupdate', compact('gamesList'));
    }

    public function imageUpload($destinationPath, $image){
        $extension = $image->getClientOriginalExtension();
        $fileName = rand(11111,99999).'.'.$extension;
        $image->move($destinationPath, $fileName);
        $destination = $destinationPath."/".$fileName;
        return $destination;
    }
}
