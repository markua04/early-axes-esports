<?php namespace App\Http\Controllers;
use App\Clan;
use App\Http\Repositories\ClanRepository;
use App\Http\Repositories\TournamentRepository;
use App\Tournament;
use App\TournamentSiegeIndividualStats;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class SearchController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function searchClans() {
        $clanRepo = new ClanRepository();
        $clanController = new ClanController();
        if(isset(Auth::user()->user_level_id) && Auth::user()->user_level_id == 1){
            $q = Input::get ( 'q' );
            if($q == null){
                $clans = $clanRepo->getAllByActiveClanStatus(10);
                return view('/admin/manage-clans/list-clans', compact('clans'));
            }
            if($q != ""){
                $clan = $clanRepo->getByLikeClanName($q);
                $clan = Clan::where ( 'clan_name', 'LIKE', '%' . $q . '%' )->paginate (10)->setPath ( '' );
                $clans = $clan->appends ( array (
                    'q' => Input::get ( 'q' )
                ) );
                if (count ( $clan ) > 0){
                    return view ( 'admin/manage-clans/list-clans',compact('clans'))->withDetails($clan)->withQuery($q);
                }
                $check = 1;
            }
            return view('/admin/manage-clans/list-clans', compact('clans','check'))->with('status','No Clans found with that name. Try to search again !');
        } else {
            $q = Input::get ( 'q' );
            if($q == null){
                $clans = Clan::where('clan_status', 1)->paginate(10);
                return view('/admin/manage-clans/list-clans', compact('clans'));
            }
            if($q != ""){
                $clan = $clanRepo->getByLikeClanName($q);
                $clans = $clan->appends ( array (
                    'q' => Input::get ( 'q' )
                ) );
                if (count ( $clan ) > 0){
                    return view('/admin/manage-clans/list-clans', compact('clans'))->withDetails($clan)->withQuery($q);
                }
                $check = 1;
            }
            return view('/admin/manage-clans/list-clans', compact('clans','check'))->with('status','No Clans found with that name. Try to search again !');
        }

    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function searchIndividualStats($id) {
            $tournamentRepo = new TournamentRepository();
            $tournament = $tournamentRepo->getById($id);
            $dateAdded = TournamentSiegeIndividualStats::where('tournament_id',$id)->first();
            $topKD = TournamentSiegeIndividualStats::where('tournament_id',$id)->orderBy('kill_death_ratio','DESC')->first();
            $topKills = TournamentSiegeIndividualStats::where('tournament_id',$id)->orderBy('kills','DESC')->first();
            $topScore = TournamentSiegeIndividualStats::where('tournament_id',$id)->orderBy('scores','DESC')->first();
            $topPlayer = TournamentSiegeIndividualStats::where('tournament_id',$id)->orderBy('scores','DESC')->orderBy('kill_death_ratio')->first();
            $q = Input::get ( 'q' );
            if($q == ''){
                $stats = TournamentSiegeIndividualStats::where('tournament_id',$tournament->id)->get();
                return view('/frontend/tournaments/list-individual-stats', compact('stats','tournament','dateAdded','topKD','topKills','topScore','topPlayer'));
            }
            if($q != ""){
                $stats = TournamentSiegeIndividualStats::where ( 'gamer_tag', 'LIKE', '%' . $q . '%' )->orWhere( 'clan_name', 'LIKE', '%' . $q . '%' )->paginate (10)->setPath ( '' );
                $stats = $stats->appends ( array (
                    'q' => Input::get ( 'q' )
                ) );
                if (count ( $stats ) > 0){
                    return view('/frontend/tournaments/list-individual-stats', compact('stats','tournament','dateAdded','topKD','topKills','topScore','topPlayer'))->withDetails($stats)->withQuery($q);
                }
                $check = 1;
            }
            return view('/frontend/tournaments/list-individual-stats', compact('stats','check','tournament','dateAdded','topKD','topKills','topScore','topPlayer'))->with('status','No User found with that name. Try to search again!');

    }
}
