<?php namespace App\Http\Controllers;

use App\Clan;
use App\ClanTournamentStanding;
use App\FixtureResult;
use App\Http\Repositories\ClanRepository;
use App\Http\Repositories\FixtureRepository;
use App\Http\Repositories\ClanTournamentStandingRepository;
use App\Http\Repositories\FixtureResultRepository;
use App\Http\Repositories\IndividualFixtureRepository;
use App\Http\Repositories\IndividualTournamentStandingRepository;
use App\Http\Repositories\TournamentRepository;
use App\IndividualFixtureResult;
use App\IndividualTournamentStanding;
use App\Mail\WeeklyFixtureResultsUpdated;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Exception;

class StandingsController extends Controller
{
    const CLAN_TOURNAMENT = 2;
    const FIFA = 15;

    public function getCurrentClanFixtureResultsByClan($clan){
        $currentClanStandings = null;
        $currentClanStandings = FixtureResult::where('clan_1_id',$clan->id)->orWhere('clan_2_id',$clan->id)->get();
        return $currentClanStandings;
    }

    public function getClanPointsForTournament($clan,$tournament){
        $results = $this->getCurrentClanFixtureResultsByClan($clan);
        $clanPoints = null;
        foreach($results as $result){
            if($result->clan_1_id == $clan){
                if($result->tournament_id == $tournament->id){
                    $clanPoints += $result->clan_1_points;
                }
            } else {
                if($result->tournament_id == $tournament->id) {
                    $clanPoints += $result->clan_2_points;
                }
            }
        }
        return $clanPoints;
    }

    public function getClanStandingsByTournament($tournamentId){
       $standings = ClanTournamentStanding::where('tournament_id',$tournamentId)->orderBy('points','desc')->get();
       return $standings;
    }
    public function getUserStandingsByTournament($tournamentId){
        $tournamentService = new TournamentRepository();
        $tournament = $tournamentService->findById($tournamentId);
        if($tournament->tournament_game == self::FIFA){
            return IndividualTournamentStanding::where('tournament_id',$tournamentId)->orderBy('points','desc')->orderBy('difference','desc')->get();
        } else {
            return IndividualTournamentStanding::where('tournament_id',$tournamentId)->orderBy('points','desc')->get();
        }
    }

    public function getClanWinsLossesDrawsForTournament($clan,$tournament){
        $fixtureResultRepo = new FixtureResultRepository();
        $results = $fixtureResultRepo->getByClanAndTournamentId($clan->id,$tournament->id);
        $clanWins = 0;
        $clanLosses = 0;
        $clanDraws = 0;
        foreach($results as $result){
            if($result->clan_1_id == $clan->id){
                if($result->clan_1_score > $result->clan_2_score){
                    $clanWins++;
                } elseif ($result->clan_1_score < $result->clan_2_score){
                    $clanLosses++;
                } elseif ($result->clan_1_score !== 0 && $result->clan_2_score !== 0 && $result->clan_1_score == $result->clan_2_score){
                    $clanDraws++;
                } else {
                    $clanLosses++;
                }
            } else {
                if($result->clan_2_score > $result->clan_1_score){
                    $clanWins++;
                } elseif ($result->clan_2_score < $result->clan_1_score){
                    $clanLosses++;
                } elseif ($result->clan_2_score !== 0 && $result->clan_1_score !== 0 && $result->clan_2_score == $result->clan_1_score){
                    $clanDraws++;
                } else {
                    $clanLosses++;
                }
            }
        }
        $clanOutcome = ['wins' => $clanWins, 'losses' => $clanLosses, 'draws' => $clanDraws];
        return $clanOutcome;
    }

    public function clansCurrentPoints($clan,$tournament){
        $clanTournamentCurrentPointsForTournament = ClanTournamentStanding::where('clan_id',$clan->id)->where('tournament_id',$tournament->id)->first();
        return $clanTournamentCurrentPointsForTournament;
    }

    public function getClanPoints($tournament,$clan1Score,$clan2Score){
        $winPoints = $tournament->win_points;
        $lossPoints = $tournament->loss_points;
        $drawPoints = $tournament->draw_points;
        if($clan1Score > $clan2Score){
            $clan_1_points = $winPoints;
            $clan_2_points = $lossPoints;
        } elseif ($clan2Score > $clan1Score){
            $clan_1_points = $lossPoints;
            $clan_2_points = $winPoints;
        } elseif($clan2Score == $clan1Score){
            $clan_1_points = $drawPoints;
            $clan_2_points = $drawPoints;
        } else {
            $clan_1_points = $lossPoints;
            $clan_2_points = $lossPoints;
        }
        return [$clan_1_points,$clan_2_points];
    }

    public function editStandings($tournamentId,Request $request){
        $x = 1;
        $autoGeneratedStandings = ClanTournamentStanding::where('tournament_id',$tournamentId)->orderBy('points','desc')->get();
        if($autoGeneratedStandings->isEmpty()){
            return redirect()->back()->with('status','No standings exist for this tournament yet.');
        }
        $service = new TournamentRepository();
        $tournament = $service->findById($tournamentId);
        $standings = [];
        if($request->isMethod('post')){
            foreach($request->standings as $key => $value){
                $clanTournamentStandingService = new ClanTournamentStandingRepository();
                $clanTournamentStandingService->editStandings($key,$value);
            }
            return redirect()->back()->with('status','Standings successfully updated!');
        }
        return view('admin.tournaments.edit-auto-clan-standings', compact('autoGeneratedStandings','tournament','x','standings'));
    }

    public function editIndividualStandings($tournamentId,Request $request){
        $x = 1;
        $service = new TournamentRepository();
        $tournament = $service->findById($tournamentId);
        $autoGeneratedStandings = IndividualTournamentStanding::where('tournament_id',$tournamentId)->orderBy('points','desc')->orderBy('difference','desc')->get();
        $individualFixturesController = new IndividualFixturesController();
        foreach ($autoGeneratedStandings as $standing){
            $standing->gamer_name = $individualFixturesController->getGamerTagByTournamentPlatform($tournament,$standing->user_id);
        }
        if($autoGeneratedStandings->isEmpty()){
            return redirect()->back()->with('status','No standings exist for this tournament yet.');
        }
        $standings = [];
        if($request->isMethod('post')){
            foreach($request->standings as $key => $value){
                $clanTournamentStandingService = new IndividualTournamentStandingRepository();
                $clanTournamentStandingService->edit($key,$value);
            }
            return redirect()->back()->with('status','Standings successfully updated!');
        }
        return view('admin.tournaments.edit-auto-user-standings', compact('autoGeneratedStandings','tournament','x','standings'));
    }

    public function getUserWinsLossesDrawsForTournament($user,$tournament){
        $results = $this->getCurrentUserFixtureResultsBy($user);
        $userWins = 0;
        $userLosses = 0;
        $userDraws = 0;
        foreach($results as $result){
            if($result->player_1 == $user->id){
                if($result->tournament_id == $tournament->id){
                    if($result->player_1_score > $result->player_2_score){
                        $userWins++;
                    } elseif ($result->player_1_score < $result->player_2_score){
                        $userLosses++;
                    } elseif ($result->player_1_score == $result->player_2_score){
                        $userDraws++;
                    }
                }
            } else {
                if($result->tournament_id == $tournament->id) {
                    if($result->player_2_score > $result->player_1_score){
                        $userWins++;
                    } elseif ($result->player_2_score < $result->player_1_score){
                        $userLosses++;
                    } elseif ($result->player_2_score == $result->player_1_score){
                        $userDraws++;
                    }
                }
            }
        }
        $userOutcome = ['wins' => $userWins, 'losses' => $userLosses, 'draws' => $userDraws];
        return $userOutcome;
    }

    public function getCurrentUserFixtureResultsBy($user){
        $currentUserStandings = null;
        $individualFixturesRepo = new IndividualFixtureRepository();
        $currentUserStandings = $individualFixturesRepo->getByUserId($user->id);
        return $currentUserStandings;
    }

    public function goalsForAndAgainst($player,$tournament){
        $goalsFor = 0;
        $goalsAgainst = 0;
        $individualFixtureService = new IndividualFixtureRepository();
        $individualFixtureResults = $individualFixtureService->getByTournamentAndUser($tournament,$player);
        foreach($individualFixtureResults as $result){
            if($result->tournament_id == $tournament->id){
                if($result->player_1 == $player->id){
                    $goalsFor+= $result->player_1_score;
                    $goalsAgainst+= $result->player_2_score;
                } elseif ($result->player_2 == $player->id){
                    $goalsFor+= $result->player_2_score;
                    $goalsAgainst+= $result->player_1_score;
                }
            }
        }
        return ['goalsFor' => $goalsFor, 'goalsAgainst' => $goalsAgainst];
    }

    public function forfeitFixtures(){
        $fixturesRepo = new FixtureRepository();
        $fixturesOlderThanSevenDays = $fixturesRepo->getFixturesOlderThanSevenDays();
        $clanRepo = new ClanRepository();
        $clansMatchesForfeited = [];
        $x = 1;
        foreach($fixturesOlderThanSevenDays as $fixture){
            if(isset($fixture->clan_1_id) && $fixture->clan_1_id !== null){
                $clan1 = $clanRepo->getById($fixture->clan_1_id);
            } else {
                $clan1 = $clanRepo->getByName($fixture->clan_1);
            }
            if(isset($fixture->clan_2_id) && $fixture->clan_2_id !== null){
                $clan2 = $clanRepo->getById($fixture->clan_2_id);
            } else {
                $clan2 = $clanRepo->getByName($fixture->clan_2);
            }
            $fixtureResultRepo = new FixtureResultRepository();
            $getResult = $fixtureResultRepo->getFixtureResult($fixture->id);
            if($getResult == null){
                $tournamentRepo = new TournamentRepository();
                $tournament = $tournamentRepo->getById($fixture->tournament_id);
                $fixtureResultRepo = new FixtureResultRepository();
                $fixtureResultRepo->forfeitBothClans($fixture->id, $clan1, $clan2);
                if(isset($clan1->clan_name) && isset($clan2->clan_name) && isset($fixture->date)){
                    if(isset($tournament->tournament_title)){
                        $string = "Tournament : ".$tournament->tournament_title." - ".$clan1->clan_name . " VS " . $clan2->clan_name. " Date: " .$fixture->date;
                    } else {
                        $string =  $clan1->clan_name . " VS " . $clan2->clan_name. " Date: " .$fixture->date;
                    }
                    $clansMatchesForfeited[] = $string;
                }
                if($fixture->result !== 1){
                    $fixture->result = 1;
                    $fixture->save();
                }
            } else {
                if($fixture->result !== 1){
                    $fixture->result = 1;
                    $fixture->save();
                }
            }
            $x++;
            sleep(5);
        }
        try {
            Mail::to(env('ADMIN_EMAIL'))->send(new WeeklyFixtureResultsUpdated($clansMatchesForfeited));
        } catch(Exception $e){
            Log::error($e);
            Log::info($e);
        }

    }
}
