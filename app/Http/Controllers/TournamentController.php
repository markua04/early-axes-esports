<?php namespace App\Http\Controllers;

use App\Clan;
use App\ClanTournamentStanding;
use App\Fixture;
use App\FixtureResult;
use App\GamesList;
use App\Genre;
use App\Helpers\LogActivity;
use App\Http\Repositories\BracketRepository;
use App\Http\Repositories\ClanMemberRepository;
use App\Http\Repositories\ClanRepository;
use App\Http\Repositories\TournamentRegisteredPlayersRepository;
use App\Http\Requests\TournamentRequest;
use App\Http\Requests\TournamentRulesRequest;
use App\Http\Requests\TournamentStandingsRequest;
use App\Http\Requests\TournamentStreamsRequest;
use App\Http\Repositories\ClanTournamentStandingRepository;
use App\Http\Repositories\FixtureRepository;
use App\Http\Repositories\TournamentIndividualRegisteredPlayersRepository;
use App\Http\Repositories\TournamentRegistrationsRepository;
use App\Http\Repositories\UserRepository;
use App\IndividualFixture;
use App\Jobs\ProcessRoundRobinFixtures;
use App\Platform;
use App\Standing;
use App\Tournament;
use App\TournamentFpsStandings;
use App\TournamentIndividualRegisteredPlayers;
use App\TournamentRegisteredPlayers;
use App\TournamentRegistrations;
use App\TournamentRules;
use App\TournamentSiegeIndividualStats;
use App\TournamentStream;
use App\TournamentTypes;
use App\TournamentStandings;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Intervention\Image\Facades\Image;
use App\Http\Repositories\TournamentRepository;
use App\Http\Repositories\GamesListRepository;

class TournamentController extends Controller
{

    const TOURNAMENT_IMAGES = 'uploads/tournaments';

    /**
     * @param TournamentRequest $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function create(TournamentRequest $request)
    {
        $gamesListRepo = new GamesListRepository();
        $games = $gamesListRepo->getAll();
        $platforms = Platform::all();
        $tournamentTypes = TournamentTypes::all();
        $genres = Genre::all();
        if ($request->isMethod('post')) {
            $request->validate([
                'tournament_title' => 'required',
                'tournament_prize' => 'required',
                'tournament_info' => 'required',
            ]);
            $tournamentService = new TournamentRepository();
            if (Input::file('image') !== null) {
                $extension = Input::file('image')->getClientOriginalExtension();
                $fileName = rand(11111, 99999) . '.' . $extension;
                Input::file('image')->move(self::TOURNAMENT_IMAGES, $fileName);
                $request->tournament_banner = "/uploads/tournaments/" . $fileName;
            }
            $saveTournament = $tournamentService->create($request);
            if ($saveTournament == true) {
                return redirect('/admin/list-tournaments')->with('status', 'Tournament has been created successfully');
            } else {
                return redirect()->back()->with('status', 'Error! Tournament not created!');
            }
        }
        return view('admin/tournaments/create-tournament', compact('games', 'tournamentTypes', 'platforms', 'genres'));
    }

    /**
     * @param $id
     * @param TournamentRequest $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function editTournament($id, TournamentRequest $request)
    {
        $tournamentRepo = new TournamentRepository();
        $tournament = $tournamentRepo->getById($id);
        $platforms = Platform::all();
        $gameListService = new GamesListRepository();
        $games = $gameListService->getAll();
        $tournamentTypes = TournamentTypes::all();
        $genres = Genre::all();
        if (isset($tournament->tournament_type_id) && $tournament->tournament_type_id == 2) {
            $tournamentRegistrationsRepo = new TournamentRegistrationsRepository();
            $registeredPlayers = $tournamentRegistrationsRepo->getByTournamentId($id);
        } elseif (isset($tournament->tournament_type_id) && $tournament->tournament_type_id == 3) {
            $tournamentRegisteredPlayersRepo = new TournamentIndividualRegisteredPlayersRepository();
            $registeredPlayers = $tournamentRegisteredPlayersRepo->getByTournamentId($id);
        }
        if ($request->isMethod('post')) {
            $request->validate([
                'tournament_title' => 'required',
                'tournament_prize' => 'required',
                'tournament_info' => 'required',
            ]);
            $tournamentRepo = new TournamentRepository();
            if (Input::file('image') !== null) {
                $extension = Input::file('image')->getClientOriginalExtension();
                $fileName = rand(11111, 99999) . '.' . $extension;
                Input::file('image')->move(self::TOURNAMENT_IMAGES, $fileName);
                $banner = "/uploads/tournaments/" . $fileName;
                $request->request->add(['tournament_banner' => $banner]);
            }
            $tournamentRepo->update($id, $request);
            return redirect("/admin/list-tournaments")->with('status', 'Tournament Updated!');
        }
        return view("admin/tournaments/edit-tournament", compact('tournament', 'games', 'tournamentTypes', 'platforms', 'genres', 'registeredPlayers'));
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function deleteTournament($id)
    {
        $deleteTournament = TournamentRepository::delete($id);
        if ($deleteTournament == 1) {
            return redirect()->back()->with('status', 'Tournament Deleted');
        } else {
            return redirect()->back()->with('status', 'Tournament Delete Failed!');
        }

    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function deleteTournamentRules($id)
    {
        if (Auth::user()->getAttributes()['user_level_id'] == 1) {
            TournamentRules::where('tournament_id', $id)->destroy($id);
            return redirect()->back()->with('status', 'Tournament Rules Deleted');
        } else {
            return redirect()->back()->with('status', 'Your permissions do not allow this.');
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function deleteTournamentStandings($id)
    {
        if (Auth::user()->getAttributes()['user_level_id'] == 1) {
            TournamentStandings::where('tournament_id', $id)->delete($id);
            $tournament = Tournament::find($id);
            $tournament->tournament_standings = null;
            $tournament->save();
            return redirect('/admin/manage-tournaments')->with('status', 'Tournament Standing Deleted');
        } else {
            return redirect()->back()->with('status', 'Your permissions do not allow this.');
        }
    }

    /**
     * @param $clanId
     * @return int
     */
    public function deleteClan($clanId)
    {
        if (Auth::user()->getAttributes()['user_level_id'] == 1) {
            $users = Clan::destroy($clanId);
            return $users;
        } else {
            return false;
        }
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function manageTournaments(Request $request)
    {
        $platforms = Platform::all();
        $games = GamesList::all();
        $data = $this->createGamesAndPlatformArray();
        $gamesObj = json_encode($data['gamesArray']);
        $platformsObj = json_encode($data['platformsArray']);
        if (!Auth::user()) {
            $userLevelId = 3;
        } else {
            $userLevelId = Auth::user()->getAttributes()['user_level_id'];
        }
        if ($userLevelId == 1) {
            if ($request->get('game') !== null && $request->get('platform') !== null) {
                $tournaments = Tournament::where('tournament_game', $request->get('game'))->where('tournament_platform',
                    $request->get('platform'))->orderBy('created_at', 'desc')->get();
                $tournamentsArray = $this->createTournamentsArray($tournaments);
            } elseif ($request->get('game') !== null && $request->get('platform') == null) {
                $tournaments = Tournament::where('tournament_game', $request->get('game'))->orderBy('created_at',
                    'desc')->get();
                $tournamentsArray = $this->createTournamentsArray($tournaments);
            } elseif ($request->get('platform') !== null && $request->get('game') == null) {
                $tournaments = Tournament::where('tournament_platform',
                    $request->get('platform'))->orderBy('created_at', 'desc')->get();
                $tournamentsArray = $this->createTournamentsArray($tournaments);
            } else {
                $tournaments = Tournament::orderBy('created_at', 'desc')->get();
                $tournamentsArray = $this->createTournamentsArray($tournaments);
            }
            $tournamentsObj = json_encode($tournamentsArray['tournamentsArray']);
            return view('/admin/tournaments/list-tournaments',
                compact('tournaments', 'games', 'platforms', 'gamesObj', 'platformsObj', 'tournamentsObj'));
        }
        return redirect()->back()->with('status', 'We dont do that here.');
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function listTournaments(Request $request)
    {
        $platforms = Platform::all();
        $games = GamesList::all();
        $data = $this->createGamesAndPlatformArray();
        $gamesObj = json_encode($data['gamesArray']);
        $platformsObj = json_encode($data['platformsArray']);
        if ($request->get('game') !== null && $request->get('platform') !== null) {
            $tournaments = Tournament::where('tournament_game', $request->get('game'))->where('tournament_platform',
                $request->get('platform'))->where('tournament_status', 1)->orderBy('created_at', 'desc')->paginate(10);
        } elseif ($request->get('game') !== null && $request->get('platform') == null) {
            $tournaments = Tournament::where('tournament_game', $request->get('game'))->where('tournament_status',
                1)->orderBy('created_at', 'desc')->paginate(10);
        } elseif ($request->get('platform') !== null && $request->get('game') == null) {
            $tournaments = Tournament::where('tournament_platform',
                $request->get('platform'))->where('tournament_status', 1)->orderBy('created_at', 'desc')->paginate(10);
        } else {
            $tournaments = Tournament::where('tournament_status', 1)->orderBy('created_at', 'desc')->paginate(10);
        }
        $tournamentTypes = TournamentTypes::all();
        if (Auth::user()['user_level_id'] == 1) {
            return view('/frontend/tournaments/list-tournaments', [
                'tournaments' => $tournaments,
                'tournamentTypes' => $tournamentTypes,
                'games' => $games,
                'platforms' => $platforms,
                'platformsObj' => $platformsObj,
                'gamesObj' => $gamesObj
            ]);
        } else {
            if ($tournaments->total() == 0) {
                if ($request->get('game')) {
                    return $this->comingSoon('Tournaments', $request->get('game'));
                } else {
                    return $this->comingSoon('Tournaments');
                }
            } else {
                return view('/frontend/tournaments/list-tournaments',
                    compact('tournaments', 'tournamentTypes', 'games', 'platforms', 'gamesObj', 'platformsObj'));
            }
        }
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getClanMembersRegistered($tournamentId)
    {

        $clans = [];
        $registeredClans = TournamentRegistrations::where('tournament_id', '=', $tournamentId)->get();
        foreach ($registeredClans as $clan) {
            $clanMemberCollection = [];
            $clanEntity = Clan::where('id', '=', $clan->clan_id)->first();
            $clanMembers = TournamentRegisteredPlayers::where('clan_id', '=', $clan->clan_id)->where('tournament_id',
                $tournamentId)->get();
            foreach ($clanMembers as $clanMember) {
                $clanMemberCollection[] = $clanMember;
            }
            $clanMemberCollection['forfeit'] = $clan->forfeit;
            $clans[$clanEntity->clan_name] = $clanMemberCollection;
        }
        return $clans;
    }


    public function getClanMembersRegisteredExcel($tournamentId)
    {

        $clans = [];
        $registeredClans = TournamentRegistrations::where('tournament_id', '=', $tournamentId)->get();

        foreach ($registeredClans as $clan) {
            $clanMemberCollection = [];
            $clanEntity = Clan::where('id', '=', $clan->clan_id)->first();
            $clanMembers = TournamentRegisteredPlayers::where('clan_id', '=', $clan->clan_id)->get();
            foreach ($clanMembers as $clanMember) {
                $clanMemberCollection[] = $clanMember;
            }
            $clans[$clanEntity->clan_name] = $clanMemberCollection;
        }
        return $clans;
    }

    public function getUsersGamerTagAccordingByTournament($userId, $tournament)
    {
        $tag = '';
        $userService = new UserRepository();
        $user = $userService->getById($userId);
        if (isset($tournament->tournament_platform)) {
            if ($tournament->tournament_platform == 1) {
                $tag = isset($user->gamer_tag) ? $user->gamer_tag : '';
            } elseif ($tournament->tournament_platform == 2) {
                $tag = isset($user->psn_id) ? $user->psn_id : '';
            } elseif ($tournament->tournament_platform == 3) {
                $tag = isset($user->steam_id) ? $user->steam_id : '';
            } elseif ($tournament->tournament_platform == 4) {
                $tag = isset($user->uplay_id) ? $user->uplay_id : '';
            }
        } else {
            $tag = getGamerTag($user);
        }
        return $tag;
    }

    /**
     * @param $id
     * @param $title
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function viewTournament($id, $slug)
    {
        $standingsController = new StandingsController();
        $tournamentService = new TournamentRepository();
        $tournament = $tournamentService->findById($id);
        $bracketsRepo = new BracketRepository();
        $brackets = $bracketsRepo->getByTournamentId($id);
        $bracketsData = $this->createBracketsArray($brackets);
        if ($tournament !== null) {
            if (isset($tournament->tournament_type_id) && $tournament->tournament_type_id == 2) {
                $autoGeneratedStandings = $standingsController->getClanStandingsByTournament($id);
            } elseif ($tournament->tournament_type_id == 3) {
                $autoGeneratedStandings = $standingsController->getUserStandingsByTournament($id);
                foreach ($autoGeneratedStandings as $standing) {
                    $standing->tag = $this->getUsersGamerTagAccordingByTournament($standing->user_id, $tournament);
                }
            }
            $tournamentRegisteredClansCount = TournamentRegistrations::where('tournament_id', $id)->count();
            $tournamentPlayerCount = TournamentIndividualRegisteredPlayers::where('tournament_id', $id)->count();
            $myDate = Carbon::now()->format('Y/m/d');
            $todaysDate = str_replace('-', '/', $myDate);
            $tournamentTypes = TournamentTypes::all();
            $tournamentRegisteredIndividuals = TournamentIndividualRegisteredPlayers::where('tournament_id',
                $id)->get();
            $this->getIndividualImages($tournamentRegisteredIndividuals);
            $x = 0;
            $endOfWeek = Carbon::now()->endOfWeek()->toDateString();
            $twoWeeksFixtures = date('Y-m-d', strtotime(Carbon::now()->endOfWeek()->toDateString() . ' + 8 days'));
            if ($tournament->tournament_type_id == 3) {
                $fixturesPool1 = IndividualFixture::where('tournament_id', $tournament->id)->where('pool',
                    1)->where('date', '>=', Carbon::now()->startOfWeek()->toDateString())->where('date', '<=',
                    $endOfWeek)->get();
                $fixturesPool2 = IndividualFixture::where('tournament_id', $tournament->id)->where('pool',
                    2)->where('date', '>=', Carbon::now()->startOfWeek()->toDateString())->where('date', '<=',
                    $endOfWeek)->get();
            } else {
                $fixturesPool1 = Fixture::where('tournament_id', $tournament->id)->where('pool', 1)->where('date', '>=',
                    Carbon::now()->startOfWeek()->toDateString())->where('date', '<=', $endOfWeek)->get();
                $fixturesPool2 = Fixture::where('tournament_id', $tournament->id)->where('pool', 2)->where('date', '>=',
                    Carbon::now()->startOfWeek()->toDateString())->where('date', '<=', $endOfWeek)->get();
            }
            $tournamentStandingsFixed = TournamentStandings::where('tournament_id', $tournament->id)->first();
            if (!Auth::user()) {
                $userLevelId = null;
            } else {
                $userLevelId = Auth::user()->getAttributes()['user_level_id'];
            }
            if ($tournament->tournament_status == 1) {
                $clanMembers = $this->getClanMembersRegistered($tournament->id);
                $registeredClans = TournamentRegistrations::where('tournament_id', $tournament->id)->get();
                $registeredClanMembers = $this->getClanMembersRegistered($tournament->id);
                return view('/frontend/tournaments/tournament-page',
                    compact('registeredClanMembers', 'tournament', 'clanMembers', 'registeredClans', 'fixturesPool1',
                        'fixturesPool2', 'x', 'todaysDate', 'standings1', 'standings2', 'tournamentTypes',
                        'tournamentRegisteredIndividuals', 'tournamentRegisteredIndividualsStats',
                        'tournamentRegisteredClansCount', 'tournamentPlayerCount', 'importedFpsStandingsPool1',
                        'importedFpsStandingsPool2', 'tournamentStandingsFixed', 'autoGeneratedStandings','bracketsData'));
            } elseif ($userLevelId !== null && $userLevelId == 1) {
                $clanMembers = $this->getClanMembersRegistered($tournament->id);
                $registeredClans = TournamentRegistrations::where('tournament_id', $tournament->id)->get();
                $registeredClanMembers = $this->getClanMembersRegistered($tournament->id);
                return view('/frontend/tournaments/tournament-page',
                    compact('registeredClanMembers', 'tournament', 'clanMembers', 'registeredClans', 'fixturesPool1',
                        'fixturesPool2', 'x', 'todaysDate', 'standings1', 'standings2', 'tournamentTypes',
                        'tournamentRegisteredIndividuals', 'tournamentRegisteredIndividualsStats',
                        'tournamentRegisteredClansCount', 'tournamentPlayerCount', 'importedFpsStandingsPool1',
                        'importedFpsStandingsPool2', 'tournamentStandingsFixed', 'autoGeneratedStandings','bracketsData'));
            } else {
                return redirect()->back()->with('status',
                    'This tournament has either finished or is no longer available.');
            }
        }
        return redirect()->back()->with('status', 'This tournament has either finished or is no longer available.');
    }

    /**
     * @param Request $request
     * @param $clanId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function clanRegisterForTournament(Request $request, $clanId, $tournamentId)
    {
        $clan = Clan::find($clanId);
        $users = User::where('clan_id', $clanId)->get();
        $tournamentService = new TournamentRepository();
        $tournament = $tournamentService->findById($tournamentId);
        if ($request->isMethod('post')) {
            if($request->get('users') == null){
                return redirect()->back()->with('status','You have not selected any players to play in the tournament!');
            }
            $tournament = Tournament::find($tournamentId);
            $tournamentRegistered = TournamentRegistrations::where('tournament_id', $tournamentId)->where('clan_id',
                $clanId)->first();
            if ($tournament->tournament_registrations == 1 || Auth::user()->user_level_id == 1) {
                if ($tournamentRegistered == null) {
                    $usersToRegister = $request->get('users');
                    $tournamentPlayerLimit = $tournament->tournament_player_limit;
                    if (count($usersToRegister) > $tournamentPlayerLimit) {
                        return redirect()->back()->with('status',
                            'Please select the correct amount of players for the tournament. Limit is ' . $tournamentPlayerLimit . '');
                    }
                    $tournamentRegistrations = new TournamentRegistrations();
                    $tournamentRegistrations->tournament_id = $tournamentId;
                    $tournamentRegistrations->clan_id = $clanId;
                    $tournamentRegistrations->save();

                    foreach ($usersToRegister as $userId) {
                        $tournamentRegisteredPlayers = new TournamentRegisteredPlayers();
                        $tournamentRegisteredPlayers->clan_member_id = $userId;
                        $tournamentRegisteredPlayers->tournament_id = $tournamentId;
                        $tournamentRegisteredPlayers->clan_id = $clanId;
                        $tournamentRegisteredPlayers->save();
                    }
                    $this->logClanRegistrationActivity('Register clan by :', $usersToRegister);
                    return redirect()->back()->with('status', 'Registered for tournament');
                } else {
                    $usersToRegister = $request->get('users');
                    $tournamentPlayerLimit = $tournament->tournament_player_limit;
                    if (count($usersToRegister) > $tournamentPlayerLimit) {
                        return redirect()->back()->with('status',
                            'Please select the correct amount of players for the tournament. Limit is ' . $tournamentPlayerLimit . '');
                    }
                    $tournamentRegistration = TournamentRegistrations::where('clan_id',
                        $clan->id)->where('tournament_id', $tournamentId)->first();
                    $tournamentRegistration->tournament_id = $tournamentId;
                    $tournamentRegistration->clan_id = $clanId;
                    $tournamentRegistration->save();

                    $tournamentRegisteredPlayers = TournamentRegisteredPlayers::where('clan_id',
                        $clanId)->where('tournament_id', $tournamentId)->get();
                    foreach ($tournamentRegisteredPlayers as $player) {
                        $player->destroy($player->id);
                    }
                    foreach ($usersToRegister as $userId) {
                        $tournamentRegisteredPlayers = new TournamentRegisteredPlayers();
                        $tournamentRegisteredPlayers->clan_member_id = $userId;
                        $tournamentRegisteredPlayers->tournament_id = $tournamentId;
                        $tournamentRegisteredPlayers->clan_id = $clanId;
                        $tournamentRegisteredPlayers->save();
                    }
                    $this->logClanRegistrationActivity('Re-register clan for TournamentID:' . $tournamentId . ' by User ID: ' . Auth::user()->id,
                        $usersToRegister);
                    return redirect('/view-tournaments')->with('status',
                        'You have already registered your clan for the ' . $tournament->tournament_title . ' tournament so we have updated the existing registration!');
                }
            } else {
                return redirect('/')->with('status', 'Tournament registrations are closed!');
            }
        }
        return view('/frontend/tournaments/register-for-tournament', compact('users', 'clan', 'tournament'));
    }

    /**
     * @param $tournamentId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getClanRegistrationsByTournamentId($tournamentId)
    {
        $tournaments = TournamentRegistrations::where('tournament_id', '=', $tournamentId)->get();
        return view('/admin/tournaments/list-tournament-clans-and-users', compact('tournaments'));
    }


    /**
     * @param TournamentRulesRequest $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function createRules(TournamentRulesRequest $request)
    {
        $tournamentsWithNoRules = Tournament::where('tournament_rules', '=', null)->get();
        if (Auth::user()->getAttributes()['user_level_id'] == 1 && count($tournamentsWithNoRules) !== 0) {
            $tournaments = Tournament::where('tournament_rules', '=', null)->get();
            if ($request->isMethod('post')) {
                $rules = new TournamentRules;
                $rules->rules = $request->get('rules');
                $rules->tournament_id = $request->get('tournament');
                $rules->save();

                $tournamentId = $request->get('tournament');
                $getTournament = Tournament::find($tournamentId);
                $tournamentRules = TournamentRules::where('tournament_id', $tournamentId)->first();
                $getTournament->tournament_rules = $tournamentRules->id;
                $getTournament->save();
                return redirect('/view-clans')->with('status', 'Tournament rules have been created successfully');
            }
            return view('admin/tournaments/create-tournament-rules', compact('games', 'tournaments'));
        }
        return redirect()->back()->with('status', 'All tournaments have rules already.');
    }

    /**
     * @param TournamentRulesRequest $request
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function editRules(TournamentRulesRequest $request, $id)
    {
        if (Auth::user()->getAttributes()['user_level_id'] == 1) {
            $rulesEntity = TournamentRules::find($id);
            if ($request->isMethod('post')) {
                $rulesEntity = TournamentRules::find($id);
                $rulesEntity->rules = $request->get('rules');
                $rulesEntity->save();
                return redirect('/admin/list-tournaments')->with('status',
                    'Tournament rules have been updated successfully');
            }
            return view('admin/tournaments/edit-tournament-rules', compact('rulesEntity'));
        }
        return redirect()->back()->with('status', 'All tournaments have rules already.');
    }

    /**
     * @param TournamentStreamsRequest $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function createStreams(Request $request)
    {
        $tournamentsWithNoStreams = Tournament::where('tournament_streams', '=', null)->get();
        if (Auth::user()->getAttributes()['user_level_id'] == 1 && count($tournamentsWithNoStreams) !== 0) {
            $tournaments = Tournament::where('tournament_streams', '=', null)->get();
            if ($request->isMethod('post')) {
                $rules = new TournamentStream();
                $rules->streams = $request->get('streams');
                $rules->tournament_id = $request->get('tournament');
                $rules->save();
                $tournamentId = $request->get('tournament');
                $tournamentStreams = TournamentStream::where('tournament_id', $tournamentId)->first();
                $tournament = Tournament::find($tournamentId);
                $tournament->tournament_streams = $tournamentStreams->id;
                $tournament->save();
                return redirect('list-tournaments')->with('status',
                    'Tournament streams have been created successfully');
            }
            return view('admin/tournaments/create-tournament-streams', compact('tournaments'));
        }
        return redirect()->back()->with('status', 'All tournaments have streams already.');
    }

    /**
     * @param TournamentStreamsRequest $request
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function editStreams(Request $request, $id)
    {
        if (Auth::user()->getAttributes()['user_level_id'] == 1) {
            $streamsEntity = TournamentStream::where('tournament_id', $id)->first();
            if ($request->isMethod('post')) {
                $streamsEntity = TournamentStream::find($id);
                $streamsEntity->streams = $request->get('streams');
                $streamsEntity->save();
                return redirect('/admin/list-tournaments')->with('status',
                    'Tournament streams have been updated successfully');
            }
            return view('admin/tournaments/edit-tournament-streams', compact('streamsEntity'));
        }
        return redirect()->back()->with('status', 'You do not have permissions to do that!');
    }

    /**
     * @param TournamentStandingsRequest $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function createTournamentStandings(TournamentStandingsRequest $request)
    {
        $tournamentsWithNoStandings = Tournament::where('tournament_standings', '=', null)->get();
        if (Auth::user()->getAttributes()['user_level_id'] == 1 && count($tournamentsWithNoStandings) !== 0) {
            $tournaments = Tournament::where('tournament_standings', '=', null)->get();
            if ($request->isMethod('post')) {
                $userId = Auth::user()->getAttributes()['id'];
                $rules = new TournamentStandings;
                $rules->title = $request->get('title');
                $rules->tournament_id = $request->get('tournament_id');
                $rules->user_id = $userId;
                $rules->standings = $request->get('standings');
                $rules->save();

                $tournamentId = $request->get('tournament_id');
                $getTournament = Tournament::find($tournamentId);
                $tournamentStandings = TournamentStandings::where('tournament_id', $tournamentId)->first();
                $getTournament->tournament_standings = $tournamentStandings->id;
                $getTournament->save();
                return redirect('/view-clans')->with('status', 'Tournament standings have been created successfully');
            }
            return view('admin/tournaments/create-tournament-standings', compact('games', 'tournaments'));
        }
        return redirect()->back()->with('status', 'All tournaments have standings.');
    }


    /**
     * @param TournamentStandingsRequest $request
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function editStandings(TournamentStandingsRequest $request, $id)
    {
        if (Auth::user()->getAttributes()['user_level_id'] == 1) {
            $standingsEntity = TournamentStandings::find($id);
            if ($request->isMethod('post')) {
                $standingsEntity = TournamentStandings::where('id',$id);
                $standingsEntity->title = $request->get('title');
                $standingsEntity->standings = $request->get('standings');
                $standingsEntity->save();
                return redirect('/admin/list-tournaments')->with('status', 'Tournament standings have been updated successfully');
            }
            return view('admin/tournaments/edit-tournament-standings', compact('standingsEntity'));
        }
        return redirect()->back()->with('status', 'All tournaments have standings already.');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function generateTournamentEntries(Request $request)
    {
        $tournaments = Tournament::all();
        if ($request->isMethod('post')) {
            $tournament = Tournament::where('id', $request->get('tournament_id'))->get();
            if ($tournament[0]['tournament_type_id'] == 3) {
                $this->generateCSVIndividualTournament($request->get('tournament_id'), $request->get('csv_name'));
            } else {
                $this->generateExcel($request->get('tournament_id'), $request->get('csv_name'));
            }
        }
        return view('admin/tournaments/export-tournament-data', compact('tournaments'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPlayerCount(Request $request)
    {
        try {
            $tournament = Tournament::where('id', $request->get('tournamentId'))->first();
            $tournamentPlayerCount = $tournament->tournament_player_limit;
            return response()->json($tournamentPlayerCount);
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), '400');
        }
    }


    /**
     * @param $tournamentId
     * @param $csvName
     */
    public function generateExcel($tournamentId, $csvName)
    {
        $tournament = Tournament::find($tournamentId);
        if ($tournament->tournament_platform == 1) {
            $gamerTag = "users.gamer_tag";
        } elseif ($tournament->tournament_platform == 2) {
            $gamerTag = "users.psn_id";
        } elseif ($tournament->tournament_platform == 3) {
            $gamerTag = "users.steam_id";
        } else {
            $gamerTag = "users.gamer_tag";
        }
        $sqlQuery = "
        SELECT
          clans.clan_name,
          users.name,
          users.surname,
          $gamerTag 
          FROM tournaments
          LEFT JOIN tournament_registered_players ON tournaments.id = tournament_registered_players.tournament_id
          LEFT JOIN clans ON tournament_registered_players.clan_id = clans.id
          LEFT JOIN users ON tournament_registered_players.clan_member_id = users.id
        WHERE tournaments.id = $tournamentId";

        $query = DB::select(DB::raw($sqlQuery));
        $records = collect($query)->map(function ($x) {
            return (array)$x;
        })->toArray();

        $formattedDate = str_replace('-', '_', Carbon::now()->toDateString());
        $filename = $csvName . "_" . $formattedDate . ".csv";
        $delimiter = ";";

        // open raw memory as file so no temp files needed, you might run out of memory though
        $f = fopen('php://memory', 'w');
        // loop over the input array
        $headers = [];
        $headers[] = 'Clan';
        $headers[] = 'Player Name';
        $headers[] = 'Player Surname';
        $headers[] = 'Gamer Tag';
        fputcsv($f, $headers);
        foreach ($records as $line) {
            // generate csv lines from the inner arrays
            fputcsv($f, $line, $delimiter);
        }
        // reset the file pointer to the start of the file
        fseek($f, 0);
        // tell the browser it's going to be a csv file
        header('Content-Type: application/csv');
        // tell the browser we want to save it instead of displaying it
        header('Content-Disposition: attachment; filename="' . $filename . '";');
        // make php send the generated csv lines to the browser
        fpassthru($f);
        exit;
    }

    public function generateCSVIndividualTournament($tournamentId, $csvName)
    {
        $sqlQuery = "
        SELECT
          users.name,
          users.surname,
          users.gamer_tag
        FROM tournaments
          LEFT JOIN tournament_individual_registered_players ON tournaments.id = tournament_individual_registered_players.tournament_id
          LEFT JOIN users ON tournament_individual_registered_players.user_id = users.id
        WHERE tournaments.id = $tournamentId";
        $query = DB::select(DB::raw($sqlQuery));
        $records = collect($query)->map(function ($x) {
            return (array)$x;
        })->toArray();

        $formattedDate = str_replace('-', '_', Carbon::now()->toDateString());
        $filename = $csvName . "_" . $formattedDate . ".csv";
        $delimiter = ";";

        // open raw memory as file so no temp files needed, you might run out of memory though
        $f = fopen('php://memory', 'w');
        // loop over the input array
        $headers = [];
        $headers[] = 'Player Name';
        $headers[] = 'Player Surname';
        $headers[] = 'Gamer Tag';
        fputcsv($f, $headers);
        foreach ($records as $line) {
            // generate csv lines from the inner arrays
            fputcsv($f, $line, $delimiter);
        }
        // reset the file pointer to the start of the file
        fseek($f, 0);
        // tell the browser it's going to be a csv file
        header('Content-Type: application/csv');
        // tell the browser we want to save it instead of displaying it
        header('Content-Disposition: attachment; filename="' . $filename . '";');
        // make php send the generated csv lines to the browser
        fpassthru($f);
        exit;
    }

    public function getIndividualStats($id)
    {
        $tournament = Tournament::where('id', $id)->first();
        $stats = TournamentSiegeIndividualStats::where('tournament_id', $id)->get();
        $dateAdded = TournamentSiegeIndividualStats::where('tournament_id', $id)->first();
        $topKD = TournamentSiegeIndividualStats::where('tournament_id', $id)->orderBy('kill_death_ratio',
            'DESC')->first();
        $topKills = TournamentSiegeIndividualStats::where('tournament_id', $id)->orderBy('kills', 'DESC')->first();
        $topScore = TournamentSiegeIndividualStats::where('tournament_id', $id)->orderBy('scores', 'DESC')->first();
        $topPlayer = TournamentSiegeIndividualStats::where('tournament_id', $id)->orderBy('scores',
            'DESC')->orderBy('kill_death_ratio')->first();
        return view('frontend.tournaments.list-individual-stats',
            compact('stats', 'tournament', 'dateAdded', 'topKD', 'topKills', 'topPlayer', 'topScore'));
    }

    public function getIndividualImages($tournamentRegisteredIndividuals)
    {
        foreach ($tournamentRegisteredIndividuals as $individual) {
            if ($individual->users) {
                if ($individual->users->gamer_pic == '') {
                    $xboxProfile = new UserController();
                    $xboxProfileData = $xboxProfile->getXboxProfile($individual->users->id);
                    if (empty($xboxProfileData->code) && !empty($xboxProfileData->GameDisplayPicRaw)) {
                        $save_path = public_path() . '/images/profile-pics/';
                        $saveName = mt_rand(1000000, 9999999) . '.png';
                        $savedPath = $save_path . $saveName;
                        if (!file_exists($save_path)) {
                            mkdir($save_path, 666, true);
                            Image::make($xboxProfileData->GameDisplayPicRaw)->resize(600, null, function ($constraint) {
                                $constraint->aspectRatio();
                            })->save($savedPath, 75);
                        } else {
                            Image::make($xboxProfileData->GameDisplayPicRaw)->resize(600, null, function ($constraint) {
                                $constraint->aspectRatio();
                            })->save($savedPath, 75);
                        }
                        $user = User::find($individual->users->id);
                        $user->gamer_pic = '/images/profile-pics/' . $saveName;
                        $user->save();
                    }
                }
            }
        }
    }

    public function getRegisteredClans($id)
    {
        $registeredClans = TournamentRegistrations::where('tournament_id', $id)->get();
        $tournamentRepo = new TournamentRepository();
        $tournament = $tournamentRepo->findById($id);
        return view('admin.tournaments.list-registered-clans', compact('registeredClans', 'tournament'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateClanForfeitStatus(Request $request)
    {
        try {
            if ($request->get('selected') == 1) {
                $registration = TournamentRegistrations::where('clan_id',
                    $request->get('clanId'))->where('tournament_id', $request->get('tournamentId'))->first();
                $registration->forfeit = $request->get('selected');
                $registration->save();
                $message = ['message' => 'Clan has been forfeited!'];
            } else {
                $registration = TournamentRegistrations::where('clan_id',
                    $request->get('clanId'))->where('tournament_id', $request->get('tournamentId'))->first();
                $registration->forfeit = null;
                $registration->save();
                $message = ['message' => 'Clan is back in the game! Nee maar mooi!'];
            }
            return response()->json($message, 200);
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 400);
        }
    }

    public function removeClanFromTournament($tournamentId, $clanId)
    {
        $registeredClan = TournamentRegistrations::where('clan_id', $clanId)->where('tournament_id',
            $tournamentId)->first();
        $registeredClan->delete($registeredClan->id);
        $registeredPlayers = TournamentRegisteredPlayers::where('clan_id', $clanId)->where('tournament_id',
            $tournamentId)->get();
        foreach ($registeredPlayers as $player) {
            $player->delete($player->id);
        }
        return redirect()->back()->with('status', 'Clan removed from tournament!');
    }

    public function getTournaments()
    {
        if (Cache::has('allTournaments')) {
            $tournaments = Cache::get('allTournaments');
        } else {
            $tournamentData = Tournament::where('tournament_status', 1)->orderBy('created_at', 'DESC')->get();
            Cache::put('allTournaments', $tournamentData, 30);
            $tournaments = Cache::get('allTournaments');
        }
        if ($tournaments !== null) {
            return response()->json($tournaments, 200);
        } else {
            return response()->json([], 500);
        }
    }

    public function getTournament($id)
    {
        $tournament = Tournament::find($id);
        if ($tournament !== null) {
            return response()->json($tournament);
        }
        return response()->json([], 500);
    }

    public function logClanRegistrationActivity($comment, $usersToRegister)
    {
        $logUsers = [];
        foreach ($usersToRegister as $user) {
            $getUser = User::find($user);
            $logUsers[] = $getUser->gamer_tag;
        }
        $data = json_encode($logUsers);
        $nameAndSurname = Auth::user()->name . ' ' . Auth::user()->surname;
        LogActivity::addToLog($comment . $nameAndSurname, $data);
    }

    public function getFixtureResultsData($id)
    {
        $results = FixtureResult::where('tournament_id', $id)->orderBy('created_at', 'DESC')->get();
        $tournament = Tournament::find($id);
        return view('frontend.tournaments.fixture-results-page', compact('results', 'tournament'));
    }

    public function getTournamentClans($id)
    {
        $tournamentRegistrationsService = new TournamentRegistrationsRepository();
        $tournamentClanRegistrations = $tournamentRegistrationsService->getByTournamentId($id);
        if ($tournamentClanRegistrations !== null) {
            $clans = [];
            $x = 0;
            foreach ($tournamentClanRegistrations as $clanRegistration) {
                $clan = Clan::where('id', $clanRegistration->clan_id)->first();
                $clans[$x]['clan_id'] = isset($clan->id) ? $clan->id : '';
                $clans[$x]['clan_name'] = isset($clan->clan_name) ? $clan->clan_name : '';
                $x++;
            }
            return response()->json($clans);
        }
        return response()->json([], 500);
    }

    public function getTournamentPlayers($id)
    {
        $tournamentRegistrationsService = new TournamentIndividualRegisteredPlayersRepository();
        $tournamentPlayerRegistrations = $tournamentRegistrationsService->getByTournamentId($id);
        $tournamentService = new TournamentRepository();
        $tournament = $tournamentService->findById($id);
        if ($tournamentPlayerRegistrations !== null) {
            $players = [];
            $x = 0;
            foreach ($tournamentPlayerRegistrations as $playerRegistration) {
                $players[$x]['user_id'] = isset($playerRegistration->user_id) ? $playerRegistration->user_id : '';
                $individualFixturesController = new IndividualFixturesController();
                $players[$x]['tag'] = $individualFixturesController->getGamerTagByTournamentPlatform($tournament,
                    $playerRegistration->user_id);
                $x++;
            }
            return response()->json($players, 200);
        }
        return response()->json([], 500);
    }

    public function getTournamentPlayersByClan($clanId, $tournamentId)
    {
        $tournamentRegisteredPlayersRepo = new TournamentRegisteredPlayersRepository();
        $players = $tournamentRegisteredPlayersRepo->getPlayersByClanAndTournamentId($clanId, $tournamentId);
        $individualFixturesController = new IndividualFixturesController();
        $clanRepo = new ClanMemberRepository();
        $clanMembers = $clanRepo->getByClanId($clanId);
        $tournamentRepo = new TournamentRepository();
        $tournament = $tournamentRepo->getById($tournamentId);
        foreach ($clanMembers as $clanMember) {
            $clanMember->gamer_name = $individualFixturesController->getGamerTagByTournamentPlatform($tournament,
                $clanMember->user_id);
        }
        foreach ($players as $player) {
            $player->gamer_name = $individualFixturesController->getGamerTagByTournamentPlatform($tournament,
                $player->clan_member_id);
        }
        return view('admin.tournaments.manage-tournament-clan-players',
            compact('players', 'tournament', 'clanMembers', 'clanId'));
    }

    public function createGamesAndPlatformArray()
    {
        $games = GamesList::all();
        $platforms = Platform::all();
        $gameArray = [];
        $platformsArray = [];
        foreach ($games as $game) {
            $gameArray[$game->id] = $game->game_name;
        }
        foreach ($platforms as $platform) {
            $platformsArray[$platform->id] = $platform->platform;
        }
        return ['platformsArray' => $platformsArray, 'gamesArray' => $gameArray];
    }

    public function createTournamentsArray($tournaments)
    {
        $x = 0;
        $tournamentsArray = [];
        foreach ($tournaments as $tournament) {
            $tournamentsArray[$x]['id'] = $tournament->id;
            $tournamentsArray[$x]['title'] = $tournament->tournament_title;
            $tournamentsArray[$x]['rules_id'] = $tournament->tournament_rules;
            $tournamentsArray[$x]['tournament_type_id'] = $tournament->tournament_type_id;
            $tournamentsArray[$x]['tournament_status'] = $tournament->tournamentStatus->title;
            $x++;
        }
        return ['tournamentsArray' => $tournamentsArray];
    }

    public function createBracketsArray($brackets)
    {

        $bracketsArray = [];
        $games = [];
        $x = 0;
        $roundCount = 0;
        foreach($brackets as $bracket){
            if($bracket->round > $roundCount){
                $roundCount++;
            }
        }

        $bracketsRepo = new BracketRepository();
        for($y = 1;$y <= $roundCount;$y++){
            unset($bracketsArray);
            $bracketByRound = $bracketsRepo->getByRound($y);
            foreach ($bracketByRound as $bracket){
                $bracketsArray[$x]['player1'] = ['id' => $bracket->id, 'name' => $bracket->clan1->clan_name, 'winner' => false];
                $bracketsArray[$x]['player2'] = ['id' => $bracket->id,'name' => $bracket->clan2->clan_name, 'winner' => true];
                $x++;
            }
            $z = $y - 1;
            $games[$z]['games'] = $bracketsArray;
            $x = 0;
        }
        if($roundCount == 1){
            $placeHolders = count($games[0]['games']);
            $placeHoldersHalved = $placeHolders / 2;
            $countPlaceholdersNeeded = $placeHoldersHalved - 1;
            $id = 1;
            for($w = 0;$w <= (int)number_format(round($countPlaceholdersNeeded));$w++){
                $games[1]['games'][$w]['player1'] = ['id' => $id,'name' => 'TBP', 'winner' => false ];
                $games[1]['games'][$w]['player2'] = ['id' => $id + 2,'name' => 'TBP', 'winner' => true ];
                $id++;
            }
        }
        return ['gamesArray' => $games];
    }

    function roundRobin($teams){
        $round = [];
        if (count($teams)%2 != 0){
            array_push($teams,"bye");
        }
        $away = array_splice($teams,(count($teams)/2));
        $home = $teams;
        for ($i=0; $i < count($home)+count($away)-1; $i++){
            for ($j=0; $j<count($home); $j++){
                $round[$i][$j]["clan_1"] = $home[$j];
                $round[$i][$j]["clan_2"] = $away[$j];
            }
            if(count($home)+count($away)-1 > 2){
                array_unshift($away, current(array_splice($home,1,1)));
                array_push($home,array_pop($away));
            }
        }
        return $round;
    }

    public function createRoundRobinFixtures(Request $request) {
            $tournamentId = $request->get('tournament');
            $tournamentRepo = new TournamentRepository();
            $fixtureRepo = new FixtureRepository();
            $clansRegistered = $tournamentRepo->getRegisteredClansByTournamentId($tournamentId);
            $x = 0;
            $clansArray = [];
            foreach ($clansRegistered as $clan){
                $clansArray[$x]['id'] = $clan->clan->id;
                $clansArray[$x]['name'] = $clan->clan->clan_name;
                $x++;
            }
            $clansObj = json_encode($clansArray);
            if($request->isMethod('post')){
                $tournamentPoolCheck = $fixtureRepo->checkIfPoolExists($request->pool,$tournamentId);
                if($tournamentPoolCheck == null){
                    ProcessRoundRobinFixtures::dispatch($request->all(),$request->clans);
                } else {
                    return redirect()->back()->with('status','Fixtures already exist for this pool and tournament');
                }
            }
            return view('admin.fixtures.autofixtures.create-round-robin',compact('clansObj','tournamentId'));
    }

    public function processRoundRobin($request,$clansRegistered){
        $fixturesRepo = new FixtureRepository();
        $generatedMatchups = $this->roundRobin($clansRegistered);
        $seperateDateTime = explode( ',', $request['date_time']);
        $start_date = date_format(date_create($seperateDateTime[0]),"Y-m-d");
        $y = 0;
        $x = 0;
        $newMatchupsCombined = [];
        $stringDate = strtotime($start_date);
        $datePlusWeek = "";
        foreach ($generatedMatchups as $generatedMatchup) {
            $newMatchups = [];
            $x = 0;
            foreach($generatedMatchup as $data){
                $newMatchups[$x]['clan_1'] = $data['clan_1'];
                $newMatchups[$x]['clan_2'] = $data['clan_2'];
                $newMatchups[$x]['time'] = date('H:i',strtotime($seperateDateTime[1]));
                $newMatchups[$x]['date'] = $start_date;
                $x++;
            }
            $newMatchupsCombined[$y] = $newMatchups;
            $y++;
            $stringDate = strtotime($start_date);
            $datePlusWeek = strtotime("+7 day", $stringDate);
            $start_date = date('Y-m-d', $datePlusWeek);
            $checkDateRange = $this->checkInDateRange($request['start_date_range'], $request['end_date_range'], $datePlusWeek);
            if($checkDateRange == true){
                $stringDateEnd = strtotime($request['end_date_range']);
                $startDateNew = strtotime("+1 day", $stringDateEnd);
                $start_date = date('Y-m-d', $startDateNew);
            }
        }
        foreach ($newMatchupsCombined as $generatedMatchup){
            foreach ($generatedMatchup as $matchUp){
                if($matchUp['clan_1'] !== "bye" && $matchUp['clan_2'] !== "bye"){
                    $request['clan_1'] = $matchUp['clan_1'];
                    $request['clan_2'] = $matchUp['clan_2'];
                    $request['date'] = $matchUp['date'];
                    $request['time'] = $matchUp['time'];
                    $request['bracket'] = 0;
                    $fixturesRepo->create((object)$request);
                    sleep(2);
                }
            }
        }
    }


    function checkInDateRange($start_date, $end_date, $date_from_user)
    {
        // Convert to timestamp
        $start_ts = strtotime($start_date);
        $end_ts = strtotime($end_date);
        $user_ts = $date_from_user;

        // Check that user date is between start & end
        return (($user_ts >= $start_ts) && ($user_ts <= $end_ts));
    }
}