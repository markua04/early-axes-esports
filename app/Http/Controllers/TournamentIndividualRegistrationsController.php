<?php namespace App\Http\Controllers;

use App\Http\Repositories\TournamentRepository;
use App\Platform;
use App\TournamentIndividualRegisteredPlayers;
use App\Http\Requests\TournamentIndividualRegisteredPlayersRequest;
use App\Tournament;
use Illuminate\Support\Facades\Auth;

class TournamentIndividualRegistrationsController extends Controller
{

    public function userRegisterForIndividualTournament(TournamentIndividualRegisteredPlayersRequest $request)
    {
        $tournaments = Tournament::where('tournament_type_id',3)->where('tournament_status',1)->get();
        $user = Auth::user();
        if ($request->isMethod('post')) {
            $verdict = $this->checkUserGamerTag($user, $request->get('tournament'));
            if($verdict !== true){
                return redirect()->back()->with('status','Please add a gamer tag for the ' . $verdict . ' platform to be eligible to play in that tournament.');
            }
            $tournamentRegistered = TournamentIndividualRegisteredPlayers::where('user_id',$user->id)
                ->where('tournament_id',$request->get('tournament'))
                ->first();
            if ($tournamentRegistered == null) {
                $tournamentIndividualPlayer = new TournamentIndividualRegisteredPlayers();
                $tournamentIndividualPlayer->user_id = $user->id;
                $tournamentIndividualPlayer->tournament_id = $request->get('tournament');
                $tournamentIndividualPlayer->save();
            } else {
                return redirect()->back()->with('status', 'You have already registered for that tournament.');
            }
            return redirect()->back()->with('status', 'Registered for tournament');
        }
        return view('frontend/tournaments/register-for-individual-tournament',compact('user','tournaments'));
    }

    public function checkUserGamerTag($user, $tournamentId){
        $tournamentService = new TournamentRepository();
        $tournament = $tournamentService->findById($tournamentId);
        if($tournament->tournament_platform == 1){
            if($user->gamer_tag == null){
                return $this->getPlatformName($tournament->tournament_platform);
            }
        } else if($tournament->tournament_platform == 2){
            if($user->psn_id == null){
                return $this->getPlatformName($tournament->tournament_platform);
            }
        } else if($tournament->tournament_platform == 3){
            if($user->steam_id == null){
                return $this->getPlatformName($tournament->tournament_platform);
            }
        } else if($tournament->tournament_platform == 4){
            if($user->uplay_id == null){
                return $this->getPlatformName($tournament->tournament_platform);
            }
        }
        return true;
    }

    public function getPlatformName($id){
        $platform = Platform::find($id);
        return $platform->platform;
    }
}
