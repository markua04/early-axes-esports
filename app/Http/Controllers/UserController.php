<?php namespace App\Http\Controllers;

use App\Clan;
use App\ClanMembers;
use App\Helpers\Lang;
use App\Helpers\LogActivity;
use App\Http\Repositories\TournamentRegisteredPlayersRepository;
use App\Http\Repositories\TournamentRepository;
use App\Http\Requests\UserRequest;
use App\Http\Repositories\GamesListRepository;
use App\Mail\GamerTagChangedMail;
use App\Mail\UserRegisteredForClanMail;
use App\Platform;
use App\TournamentRegisteredPlayers;
use App\User;
use App\UserGames;
use App\Http\Repositories\UserRepository;
use App\Http\Repositories\ClanRepository;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Intervention\Image\Facades\Image;

class UserController extends Controller
{

    const XBOX_PLATFORM = 1;
    const STEAM_PLATFORM = 2;
    const PS_PLATFORM = 3;

    /**
     * @param $game
     * @param $platform
     * @return \Illuminate\Http\JsonResponse|null
     */
    public function getSiegeStats($game, $platform, $gamerTag)
    {
        if ($game == "siege") {
            $apiController = new ApiController();
            $siegeStats = $apiController->getSiegeStats($platform, $gamerTag);
            $operatorStats = $apiController->getSiegeOperatorStats($platform, $gamerTag);
            $operatorMostPlayed = $apiController->getMostUsedOperator($operatorStats);
            if (Cache::has('siegeStats-' . $gamerTag)) {
                $siegeStats = Cache::get('siegeStats-' . $gamerTag);
            } else {
                $stats = array('siegeStats' => $siegeStats, 'operatorStats' => $operatorStats, 'operatorMostPlayed' => $operatorMostPlayed);
                Cache::put('siegeStats-' . $gamerTag, $stats, 1440);
                $siegeStats = Cache::get('siegeStats-' . $gamerTag);
            }
            return response()->json(['stats' => $siegeStats]);
        } else {
            return null;
        }

    }

    /**
     * @param $userId
     * @return mixed
     */
    public function getXboxProfile($userId)
    {
        $gamerTag = User::find($userId)->gamer_tag;
        if (Cache::has('xboxprofile-' . $gamerTag)) {
            $xboxProfileData = Cache::get('xboxprofile-' . $gamerTag);
        } else {
            $xboxData = new ApiController();
            $xboxProfileData = $xboxData->getXboxLiveAccount($gamerTag);
            Cache::put('xboxprofile-' . $gamerTag, $xboxProfileData, 1440);
            $xboxProfileData = Cache::get('xboxprofile-' . $gamerTag);
        }
        return json_decode($xboxProfileData);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function viewClanUserProfile($id)
    {
        $userData = User::find($id);
        $userGames = UserGames::where('user_id', $id)->get();
        $platformController = new PlatformController();
        if ($userData->selected_source == self::XBOX_PLATFORM) {
            $userPlatformData = $platformController->getXboxUserProfile($userData);
        } elseif ($userData->selected_source == self::STEAM_PLATFORM) {
            $userPlatformData = $platformController->getSteamUserProfile($userData);
        } elseif ($userData->selected_source == self::PS_PLATFORM) {
            $userPlatformData = $platformController->getPSUserProfile($userData);
        }
        return view('admin.manage-users.user-profile', compact('userPlatformData', 'userData', 'userGames'));

    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getUserProfile()
    {
        $userData = Auth::user();
        return view('admin.manage-users.user-profile', compact('userData', 'xboxProfile', 'siegeStats', 'operatorMostPlayed'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function listUsers()
    {
        $gamers = User::all();
        return view('admin.manage-users.list-users', compact('gamers'));
    }


    /**
     * @param null $id
     * @param UserRequest $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function editUser($id, UserRequest $request)
    {
        $gamesListService = new GamesListRepository();
        $games = $gamesListService->getAll();
        $profileData = $this->getXboxProfile($id);
        $platforms = Platform::all();
        $userGames = UserGames::where('user_id', $id)->get();
        $userTournamentRegistrations = TournamentRegisteredPlayers::where('clan_member_id', $id)->get();
        $emailError = "";
        $currentGamerTag = isset(Auth::user()->gamer_tag) ? Auth::user()->gamer_tag : '';
        $clanRepo = new ClanRepository();
        $checkIfClanOwner = $clanRepo->getByUserId($id);
        $user = User::find($id);
        $gamerTags = $this->getUsersTags($user);
        if ($request->isMethod('post')) {
            $checkIfUserRegisteredForTournament = $this->checkIfUserInTournament($user);
            if(isset($checkIfClanOwner->clan_owner_id) && $checkIfClanOwner->clan_owner_id == $user->id && (int)$request->get('clan_id') !== $checkIfClanOwner->id){
                return redirect()->back()->with('status','You still own a clan. Please reassign ownership of the clan or delete the clan before changing clans. You can change ownership or delete the clan from the edit clan page.');
            }
            $request->request->add(['gt_change' => null]);
            $checkIfGamerTagChanged = $this->checkIfGamerTagHasChanged($checkIfUserRegisteredForTournament,$gamerTags,$request);
            if($checkIfGamerTagChanged['response'] == true){
                $request->request->add(['gt_change' => $checkIfGamerTagChanged['gt_id']]);
                $this->emailAdmin($user);
            }
            $userRepo = new UserRepository();
            $userRepo->edit($request,$id);
            $clanId = $user->clan_id;
            if ($request->get('clan_id') !== null && (int)$request->get('clan_id') !== $clanId) {
                $user->clan_id = $request->get('clan_id');
                $this->userChangedClan($request,$user,$userTournamentRegistrations);
            } else {
                $user->clan_id = $clanId;
            }
            if ($request->get('games') !== null) {
                $this->saveGames($request, $user);
            }
            $gamerTagChangedLog = $this->gamerTagChangeLog($currentGamerTag,$request);
            LogActivity::addToLog('User Edited by : ' . $user->name . ' ' . $user->surname . $gamerTagChangedLog);
            return $emailError !== "" ? redirect("user-profile/$user->id/$user->name")->with('status',$emailError) : redirect("user-profile/$user->id/$user->name");
        }

        if (Auth::check() && Auth::user()->id == $user->id) {
            $clans = Clan::all();
            return view('admin/manage-users/edit-user', compact('user', 'clans', 'profileData', 'platforms', 'games', 'userGames'));
        } else {
            return redirect()->back()->with('status', Lang::errorStatus('no_permission_edit_user'));
        }
    }

    /**
     * @param $userId
     * @return mixed|null
     */
    public function getSteamProfile($userId)
    {
        $user = User::find($userId);
        if ($user->steam_id !== null) {
            $apiController = new ApiController();
            $steamId = $apiController->getSteamId($user->steam_id);
            if ($steamId == 42) {
                return null;
            }
            if (Cache::has('steamProfile-' . $steamId)) {
                $steamProfileData = $apiController->getSteamProfile($steamId);
                $steamProfile = json_decode($steamProfileData);
            } else {
                $steamProfileData = $apiController->getSteamProfile($steamId);
                Cache::put('steamProfile-' . $steamId, $steamProfileData, 1440);
                $steamProfile = Cache::get('steamProfile-' . $steamId);
            }

            return $steamProfile;
        }
    }

    public function getSteamAchievements($userId)
    {
        $user = User::find($userId);
        $apiController = new ApiController();
        $steamId = $apiController->getSteamId($user->steam_id);
        $steamProfileData = $apiController->getSteamAchievements($steamId);
        $steamAchievements = json_decode($steamProfileData);
        return $steamAchievements;
    }

    /**
     * @param $userId
     * @param $csvName
     */
    public function generateExcel($csvName)
    {
        $sqlQuery = "SELECT * FROM users";
        $query = DB::select(DB::raw($sqlQuery));
        $records = collect($query)->map(function ($x) {
            return (array)$x;
        })->toArray();

        $formattedDate = str_replace('-', '_', Carbon::now()->toDateString());
        $filename = $csvName . "_" . $formattedDate . ".csv";
        $delimiter = ";";

        // open raw memory as file so no temp files needed, you might run out of memory though
        $f = fopen('php://memory', 'w');
        // loop over the input array
        $headers = [];
        $headers[] = 'ID';
        $headers[] = 'Name';
        $headers[] = 'Surname';
        $headers[] = '';
        $headers[] = 'Xbox Gamer Tag';
        $headers[] = 'Steam ID';
        $headers[] = 'Image';
        $headers[] = 'Clan ID';
        $headers[] = 'Email Address';
        fputcsv($f, $headers);
        foreach ($records as $line) {
            // generate csv lines from the inner arrays
            fputcsv($f, $line, $delimiter);
        }
        // reset the file pointer to the start of the file
        fseek($f, 0);
        // tell the browser it's going to be a csv file
        header('Content-Type: application/csv');
        // tell the browser we want to save it instead of displaying it
        ob_end_clean();
        header('Content-Disposition: attachment; filename="' . $filename . '";');
        // make php send the generated csv lines to the browser
        fpassthru($f);
        exit;
    }

    public function getLatestXboxImage($userId)
    {
        $xboxProfile = new UserController();
        $xboxProfileData = $xboxProfile->getXboxProfile($userId);
        if (empty($xboxProfileData->code) && !empty($xboxProfileData->GameDisplayPicRaw)) {
            $save_path = public_path() . '/images/profile-pics/';
            $saveName = mt_rand(1000000, 9999999) . '.png';
            $savedPath = $save_path . $saveName;
            if (!file_exists($save_path)) {
                mkdir($save_path, 666, true);
                Image::make($xboxProfileData->GameDisplayPicRaw)->save($savedPath);
            } else {
                Image::make($xboxProfileData->GameDisplayPicRaw)->save($savedPath);
            }
            $user = User::find($userId);
            $user->gamer_pic = '/images/profile-pics/' . $saveName;
            $user->save();
        }
    }

    public function getSteamUserProfile($user)
    {
        if ($user->selected_source == 2) {
            $data = $this->getSteamProfile($user);
        }
    }

    public function saveGames($request, $user)
    {
        $gamesList = UserGames::where('user_id', $user->id)->get();
        if ($request->get('games') !== null) {
            foreach ($gamesList as $game) {
                $game->delete();
            }
        }
        if ($request->get('games') !== null) {
            foreach ($request->get('games') as $game) {
                $userGames = new UserGames;
                $userGames->user_id = $user->id;
                $userGames->game_id = $game;
                $userGames->save();
            }
        }
    }

    public function getUsersTags($user){
        $tags = [
            'xbox' => $user->gamer_tag,
            'psn_id' => $user->psn_id,
            'steam_id' => $user->psn_id,
            'uplay_id' => $user->psn_id
        ];
        return $tags;
    }

    public function checkIfUserInTournament($user){
        $tournamentRepo = new TournamentRepository();
        $tournamentRegistrationsRepo = new TournamentRegisteredPlayersRepository();
        $tournamentRegistrations = $tournamentRegistrationsRepo->getByClanMemberId($user->id);
        foreach ($tournamentRegistrations as $registration){
            $tournamentCheck = $tournamentRepo->getById($registration->tournament_id);
            if($tournamentCheck !== null && $tournamentCheck->tournament_status == 1){
                return true;
            }
        }
        return false;
    }

    public function userChangedClan($request,$user,$userTournamentRegistrations){
        $clanService = new ClanRepository();
        $clan = $clanService->getById($request->get('clan_id'));
        $clanMembers = ClanMembers::where('user_id', $user->id)->first();
        if ($clanMembers == null) {
            $clanMembers = new ClanMembers();
        }
        $clanMembers->clan_id = $request->get('clan_id');
        $clanMembers->user_id = $user->id;
        $clanMembers->approval_id = 0;
        $clanMembers->save();
        $gamerTag = getGamerTag($user);
        if ($clan !== null && $gamerTag !== null && $clan->id !== 0) {
            foreach ($userTournamentRegistrations as $registration) {
                $registration->destroy($registration->id);
            }
            if ($clan->clan_email !== null) {
                try {
                    Mail::to($clan->clan_email)->send(new UserRegisteredForClanMail($user));
                } catch (Exception $e){
                    if($e->getMessage() == "Connection could not be established with host smtp.mailgun.org [php_network_getaddresses: getaddrinfo failed: Name or service not known #0]"){
                        $emailError = "Please contact the clan leader to accept you into the clan as we tried to email the leader but the clan email is invalid.";
                        Log::error($emailError);
                    }
                }
            }
        }
    }

    public function gamerTagChangeLog($currentGamerTag,$request){
        if($currentGamerTag !== "" && $currentGamerTag !== null){
            if($request->gamer_tag !== $currentGamerTag){
                $gamerTagChanged = " and changed gamer tag from $currentGamerTag to $request->gamer_tag";
            }
        }
    }

    public function checkIfGamerTagHasChanged($checkIfUserRegisteredForTournament,$gamerTags,$request){
        if($checkIfUserRegisteredForTournament == true){
            $gamerTagsRequest = [$request->gamer_tag,$request->psn_id,$request->steam_id,$request->uplay_id];
            $x = 0;
            foreach ($gamerTagsRequest as $tag){
                if(!in_array($tag,$gamerTags)){
                    return [
                        'gt_id' => $x,
                        'response' => true
                    ];
                }
                $x++;
            }
        }
        return [
            'gt_id' => null,
            'response' => 'false'
        ];
    }

    public function emailAdmin($user){
        $emails = [env('ADMIN_EMAIL'),env('TOURNAMENT_ADMIN_EMAIL')];
        try {
            Mail::to($emails)->send(new GamerTagChangedMail($user));
        } catch (Exception $e){
            Log::error($e);
        }
    }

}
