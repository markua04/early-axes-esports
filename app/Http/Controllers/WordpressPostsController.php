<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;

class WordpressPostsController extends Controller
{

    const BASE_EAX_URL = "https://earlyaxes.co.za";
    /**
     * @param $url
     * @return mixed
     */
    public function getPost($postId)
    {
        $url = "https://earlyaxes.co.za/wp-json/wp/v2/posts/$postId?_embed";
        // Get cURL resource
        $curl = curl_init();
        // Set some options - we are passing in a useragent too here
        curl_setopt_array($curl, [
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $url,
        ]);
        // Send the request & save response to $resp
        $resp = curl_exec($curl);
        // Close request to clear up some resources
        curl_close($curl);
        return $resp;
    }

    public function displayPost($postId){
        $string = $this->getPost($postId);
        $converted = substr($string, 3);
        $post = json_decode($converted);
        return view('frontend.wordpress.post', compact('post'));
    }

    /**
     * @param $url
     * @return mixed
     */
    public function getCustomPost($postId)
    {
        $url = self::BASE_EAX_URL . "/wp-json/wp/v2/early-axes-news/$postId?_embed";
        // Get cURL resource
        $curl = curl_init();
        // Set some options - we are passing in a useragent too here
        curl_setopt_array($curl, [
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $url,
        ]);
        // Send the request & save response to $resp
        $resp = curl_exec($curl);
        // Close request to clear up some resources
        curl_close($curl);
        return $resp;
    }

    public function displayCustomPost($postId){
        $string = $this->getCustomPost($postId);
        $converted = substr($string, 3);
        $post = json_decode($converted);
        return view('frontend.wordpress.post', compact('post'));
    }

    public function getArticlesByCount($count){
        $url = self::BASE_EAX_URL . "/wp-json/wp/v2/posts?per_page=" . $count . "&_embed&orderby=modified&order=desc";
        // Get cURL resource
        $curl = curl_init();
        // Set some options - we are passing in a useragent too here
        curl_setopt_array($curl, [
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $url,
        ]);
        // Send the request & save response to $resp
        $resp = curl_exec($curl);
        // Close request to clear up some resources
        curl_close($curl);
        var_dump($resp);die;
        return $resp;
    }

    public function processArticles($count){
        $jsonArticles = $this->getArticlesByCount($count);
        $removeChars = preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $jsonArticles);
        $articles = json_decode($removeChars , true );
        $articlesArray = [];
        $x = 0;
        foreach($articles as $article){
            if($article['_embedded']['wp:featuredmedia'][0]['source_url'] !== ""){
                $articlesArray[$x] = $article;
                $x++;
            }
        }
        die;
    }
}
