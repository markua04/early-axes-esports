<?php namespace App\Http\Middleware;

use App\Helpers\Lang;
use Closure;
use Illuminate\Support\Facades\Cache;

class APIToken
{
    public function handle($request, Closure $next)
    {
        if($this->verifyToken($request) == true){
            return $next($request);
        }
        return response()->json(['message' => Lang::errorStatus('invalid_request')],401);
    }

    private function verifyToken($request){
        $token = $request->header('Authorization');
        $isTokenSet = isset($token);
        if($isTokenSet == true){
            $getUserId = Cache::get($token);
            if($getUserId !== ""){
                return true;
            }
        }
        return false;
    }
}
