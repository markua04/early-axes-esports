<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;

class CheckLoggedInStatus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check() == false) {
            if(strpos(Request::url(),"join-clan")){
                session_start();
                $_SESSION['joinUrl'] = Request::url();
            }
            return redirect('/login')->with('status','You need to be registered to view that.');
        }
        return $next($request);
    }
}
