<?php namespace App\Http\Middleware;

use App\Helpers\Lang;
use App\Http\Repositories\UserRepository;
use Closure;

class VerifyAdminToken
{
    public function handle($request, Closure $next)
    {
        $userRepo = new UserRepository();
        $user = $userRepo->getEAXUserByToken($request->header('Authorization'));
        if($request->header('Authorization') !== null){
            $userRepo = new UserRepository();
            $user = $userRepo->getEAXUserByToken($request->header('Authorization'));
            if($user !== null){
                if($user->user_level_id == 1){
                    if($this->verifyAdminUser($request->header('Authorization'),$user) == true){
                        return $next($request);
                    }
                }
            }
        }
        return response()->json(['message' => $user],401);
    }

    private function verifyAdminUser($token,$user){
        if($user->api_token == $token){
            return true;
        }
        return false;
    }
}
