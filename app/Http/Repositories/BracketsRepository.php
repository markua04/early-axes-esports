<?php namespace App\Http\Repositories;

use App\Bracket;
use App\FixtureResult;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class BracketRepository
{
    /**
     * @param $fixtureId
     * @param $tournamentId
     * @param $round
     * @return void
     */
    public function create($fixtureId,$tournamentId,$round,$clans,$winner)
    {
        $bracket = new Bracket();
        $bracket->tournament_id = $tournamentId;
        $bracket->fixture_id = $fixtureId;
        $bracket->round = $round;
        $bracket->clan_1 = $clans[0];
        $bracket->clan_2 = $clans[1];
        $bracket->winner = $winner;
        $bracket->save();
    }

    public function update($id,$tournamentId)
    {
        $fixtureResultRepo = new FixtureResultRepository();
        $fixtureResult = $fixtureResultRepo->getByFixtureId($id);
        $winner = $this->calculateFixtureResult($fixtureResult->clan_1_score,$fixtureResult->clan_2_score);
        $bracket = Bracket::where('fixture_id',$id)->first();
        $round = $bracket->round + 1;
        if($winner == 1){
            $this->createDynamicBracket(null,$tournamentId,$round,$fixtureResult->clan_1_id);
        } else {
            $this->createDynamicBracket(null,$tournamentId,$round,$fixtureResult->clan_2_id);
        }
    }

    public function delete($id)
    {
        $fixture = Bracket::find($id);
        $fixture->delete();
        $fixtureResults = FixtureResult::where('fixture_id', $id)->get();
        foreach ($fixtureResults as $fixtureResult) {
            $fixtureResult->delete();
        }
    }

    /**
     * @param $request
     * @return bool
     */
    public function createDynamicBracket($fixtureId,$tournamentId,$round,$clan)
    {
        $roundNumber = "round_$round";
        $bracket = new Bracket();
        $bracket->tournament_id = $tournamentId;
        $bracket->fixture_id = $fixtureId;
        $bracket->round = $round;
        $bracket->$roundNumber = $clan;
        $bracketId = $bracket->save();
        return $bracketId;
    }

    public function getByClanId($clanId)
    {
        return Fixture::where('clan_1_id', $clanId)->orWhere('clan_2_id', $clanId)->get();
    }

    public function getFixturesOlderThanSevenDays()
    {
        return Fixture::where('date', '<=', Carbon::now()->subDays(7)->toDateTimeString())->where('result', '=',
            null)->get();
    }

    public function getById($id)
    {
        return Fixture::find($id);
    }

    public function getByRound($round)
    {
        return Bracket::where('round',$round)->get();
    }

    public function getByTournamentId($id)
    {
        return Bracket::where('tournament_id',$id)->get();
    }

    public function getByFixtureId($id)
    {
        return Fixture::find($id);
    }

    public function calculateFixtureResult($clan1Score,$clan2Score){
        $winner = null;
        if($clan1Score > $clan2Score){
            $winner = 1;
        } elseif($clan2Score > $clan1Score) {
            $winner = 2;
        }
        return $winner;
    }

}