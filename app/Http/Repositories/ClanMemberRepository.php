<?php namespace App\Http\Repositories;

use App\ClanMembers;
use App\User;

class ClanMemberRepository
{
    public function getById($id){
        return ClanMembers::find($id);
    }

    public function getAll(){
        return ClanMembers::all();
    }

    public function getByClanId($id){
        return ClanMembers::where('clan_id',$id)->get();
    }

    public function getApprovedClanMembersById($id){
        return ClanMembers::where('clan_id', $id)->where('approval_id', 1)->paginate(15);
    }

    public function getUnApprovedClanMembersByClanIdPaginated($id,$paginate){
        return ClanMembers::where('clan_id', $id)->where('approval_id', 0)->paginate($paginate);
    }

    public function getByUserId($userId){
        return ClanMembers::where('user_id',$userId)->first();
    }

    public function approveClanMember($request){
        $member = ClanMembers::where('user_id',$request->get('userId'))->first();
        $member->approval_id = $request->get('selected');
        $member->save();
        return $member;
    }

    public function dontApproveClanMemberAndRemoveApplication($request){
        $member = ClanMembers::where('user_id',$request->get('userId'))->first();
        $member->delete();
        $user = User::where('id',$request->get('userId'))->first();
        $user->clan_id = 0;
        $user->save();
        return $member;
    }

    public function create($user,$clan){
        $newClanMember = new ClanMembers();
        $newClanMember->user_id = $user->id;
        $newClanMember->clan_id = $clan->id;
        $newClanMember->approval_id = 0;
        $newClanMember->save();
    }
}
