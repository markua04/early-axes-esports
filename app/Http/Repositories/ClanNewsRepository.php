<?php namespace App\Http\Repositories;

use App\ClanNews;

class ClanNewsRepository
{
    public function getById($id){
        return ClanNews::find($id);
    }

    public function getBySlug($slug){
        return ClanNews::where('slug',$slug)->first();
    }

    public function getAll(){
        return ClanNews::all();
    }
}
