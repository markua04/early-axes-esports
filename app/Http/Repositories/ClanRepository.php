<?php namespace App\Http\Repositories;

use App\Clan;
class ClanRepository
{
    public function getById($id){
        return Clan::find($id);
    }

    public function getAll(){
        return Clan::all();
    }

    public function getByUserId($userId){
        return Clan::where('clan_owner_id',$userId)->first();
    }

    public function getByName($name){
        return Clan::where('clan_name',$name)->first();
    }

    public function getByClanStatus($status){
        return Clan::where('clan_status', $status)->get();
    }

    public function getByLikeClanName($q){
        return Clan::where ( 'clan_name', 'LIKE', '%' . $q . '%' )->paginate (10)->setPath ( '' );
    }

    public function getAllByActiveClanStatus($pagination){
        return Clan::where('clan_status', 1)->paginate($pagination);
    }

    public function updateClanStatus($request){
        $clan = Clan::where('id',$request->get('clanId'))->first();
        $clan->clan_status = $request->get('selected');
        $clan->save();
        return $clan;
    }

    public function getClanName($id){
        $clan = Clan::find($id);
        return $clan->clan_name;
    }
}
