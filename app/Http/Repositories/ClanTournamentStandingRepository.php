<?php namespace App\Http\Repositories;

use App\ClanTournamentStanding;
use Illuminate\Support\Facades\Request;

class ClanTournamentStandingRepository
{
    public function create($clan, $tournamentId, $clanWinsLossesDraws, $points, $tournamentPlatform){
        $tournamentRepo = new TournamentRepository();
        $tournament = $tournamentRepo->getById($tournamentId);
        $clanStandings = new ClanTournamentStanding();
        $clanStandings->clan_id = $clan;
        $clanStandings->tournament_id = $tournamentId;
        $clanStandings->wins = $clanWinsLossesDraws['wins'];
        $clanStandings->losses = $clanWinsLossesDraws['losses'];
        $clanStandings->draws = $clanWinsLossesDraws['draws'];
        $clanStandings->points = $points;
        $clanStandings->game_id = $tournament->tournament_game;
        $clanStandings->platform = isset($tournamentPlatform) ? $tournamentPlatform : null;
        $clanStandings->forfeit = null;
        return $clanStandings->save();
    }

    /**
     * @param $id
     * @param $clanStandings
     * @param $clanWinsLossesDraws
     * @param $points
     * @return
     */
    public function edit($id, $clanStandings, $clanWinsLossesDraws,$points, $tournamentPlatform)
    {
        $clanTournamentStanding = ClanTournamentStanding::find($id);
        $clanTournamentStanding->points = $points;
        $clanTournamentStanding->wins = $clanWinsLossesDraws['wins'];
        $clanTournamentStanding->losses = $clanWinsLossesDraws['losses'];
        $clanTournamentStanding->draws = $clanWinsLossesDraws['draws'];
        $clanTournamentStanding->played = $clanStandings->played + 1;
        $clanTournamentStanding->forfeit = null;
        $clanTournamentStanding->game_id = isset($clanTournamentStanding->game_id) ? $clanTournamentStanding->game_id : null;
        $clanTournamentStanding->platform = isset($tournamentPlatform) ? $tournamentPlatform : null;
        return $clanTournamentStanding->save();
    }

    /**
     * @param $id
     * @param $clanStandings
     * @return
     */
    public function editStandings($id, $clanStandings)
    {
        $clanTournamentStanding = ClanTournamentStanding::find($id);
        $clanTournamentStanding->points = $clanStandings['points'];
        $clanTournamentStanding->wins = $clanStandings['wins'];
        $clanTournamentStanding->losses = $clanStandings['losses'];
        $clanTournamentStanding->draws = $clanStandings['draws'];
        $clanTournamentStanding->played = $clanStandings['played'];
        $clanTournamentStanding->forfeit = $clanStandings['forfeit'];
        $clanTournamentStanding->game_id = isset($clanTournamentStanding->game_id) ? $clanTournamentStanding->game_id : null;
        $clanTournamentStanding->platform = isset($tournamentPlatform) ? $tournamentPlatform : null;
        return $clanTournamentStanding->save();
    }

    public function getByTournamentId($id){
        return ClanTournamentStanding::where('tournament_id',$id)->get();
    }

    public function getStandingsByClanAndTournamentId($clan,$tournamentId){
        return ClanTournamentStanding::where('clan_id',$clan)->where('tournament_id',$tournamentId)->first();
    }

    public function getByClanAndTournament($clan,$tournamentId){
        return ClanTournamentStanding::where('clan_id',$clan)->where('tournament_id',$tournamentId)->first();
    }
}
