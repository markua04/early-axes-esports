<?php namespace App\Http\Repositories;

use App\Contact;

class ContactRepository
{
    /**
     * @param $request
     * @return bool
     */
    public function create($request)
    {
        $contact = new Contact();
        $contact->name = $request->name;
        $contact->surname = $request->surname;
        $contact->email = $request->email;
        $contact->contact_number = $request->contact_number;
        $contact->description = $request->description;
        return $contact->save();
    }
}
