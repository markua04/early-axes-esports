<?php namespace App\Http\Repositories;

use App\Clan;
use App\Fixture;
use App\FixtureResult;
use Illuminate\Support\Carbon;

class FixtureRepository
{
    /**
     * @param $request
     * @return bool
     */
    public function create($request)
    {
        $clan1 = Clan::find($request->clan_1);
        $clan2 = Clan::find($request->clan_2);
        if ($request->clan_1 && $request->clan_2) {
            $clan1Image = isset($clan1->clan_logo) ? $clan1->clan_logo : '/images/default-no-image-eax.jpeg';
            $clan2Image = isset($clan2->clan_logo) ? $clan2->clan_logo : '/images/default-no-image-eax.jpeg';
        }
        $fixture = new Fixture();
        $fixture->date = $request->date;
        $fixture->time = $request->time;
        $fixture->tournament_id = $request->tournament_id;
        $fixture->clan_1 = isset($clan1->clan_name) ? $clan1->clan_name : '';
        $fixture->clan_2 = isset($clan2->clan_name) ? $clan2->clan_name : '';
        $fixture->clan_1_id = $clan1->id;
        $fixture->clan_2_id = $clan2->id;
        $fixture->clan_1_image = $clan1Image;
        $fixture->clan_2_image = $clan2Image;
        $fixture->pool = $request->pool;
        $fixture->bracket = (int)$request->bracket == 1 ? 1 : 0;
        $fixture->save();
        return $fixture->id;
    }

    public function update($request, $resultImage, $id)
    {
        $clan1 = Clan::find($request->clan_1);
        $clan2 = Clan::find($request->clan_2);
        if ($request->clan_1 && $request->clan_2) {
            $clan1Image = isset($clan1->clan_logo) ? $clan1->clan_logo : '/images/default-no-image-eax.jpeg';
            $clan2Image = isset($clan2->clan_logo) ? $clan2->clan_logo : '/images/default-no-image-eax.jpeg';
        }
        $fixture = Fixture::find($id);
        $fixture->result = $resultImage;
        $fixture->date = $request->get('date');
        $fixture->time = $request->get('time');
        $fixture->tournament_id = $request->tournament_id;
        $fixture->clan_1 = $clan1->clan_name;
        $fixture->clan_2 = $clan2->clan_name;
        $fixture->clan_1_image = $clan1Image;
        $fixture->clan_2_image = $clan2Image;
        $fixture->save();
    }

    public function delete($id)
    {
        $fixture = Fixture::find($id);
        $fixture->delete();
        $fixtureResults = FixtureResult::where('fixture_id', $id)->get();
        foreach ($fixtureResults as $fixtureResult) {
            $fixtureResult->delete();
        }
    }

    /**
     * @param $request
     * @return bool
     */
    public function createAutoFixture($matchUp)
    {
        $clan1 = Clan::find($request->clan_1);
        $clan2 = Clan::find($request->clan_2);
        if ($request->clan_1 && $request->clan_2) {
            $clan1Image = isset($clan1->clan_logo) ? $clan1->clan_logo : '/images/default-no-image-eax.jpeg';
            $clan2Image = isset($clan2->clan_logo) ? $clan2->clan_logo : '/images/default-no-image-eax.jpeg';
        }
        $fixture = new Fixture();
        $fixture->date = $request->date;
        $fixture->time = $request->time;
        $fixture->tournament_id = $request->tournament_id;
        $fixture->clan_1 = isset($clan1->clan_name) ? $clan1->clan_name : '';
        $fixture->clan_2 = isset($clan2->clan_name) ? $clan2->clan_name : '';
        $fixture->clan_1_id = $clan1->id;
        $fixture->clan_2_id = $clan2->id;
        $fixture->clan_1_image = $clan1Image;
        $fixture->clan_2_image = $clan2Image;
        $fixture->pool = $request->pool;
        $fixture->bracket = (int)$request->bracket == 1 ? 1 : 0;
        $fixture->save();
        return $fixture->id;
    }

    public function getByClanId($clanId)
    {
        return Fixture::where('clan_1_id', $clanId)->orWhere('clan_2_id', $clanId)->get();
    }

    public function getFixturesOlderThanSevenDays()
    {
        return Fixture::where('date', '<=', Carbon::now()->subDays(7)->toDateTimeString())->where('result', '=',
            null)->get();
    }

    public function getById($id)
    {
        return Fixture::find($id);
    }

    public function getByFixtureId($id)
    {
        return Fixture::find($id);
    }

    public function checkIfPoolExists($tournamentId,$pool){
        $fixture = Fixture::where('tournament_id',$tournamentId)->where('pool',$pool)->first();
        return $fixture;
    }

    public function deleteAutoFixturesByPoolAndTournament($tournamentId,$pool){
        $fixtures = Fixture::where('tournament_id',$tournamentId)->where('pool',$pool)->get();
        foreach ($fixtures as $fixture) {
            $fixture->delete();
        }
        return true;
    }

}