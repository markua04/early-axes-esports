<?php namespace App\Http\Repositories;

use App\Fixture;
use App\FixtureResult;
use App\Http\Controllers\FixturesController;
use App\Http\Controllers\StandingsController;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class FixtureResultRepository
{
    public function forfeitBothClans($id, $clan1, $clan2)
    {
        $fixture = Fixture::find($id);
        $tournamentRepo = new TournamentRepository();
        $tournament = $tournamentRepo->findById($fixture->tournament_id);
        if($clan1 !== null && $clan2 !== null && $fixture !== null && $tournament !== null){
            $fixtureResultExists = FixtureResult::where('clan_1',$clan1->id)->where('clan_2',$clan2->id)->where('tournament_id',$fixture->tournament_id)->where('fixture_id',$fixture->id)->first();
            if($fixtureResultExists == null){
                Log::info("Updating Fixture Result for : ". $clan1->clan_name. " VS " . $clan2->clan_name);
                $fixtureResult = new FixtureResult();
                $fixtureResult->fixture_id = $fixture->id;
                $fixtureResult->tournament_id = $fixture->tournament_id;
                $fixtureResult->clan_1 = $clan1->clan_name;
                $fixtureResult->clan_2 = $clan2->clan_name;
                $fixtureResult->clan_1_id = $clan1->id;
                $fixtureResult->clan_2_id = $clan2->id;
                $fixtureResult->clan_1_score = 0;
                $fixtureResult->clan_2_score = 0;
                $fixtureResult->clan_1_points = 0;
                $fixtureResult->clan_2_points = 0;
                $fixtureResult->forfeit = 1;
                $fixtureResult->save();
                $fixture->result = 1;
                $fixture->forfeit = 1;
                $verdict = $fixture->save();
                if($verdict !== true){
                    Log::error("Fixture " .$fixture->id." has failed to save result");
                    Log::info("Fixture " .$fixture->id." has failed to save result");
                }
                $clanTournamentStandingsRepo = new ClanTournamentStandingRepository();
                $standings = $clanTournamentStandingsRepo->getByTournamentId($tournament->id);
                if($standings !== null){
                    $clanFixturesController = new FixturesController();
                    $standingsController = new StandingsController();
                    $clan1WinsLossesDraws = $standingsController->getClanWinsLossesDrawsForTournament($clan1,$tournament);
                    $clan2WinsLossesDraws = $standingsController->getClanWinsLossesDrawsForTournament($clan2,$tournament);
                    $clanFixturesController->saveForfeitedClanStanding($clan1,$fixture->tournament_id,0,$clan1WinsLossesDraws);
                    $clanFixturesController->saveForfeitedClanStanding($clan2,$fixture->tournament_id,0,$clan2WinsLossesDraws);
                }
            }
        } elseif($tournament == null){
            $fixtureResult = FixtureResult::where('fixture_id',$fixture->id)->first();
            if($fixtureResult !== null){
                $verdict = $fixtureResult->delete();
                if($verdict == true){
                    Log::info("Fixture Result deleted because tournament no longer exists.");
                }
            }
            $verdict = $fixture->delete();
            if($verdict == true){
                Log::info("Fixture deleted because tournament no longer exists.");
            }
        }
    }

    public function getByFixtureId($id){
        return FixtureResult::where('fixture_id',$id)->first();
    }

    public function getFixtureResult($fixtureId){
        return FixtureResult::where('fixture_id',$fixtureId)->first();
    }

    public function getByClanId($clanId){
        return FixtureResult::where('clan_1_id',$clanId)->orWhere('clan_2_id',$clanId)->paginate(20);
    }

    public function getByTournamentId($id){
        return FixtureResult::where('tournament_id',$id)->get();
    }

    public function getByClanAndTournamentId($clanId,$tournamentId){
        return DB::select("SELECT * FROM fixture_results where (tournament_id = $tournamentId and clan_1_id = $clanId) or (tournament_id = $tournamentId and clan_2_id = $clanId)");
    }
}
