<?php namespace App\Http\Repositories;

use App\GamesList;
class GamesListRepository
{
    public function getById($id){
        return GamesList::find($id);
    }

    public function getAll(){
        return GamesList::all();
    }
}
