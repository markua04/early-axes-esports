<?php namespace App\Http\Repositories;

use App\Http\Controllers\IndividualFixturesController;
use App\Http\Controllers\TournamentController;
use App\IndividualFixture;
use App\IndividualFixtureResult;

class IndividualFixtureRepository
{
    /**
     * @param $request
     * @return bool
     */
    public function create($request,$data)
    {
        $fixture = new IndividualFixture();
        $individualFixturesController = new IndividualFixturesController();
        $tournamentService = new TournamentRepository();
        $tournament = $tournamentService->findById($request->get('tournament_id'));
        $fixture->date = $request->date;
        $fixture->time = $request->time;
        $fixture->tournament_id = $request->get('tournament_id');
        $fixture->player_1_id = $data['player_1'];
        $fixture->player_2_id = $data['player_2'];
        $fixture->player_1_gt = $individualFixturesController->getGamerTagByTournamentPlatform($tournament,$data['player_1']);
        $fixture->player_2_gt = $individualFixturesController->getGamerTagByTournamentPlatform($tournament,$data['player_2']);
        $fixture->pool = $request->pool;
        return $fixture->save();
    }

    public function delete($id){
        $fixture = IndividualFixture::find($id);
        return $fixture->delete();
    }

    public function getByTournamentAndUser($tournament,$user){
        return IndividualFixtureResult::where('tournament_id',$tournament->id)->where('player_1',$user->id)->orWhere('player_2',$user->id)->get();
    }

    public function findById($id){
        return IndividualFixture::find($id);
    }

    public function getByTournamentId($id){
        return IndividualFixture::orderBy('created_at', 'desc')->where('tournament_id',$id)->get();
    }

    public function getByUser($user){
        return IndividualFixture::where('player_1_id',$user['id'])->orWhere('player_2_id',$user['id'])->get();
    }

    public function resultFixture($id){
        $fixtureRepo = new IndividualFixtureRepository();
        $fixture = $fixtureRepo->findById($id);
        $fixture->resulted = 1;
        return $fixture->save();
    }

    public function addMissingGamerTagsForFixture($fixture){
        $player1Gt = null;
        $player2Gt = null;
        $tournamentController = new TournamentController();
        $tournamentRepo = new TournamentRepository();
        $tournament = $tournamentRepo->findById($fixture->tournament_id);
        if($fixture->player_1_gt == null){
            $player1Gt = $tournamentController->getUsersGamerTagAccordingByTournament($fixture->player_1_id,$tournament);
        }
        if($fixture->player_2_gt == null){
            $player2Gt = $tournamentController->getUsersGamerTagAccordingByTournament($fixture->player_2_id,$tournament);
        }
        $fixture->player_1_gt = $player1Gt;
        $fixture->player_2_gt = $player2Gt;
        $fixture->save();
    }

    public function getByUserId($userId){
        return IndividualFixtureResult::where('player_1',$userId)->orWhere('player_2',$userId)->get();
    }
}
