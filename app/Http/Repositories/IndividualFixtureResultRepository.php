<?php namespace App\Http\Repositories;

use App\IndividualFixtureResult;

class IndividualFixtureResultRepository
{
    /**
     * @param $request
     * @param $fixtureId
     * @param $tournamentId
     * @param $player1Id
     * @param $player2Id
     */
    public function create($request, $fixtureId, $tournamentId, $player1Id, $player2Id)
    {
        $fixtureResult = new IndividualFixtureResult();
        $fixtureResult->fixture_id = $fixtureId;
        $fixtureResult->tournament_id = $tournamentId;
        $fixtureResult->player_1 = $player1Id;
        $fixtureResult->player_2 = $player2Id;
        $fixtureResult->player_1_score = $request->player_1_score;
        $fixtureResult->player_2_score = $request->player_2_score;
        $fixtureResult->save();
    }

    /**
     * @param $tournament
     * @param $user
     * @return mixed
     */
    public function getByTournamentAndUser($tournament, $user){
        return IndividualFixtureResult::where('tournament_id',$tournament->id)
            ->where('player_1',$user->id)
            ->orWhere('player_2',$user->id)->get();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function findById($id){
        return IndividualFixtureResult::find($id);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function findByFixtureId($id){
        return IndividualFixtureResult::find($id);
    }

    /**
     * @param $player1
     * @param $player2
     * @param $tournament
     * @param $fixture
     * @return mixed
     */
    public function getFixtureResultsByPlayersAndTournament($player1, $player2, $tournament, $fixture){
        return IndividualFixtureResult::where('player_1',$player1->id)
            ->where('player_2',$player2->id)
            ->where('tournament_id',$tournament->id)
            ->where('fixture_id',$fixture->id)->first();
    }
}
