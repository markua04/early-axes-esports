<?php namespace App\Http\Repositories;

use App\IndividualTournamentStanding;

class IndividualTournamentStandingRepository
{
    const FIFA = 15;
    public function create($playerWinsLossesDraws,$playerStandings,$tournament,$points,$playerGoals,$player){
        if($playerStandings !== null){
            $playerStandings->points = $playerStandings->points + $points;
            $playerStandings->wins = $playerWinsLossesDraws['wins'];
            $playerStandings->losses = $playerWinsLossesDraws['losses'];
            $playerStandings->draws = $playerWinsLossesDraws['draws'];
            if($tournament->tournament_game == self::FIFA){
                $playerStandings->goals_for = $playerGoals['goalsFor'];
                $playerStandings->goals_against = $playerGoals['goalsAgainst'];
                $playerStandings->difference = $playerGoals['goalsFor'] - $playerGoals['goalsAgainst'];
            }
            $playerStandings->save();
        } else {
            $playerStandings = new IndividualTournamentStanding();
            $playerStandings->user_id = $player;
            $playerStandings->tournament_id = $tournament->id;
            $playerStandings->wins = $playerWinsLossesDraws['wins'];
            $playerStandings->losses = $playerWinsLossesDraws['losses'];
            $playerStandings->draws = $playerWinsLossesDraws['draws'];
            $playerStandings->points = $points;
            if($tournament->tournament_game == self::FIFA){
                $playerStandings->goals_for = $playerGoals['goalsFor'];
                $playerStandings->goals_against = $playerGoals['goalsAgainst'];
                $playerStandings->difference = $playerGoals['goalsFor'] - $playerGoals['goalsAgainst'];
            }
            $playerStandings->save();
        }
    }

    /**
     * @param $id
     * @param $data
     * @param $tournament
     * @return mixed
     */
    public function edit($id, $data)
    {
        $userTournamentStanding = IndividualTournamentStanding::find($id);
        $userTournamentStanding->points = isset($data['points']) ? $data['points'] : 0;
        $userTournamentStanding->bonus_points = isset($data['bonus_points']) ? $data['bonus_points'] : 0;
        $userTournamentStanding->wins = $data['wins'];
        $userTournamentStanding->losses = $data['losses'];
        $userTournamentStanding->draws = $data['draws'];
        $userTournamentStanding->goals_for = isset($data['goals_for']) ? $data['goals_for'] : 0;
        $userTournamentStanding->goals_against = isset($data['goals_against']) ? $data['goals_against'] : 0;
        $userTournamentStanding->difference = $data['goals_for'] - $data['goals_against'];
        return $userTournamentStanding->save();
    }

    public function getById($id){
        return IndividualTournamentStanding::find($id);
    }

    public function getByTournamentId($id){
        $tournamentService = new TournamentRepository();
        $tournament = $tournamentService->findById($id);
        if($tournament->tournament_game == self::FIFA){
            return IndividualTournamentStanding::where('tournament_id',$id)->orderBy('points','desc')->orderBy('difference','desc')->get();
        } else {
            return IndividualTournamentStanding::where('tournament_id',$id)->orderBy('points','desc')->get();
        }
    }

    public function getByTournamentPlayerAndId($userId,$tournamentId){
        return IndividualTournamentStanding::where('user_id',$userId)->where('tournament_id',$tournamentId)->first();
    }
}
