<?php namespace App\Http\Repositories;

use App\ClanTournamentStanding;
use App\OverallClanStanding;

class OverallClanStandingsRepository
{
    public function getById($id){
        return ClanTournamentStanding::find($id);
    }

    public function getAll(){
        return ClanTournamentStanding::all();
    }

    public function create($clan,$tournamentId,$clanWinsLossesDraws,$points){
        $clanStandings = new OverallClanStanding();
        $clanStandings->clan_id = $clan;
        $clanStandings->tournament_id = $tournamentId;
        $clanStandings->wins = $clanWinsLossesDraws['wins'];
        $clanStandings->losses = $clanWinsLossesDraws['losses'];
        $clanStandings->draws = $clanWinsLossesDraws['draws'];
        $clanStandings->points = $points;
        $clanStandings->played = 1;
        $clanStandings->save();
    }

//    public function edit(){
//        $clanStandings->points = $clanStandings->points + $points;
//        $clanStandings->wins = $clanWinsLossesDraws['wins'];
//        $clanStandings->losses = $clanWinsLossesDraws['losses'];
//        $clanStandings->draws = $clanWinsLossesDraws['draws'];
//        $clanStandings->played = $clanStandings->played + 1;
//        $clanStandings->save();
//    }

}
