<?php namespace App\Http\Repositories;

use App\TournamentIndividualRegisteredPlayers;
class TournamentIndividualRegisteredPlayersRepository
{
    public function getByTournamentId($id){
        return TournamentIndividualRegisteredPlayers::where('tournament_id',$id)->get();
    }
}
