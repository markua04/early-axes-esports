<?php namespace App\Http\Repositories;

use App\TournamentRegisteredPlayers;

class TournamentRegisteredPlayersRepository
{
    public function getById($id){
        return TournamentRegisteredPlayers::find($id);
    }

    public function create($playerId,$tournamentId,$clanId){
        $tournamentRegisteredPlayer = new TournamentRegisteredPlayers();
        $tournamentRegisteredPlayer->clan_member_id = $playerId;
        $tournamentRegisteredPlayer->tournament_id = $tournamentId;
        $tournamentRegisteredPlayer->clan_id = $clanId;
        return $tournamentRegisteredPlayer->save();
    }

    public function getPlayersByClanAndTournamentId($clanId,$tournamentId){
        $matchThese = ['tournament_id' => $tournamentId, 'clan_id' => $clanId];
        return TournamentRegisteredPlayers::where($matchThese)->get();
    }

    public function deleteByPlayerAndTournamentId($playerId,$tournamentId){
        $player = TournamentRegisteredPlayers::where('clan_member_id',$playerId)->where('tournament_id',$tournamentId)->first();
        return $player->delete();
    }

    public function getByClanMemberId($userId){
        return TournamentRegisteredPlayers::where('clan_member_id',$userId)->get();
    }

    public function getByTournamentId($id){
        return TournamentRegisteredPlayers::where('tournament_id',$id)->get();
    }
}
