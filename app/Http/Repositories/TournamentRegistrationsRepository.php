<?php namespace App\Http\Repositories;

use App\TournamentRegistrations;
class TournamentRegistrationsRepository
{
    public function getByTournamentId($id){
        return TournamentRegistrations::where('tournament_id',$id)->get();
    }
}
