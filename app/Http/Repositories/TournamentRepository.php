<?php namespace App\Http\Repositories;
use App\Clan;
use App\GamesList;
use App\Http\Requests\TournamentRequest;
use App\Http\Requests\TournamentRulesRequest;
use App\Platform;
use App\Tournament;
use App\TournamentRegisteredPlayers;
use App\TournamentRegistrations;
use App\TournamentRules;
use App\TournamentTypes;
use App\TournamentStandings;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TournamentRepository
{
    /**
     * @param $request
     * @return bool
     */
    public function create($request)
    {
        $tournament = new Tournament();
        $tournament->tournament_banner = $request->tournament_banner;
        $tournament->tournament_title = $request->tournament_title;
        $tournament->tournament_info = $request->tournament_info;
        $tournament->tournament_type_id = $request->tournament_type_id;
        $tournament->tournament_prize = $request->tournament_prize;
        $tournament->tournament_game = $request->tournament_game;
        $tournament->tournament_player_limit = $request->tournament_player_limit;
        $tournament->tournament_admin_id = Auth::user()->getAttributes()['id'];
        $tournament->tournament_status = $request->tournament_status;
        $tournament->tournament_platform = $request->tournament_platform;
        $tournament->tournament_registrations = $request->tournament_registrations;
        $tournament->challonge_bracket_1 = $request->challonge_bracket_1;
        $tournament->toornament_standings = $request->toornament_standings;
        $tournament->win_points = $request->win_points;
        $tournament->loss_points = $request->loss_points;
        $tournament->draw_points = $request->draw_points;
        $tournament->genre = $request->genre;
        $tournament->standings_type = $request->standings_type;
        $tournament->discord_link = $request->discord_link;
        return $tournament->save();
    }


    /**
     * @param $id
     * @param TournamentRequest $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function update($id, TournamentRequest $request)
    {
        $tournament = Tournament::find($id);
        $tournamentBanner = $tournament->tournament_banner;
        $tournamentRules = $tournament->tournament_rules;
        $tournament->tournament_title = $request->get('tournament_title');
        $tournament->tournament_info = $request->get('tournament_info');
        $tournament->tournament_rules = $tournamentRules;
        $tournament->tournament_type_id = $request->get('tournament_type_id');
        $tournament->tournament_player_limit = $request->get('tournament_player_limit');
        $tournament->winner = $request->get('winner');
        $tournament->tournament_game = $request->get('tournament_game');
        $tournament->tournament_platform = $request->get('tournament_platform');
        $tournament->genre = $request->get('genre');
        $tournament->tournament_status = $request->get('tournament_status');
        $tournament->genre = $request->get('genre');
        $tournament->toornament_standings = $request->get('toornament_standings');
        $tournament->tournament_status = $request->get('tournament_status');
        $tournament->tournament_banner = $request->get('tournament_banner');
        $tournament->tournament_registrations = $request->get('tournament_registrations');
        $tournament->win_points = $request->get('win_points');
        $tournament->loss_points = $request->get('loss_points');
        $tournament->draw_points = $request->get('draw_points');
        if($request->tournament_banner == null){
            $tournament->tournament_banner = $tournamentBanner;
        } else {
            $tournament->tournament_banner = $request->get('tournament_banner');
        }
        $tournament->standings_type = $request->standings_type;
        $tournament->discord_link = $request->discord_link;
        $tournament->save();
        return redirect("/admin/list-tournaments")->with('status', 'Tournament Updated!');
    }

    /**
     * @param $id
     * @return int
     */
    public static function delete($id)
    {
        return Tournament::destroy($id);
    }

    /**
     * @param $id
     * @return int
     */
    public function findById($id)
    {
        return Tournament::find($id);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function deleteTournamentRules($id)
    {
        if (Auth::user()->getAttributes()['user_level_id'] == 1) {
            TournamentRules::where('tournament_id', $id)->destroy($id);
            return redirect()->back()->with('status', 'Tournament Rules Deleted');
        } else {
            return redirect()->back()->with('status', 'Your permissions do not allow this.');
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function deleteTournamentStandings($id)
    {
        if (Auth::user()->getAttributes()['user_level_id'] == 1) {
            TournamentStandings::where('tournament_id', $id)->delete($id);
            $tournament = Tournament::find($id);
            $tournament->tournament_standings = null;
            $tournament->save();
            return redirect('/admin/manage-tournaments')->with('status', 'Tournament Standing Deleted');
        } else {
            return redirect()->back()->with('status', 'Your permissions do not allow this.');
        }
    }

    /**
     * @param $clanId
     * @return int
     */
    public function deleteClan($clanId)
    {
        if (Auth::user()->getAttributes()['user_level_id'] == 1) {
            $users = Clan::destroy($clanId);
            return $users;
        } else {
            return false;
        }
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function manageTournaments(Request $request)
    {
        $games = GamesList::all();
        $platforms = Platform::all();
        if(!Auth::user()){
            $userLevelId = 3;
        } else {
            $userLevelId = Auth::user()->getAttributes()['user_level_id'];
        }
        if ($userLevelId == 1) {
            if($request->get('game') !== null && $request->get('platform') !== null) {
                $tournaments = Tournament::where('tournament_game',$request->get('game'))->where('tournament_platform',$request->get('platform'))->orderBy('created_at','desc')->get();
            } elseif($request->get('game') !== null && $request->get('platform') == null){
                $tournaments = Tournament::where('tournament_game',$request->get('game'))->orderBy('created_at','desc')->get();
            } elseif($request->get('platform') !== null && $request->get('game') == null){
                $tournaments = Tournament::where('tournament_platform',$request->get('platform'))->orderBy('created_at','desc')->get();
            } else {
                $tournaments = Tournament::orderBy('created_at','desc')->get();
            }
            return view('/admin/tournaments/list-tournaments', compact('tournaments','games','platforms'));
        }
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function listTournaments(Request $request)
    {
        $games = GamesList::all();
        $platforms = Platform::all();
        if($request->get('game') !== null && $request->get('platform') !== null) {
            $tournaments = Tournament::where('tournament_game',$request->get('game'))->where('tournament_platform',$request->get('platform'))->where('tournament_status',1)->orderBy('created_at','desc')->paginate(10);
        } elseif($request->get('game') !== null && $request->get('platform') == null){
            $tournaments = Tournament::where('tournament_game',$request->get('game'))->where('tournament_status',1)->orderBy('created_at','desc')->paginate(10);
        } elseif($request->get('platform') !== null && $request->get('game') == null){
            $tournaments = Tournament::where('tournament_platform',$request->get('platform'))->where('tournament_status',1)->orderBy('created_at','desc')->paginate(10);
        } else {
            $tournaments = Tournament::where('tournament_status',1)->orderBy('created_at','desc')->paginate(10);
        }
        $tournamentTypes = TournamentTypes::all();
        if(Auth::user()['user_level_id'] == 1){
            return view('/frontend/tournaments/list-tournaments', ['tournaments' => $tournaments,'tournamentTypes' => $tournamentTypes, 'games' => $games, 'platforms' => $platforms]);
        } else {
            if($tournaments->total() == 0){
                if($request->get('game')){
                    return $this->comingSoon('Tournaments',$request->get('game'));
                } else {
                    return $this->comingSoon('Tournaments');
                }
            } else {
                return view('/frontend/tournaments/list-tournaments',compact('tournaments','tournamentTypes','games','platforms'));
            }
        }
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getClanMembersRegistered($tournamentId)
    {

        $clans = [];
        $registeredClans = TournamentRegistrations::where('tournament_id', '=', $tournamentId)->get();
        foreach ($registeredClans as $clan) {
            $clanMemberCollection = [];
            $clanEntity = Clan::where('id', '=', $clan->clan_id)->first();
            $clanMembers = TournamentRegisteredPlayers::where('clan_id', '=', $clan->clan_id)->where('tournament_id', $tournamentId)->get();
            foreach ($clanMembers as $clanMember) {
                $clanMemberCollection[] = $clanMember;
            }
            $clanMemberCollection['forfeit'] = $clan->forfeit;
            $clans[$clanEntity->clan_name] = $clanMemberCollection;
        }
        return $clans;
    }


    public function getClanMembersRegisteredExcel($tournamentId)
    {

        $clans = [];
        $registeredClans = TournamentRegistrations::where('tournament_id', '=', $tournamentId)->get();

        foreach ($registeredClans as $clan) {
            $clanMemberCollection = [];
            $clanEntity = Clan::where('id', '=', $clan->clan_id)->first();
            $clanMembers = TournamentRegisteredPlayers::where('clan_id', '=', $clan->clan_id)->get();
            foreach ($clanMembers as $clanMember) {
                $clanMemberCollection[] = $clanMember;
            }
            $clans[$clanEntity->clan_name] = $clanMemberCollection;
        }
        return $clans;
    }

    /**
     * @param TournamentRulesRequest $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function createRules(TournamentRulesRequest $request)
    {
        $tournamentsWithNoRules = Tournament::where('tournament_rules', '=', null)->get();
        if (Auth::user()->getAttributes()['user_level_id'] == 1 && count($tournamentsWithNoRules) !== 0) {
            $tournaments = Tournament::where('tournament_rules', '=', null)->get();
            if ($request->isMethod('post')) {
                $rules = new TournamentRules;
                $rules->rules = $request->get('rules');
                $rules->tournament_id = $request->get('tournament');
                $rules->save();

                $tournamentId = $request->get('tournament');
                $getTournament = Tournament::find($tournamentId);
                $tournamentRules = TournamentRules::where('tournament_id', $tournamentId)->first();
                $getTournament->tournament_rules = $tournamentRules->id;
                $getTournament->save();
                return redirect('/view-clans')->with('status', 'Tournament rules have been created successfully');
            }
            return view('admin/tournaments/create-tournament-rules', compact('games', 'tournaments'));
        }
        return redirect()->back()->with('status', 'All tournaments have rules already.');
    }

    /**
     * @param TournamentRulesRequest $request
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function editRules(TournamentRulesRequest $request, $id)
    {
        if (Auth::user()->getAttributes()['user_level_id'] == 1) {
            $rulesEntity = TournamentRules::find($id);
            if ($request->isMethod('post')) {
                $rulesEntity = TournamentRules::find($id);
                $rulesEntity->rules = $request->get('rules');
                $rulesEntity->save();
                return redirect('/admin/list-tournaments')->with('status', 'Tournament rules have been updated successfully');
            }
            return view('admin/tournaments/edit-tournament-rules', compact('rulesEntity'));
        }
        return redirect()->back()->with('status', 'All tournaments have rules already.');
    }

    public function getById($id){
        return Tournament::find($id);
    }

    public function getAll(){
        return Tournament::all();
    }

    public function getRegisteredClansByTournamentId($id){
        return TournamentRegistrations::where('tournament_id',$id)->get();
    }

    public function getActiveTournaments(){
        return Tournament::where('tournament_status',1)->get();
    }
}
