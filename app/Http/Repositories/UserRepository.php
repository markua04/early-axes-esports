<?php namespace App\Http\Repositories;

use App\Http\Controllers\API\UserController;
use App\News;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Input;


class UserRepository
{

    const ACTIVE_USER = 1;
    const VERIFIED_USER = 1;

    public function getById($id){
        return User::find($id);
    }

    public function getByEmail($email){
        return User::where('email',$email)->first();
    }

    /**
     * @param $data
     * @return bool
     */
    public function create($data)
    {
        $user = new User();
        $user->name = $data['name'];
        $user->surname = $data['surname'];
        $user->steam_id = $data['steam_id'];
        $user->uplay_id = $data['uplay_id'];
        $user->email = $data['email'];
        $user->gamer_tag = $data['gamer_tag'];
        $user->psn_id = $data['psn_id'];
        $user->clan_id = $data['clan_id'];
        $user->user_level_id = User::USER_LEVEL;
        $user->phone_number = $data['phone_number'];
        $user->selected_source = $data['selected_source'];
        $user->gamer_pic = $data['gamer_pic'];
        $user->user_status_id = self::ACTIVE_USER;
        $user->password = bcrypt($data['password']);
        $user->gt_change = null;
        return $user->save();
    }

    public function edit($request,$id){
        $userRepo = new UserRepository();
        $user = $userRepo->getById($id);
        $user->name = $request->get('name');
        $user->surname = $request->get('surname');
        $user->email = $request->get('email');
        $user->gamer_tag = $request->get('gamer_tag');
        $user->select_image = $request->get('select_image');
        $user->steam_id = $request->get('steam_id');
        $user->psn_id = $request->get('psn_id');
        $user->uplay_id = $request->get('uplay_id');
        $user->selected_source = $request->get('selected_source');
        $user->phone_number = $request->get('phone_number');
        $user->select_image = $request->get('select_image');
        $user->gt_change = $request->gt_change;
        $user->clan_id = $request->clan_id;
        if (Input::file('image') !== null) {
            if($user->gamer_pic !== null){
                deleteImage(public_path().$user->gamer_pic);
            }
            $fileName = saveImage('uploads');
            $user->gamer_pic = "/uploads/" . $fileName;
        }
        $user->save();
    }

    public function updateAdminToken($token){
        $user = User::find(Auth::user()->id);
        $user->api_token = $token;
        $user->save();
    }

    public function getUserByToken($token){
        if($token !== null){
            $userId = Cache::get($token);
            $userController = new UserController();
            $user = $userController->getUserById($userId);
            if($user !== null){
                return response()->json($user, 200);
            }
        }
        return response()->json([], 500);
    }

    public function getUserById($id){
        if (Cache::has('user_'.$id)) {
            $user = Cache::get('user_'.$id);
        } else {
            $getUser = User::where('api_token', $id)->first();
            Cache::put('user_'.$id, $getUser,30);
            $user = Cache::get('user_'.$id);
        }
        return $user;
    }

    public function updateUserGtApproval($id){
        $user = User::find($id);
        $user->gt_change = null;
        return $user->save();
    }

    public function getUserByEmail($request){
        return User::where('email',$request->email)->first();
    }

    public function getUserByTokenData($request){
        $userId = Cache::get($request->header('Authorization'));
        return User::find($userId);
    }

    public function removeAdminToken($id){
        $user = User::find($id);
        $user->api_token = null;
        $user->save();
    }

    public function getEAXUserByToken($token){
        return User::where('api_token',$token)->first();
    }

    public function getAllPaginated($paginate){
        return User::paginate($paginate);
    }

    public function getAllNormalUsersPaginated($paginate){
        return User::where('user_level_id',3)->paginate($paginate);
    }

    public function getByUserLevelIdPaginated($level,$paginate){
        return User::where('user_level_id',$level)->paginate($paginate);
    }

    public function getUsersByGamerTagApprovals(){
        return User::whereNotNull('gt_change')->get();
    }
}
