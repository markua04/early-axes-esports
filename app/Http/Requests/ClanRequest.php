<?php namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class ClanRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
//            'title.required' => 'The title field is required.',
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if($this->isMethod('get')){
            return [];
        }
        return [
            'clan_name' => 'required|unique:clans,clan_name,'.Auth::user()->clan_id,
            'clan_email' => 'required|email|unique:clans,clan_email,'.Auth::user()->clan_id,
            'clan_description' => 'required',
            'clan_tel' => 'required',
            'clan_logo' => 'required',
        ];
    }
}
