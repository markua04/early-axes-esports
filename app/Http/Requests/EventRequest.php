<?php namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EventRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
//            'title.required' => 'The title field is required.',
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if($this->isMethod('get')){
            return [];
        }
        return [
            'title' => 'required',
            'date' => 'required',
            'description' => 'required',
            'time' => 'required',
        ];
    }
}
