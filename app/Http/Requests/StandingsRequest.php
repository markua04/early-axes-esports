<?php namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StandingsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

//    public function messages()
//    {
//        return [
//            'selected_source.required' => 'Please select a platform.',
//            'gamer_tag.required_if' => 'Xbox Gamer Tag is required if you have selected Xbox.',
//            'steam_id.required_if' => 'Steam ID is required if you have selected Steam.',
//            'psn_id.required_if' => 'PSN ID is required if you have selected Playstation.',
//        ];
//    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->isMethod('get')) {
            return [];
        }
        return [
            'points' => 'required',
            'bonus_points' => 'required',
            'wins' => 'required',
            'losses' => 'required',
            'draws' => 'required',
        ];
    }
}

