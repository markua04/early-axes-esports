<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'selected_source.required' => 'Please select a platform.',
            'gamer_tag.required_if' => 'Xbox Gamer Tag is required if you have selected Xbox.',
            'steam_id.required_if' => 'Steam ID is required if you have selected Steam.',
            'psn_id.required_if' => 'PSN ID is required if you have selected Playstation.',
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if($this->isMethod('get')){
            return [];
        }
        return [
            'gamer_tag' => 'required_if:selected_source,==,1',
            'psn_id' => 'required_if:selected_source,==,2',
            'steam_id' => 'required_if:selected_source,==,3',
            'phone_number' => 'required|regex:/(0)[0-9]{9}/',
        ];
    }
}
