<?php

use App\Clan;
use App\ClanMembers;
use App\Http\Controllers\ApiController;
use App\IndividualFixture;
use App\IndividualFixtureResult;
use App\Tournament;
use App\TournamentTypes;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Input;

/**
 * @return string
 */
function isOwnerOfClan(){
    if(!Auth::user()){
        $ownerOfClan = "No";
    } else {
        $userId = Auth::user()->id;
        $userClan = Clan::where('clan_owner_id', '=', $userId)->first();
        $ownerOfClan = "No";
        if($userClan !== null){
            $ownerOfClan = 'Yes';
        }
        return $ownerOfClan;
    }
}

function isSecondaryCaptain(){
    if(!Auth::user()){
        $secondaryCaptain = "No";
    } else {
        $userId = Auth::user()->id;
        $userClan = Clan::where('secondary_captain', $userId)->first();
        $secondaryCaptain = "No";
        if($userClan !== null){
            $secondaryCaptain = 'Yes';
        }
        return $secondaryCaptain;
    }
}

/**
 * @return string
 */
function approvedForClan($userId){
    $memberClanStatus = ClanMembers::where('user_id',$userId)->first();
    $approved = 'No';
    if(isset($memberClanStatus->approval_id) !== false){
        if($memberClanStatus->approval_id !== 0){
            $approved = 'Yes';
        }
    } else {
        $approved = 'No';
    }
    return $approved;
}

/**
 * @param $userId
 * @return string
 */
function userIsAdmin($userId){
    $user = User::find($userId);
    if($user->user_level_id == 1){
        return "yes";
    } else {
        return "no";
    }

}

/**
 * @return mixed
 */
function getClanSlug($specificRequirement = null){
    $userClanId = Auth::user()->getAttributes()['clan_id'];
    $clan = Clan::where('id', $userClanId)->first();
    $clanSlug = strtolower(str_replace(' ','-',$clan['clan_name']));
    return $clanSlug;
}

/**
 * @param $id
 * @return mixed
 */
function getClanSlugByUserClanId($id)
{
    $clan = Clan::find($id);
    $clanSlug = str_replace(' ','-',strtolower($clan['clan_name']));
    return $clanSlug;
}

/**
 * @param $seconds
 * @return string
 */
function secondsToHoursAndMinutes($seconds)
{
    $hours = floor($seconds / 3600);
    $seconds -= $hours * 3600;
    $minutes = floor($seconds / 60);
    $seconds -= $minutes * 60;
    if($hours == 0){
        $amount = $minutes.'m '.$seconds.'s';
    } else {
        $amount = $hours.'h '.$minutes.'m '.$seconds.'s';
    }
    return $amount;
}

function gamerTagChecker($gamerTag){

    if (Cache::has('xboxprofile-' . $gamerTag)) {
        $xboxProfileData = Cache::get('xboxprofile-' . $gamerTag);
    } else {
        $xboxData = new ApiController();
        $xboxProfileData = $xboxData->getXboxLiveAccount($gamerTag);
        Cache::put('xboxprofile-' . $gamerTag, $xboxProfileData, 1440);
        $xboxProfileData = Cache::get('xboxprofile-' . $gamerTag);
    }
    $decodedData = json_decode($xboxProfileData);
    return $decodedData;
}

function getLatestTournaments(){
    $tournaments = Tournament::where('tournament_status',1)->orderBy('id', 'desc')->take(3)->get();
    return $tournaments;
}

function getClanOwner($clanId){
    if($clanId !== null){
        $clan = Clan::find($clanId);
        $owner = User::find($clan->clan_owner_id);
        if($owner !== null){
            $ownerName = getGamerTag($owner);
        } else {
            $ownerName = "N/A";
        }
        return $ownerName;
    } else {
        return false;
    }
}

function getClanOwnerName($clanId){
    if($clanId !== null){
        $clan = Clan::find($clanId);
        $owner = User::find($clan->clan_owner_id);
        if(!empty($owner->name)){
            $ownerName = "$owner->name $owner->surname";
        } else {
            $ownerName = '';
        }
        return $ownerName;
    } else {
        return false;
    }
}

function getClanOwnerGamerTag($clanId){
    if($clanId !== null){
        $clan = Clan::find($clanId);
        $owner = User::find($clan->clan_owner_id);
        if(!empty($owner->name)){
            $ownerName = "$owner->gamer_tag";
        } else {
            $ownerName = '';
        }
        return $ownerName;
    } else {
        return false;
    }
}

function checkIfClanResultSubmitted($fixtureId){
    $fixture = \App\FixtureResult::where('fixture_id',$fixtureId)->first();
    if($fixture !== null){
        return 1;
    } else {
        return 0;
    }
}


function checkIfUserResultSubmitted($fixtureId){
    $fixture = \App\IndividualFixtureResult::where('fixture_id',$fixtureId)->first();
    if($fixture !== null){
        return 1;
    } else {
        return 0;
    }
}


function getFixtureResult($fixtureId){
    $fixture = \App\FixtureResult::where('fixture_id',$fixtureId)->first();
    if($fixture->clan_1_score == 0 && $fixture->clan_2_score == 0){
        return "Forfeit";
    } elseif($fixture->clan_1_score == $fixture->clan_2_score){
        return "Draw";
    } elseif($fixture->clan_1_score > $fixture->clan_2_score) {
        return "Winner: $fixture->clan_1";
    } else {
        return "Winner: $fixture->clan_2";
    }

}

function getIndividualFixtureResult($fixtureId){
    $fixtureResult = IndividualFixtureResult::where('fixture_id',$fixtureId)->first();
    $fixture = IndividualFixture::find($fixtureId);
    if($fixtureResult->player_1_score == $fixtureResult->player_2_score){
        return "DRAW";
    } elseif($fixtureResult->player_1_score > $fixtureResult->player_2_score) {
        return "Winner: " . $fixture->player_1_gt;
    } else {
        return "Winner: " . $fixture->player_2_gt;

    }
    return null;

}

function checkDynamicStandings($tournamentId){
    $tournamentStanding = \App\Standing::where('tournament_id',$tournamentId)->first();
    if($tournamentStanding !== null){
        return true;
    } else {
        return false;
    }

}

function getTournamentById($id){
    $tournament = Tournament::find($id);
    return $tournament;
}

function getTournamentTypeNameById($id){
    $tournamentType = TournamentTypes::find($id);
    $typeName = $tournamentType->tournament_type_name;
    return $typeName;
}

function getGamerTag($user){
    $tag = null;
    if(isset($user->gamer_tag) && $user->gamer_tag !== ""){
        $tag = $user->gamer_tag;
    } elseif (isset($user->psn_id) && $user->psn_id !== ""){
        $tag = $user->psn_id;
    } elseif (isset($user->uplay_id) && $user->uplay_id == ""){
        $tag = $user->uplay_id;
    } elseif(isset($user->steam_id) && $user->steam_id !== ""){
        $tag = $user->steam_id;
    } else {
        $tag = "N'A";
    }
    return $tag;
}

function deleteImage($path){
    if(File::exists($path)) {
        File::delete($path);
    }
}

function saveImage($destinationPath){
    $extension = Input::file('image')->getClientOriginalExtension();// getting image extension
    $fileName = rand(11111, 99999) . '.' . $extension; // renaming image
    Input::file('image')->move($destinationPath, $fileName); // uploading file to given path
    return $fileName;
}