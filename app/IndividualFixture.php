<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class IndividualFixture extends Model
{

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tournament() {
        return $this->belongsTo('App\Tournament','tournament_id');
    }


    public function individualFixtureResult() {
        return $this->hasMany('App\IndividualFixtureResult','fixture_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user() {
        return $this->belongsTo('App\User','player_1_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user2() {
        return $this->belongsTo('App\User','player_2_id');
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'tournament_id', 'fixture_id', 'player_1_id','player_2_id','player_1_gt','player_2_gt','date','time','pool'
    ];


}
