<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class IndividualFixtureResult extends Model
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function individualFixture() {
        return $this->belongsTo('App\IndividualFixture','id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user() {
        return $this->belongsTo('App\User','player_1');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user2() {
        return $this->belongsTo('App\User','player_2');
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'tournament_id', 'fixture_id', 'player_1', 'player_2', 'player_1_score', 'player_2_score', 'player_1_id', 'player_2_id'
    ];
}
