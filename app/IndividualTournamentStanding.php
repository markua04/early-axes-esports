<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class IndividualTournamentStanding extends Model
{

    public function tournament() {
        return $this->hasMany('App\Tournament','tournament_id');
    }

    public function user() {
        return $this->hasMany('App\User','id', 'user_id');
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id','tournament_id', 'points', 'bonus_points'
    ];
}