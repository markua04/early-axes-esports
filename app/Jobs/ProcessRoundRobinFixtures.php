<?php

namespace App\Jobs;

use App\Http\Controllers\TournamentController;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ProcessRoundRobinFixtures implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $request;
    protected $clansRegistered;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($request,$clansRegistered)
    {
        $this->request = $request;
        $this->clansRegistered = $clansRegistered;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $tournamentController = new TournamentController();
        $tournamentController->processRoundRobin($this->request,$this->clansRegistered);
    }
}
