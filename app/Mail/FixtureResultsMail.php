<?php namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class FixtureResultsMail extends Mailable
{
    use Queueable, SerializesModels;

    public $clan;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($clan)
    {
        $this->clan = $clan;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.fixture-results-reminder');
    }
}
