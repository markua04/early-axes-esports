<?php namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class WeeklyFixtureResultsUpdated extends Mailable
{
    use Queueable, SerializesModels;

    public $clansMatchesForfeited;
    public $x;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($clansMatchesForfeited)
    {
        $this->clansMatchesForfeited = $clansMatchesForfeited;
        $this->x = 1;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.weekly-fixture-results-forfeit-email');
    }
}
