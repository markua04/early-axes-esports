<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Standing extends Model
{
    public function clan() {
        return $this->belongsTo('App\Clan','clan_id');
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'pool', 'clan', 'matches_played' , 'matches_won', 'matches_lost', 'rounds_won'
    ];
}
