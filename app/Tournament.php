<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Tournament extends Model
{
    public function tournamentType() {
        return $this->belongsTo('App\TournamentType','tournament_type_id');
    }

    public function tournamentRules() {
        return $this->belongsTo('App\TournamentRules','tournament_rules');
    }

    public function tournamentStreams() {
        return $this->belongsTo('App\TournamentStream','tournament_streams');
    }

    public function tournamentStandings() {
        return $this->belongsTo('App\TournamentStandings','tournament_standings');
    }

    public function fixtureResult() {
        return $this->hasMany('App\FixtureResult','tournament_id');
    }

    public function gamesList() {
        return $this->belongsTo('App\GamesList','tournament_game');
    }

    public function standings() {
        return $this->belongsTo('App\Standing','tournament_id');
    }

    public function platform() {
        return $this->belongsTo('App\Platform','tournament_platform');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tournamentStatus() {
        return $this->belongsTo('App\TournamentStatus','tournament_status');
    }

    public function clan() {
        return $this->belongsTo('App\Clan','winner');
    }

    public function fixture() {
        return $this->hasMany('App\Fixture','tournament_id');
    }

    public function individualFixture() {
        return $this->hasMany('App\IndividualFixture');
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'tournament_title','tournament_info','tournament_rules','tournament_player_limit','tournament_type','game_id','content_creator','participants','tournament_status','tournament_registrations','challonge_bracket', 'winner'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'remember_token',
    ];
}
