<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class TournamentFpsStandings extends Model
{
    public $timestamps = true;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'tournament_id', 'clan_name','avg_score', 'assists', 'deaths', 'k_d_ratio', 'round_w', 'round_loss', 'round_win_loss_ratio', 'map_win_loss', 'year', 'date', 'points', 'created_at'
    ];
}
