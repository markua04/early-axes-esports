<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class TournamentIndividualRegisteredPlayers extends Model
{

    public function users() {
        return $this->belongsTo('App\User','user_id');
    }

    public function tournament() {
        return $this->hasMany('App\Tournament', 'tournament_id');
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id','tournament_id'
    ];
}
