<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TournamentRegisteredPlayers extends Model
{

    public function users() {
        return $this->belongsTo('App\User','clan_member_id');
    }

    public function clan() {
        return $this->belongsTo('App\Clan', 'clan_id');
    }

    public function tournament() {
        return $this->hasMany('App\Tournament', 'tournament_id');
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'clan_member_id','clan_id','tournament_id'
    ];
}
