<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class TournamentRegistrations extends Model
{

    public function clan() {
        return $this->belongsTo('App\Clan','clan_id');
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'tournament_id','clan_id'
    ];
}
