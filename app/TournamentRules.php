<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class TournamentRules extends Model
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tournament() {
        return $this->hasMany('App\Tournament','tournament_rules');
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'rules'
    ];
}
