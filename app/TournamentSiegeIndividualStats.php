<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class TournamentSiegeIndividualStats extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'gamer_tag', 'clan_name','matches_played','avg_score','scores','kills','deaths','kill_death_ratio','year','tournament_id'
    ];
}
