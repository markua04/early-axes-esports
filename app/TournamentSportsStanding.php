<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class TournamentSportsStanding extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'tournament_id', 'gamer_tag','games_played', 'goals_for', 'goals_against', 'goals_diff', 'points', 'created_at'
    ];
}
