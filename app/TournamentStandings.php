<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class TournamentStandings extends Model
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tournament() {
        return $this->hasMany('App\Tournament','tournament_standings');
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title','standings','tournament_id','user_id'
    ];
}
