<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class TournamentStatus extends Model
{


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tournament() {
        return $this->hasMany('App\Tournament','tournament_status');
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title'
    ];
}
