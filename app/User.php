<?php namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Validator;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    const SUPER_ADMIN_LEVEL = 1;
    const USER_LEVEL = 3;

    protected $table = 'users';

    public function clan() {
        return $this->belongsTo('App\Clan','clan_id');
    }

    public function level() {
        return $this->belongsTo('App\UserLevel', 'user_level_id');
    }

    public function clanMembers() {
        return $this->hasMany('App\ClanMembers','user_id');
    }

    public function tournamentRegisteredPlayers() {
        return $this->belongsTo('App\TournamentRegisteredPlayers','clan_member_id');
    }

    public function clanNews() {
        return $this->belongsTo('App\ClanNews','user_id');
    }

    public function userStatuses() {
        return $this->belongsTo('App\UserStatuses','user_status_id');
    }

    public function verifyUser()
    {
        return $this->hasOne('App\VerifyUser');
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'surname' , 'gamer_tag', 'steam_id', 'psn_id', 'uplay_id', 'gamer_pic', 'clan_id', 'email', 'password','user_level_id','selected_source','phone_number'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
