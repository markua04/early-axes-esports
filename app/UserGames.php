<?php namespace App;

use Illuminate\Database\Eloquent\Model;
/**
 * @mixin \Illuminate\Database\Eloquent\Builder
 * @mixin \Eloquent
 */
class UserGames extends Model
{
    public function user() {
        return $this->hasMany('App\User');
    }

    public function gamesList() {
        return $this->belongsTo('App\GamesList','game_id');
    }
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'game_id'
    ];
}
