create table fixtures
(
  id int(10) unsigned auto_increment primary key,
  date varchar(255) not null,
  time varchar(255) not null,
  tournament_id varchar(255) not null,
  clan_1 int(255) not null,
  clan_2 int(255) not null,
  result varchar(255) null,
  created_at timestamp null,
  updated_at timestamp null
)
;