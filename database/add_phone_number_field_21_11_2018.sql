ALTER TABLE users ADD phone_number int NULL;
ALTER TABLE users
	MODIFY COLUMN phone_number int AFTER email;