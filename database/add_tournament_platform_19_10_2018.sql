create table platforms
(
  id int unsigned auto_increment
    primary key,
  platform varchar(255) not null
);

insert into platforms(platform) values ('Xbox');
insert into platforms(platform) values ('PS4');
insert into platforms(platform) values ('PC');

ALTER TABLE tournaments ADD tournament_platform int NULL;
ALTER TABLE tournaments
  MODIFY COLUMN tournament_platform int AFTER tournament_registrations;