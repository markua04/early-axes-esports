ALTER TABLE tournaments ADD tournament_registrations SMALLINT NULL;
ALTER TABLE tournaments
  MODIFY COLUMN tournament_registrations SMALLINT AFTER tournament_status;