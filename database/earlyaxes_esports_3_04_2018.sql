-- MySQL dump 10.13  Distrib 5.7.21, for Linux (x86_64)
--
-- Host: localhost    Database: earlyaxes_esports
-- ------------------------------------------------------
-- Server version	5.7.21-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `clan_approvals`
--

DROP TABLE IF EXISTS `clan_approvals`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clan_approvals` (
  `user_id` int(11) NOT NULL,
  `clan_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_clan_status` smallint(6) NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clan_approvals`
--

LOCK TABLES `clan_approvals` WRITE;
/*!40000 ALTER TABLE `clan_approvals` DISABLE KEYS */;
/*!40000 ALTER TABLE `clan_approvals` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clan_members`
--

DROP TABLE IF EXISTS `clan_members`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clan_members` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `clan_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `approval_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clan_members`
--

LOCK TABLES `clan_members` WRITE;
/*!40000 ALTER TABLE `clan_members` DISABLE KEYS */;
INSERT INTO `clan_members` VALUES (65,17,39,0,'2018-02-20 06:47:13','2018-02-20 06:47:13'),(80,0,43,0,'2018-04-03 07:06:26','2018-04-03 07:06:26');
/*!40000 ALTER TABLE `clan_members` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clan_news`
--

DROP TABLE IF EXISTS `clan_news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clan_news` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `clan_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `banner` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clan_news`
--

LOCK TABLES `clan_news` WRITE;
/*!40000 ALTER TABLE `clan_news` DISABLE KEYS */;
INSERT INTO `clan_news` VALUES (1,2,1,'This is clan news','<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type','/uploads/clan-news/92398.png','2017-12-20 09:56:27','2017-12-20 09:56:27'),(2,2,1,'Test','<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has','/uploads/clan-news/67589.png','2017-12-20 10:03:36','2017-12-20 10:03:36'),(3,2,1,'Testing 1234','<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.<br />Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.<br />Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.<br />Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.<br />Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.<br />Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make ...','/uploads/clan-news/26473.jpg','2017-12-20 10:05:21','2017-12-20 10:05:21'),(4,2,1,'dfssdfjsdj','<p>dfjdsfjsdfj</p>','','2018-01-04 03:56:07','2018-01-04 03:56:07');
/*!40000 ALTER TABLE `clan_news` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clans`
--

DROP TABLE IF EXISTS `clans`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clans` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `clan_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `clan_email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `clan_tel` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `clan_description` varchar(5000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `clan_logo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `clan_owner_id` int(11) NOT NULL,
  `clan_status` smallint(6) NOT NULL DEFAULT '1',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `clans_clan_name_unique` (`clan_name`),
  UNIQUE KEY `clans_clan_email_unique` (`clan_email`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clans`
--

LOCK TABLES `clans` WRITE;
/*!40000 ALTER TABLE `clans` DISABLE KEYS */;
/*!40000 ALTER TABLE `clans` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `event_registrations`
--

DROP TABLE IF EXISTS `event_registrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event_registrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `surname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `gamer_tag` varchar(255) NOT NULL,
  `event_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `event_registrations`
--

LOCK TABLES `event_registrations` WRITE;
/*!40000 ALTER TABLE `event_registrations` DISABLE KEYS */;
INSERT INTO `event_registrations` VALUES (1,'Solomon Jarvis','Walsh','kizuqa@gmail.com','Qui iure qui enim id autem explicabo Culpa autem',1,'2018-02-19 08:00:35','2018-02-19 08:00:35'),(2,'JP','Strydom','jp.strydom5055@gmail.com','DLG HeLlFiiRe',1,'2018-02-19 17:13:33','2018-02-19 17:13:33'),(3,'Wayde','Lyle','Wayde.lyle@gmail.com','DeadZebra',1,'2018-02-26 21:10:33','2018-02-26 21:10:33'),(4,'Stacey','Markus','staceyleemarkus17@gmail.com','MrsUnicornQueen',2,'2018-03-01 17:44:25','2018-03-01 17:44:25'),(5,'JP','Strydom','jp.strydom5055@gmail.com','DLG HeLlFiiRe',1,'2018-03-23 18:32:32','2018-03-23 18:32:32'),(6,'JP','Strydom','jp.strydom5055@gmail.com','DLG HeLlFiiRe',2,'2018-03-23 18:32:49','2018-03-23 18:32:49'),(7,'JP','du Preez','jpdupreez1704@gmail.com','BigJapester11',1,'2018-03-29 08:02:30','2018-03-29 08:02:30'),(8,'JP','du Preez','jpdupreez1704@gmail.com','BigJapester11',2,'2018-03-29 08:03:13','2018-03-29 08:03:13');
/*!40000 ALTER TABLE `event_registrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `events`
--

DROP TABLE IF EXISTS `events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `events` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rules` longtext NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `game_id` int(11) NOT NULL,
  `date` varchar(255) NOT NULL,
  `time` varchar(255) NOT NULL,
  `banner` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `events`
--

LOCK TABLES `events` WRITE;
/*!40000 ALTER TABLE `events` DISABLE KEYS */;
/*!40000 ALTER TABLE `events` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `games_lists`
--

DROP TABLE IF EXISTS `games_lists`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `games_lists` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `game_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `game_banner` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `games_lists`
--

LOCK TABLES `games_lists` WRITE;
/*!40000 ALTER TABLE `games_lists` DISABLE KEYS */;
INSERT INTO `games_lists` VALUES (1,'Rainbow Six Siege','',NULL,NULL),(2,'For Honor','',NULL,NULL),(3,'PUBG','','2018-02-07 09:05:33','2018-02-07 09:05:33'),(4,'Call of Duty® WWII','/uploads/games/95923.jpg','2018-02-09 07:17:18','2018-02-09 07:17:18'),(5,'Overwatch','/uploads/games/99708.jpg','2018-02-16 14:07:25','2018-02-16 14:07:25');
/*!40000 ALTER TABLE `games_lists` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_07_10_061634_create_clans_table',1),(2,'2014_08_01_070406_create_user_levels',1),(3,'2014_09_12_000000_create_users_table',1),(4,'2014_10_12_100000_create_password_resets_table',1),(5,'2017_01_30_135332_create_clan_approvals_table',1),(6,'2017_01_31_132038_create_tournament_registered_players_table',1),(7,'2017_02_01_113349_create_tournament_registration_table',1),(8,'2017_02_02_070802_create_tournament_rules_table',1),(9,'2017_02_03_061426_create_tournament_types',1),(10,'2017_02_04_111202_create_tournament_table',1),(11,'2017_04_30_143148_create_news_articles',1),(12,'2017_12_05_072542_create_games_list',1),(13,'2017_12_12_054727_create_clan_members_table',1),(14,'2017_12_14_052717_create_clan_news_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `news`
--

DROP TABLE IF EXISTS `news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `news` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '0',
  `type` smallint(6) NOT NULL DEFAULT '0',
  `creator_id` smallint(6) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `news`
--

LOCK TABLES `news` WRITE;
/*!40000 ALTER TABLE `news` DISABLE KEYS */;
/*!40000 ALTER TABLE `news` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `news_categories`
--

DROP TABLE IF EXISTS `news_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `news_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `news_categories`
--

LOCK TABLES `news_categories` WRITE;
/*!40000 ALTER TABLE `news_categories` DISABLE KEYS */;
INSERT INTO `news_categories` VALUES (1,'Tournament News','2018-01-22 02:14:34','2018-01-22 02:14:34');
/*!40000 ALTER TABLE `news_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tournament_registered_players`
--

DROP TABLE IF EXISTS `tournament_registered_players`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tournament_registered_players` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `clan_member_id` int(11) NOT NULL,
  `tournament_id` int(11) NOT NULL,
  `clan_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tournament_registered_players`
--

LOCK TABLES `tournament_registered_players` WRITE;
/*!40000 ALTER TABLE `tournament_registered_players` DISABLE KEYS */;
INSERT INTO `tournament_registered_players` VALUES (1,1,2,16,'2018-01-19 11:11:31','2018-01-19 11:11:31'),(2,3,2,16,'2018-01-19 11:11:31','2018-01-19 11:11:31'),(3,14,2,16,'2018-01-19 11:11:31','2018-01-19 11:11:31'),(4,18,1,17,'2018-01-19 12:18:51','2018-01-19 12:18:51'),(5,19,1,17,'2018-01-19 12:18:51','2018-01-19 12:18:51'),(6,20,1,17,'2018-01-19 12:18:51','2018-01-19 12:18:51'),(12,3,1,16,'2018-02-09 07:20:43','2018-02-09 07:20:43'),(13,23,1,16,'2018-02-09 07:20:43','2018-02-09 07:20:43'),(14,25,1,16,'2018-02-09 07:20:43','2018-02-09 07:20:43');
/*!40000 ALTER TABLE `tournament_registered_players` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tournament_registrations`
--

DROP TABLE IF EXISTS `tournament_registrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tournament_registrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tournament_id` int(10) unsigned NOT NULL,
  `clan_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tournament_registrations`
--

LOCK TABLES `tournament_registrations` WRITE;
/*!40000 ALTER TABLE `tournament_registrations` DISABLE KEYS */;
/*!40000 ALTER TABLE `tournament_registrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tournament_rules`
--

DROP TABLE IF EXISTS `tournament_rules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tournament_rules` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rules` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `tournament_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tournament_rules`
--

LOCK TABLES `tournament_rules` WRITE;
/*!40000 ALTER TABLE `tournament_rules` DISABLE KEYS */;
/*!40000 ALTER TABLE `tournament_rules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tournament_standings`
--

DROP TABLE IF EXISTS `tournament_standings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tournament_standings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `tournament_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `standings` longtext NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tournament_standings`
--

LOCK TABLES `tournament_standings` WRITE;
/*!40000 ALTER TABLE `tournament_standings` DISABLE KEYS */;
/*!40000 ALTER TABLE `tournament_standings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tournament_statuses`
--

DROP TABLE IF EXISTS `tournament_statuses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tournament_statuses` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tournament_statuses`
--

LOCK TABLES `tournament_statuses` WRITE;
/*!40000 ALTER TABLE `tournament_statuses` DISABLE KEYS */;
INSERT INTO `tournament_statuses` VALUES (1,'Active','2018-01-22 02:14:34','2018-01-22 02:14:34'),(2,'Inactive','2018-01-22 03:14:34','2018-01-22 03:14:34');
/*!40000 ALTER TABLE `tournament_statuses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tournament_types`
--

DROP TABLE IF EXISTS `tournament_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tournament_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tournament_type_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tournament_types`
--

LOCK TABLES `tournament_types` WRITE;
/*!40000 ALTER TABLE `tournament_types` DISABLE KEYS */;
INSERT INTO `tournament_types` VALUES (2,'Team Deathmatch',NULL,NULL,NULL);
/*!40000 ALTER TABLE `tournament_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tournaments`
--

DROP TABLE IF EXISTS `tournaments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tournaments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tournament_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tournament_info` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `tournament_rules` int(10) unsigned DEFAULT NULL,
  `tournament_type_id` int(10) unsigned NOT NULL,
  `tournament_prize` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tournament_admin_id` int(11) NOT NULL,
  `tournament_banner` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tournament_game` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tournament_player_limit` int(11) NOT NULL,
  `tournament_standings` int(11) DEFAULT NULL,
  `tournament_status` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tournaments_tournament_type_id_foreign` (`tournament_type_id`),
  CONSTRAINT `tournaments_tournament_type_id_foreign` FOREIGN KEY (`tournament_type_id`) REFERENCES `tournament_types` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tournaments`
--

LOCK TABLES `tournaments` WRITE;
/*!40000 ALTER TABLE `tournaments` DISABLE KEYS */;
/*!40000 ALTER TABLE `tournaments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_levels`
--

DROP TABLE IF EXISTS `user_levels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_levels` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_level` smallint(6) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_levels`
--

LOCK TABLES `user_levels` WRITE;
/*!40000 ALTER TABLE `user_levels` DISABLE KEYS */;
INSERT INTO `user_levels` VALUES (1,'Super Admin',1,NULL,NULL,NULL),(2,'Admin',2,NULL,NULL,NULL),(3,'User',3,NULL,NULL,NULL),(4,'Super Admin',1,NULL,NULL,NULL),(5,'Admin',2,NULL,NULL,NULL),(6,'User',3,NULL,NULL,NULL),(7,'Super Admin',1,NULL,NULL,NULL),(8,'Admin',2,NULL,NULL,NULL),(9,'User',3,NULL,NULL,NULL);
/*!40000 ALTER TABLE `user_levels` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_statuses`
--

DROP TABLE IF EXISTS `user_statuses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_statuses` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_statuses`
--

LOCK TABLES `user_statuses` WRITE;
/*!40000 ALTER TABLE `user_statuses` DISABLE KEYS */;
INSERT INTO `user_statuses` VALUES (1,'Active',NULL,NULL),(2,'Disabled',NULL,NULL);
/*!40000 ALTER TABLE `user_statuses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `surname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(5000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gamer_tag` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `steam_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gamer_pic` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `clan_id` int(10) unsigned NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_level_id` int(10) unsigned NOT NULL,
  `user_clan_status` int(10) unsigned NOT NULL,
  `selected_source` int(10) DEFAULT NULL,
  `user_status_id` int(10) DEFAULT NULL,
  `select_image` int(11) DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `verified` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_user_level_id_foreign` (`user_level_id`),
  CONSTRAINT `users_user_level_id_foreign` FOREIGN KEY (`user_level_id`) REFERENCES `user_levels` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (43,'Aiden','Markus','','MaxRager1990',NULL,NULL,0,'markua04@gmail.com','$2y$10$z1TpS9WjTOT4k/PUGL.m6e9ZrPAU2V06k1bG2ipuh3jOh1wWf2BUm',1,0,1,1,NULL,'1TVKOCqq3ccnkYG7lOwt20eMSXmC8SAK8iKBtv0FR7euY2gXmMkZgpLCMtbW','2018-03-09 12:04:56','2018-04-03 07:04:13',1),(50,'alex','pfoss','','MaxRager1990',NULL,NULL,0,'markua04+1@gmail.com','$2y$10$DJJ8qaHx8ZwPoQ0OQ.gA/ewHIGEKFJKLRZ4UeFIWECYfF.3K9UMPW',3,0,1,1,NULL,'N34m2JbRdP7kFQ31pUlW3xFSeOpk6rIajUuHxSFwUfdNQjHnouF6aeoeLlzM','2018-04-03 07:06:26','2018-04-03 07:06:38',1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `verify_users`
--

DROP TABLE IF EXISTS `verify_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `verify_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `verify_users`
--

LOCK TABLES `verify_users` WRITE;
/*!40000 ALTER TABLE `verify_users` DISABLE KEYS */;
INSERT INTO `verify_users` VALUES (1,33,'XnmDEDJf9da1gFlYFdcHpTjwEi1EUw60XomVmI5H','2018-02-08 07:09:08','2018-02-08 07:09:08'),(2,34,'NRaV5kpUfZoAtDqEBPkTFD4IufbCyyz9U6xQ2P4L','2018-02-09 06:26:00','2018-02-09 06:26:00'),(3,35,'RWc0tNKU7kGehKyYikDjHahDm0cWvy2O9RtyK7E7','2018-02-19 16:16:28','2018-02-19 16:16:28'),(4,36,'IHLuf2ZpWx6PS3dlIsZfncJarLFGCBXffcCyzZJ8','2018-02-19 16:20:16','2018-02-19 16:20:16'),(5,37,'oBzIUjEvA4NSYFRPX0wdAIR3fabJxUumFC9VXkEg','2018-02-19 16:29:14','2018-02-19 16:29:14'),(6,38,'xsx5mQlbRPBqXRI9KIAsZS2ee59wrJ49eNWRsGER','2018-02-20 06:34:00','2018-02-20 06:34:00'),(7,39,'h0GWgFevYTGxbbHn35RWTu3sSAZW8JZwFrUK4O1b','2018-02-20 06:47:13','2018-02-20 06:47:13'),(8,40,'qFwVRUDnZ5RCvEFBA178SX83NRP7qRXIhbL8gZn5','2018-02-20 06:54:19','2018-02-20 06:54:19'),(9,41,'kJGKzSBvctQ5GkAAVu1ZUOV9Rtl36oMmxeKqpBi3','2018-02-20 07:00:46','2018-02-20 07:00:46'),(10,42,'oJ08382m3iGou0ZQnKbQrdoLDvigUzdUwrmTBKkr','2018-02-20 13:45:13','2018-02-20 13:45:13'),(11,43,'cexSnPsgzMOFopyJXy8TrMhDH7dIa6Dr9U6xebxT','2018-03-09 12:04:56','2018-03-09 12:04:56'),(12,44,'nYWIPi1UBEmXZSefUp5IZXL6J6igR7rLbt7S7e0c','2018-03-14 11:32:50','2018-03-14 11:32:50'),(13,45,'B4sY1uZnpzLlgbQfsAtUrqCIhYpuwqVeCptaMwAH','2018-03-22 08:23:38','2018-03-22 08:23:38'),(14,46,'0h9VL7u9B3JZwyZG3JxnoKONRmmcdMdwsHKboywD','2018-03-22 12:27:06','2018-03-22 12:27:06'),(15,47,'aLdaraivf6PNpgvJ3STXOhqppEefmUFfBWTKyXn3','2018-03-22 20:19:00','2018-03-22 20:19:00'),(16,48,'lCtN84SXT6xVzEbO8VumSVz4Ah9RxuYorua54KTl','2018-03-22 20:27:19','2018-03-22 20:27:19'),(17,50,'iFo8yzzDDmMuttK0Htv9ltC9wJ8Q2eq6HQfQYWLG','2018-04-03 07:06:26','2018-04-03 07:06:26');
/*!40000 ALTER TABLE `verify_users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-04-03  7:11:30
