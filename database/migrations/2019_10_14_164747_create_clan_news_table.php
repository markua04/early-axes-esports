<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateClanNewsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('clan_news', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('clan_id');
			$table->integer('user_id');
			$table->string('title');
			$table->text('content');
			$table->string('banner');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('clan_news');
	}

}
