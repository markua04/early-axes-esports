<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateClanTournamentStandingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('clan_tournament_standings', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('clan_id');
			$table->integer('tournament_id');
			$table->integer('played')->nullable();
			$table->integer('points')->nullable();
			$table->integer('wins')->nullable();
			$table->integer('losses')->nullable();
			$table->integer('draws')->nullable();
			$table->integer('forfeit')->nullable();
			$table->integer('game_id')->nullable();
			$table->integer('platform')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('clan_tournament_standings');
	}

}
