<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateClansTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('clans', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('clan_name')->unique();
			$table->string('clan_email')->unique();
			$table->string('clan_tel');
			$table->text('clan_description', 65535)->nullable();
			$table->string('clan_logo');
			$table->integer('clan_owner_id');
			$table->smallInteger('clan_status')->default(1);
			$table->integer('secondary_captain')->nullable();
			$table->string('remember_token', 100)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('clans');
	}

}
