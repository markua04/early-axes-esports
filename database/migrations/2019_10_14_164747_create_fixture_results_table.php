<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFixtureResultsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('fixture_results', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('fixture_id')->nullable();
			$table->integer('tournament_id')->nullable();
			$table->string('clan_1')->nullable();
			$table->string('clan_2')->nullable();
			$table->integer('clan_1_id')->nullable();
			$table->integer('clan_2_id')->nullable();
			$table->integer('clan_1_score');
			$table->integer('clan_2_score');
			$table->integer('clan_1_points')->nullable();
			$table->integer('clan_2_points')->nullable();
			$table->integer('forfeit')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('fixture_results');
	}

}
