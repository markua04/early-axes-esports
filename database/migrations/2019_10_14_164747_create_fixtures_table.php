<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFixturesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('fixtures', function(Blueprint $table)
		{
			$table->increments('id');
			$table->date('date');
			$table->time('time');
			$table->string('tournament_id');
			$table->string('clan_1', 500);
			$table->string('clan_2', 500);
			$table->integer('clan_1_id')->nullable();
			$table->integer('clan_2_id')->nullable();
			$table->string('clan_1_image')->nullable();
			$table->string('clan_2_image')->nullable();
			$table->integer('forfeit')->nullable();
			$table->string('result')->nullable();
			$table->integer('pool')->nullable();
			$table->integer('reminded')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('fixtures');
	}

}
