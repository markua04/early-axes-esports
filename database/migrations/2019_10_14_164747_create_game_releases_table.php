<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGameReleasesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('game_releases', function(Blueprint $table)
		{
			$table->increments('id');
			$table->date('start_countdown_date');
			$table->time('start_countdown_time');
			$table->date('end_countdown_date');
			$table->time('end_countdown_time');
			$table->string('image', 500)->nullable();
			$table->string('game', 500)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('game_releases');
	}

}
