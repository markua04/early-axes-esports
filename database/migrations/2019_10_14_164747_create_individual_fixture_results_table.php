<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateIndividualFixtureResultsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('individual_fixture_results', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('tournament_id')->nullable();
			$table->integer('fixture_id')->nullable();
			$table->integer('player_1')->nullable();
			$table->integer('player_2')->nullable();
			$table->integer('player_1_score')->nullable();
			$table->integer('player_2_score')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('individual_fixture_results');
	}

}
