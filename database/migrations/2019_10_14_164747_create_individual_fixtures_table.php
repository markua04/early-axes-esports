<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateIndividualFixturesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('individual_fixtures', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('tournament_id')->nullable();
			$table->integer('player_1_id')->nullable();
			$table->integer('player_2_id')->nullable();
			$table->date('date');
			$table->time('time');
			$table->timestamps();
			$table->integer('pool')->nullable();
			$table->string('player_1_gt')->nullable();
			$table->string('player_2_gt')->nullable();
			$table->integer('resulted')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('individual_fixtures');
	}

}
