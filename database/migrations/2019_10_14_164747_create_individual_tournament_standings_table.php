<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateIndividualTournamentStandingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('individual_tournament_standings', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id');
			$table->integer('tournament_id');
			$table->integer('points')->nullable();
			$table->integer('bonus_points')->nullable();
			$table->integer('wins')->nullable();
			$table->integer('losses')->nullable();
			$table->integer('draws')->nullable();
			$table->integer('goals_for')->nullable();
			$table->integer('goals_against')->nullable();
			$table->integer('difference')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('individual_tournament_standings');
	}

}
