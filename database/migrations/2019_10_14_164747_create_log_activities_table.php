<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLogActivitiesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('log_activities', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('subject');
			$table->string('url');
			$table->string('method');
			$table->string('ip');
			$table->string('agent')->nullable();
			$table->integer('user_id')->nullable();
			$table->string('user_name')->nullable();
			$table->string('user_surname')->nullable();
			$table->string('data', 500)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('log_activities');
	}

}
