<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStandingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('standings', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('pool')->nullable();
			$table->integer('clan_id')->nullable();
			$table->integer('tournament_id')->nullable();
			$table->integer('matches_played')->nullable();
			$table->integer('matches_won')->nullable();
			$table->integer('matches_lost')->nullable();
			$table->integer('maps_won')->nullable();
			$table->integer('maps_lost')->nullable();
			$table->integer('rounds_won')->nullable();
			$table->integer('forfeit')->nullable();
			$table->integer('total_score')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('standings');
	}

}
