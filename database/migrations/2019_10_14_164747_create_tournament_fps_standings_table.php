<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTournamentFpsStandingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tournament_fps_standings', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('tournament_id')->nullable();
			$table->string('clan')->nullable();
			$table->decimal('avg_score', 10, 0)->nullable();
			$table->decimal('kills', 10, 0)->nullable();
			$table->decimal('assists', 10, 0)->nullable();
			$table->decimal('deaths', 10, 0)->nullable();
			$table->decimal('k_d_ratio', 10, 0)->nullable();
			$table->decimal('round_w', 10, 0)->nullable();
			$table->decimal('round_l', 10, 0)->nullable();
			$table->decimal('round_win_loss_ratio', 10, 0)->nullable();
			$table->decimal('map_win_loss', 10, 0)->nullable();
			$table->integer('pool')->nullable();
			$table->dateTime('created_at')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tournament_fps_standings');
	}

}
