<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTournamentRegisteredPlayersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tournament_registered_players', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('clan_member_id');
			$table->integer('tournament_id');
			$table->integer('clan_id');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tournament_registered_players');
	}

}
