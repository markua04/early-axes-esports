<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTournamentRegistrationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tournament_registrations', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('tournament_id')->unsigned();
			$table->integer('clan_id')->unsigned();
			$table->integer('forfeit')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tournament_registrations');
	}

}
