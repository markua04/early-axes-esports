<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTournamentSiegeIndividualStatsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tournament_siege_individual_stats', function(Blueprint $table)
		{
			$table->string('gamer_tag')->nullable();
			$table->string('clan_name')->nullable();
			$table->string('matches_played')->nullable();
			$table->string('avg_score')->nullable();
			$table->string('scores')->nullable();
			$table->string('kills')->nullable();
			$table->string('deaths')->nullable();
			$table->decimal('kill_death_ratio', 4)->nullable();
			$table->string('assists')->nullable();
			$table->string('year')->nullable();
			$table->string('tournament_id')->nullable();
			$table->date('date_added')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tournament_siege_individual_stats');
	}

}
