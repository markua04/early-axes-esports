<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTournamentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tournaments', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('tournament_title');
			$table->text('tournament_info');
			$table->integer('tournament_rules')->unsigned()->nullable();
			$table->text('tournament_streams')->nullable();
			$table->integer('tournament_type_id')->unsigned()->index('tournaments_tournament_type_id_foreign');
			$table->string('tournament_prize');
			$table->integer('tournament_admin_id');
			$table->string('tournament_banner');
			$table->string('tournament_game');
			$table->integer('tournament_player_limit');
			$table->integer('tournament_standings')->nullable();
			$table->integer('tournament_status')->nullable();
			$table->smallInteger('tournament_registrations')->nullable();
			$table->integer('tournament_platform')->nullable();
			$table->integer('standings_type')->nullable();
			$table->string('discord_link')->nullable();
			$table->integer('win_points')->nullable();
			$table->integer('draw_points')->nullable();
			$table->integer('loss_points')->nullable();
			$table->string('challonge_bracket_1', 500)->nullable();
			$table->string('challonge_bracket_2', 500)->nullable();
			$table->integer('genre')->nullable();
			$table->string('toornament_standings', 500)->nullable();
			$table->integer('winner')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tournaments');
	}

}
