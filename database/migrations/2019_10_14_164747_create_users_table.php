<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->string('surname');
			$table->string('xuid')->nullable();
			$table->string('description', 5000);
			$table->string('gamer_tag')->nullable();
			$table->string('psn_id')->nullable();
			$table->string('steam_id')->nullable();
			$table->string('uplay_id')->nullable();
			$table->string('gamer_pic')->nullable();
			$table->integer('clan_id')->nullable();
			$table->string('email')->unique();
			$table->string('phone_number', 11)->nullable();
			$table->string('password');
			$table->integer('user_level_id')->unsigned()->index('users_user_level_id_foreign');
			$table->integer('user_clan_status')->unsigned();
			$table->integer('selected_source')->nullable();
			$table->integer('user_status_id')->nullable();
			$table->integer('select_image')->nullable();
			$table->string('remember_token', 100)->nullable();
			$table->timestamps();
			$table->boolean('verified')->nullable()->default(0);
			$table->string('api_token', 100)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
