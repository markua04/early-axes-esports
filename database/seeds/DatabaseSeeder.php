<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            ClansTableSeeder::class,
            UserLevelsTableSeeder::class,
            UsersTableSeeder::class,
            GamesListSeeder::class,
            TournamentTypesSeeder::class,
            UserStatusesTableSeeder::class,
        ]);
    }
}
