<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GamesListSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('games_lists')->insert(
            [
                'game_name' => 'Rainbow Six Siege',
            ]
        );
        DB::table('games_lists')->insert(
            [
                'game_name' => 'For Honor',
            ]
        );
    }
}
