<?php

use Illuminate\Database\Seeder;

class TournamentTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tournament_types')->insert([
            'tournament_type_name' => 'Team Deathmatch',
        ]);
    }
}
