<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserLevelsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_levels')->insert([
            'name' => 'Super Admin',
            'user_level' => '1',
        ]);
        DB::table('user_levels')->insert([
            'name' => 'Admin',
            'user_level' => '2',
        ]);
        DB::table('user_levels')->insert([
            'name' => 'User',
            'user_level' => '3',
        ]);
    }
}
