<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            [
            'name' => 'Aiden',
            'surname' => 'Markus',
            'email' => 'markua04@gmail.com',
            'gamer_tag' => 'MaxRager1990',
            'clan_id' => 1,
            'user_level_id' => 1,
            'password' => bcrypt('password123'),
            ]
        );
        DB::table('users')->insert(
            [
                'name' => 'Alex',
                'surname' => 'Pfoss',
                'email' => 'vosterr@gmail.com',
                'gamer_tag' => 'mR fUnKtAsTiC33',
                'clan_id' => 1,
                'user_level_id' => 3,
                'password' => bcrypt('password123'),
            ]
        );
        DB::table('users')->insert(
            [
                'name' => 'Richard',
                'surname' => 'Roth',
                'email' => 'richard@gmail.com',
                'gamer_tag' => '',
                'clan_id' => 1,
                'user_level_id' => 3,
                'password' => bcrypt('password123'),
            ]
        );
        DB::table('users')->insert(
            [
                'name' => 'Rudi',
                'surname' => 'Cronje',
                'email' => 'rudi@gmail.com',
                'gamer_tag' => '',
                'clan_id' => 1,
                'user_level_id' => 3,
                'password' => bcrypt('password123'),
            ]
        );
    }
}
