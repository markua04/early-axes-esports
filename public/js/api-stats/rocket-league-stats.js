$(document).ready(function() {
    var updateMe = $('.updateMe');
    var game = $('.game-select');
    game.show();
    updateMe.attr('disabled', true);
    var term = "";
    $('.platform').on('change',function () {
        var platform = $(this).val();
        if(platform !== ''){
            updateMe = updateMe.attr('disabled', false);
            game.hide();
        }
        term = platform;
    });
    updateMe.on('change',function () {
        if(term === ""){
            alert('Please select a platform.');
        } else {
            var selected = $(this).val();
            if (selected === "rocket-league") {
                var gamerTag = $('#gamer_tag').text();
                $(".siege-stats").hide();
                $(".loading-rl").show();
                $.ajax({
                    url: '/get-rl-stats/' + term + "/" + gamerTag,
                    type: 'post',
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    data: {platform: term, gamerTag: gamerTag},
                    success: function (response) {
                        console.log(response.rlStats.stats);
                        $(".loading-rl").hide();
                        if (selected === "rocket-league") {
                            $(".rocket-league").show(700);
                            $(".rocket-league-2").show(700);
                        }
                        $('#rl-overall-wins').html(response.rlStats.stats.wins);
                        $('#rl-overall-goals').html(response.rlStats.stats.goals);
                        $('#rl-overall-mvps').html(response.rlStats.stats.mvps);
                        $('#rl-overall-saves').html(response.rlStats.stats.saves);
                        $('#rl-overall-shots').html(response.rlStats.stats.shots);
                        $('#rl-overall-assists').html(response.rlStats.stats.assists);
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            }
        }
    });

    function secondsToHms(d) {
        d = Number(d);
        var h = Math.floor(d / 3600);
        var m = Math.floor(d % 3600 / 60);
        var s = Math.floor(d % 3600 % 60);

        var hDisplay = h > 0 ? h + (h == 1 ? " hr, " : " hrs, ") : "";
        var mDisplay = m > 0 ? m + (m == 1 ? " min, " : " mins ") : "";
        var sDisplay = s > 0 ? s + (s == 1 ? " sec" : " secs") : "";
        return hDisplay + mDisplay;
    }

    function getPlatform() {
    }

    function displayOperatorStats(operatorStats) {

        if(operatorStats){
            var i;
            for (i = 0; i < 21; i++) {
                var row = $(
                    '<div style="height: 690px;" class="col-md-3 col-xs-12 overall-table operator-stats-table">'+
                    '<div class="bust">'+
                    '<img src="/images/busts/' + operatorStats[i].operator.name.toLowerCase() + '.png" width="150" class="center-block operator-image">'+
                    '</div>'+
                    '<div class="slide-out-div" id="operator-table">'+
                    '<div class="table-responsive">' +
                    '<table class="table table-striped">'+
                    '<tr>'+
                    '<thead>' +
                    '<tbody id="operator-stats">'+
                    '<th style="background-color:#007AF7;color: #fff;">Stat</th>'+
                    '<th style="background-color:#007AF7;color: #fff;">Value</th>'+
                    '</thead>' +
                    '</tr>'+
                    '<tr>'+
                    '<td>Name</td>'+
                    '<td>'+operatorStats[i].operator.name+'</td>'+
                    '</tr>'+
                    '<tr>'+
                    '<td>Role</td>'+
                    '<td>'+operatorStats[i].operator.role+'</td>'+
                    '</tr>'+
                    '<tr>'+
                    '<td>Times Played</td>'+
                    '<td>'+operatorStats[i].stats.played+'</td>'+
                    '</tr>'+
                    '<tr>'+
                    '<td>Wins</td>'+
                    '<td>'+operatorStats[i].stats.wins+'</td>'+
                    '</tr>'+
                    '<tr>'+
                    '<td>Losses</td>'+
                    '<td>'+operatorStats[i].stats.losses+'</td>'+
                    '</tr>'+
                    '<tr>'+
                    '<td>Play Time</td>'+
                    '<td>'+secondsToHms(operatorStats[i].stats.playtime)+'</td>'+
                    '</tr>'+
                    '</tbody>'+
                    '</table>'+
                    '</div>'+
                    '</div>'
                );
                $("#operator-table").append(row);
            }
        }
    }



});