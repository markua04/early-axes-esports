$(document).ready(function() {
    var term = "";
    $('.platform').on('change',function () {
        var platform = $(this).val();
        term = platform;
    });
    $('.updateMe').on('change',function () {
        if(term === ""){
            alert('Please select a platform.');
        } else {
            var selected = $(this).val();
            if(selected === "siege"){
                var gamerTag = $('#gamer_tag').text();
                $(".loading-rl").hide();
                $(".loading-rb6").show();
                $(".rocket-league").hide();
                $(".rocket-league-2").hide();
                $.ajax({
                    url: '/get-stats/' + selected + "/" + term + "/" + gamerTag,
                    type: 'post',
                    headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                    data: { selected : selected, platform:term } ,
                    success: function (response) {
                        $(".loading-rb6").hide();
                        var muOpName = response.stats.operatorMostPlayed.operatorSimpleStat.name.toLowerCase();
                        var casualStats = response.stats.siegeStats.player.stats.casual;
                        var rankedStats = response.stats.siegeStats.player.stats.ranked;
                        var playerStats = response.stats.siegeStats.player;
                        var operatorMostPlayed = response.stats.operatorMostPlayed;
                        $(".siege-stats").show(700);
                        $('#mu-siege-ctu').html(operatorMostPlayed.operatorSimpleStat.ctu);
                        $('#mu-siege-name').html(muOpName);
                        $('#mu-siege-played').html(operatorMostPlayed.operatorAdvancedStat.played);
                        $('#mu-siege-kills').html(operatorMostPlayed.operatorAdvancedStat.kills);
                        $('#mu-siege-playtime').html(secondsToHms(operatorMostPlayed.operatorAdvancedStat.playtime));
                        $("#mu-siege-op-img").attr('src', "/images/siege-used/" + muOpName + ".png");

                        $('#ss-siege-username').html(playerStats.username);
                        $('#ss-siege-level').html(playerStats.stats.progression.level);
                        $('#ss-siege-xp').html(playerStats.stats.progression.xp);

                        $('#ss-siege-platform').attr('src','/images/base-images/' + playerStats.platform + '.png');

                        $('#c-siege-wins').html(casualStats.wins);
                        $('#c-siege-losses').html(casualStats.losses);
                        $('#c-siege-wlr').html(casualStats.wlr);
                        $('#c-siege-kills').html(casualStats.kills);
                        $('#c-siege-deaths').html(casualStats.deaths);
                        $('#c-siege-kd').html(casualStats.kd);
                        $('#c-siege-playtime').html(secondsToHms(casualStats.playtime));

                        if(rankedStats.playtime === 0){
                            $(".ranked-stats").hide();
                        } else {
                            $('#comp-siege-wins').html(rankedStats.wins);
                            $('#comp-siege-losses').html(rankedStats.losses);
                            $('#comp-siege-wlr').html(rankedStats.wlr);
                            $('#comp-siege-kills').html(rankedStats.kills);
                            $('#comp-siege-deaths').html(rankedStats.deaths);
                            $('#comp-siege-kd').html(rankedStats.kd);
                            $('#comp-siege-playtime').html(secondsToHms(rankedStats.playtime));
                        }
                        var operatorStats = response.stats.operatorStats.operator_records;
                        displayOperatorStats(operatorStats);
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            }
        }
    });

    function secondsToHms(d) {
        d = Number(d);
        var h = Math.floor(d / 3600);
        var m = Math.floor(d % 3600 / 60);
        var s = Math.floor(d % 3600 % 60);

        var hDisplay = h > 0 ? h + (h == 1 ? " hr, " : " hrs, ") : "";
        var mDisplay = m > 0 ? m + (m == 1 ? " min, " : " mins ") : "";
        var sDisplay = s > 0 ? s + (s == 1 ? " sec" : " secs") : "";
        return hDisplay + mDisplay;
    }

    function getPlatform() {
    }

    function displayOperatorStats(operatorStats) {

        if(operatorStats){
            var i;
            for (i = 0; i < 21; i++) {
                var row = $(
                    '<div style="height: 690px;" class="col-md-3 col-xs-12 overall-table operator-stats-table">'+
                    '<div class="bust">'+
                    '<img src="/images/busts/' + operatorStats[i].operator.name.toLowerCase() + '.png" width="150" class="center-block operator-image">'+
                    '</div>'+
                    '<div class="slide-out-div" id="operator-table">'+
                    '<div class="table-responsive">' +
                    '<table class="table table-striped">'+
                    '<tr>'+
                    '<thead>' +
                    '<tbody id="operator-stats">'+
                    '<th style="background-color:#007AF7;color: #fff;">Stat</th>'+
                    '<th style="background-color:#007AF7;color: #fff;">Value</th>'+
                    '</thead>' +
                    '</tr>'+
                    '<tr>'+
                    '<td>Name</td>'+
                    '<td>'+operatorStats[i].operator.name+'</td>'+
                    '</tr>'+
                    '<tr>'+
                    '<td>Role</td>'+
                    '<td>'+operatorStats[i].operator.role+'</td>'+
                    '</tr>'+
                    '<tr>'+
                    '<td>Times Played</td>'+
                    '<td>'+operatorStats[i].stats.played+'</td>'+
                    '</tr>'+
                    '<tr>'+
                    '<td>Wins</td>'+
                    '<td>'+operatorStats[i].stats.wins+'</td>'+
                    '</tr>'+
                    '<tr>'+
                    '<td>Losses</td>'+
                    '<td>'+operatorStats[i].stats.losses+'</td>'+
                    '</tr>'+
                    '<tr>'+
                    '<td>Play Time</td>'+
                    '<td>'+secondsToHms(operatorStats[i].stats.playtime)+'</td>'+
                    '</tr>'+
                    '</tbody>'+
                    '</table>'+
                    '</div>'+
                    '</div>'
                );
                $("#operator-table").append(row);
            }
        }
    }



});