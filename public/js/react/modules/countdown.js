function getTimeRemaining(endtime) {
    const total = Date.parse(endtime) - Date.now(),
        seconds = Math.floor((total/1000) % 60),
        minutes = Math.floor((total/1000/60) % 60),
        hours12 = Math.floor((total/(1000*60*60)) % 24),
        hours24 = Math.floor((total/(1000*60*60))),
        days = Math.floor(total/(1000*60*60*24));

    return {
        total: total,
        days: days,
        hours12: hours12,
        hours24: hours24,
        minutes: minutes,
        seconds: seconds
    };
}

// this is for the random background color
const r = Math.floor(Math.random() * 255);
const g = Math.floor(Math.random() * 255);
const b = Math.floor(Math.random() * 255);
const backgroundColor = `rgb(${r}, ${g}, ${b})`;
const game = 'RELEASES :';
const logo = <img className="logoSize" src={'/images/logos/bfv-logo.png'} />;

class Clock extends React.Component {
    render() {
        return(
            <div id={'mainContainerDivStyling'}>
                <div>
                    <span>{logo}</span>
                    <span>{game}</span>
                    <span>{this.props.daysRemaining}D </span>
                    <span>{this.props.hours12Remaining}H </span>
                    <span>{this.props.minutesRemaining}M </span>
                    <span>{this.props.secondsRemaining}S </span>
                </div>
            </div>
        )
    }
}

class Countdown extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            currentTime: Date.now(),
            startTime: this.props.startTime,
            endTime: this.props.endTime,
            isAfter: false,
            timeRemaining: null,
            total: null,
            days: null,
            hours12: null,
            hours24: null,
            minutes: null,
            seconds: null
        }
    }

    componentWillMount() {
        const whateverIntervalKey = setInterval(function() {
            const timeRemaining = getTimeRemaining(this.props.endTime);
            if ((timeRemaining.hours12 <= 0 || timeRemaining.hours24 <= 0) && timeRemaining.minutes <= 0 && timeRemaining.seconds <= 0) {
                // countdown style when it ends
                <Clock
                    style={this.props.style}
                    backgroundColor={backgroundColor}
                    onChange={this.onSecondPassed.bind(this)}
                    daysRemaining={this.state.days}
                    hours12Remaining={this.state.hours12}
                    minutesRemaining={this.state.minutes}
                    secondsRemaining={this.state.seconds}
                />

                clearInterval(whateverIntervalKey);

                this.setState({
                    isAfter: true
                });

            } else {
                this.setState({
                    days: timeRemaining.days,
                    hours12: timeRemaining.hours12,
                    hours24: timeRemaining.hours24,
                    minutes: timeRemaining.minutes,
                    seconds: timeRemaining.seconds
                });
            }
        }.bind(this), 1000);
    }

    onSecondPassed(event) {
        this.setState({
            endTime: event.target.value,
            days: getTimeRemaining(this.state.endTime.days)
        });
    }

    render() {
        let content = null;
        if(this.state.minutes <= 0 && this.state.seconds <= 0 && this.state.hours24 <= 0 && this.state.hours12 <= 0){
           content = <span>{logo} RELEASED IN SA!</span>;
        } else {
            content = <Clock
                style={this.props.style}
                backgroundColor={backgroundColor}
                onChange={this.onSecondPassed.bind(this)}
                daysRemaining={this.state.days}
                hours12Remaining={this.state.hours12}
                minutesRemaining={this.state.minutes}
                secondsRemaining={this.state.seconds}
            />
        }

        const { bgColor } = this.state;

        return (
            <div>
                { content }
            </div>
        )
    }
}

function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min;
}

const endYear = getRandomInt(2018, 2018);
// console.log(endYear);

ReactDOM.render(<Countdown endTime={`"Nov 20" ${endYear}`} style={"display:none"} />, document.getElementById('countdown'));