'use strict';

const e = React.createElement;

class Articles extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            items: []
        };
    }

    componentDidMount() {
        fetch("https://earlyaxes.co.za/wp-json/wp/v2/posts?_embed&per_page=5")
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        isLoaded: true,
                        items: result
                    });
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            );
    }

    render() {
        const { error, isLoaded, items } = this.state;
        console.log(items);
        if (error) {
            return <div>Error: {error.message}</div>;
        } else if (!isLoaded) {
            let logo = '/images/base-images/yoda.gif';
            return <img className="yodaGif" src={logo} alt="Logo" />;
        } else {
            return (
                <div>
                    {items.map(item => (
                        <div className="box-content">
                            <a className="eax-post-link black-text " target="_blank" href={item.link}><span className="postTitle black-text ">{item.title.rendered.replace(/&#?[a-z0-9]+;/g,"").toLocaleUpperCase()}</span></a>
                            <p>{ item.excerpt.rendered.replace(/&#?[a-z0-9]+;/g,"").replace('<p>',"").replace('</p>',"")}</p>
                        </div>
                    ))}
                </div>
            );
        }
    }
}

const domContainer = document.querySelector('#articles-container');
ReactDOM.render(e(Articles), domContainer);