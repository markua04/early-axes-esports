/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
import Vue from 'vue';

import { Datetime } from 'vue-datetime';
import VueLazyload from 'vue-lazyload'
Vue.component('datetime', Datetime);
Vue.use(VueLazyload);
// You need a specific loader for CSS files
import 'vue-datetime/dist/vue-datetime.css'

window.axios = require('axios');
window.axios.defaults.headers.common = {
    'X-Requested-With': 'XMLHttpRequest',
    'X-CSRF-TOKEN' : document.querySelector('meta[name="csrf-token"]').getAttribute('content')
};

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example-component', require('./components/ExampleComponent.vue'));
Vue.component('sidebar-menu-component', require('./components/SidebarMenuComponent.vue'));
Vue.component('tournaments-list-component', require('./components/TournamentsListComponent.vue'));
Vue.component('moderate-select-component', require('./components/ModerateSelectComponent.vue'));
Vue.component('fixture-results-ticker-component', require('./components/FixtureResultsTickerComponent.vue'));
Vue.component('manage-tournaments-component', require('./components/ManageTournamentsComponent.vue'));
Vue.component('create-fixtures', require('./components/CreateFixtures'));
Vue.component('double-select', require('./components/SmallComponents/DoubleSelect'));
Vue.component('modal', require('./components/SmallComponents/Modal'));
Vue.component('simple-dropdown', require('./components/SmallComponents/SimpleDropdown'));
Vue.component('unapproved-members', require('./components/UnapprovedMembers'));
Vue.component('date-time', require('./components/SmallComponents/DateTime'));
Vue.component('tournament-bracket', require('./components/TournamentBracket'));
Vue.component('moderate-users-gt', require('./components/ModerateUsersGt'));

const app = new Vue({
    el: '#app'
});
const app2 = new Vue({
    el: '#app2'
});
