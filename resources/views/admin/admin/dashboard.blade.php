@extends('layouts.app')

@section('content')
    <style>
        .btn {
            display: inline-block;
            padding: 6px 12px;
            margin-bottom: 0;
            font-size: 14px;
            font-weight: 400;
            line-height: 1.42857143;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            -ms-touch-action: manipulation;
            touch-action: manipulation;
            cursor: pointer;
            background-image: none;
            border: 1px solid transparent;
            border-radius: 4px;
            width: 100%;
            height: 35px;
        }
    </style>
    <div class="container dash-contain">
        <div class="col-md-12">
            <h2 style="margin-top:20px;" class="margin-b-10 margin-t-30">Welcome {{ Auth::user()->name }}! Take a look at some of the stats of the system below:</h2>
            <div class="row margin-b-20">
                @if($clanJoinUrl !== "")
                    <p class="text-center margin-b-20">Send this URL to your friends to easily join your clan.</p>
                    <div class="col-md-12 text-center input-group margin-b-20">
                        <input class="margin-t-5 form-control" id="joinUrl" value="{{ URL::to('/').$clanJoinUrl }}">
                        <a href="#ex2" rel="modal:open" id="copyToClipboard" class="btn btn-primary btn-xs" data-clipboard-target="#joinUrl">Copy Link</a>
                        <div id="ex2" class="modal row">
                            <a href="#" rel="modal:close">Close</a>
                            <p>Clan join link copied</p>
                        </div>
                    </div>
                @endif
                <div class="col-md-12">
                    @if($quote !== null)
                        <div class="col-md-12 caret padding-t-10 padding-b-10">
                            <p class="margin-b-10"><b>Quote of the day:</b></p>
                            <p class="margin-b-10">{{ $quote->contents->quotes[0]->author }}</p>
                            <p class="margin-b-10">{{ $quote->contents->quotes[0]->quote }}</p>
                        </div>
                    @endif
                </div>
            </div>
            <div class="tournament-wrapper caret dashboard-pad">
                <p><i class="fa fa-users fa-1x"></i><b class="spacing-dash">Registered users : {{ $userCount }}</b></p>
                <p><i class="fa fa-gamepad fa-1x"></i><b class="spacing-dash">Registered clans : {{ $clanCount }}</b></p>
                <p><i class="fa fa-trophy fa-1x"></i><b class="spacing-dash">Tournaments : {{ $tournamentCount }}</b></p>
                <p><i class="fa fa-user fa-1x"></i><b class="spacing-dash">New users this month : {{ $newUsersThisMonth }}</b></p>
                <p><i class="fa fa-headphones fa-1x"></i><b class="spacing-dash">Clans needing Moderation : {{ $clansNeedingModeration->count() }}</b></p>
            </div>
            <div class="row margin-t-15">
                <div class="col-md-12 margin-b-15">
                    <h2>Fixtures needing results</h2>
                    <div id="fixtures" class="btn button-primary open-close-operator">View/Close</div>
                </div>
                <div class="col-md-12 slide-out-div-1">
                    <table id="pool-table" class="table scrollable-table-fix standings-table">
                        <thead>
                        <tr style="height: 15px;">
                            <th style="height: 15px;">Tournament</th>
                            <th style="height: 15px;">Clan 1</th>
                            <th style="height: 15px;">Clan 2</th>
                            <th style="height: 15px;">Date</th>
                            <th style="height: 15px;">Delete</th>
                            <th style="height: 15px;">Reminder</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($fixturesNeedingResults as $fixture)
                            <tr style="height: 15px;">
                                <td style="height: 15px;">{{ isset($fixture->tournament) ? $fixture->tournament->tournament_title : '' }}</td>
                                <td style="height: 15px;">{{ $fixture->clan_1 }}</td>
                                <td style="height: 15px;">{{ $fixture->clan_2 }}</td>
                                <td style="height: 15px;">{{ $fixture->date }}</td>
                                <td style="height: 15px;"><a href="/admin/delete-fixture/{{$fixture->id}}/">Delete</a></td>
                                @if($fixture->reminded == 1)
                                    <td style="height: 15px;">Reminded</td>
                                @else
                                    <td style="height: 15px;"><a href="/admin/reminder-fixture/{{$fixture->id}}">Remind</a></td>
                                @endif
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/2.0.0/clipboard.min.js"></script>
    <script>
        jQuery(document).ready(function ($) {
            let slider1 = $('.slide-out-div-1');
            let fixtures = $('#fixtures');
            slider1.slideUp('quick');
            fixtures.click(function () {
                if (slider1.is(":hidden")) {
                    $(slider1).slideDown('slow', function () {
                    });
                } else {
                    $(slider1).slideUp('slow');
                }
            });

        });
        new ClipboardJS('#copyToClipboard');
    </script>
@endsection