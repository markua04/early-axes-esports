<h3 class="padding-mobile-left-15">Operator Stats</h3>
<div id="clickme" class="btn button-primary open-close-operator">View/Close Operator Stats</div>
<?php
foreach($operatorStats->operator_records as $operator){
?>
<div class="slide-out-div">
    <div class="col-md-3 col-xs-12 overall-table operator-stats-table">
        <div class="bust">
            <img src="<?php echo '/wp-content/images/busts/'.strtolower($operator->operator->name).'.png' ?>" width="150" class="center-block operator-image">
        </div>
        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Stat</th>
                    <th>Value</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>Role</td>
                    <td><?php echo ucfirst($operator->operator->role) ?></td>
                </tr>
                <tr>
                    <td>Played</td>
                    <td><?php echo $operator->stats->played ?></td>
                </tr>
                <tr>
                    <td>Wins</td>
                    <td><?php echo $operator->stats->wins ?></td>
                </tr>
                <tr>
                    <td>Losses</td>
                    <td><?php echo $operator->stats->losses ?></td>
                </tr>
                <tr>
                    <td>Play Time</td>
                    <td><?php echo gmdate("H:i:s", $operator->stats->playtime); ?></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?php } ?>