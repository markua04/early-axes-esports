<div class="rocket-league-2">
    <div class="padding-l-r-31 col-md-12">
    <div class="row scrollable-table">
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>Wins</th>
                <th>Goals</th>
                <th>MVPS</th>
                <th>Saves</th>
                <th>Shots</th>
                <th>Assists</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td id="rl-overall-wins"></td>
                <td id="rl-overall-goals"></td>
                <td id="rl-overall-mvps"></td>
                <td id="rl-overall-saves"></td>
                <td id="rl-overall-shots"></td>
                <td id="rl-overall-assists"></td>
            </tr>
            </tbody>
        </table>
    </div>
    </div>
    {{--<div class="ranked-stats">--}}
        {{--<h3>Ranked Stats</h3>--}}
        {{--<div class="row scrollable-table">--}}
            {{--<table class="table table-bordered table-second-ranked">--}}
                {{--<thead>--}}
                {{--<tr>--}}
                    {{--<th>Wins</th>--}}
                    {{--<th>Losses</th>--}}
                    {{--<th>Win/Loss Ratio</th>--}}
                    {{--<th>Kills</th>--}}
                    {{--<th>Deaths</th>--}}
                    {{--<th>KD Ratio</th>--}}
                    {{--<th>Play Time</th>--}}
                {{--</tr>--}}
                {{--</thead>--}}
                {{--<tbody>--}}
                {{--<tr>--}}
                    {{--<td id="comp-siege-wins"></td>--}}
                    {{--<td id="comp-siege-losses"></td>--}}
                    {{--<td id="comp-siege-wlr"></td>--}}
                    {{--<td id="comp-siege-kills"></td>--}}
                    {{--<td id="comp-siege-deaths"></td>--}}
                    {{--<td id="comp-siege-kd"></td>--}}
                    {{--<td id="comp-siege-playtime"></td>--}}
                {{--</tr>--}}
                {{--</tbody>--}}
            {{--</table>--}}
        {{--</div>--}}
    {{--</div>--}}
    {{--<h3 class="padding-mobile-left-15">Operator Stats</h3>--}}
    {{--<div id="clickme" class="btn btn-siege-show-hide-ops button-primary open-close-operator">View/Close Operator Stats</div>--}}
    {{--<div class="slide-out-div">--}}
        {{--<div id="operator-table"></div>--}}
    {{--</div>--}}
</div>
