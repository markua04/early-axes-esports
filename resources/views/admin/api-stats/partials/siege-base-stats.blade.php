<div class="row siege-stats">
    <div class="padding-l-r-31 col-md-12">
    <img style="margin-top:5px;margin-bottom:5px" width="100%" src="/images/siege/siege-stats.jpeg">
    <div class="col-md-6">
        <div class="col-md-12 center-mobile-text">
            <img id="ss-siege-platform" src="" width="150" class="banner">
        </div>
        <div class="col-md-12">
            <h3 class="center-mobile-text">RB6 Siege Stats for <span class="blue-text" id="ss-siege-username"></span></h3>
            <h4 style="line-height: 15px;" class="center-mobile-text text-level-xp">Level : <span class="blue-text" id="ss-siege-level"></span></h4>
            <h4 style="line-height: 15px;" class="center-mobile-text text-level-xp">XP : <span class="blue-text" id="ss-siege-xp"></span></h4>
        </div>
    </div>
    <div class="col-md-6">
        <div class="col-md-4">
            <img id="mu-siege-op-img" width="170" src="" class="center-block operator-image">
        </div>
        <div class="col-md-8 center-mobile-text">
            <h3>Most Used Op : <span class="blue-text" id="mu-siege-name"></span></h3>
            <p class="most-used-p">CTU : <span id="mu-siege-ctu"></span></p>
            <p class="most-used-p">Played : <span id="mu-siege-played"></span></p>
            <p class="most-used-p">Kills : <span id="mu-siege-kills"></span></p>
            <p class="most-used-p">Play Time : <span id="mu-siege-playtime"></span></p>
        </div>
    </div>
    </div>
</div>
