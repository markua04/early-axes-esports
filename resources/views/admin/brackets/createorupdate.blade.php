@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row margin-bottom-15-p">
            <div class="col-md-12 margin-top-10">
                <div class="margin-t-50 panel panel-default">
                    <div class="panel-heading">{{ "Create Fixture" }}</div>
                    <div class="panel-body">
                        <form class="form-horizontal" method="POST" enctype="multipart/form-data"
                              action="{{ "/admin/create-brackets" }}">
                            {{ csrf_field() }}
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="name" class="col-md-12 control-label">Select Tournament</label>
                                    <div class="col-md-12">
                                        <select name="tournament_id" class="fstdropdown-select">
                                            <option value="">Select a tournament</option>
                                            @foreach($tournaments as $tournament)
                                                <option value="{{ $tournament->id }}">{{ $tournament->tournament_title }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="pool" class="col-md-12 control-label">Pools</label>
                                    <div class="col-md-12">
                                        <select name="pool" class="fstdropdown-select">
                                            <option value="1">Pool 1</option>
                                            <option value="2">Pool 2</option>
                                            <option value="3">Pool 3</option>
                                            <option value="4">Pool 4</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <input id="bracket" class="bracket" type="hidden" name="bracket" value="1">
                            <div id="app2">
                                <div class="col-md-4">
                                    <label for="name" class="control-label">Select Date and Time</label>
                                    <div id="app2">
                                        <date-time></date-time>
                                    </div>
                                </div>
                                <div class="col-md-12 margin-t-10">
                                    <p id="count"></p>
                                    <table id="myTable" class="table table-bordered dark-table scrollable-table">
                                        <thead>
                                        <tr>
                                            <th>Clan</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr></tr>
                                        </tbody>
                                    </table>
                                    <div onclick="addRow()" class="btn btn-primary">Add bracket row</div>
                                </div>
                            </div>

                            <div class="form-group col-md-12">
                                <div class="col-md-12 margin-t-10">
                                    <button type="submit" class="btn btn-primary pull-left">
                                        {{ "Create" }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        let x = 1;
        function addRow() {
            document.getElementById('count').innerText = x;
            x++;
            let clans = <?php echo json_encode($clansObj) ?>;
            let data = JSON.parse(clans);
            let newData = "";
            Object.keys(data).forEach(function(key) {
                newData += '<option value="' + data[key].id +'">' + data[key].name + '</option>';
            });
            let tableRef = document.getElementById('myTable');
            let newRow = tableRef.insertRow(-1);
            let newCell = newRow.insertCell(0);
            var e = document.createElement('select');
            e.className = "fstdropdown-select";
            e.innerHTML = newData;
            e.name = "clans[]";
            newCell.appendChild(e);
            setFstDropdown().rebind();
        }
    </script>
    <style>
        .vdatetime-input {
            display: block;
            width: 100%;
            height: 31px;
            padding: 6px 12px;
            font-size: 12px;
            line-height: 1.42857;
            color: #555555;
            background-color: white;
            background-image: none;
            border: 1px solid #cccccc;
            border-radius: 4px;
            -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
            -webkit-transition: border-color ease-in-out 0.15s, box-shadow ease-in-out 0.15s;
            transition: border-color ease-in-out 0.15s, box-shadow ease-in-out 0.15s;
        }
    </style>
@endsection
