@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row margin-bottom-15-p">
            <div class="col-md-12 margin-top-10">
                <div class="margin-t-50 panel panel-default">
                    <div class="panel-heading">{{ request()->route('id') ? "Edit Event" : "Create Event" }}</div>
                    <div class="panel-body">
                          <form class="form-horizontal" method="POST" enctype="multipart/form-data" action="{{ request()->route('id') ? "/admin/edit-event/".request()->route('id') : "/admin/create-event" }}">
                            {{ csrf_field() }}
                            <div class="col-md-12">
                                <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                                    <label for="name" class="col-md-12 control-label">Title</label>
                                    <div class="col-md-12">
                                        <input id="name" type="text" class="form-control" name="title"
                                               value="{{ isset($event->title) ? $event->title : "" }}">
                                        @include('alerts.error', ['error' => 'title'])
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name" class="col-md-12 control-label">Date</label>
                                    <div class="col-md-12">
                                        <input value="{{ isset($event->date) ? $event->date : "" }}" name="date" class="form-control" type="text" id="datepicker">
                                        @include('alerts.error', ['error' => 'date'])
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                                    <label for="name" class="col-md-12 control-label">Time</label>
                                    <div class="col-md-12">
                                        <input value="{{ isset($event->time) ? $event->time : "" }}" name="time" id="timepicker1" type="text" class="form-control input-small">
                                        @include('alerts.error', ['error' => 'time'])
                                    </div>
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                                <label for="description" class="col-md-12 control-label text-left-label"></label>
                                <div class="col-md-12">
                                    <textarea id="editor" name='description' class="form-control editor">{{ isset($event->description) ? $event->description : "" }}</textarea>
                                    @include('alerts.error', ['error' => 'description'])
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="name" class="col-md-12 control-label">Select Game</label>
                                    <div class="col-md-12">
                                        <select name="game_id">
                                            @foreach($games as $game)
                                                <option value="{{ $game->id }}">{{ $game->game_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="gamer_pic" class="col-md-12 control-label">Upload Event Banner</label>
                                <div class="col-md-12">
                                    @if(Session::has('success'))
                                        <div class="alert-box success">
                                            <h2>{!! Session::get('success') !!}</h2>
                                        </div>
                                    @endif
                                    <div class="control-group">
                                        {!! Form::file('image',['class' => 'upload-image','type' => 'file','data-max-size' => '2048']) !!}
                                        <p class="errors">{!!$errors->first('image')!!}</p>
                                        @if(Session::has('error'))
                                            <p class="errors">{!! Session::get('error') !!}</p>
                                        @endif
                                    </div>
                                    <div id="success"></div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-primary pull-left">
                                        {{ request()->route('id') ? "Update" : "Create" }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
