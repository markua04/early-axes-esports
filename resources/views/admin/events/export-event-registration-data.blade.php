@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="col-md-12 margin-top-10">
            <div class="margin-t-50 panel panel-default">
                <div class="panel-heading">Generate Event Export Data</div>
                <div class="panel-body">
                    <form class="form-horizontal" method="POST" enctype="multipart/form-data" action="/admin/export-event-data/">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="name" class="col-md-12 control-label">Event</label>
                            <div class="col-md-12">
                                <select name="event_id">
                                    @foreach($events as $event)
                                        <option value="{{ $event->id }}">{{ $event->title }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('csv_name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-12 control-label">CSV Title</label>
                            <div class="col-md-12">
                                <input id="name" type="text" class="form-control" name="csv_name" value="{{ old('csv_name') }}" >
                                @if ($errors->has('csv_name'))
                                    <span class="help-block">
                                    <strong>{{ $errors->first('csv_name') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary pull-right">
                                    Export
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

