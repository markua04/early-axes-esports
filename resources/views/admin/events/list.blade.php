@extends('layouts.app')

@section('content')
    <div class="container margin-b-30">
        <h1 class="tournament-head">Events</h1>
        @foreach($events as $event)
            <div class="col-md-12 event-wrap">
            <div class="col-md-3 remove-padding-col-md-3" style="margin-top: 4px;">
                <a class="normal-header-link" href="/view-event/{{ $event->id }}/{{ str_slug($event->title, '-') }}">
                <img src="{{ $event->banner }}" class="">
                </a>
            </div>
            <div class="col-md-9" style="font-size:14px;">
            <a class="normal-header-link" href="/view-event/{{ $event->id }}/{{ str_slug($event->title, '-') }}"><div class="normal-header-link">{{$event->title}}</div></a>
                <div class="col-md-6 col-sm-6">Date : {{ $event->date  }}</div>
                <div class="col-md-6 col-sm-6">Time : {{ $event->time  }}</div>
                <br/>
            <span>{!! strip_tags(str_limit($event->description,1500, '')) !!}<a style="font-size:13px;color:black;" class="read-more" href="/view-event/{{ $event->id }}/{{ str_slug($event->title, '-') }}">&nbsp;&nbsp;read more...</a></span>
            </div>
            @if(Auth::check() && Auth::user()->user_level_id == 1)
                <div onclick="return confirm('Are you sure?')" class="edit-link"><a href="/admin/delete-event/{{ $event->id }}">Delete</a></div>
                <div class="edit-link"><a href="/admin/edit-event/{{ $event->id }}">Edit</a></div>
            @endif
            </div>
        @endforeach
        {{ $events->links() }}
    </div>
@endsection