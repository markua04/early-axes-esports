@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="home-page-banner"><img src="{{ $event->banner }}"></div>
        <div class="col-md-12 margin-t-5 home-page">
            <div class="home-page-banner-2"><img src="{{ $event->banner }}"></div>
            <h3>{{ $event->title }}</h3>
            <div class="row">
                <div class="col-md-6 bold-content">
                    <p>Date : {{ $event->date }}</p>
                </div>
                <div class="col-md-6 bold-content">
                    <p>Time : {{ $event->time }}</p>
                </div>
            </div>
            <span>{!! $event->description !!}</span>
            <br/><br/>
        </div>
        @if($todaysDate <= $event->date)
            <div id="success" style="display:none;" class="alert alert-success">
                You have registered successfully for the event!
            </div>
            <div class="col-md-12" id="event">
                <h3>Register for event</h3>
                <div class="hide form-group col-md-6 {{ $errors->has('event_id') ? ' has-error' : '' }}">
                    <div class="col-md-12">
                        <input id="event_id" type="text" class="form-control" name="event_id" value="{{ $event->id }}">
                        @if ($errors->has('event_id'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>

                <div class="form-group col-md-6 {{ $errors->has('name') ? ' has-error' : '' }}">
                    <label for="name" class="col-md-12 control-label">Name</label>
                    <div class="col-md-12">
                        <input id="name" required="required" type="text" class="form-control" name="name"
                               value="{{ Auth::check() ? Auth::user()->name : '' }}">
                        @if ($errors->has('name'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>

                <div class="form-group col-md-6 {{ $errors->has('surname') ? ' has-error' : '' }}">
                    <label for="surname" class="col-md-12 control-label">Surname</label>
                    <div class="col-md-12">
                        <input id="surname" required="required" type="text" class="form-control" name="surname"
                               value="{!! Auth::check() ? Auth::user()->surname : '' !!}">
                        @if ($errors->has('surname'))
                            <span class="help-block">
                                    <strong>{{ $errors->first('surname') }}</strong>
                                </span>
                        @endif
                    </div>
                </div>

                <div class="form-group col-md-6 {{ $errors->has('gamer_tag') ? ' has-error' : '' }}">
                    <label for="name" class="col-md-12 control-label">Gamer Tag</label>
                    <div class="col-md-12">
                        <input id="gamer_tag" required="required" type="text" class="form-control" name="gamer_tag"
                               value="{!! Auth::check() ? Auth::user()->gamer_tag : '' !!}">
                        @if ($errors->has('gamer_tag'))
                            <span class="help-block">
                                    <strong>{{ $errors->first('gamer_tag') }}</strong>
                                </span>
                        @endif
                    </div>
                </div>

                <div class="form-group col-md-6{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label for="name" class="col-md-12 control-label">Email</label>
                    <div class="col-md-12">
                        <input id="email" required="required" type="text" class="form-control" name="email"
                               value="{{ Auth::check() ? Auth::user()->email : '' }}">
                        @if ($errors->has('email'))
                            <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                        @endif
                    </div>
                </div>

                <div class="form-group col-md-12">
                    <div class="col-md-12">
                        <button class="btn btn-primary pull-left register">
                            Register
                        </button>
                    </div>
                </div>
            </div>
        @else
            <div class="col-md-12">
                <h3>Register for event</h3>
                <p>Event registrations are closed for this event as the event has finished.</p>
                <a style="color:#007AF7" href="/list-events">Click here to view other events</a>
            </div>
        @endif

    </div>

    <script>
        $(document).ready(function () {
            $('.register').click(function () {
                $("#btn").attr("type", "submit");
                $.ajax({
                    url: '/event-registration/',
                    type: 'post',
                    _method: 'POST',
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    data: {
                        event_id: $('#event_id').val(),
                        name: $('#name').val(),
                        surname: $('#surname').val(),
                        email: $('#email').val(),
                        gamer_tag: $('#gamer_tag').val()
                    },
                    success: function (response) {
                        $('#success').show();
                        $('#event').fadeOut();
                    }
                });
            });
        });

    </script>
@endsection