@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row margin-bottom-15-p">
            <div class="col-md-12 margin-top-10">
                <div class="margin-t-50 panel panel-default">
                    <div class="panel-heading">{{ request()->route('id') ? "Edit Fixture" : "Create Round Robin Fixtures" }}</div>
                    <div class="panel-body">
                        <form class="form-horizontal" method="POST" enctype="multipart/form-data"
                              action="{{ request()->route('id') ? "/admin/edit-fixture/".request()->route('id') : "/admin/create-auto-fixtures" }}">
                            {{ csrf_field() }}
                            <input type="hidden" id="tournament_id" name="tournament_id" value="{{ $tournamentId }}">
                            <div id="app2">
                                <div class="col-md-6">
                                    <label for="pool" class="control-label">Select Fixtures Start Date Time</label>
                                   <date-time :dt_name="{{ json_encode(['date_time']) }}"></date-time>
                                </div>
                                <div class="col-md-6">
                                    <label for="pool" class="control-label">Pools</label>
                                    <select name="pool" class="fstdropdown-select">
                                        <option value="1">Pool 1</option>
                                        <option value="2">Pool 2</option>
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <label for="pool" class="control-label">Excluded Date Range Fixtures Start Date</label>
                                    <date-time :dt_name="{{ json_encode(['start_date_range']) }}"></date-time>
                                </div>
                                <div class="col-md-6">
                                    <label for="pool" class="control-label">Excluded Date Range Fixtures End Date</label>
                                    <date-time :dt_name="{{ json_encode(['end_date_range']) }}"></date-time>
                                </div>
                            </div>

                            <div class="col-md-12 margin-t-10">
                                <p id="count"></p>
                                <table id="myTable" class="table table-bordered dark-table scrollable-table">
                                    <thead>
                                    <tr>
                                        <th>Clan</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr></tr>
                                    </tbody>
                                </table>
                                <div onclick="addRow()" class="btn btn-primary">Add fixture row</div>
                            </div>

                            <div class="form-group col-md-12">
                                <div class="col-md-12 margin-t-10">
                                    <button type="submit" class="btn btn-primary pull-left">
                                        {{ request()->route('id') ? "Update" : "Create" }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        let x = 1;
        function addRow() {
            document.getElementById('count').innerText = x + ' clans selected';
            x++;
            let clans = <?php echo json_encode($clansObj) ?>;
            let data = JSON.parse(clans);
            let newData = "";
            Object.keys(data).forEach(function(key) {
                newData += '<option value="' + data[key].id +'">' + data[key].name + '</option>';
            });
            let tableRef = document.getElementById('myTable');
            let newRow = tableRef.insertRow(-1);
            let newCell = newRow.insertCell(0);
            var e = document.createElement('select');
            e.className = "fstdropdown-select";
            e.innerHTML = newData;
            e.name = "clans[]";
            newCell.appendChild(e);
            setFstDropdown().rebind();
        }
    </script>
    <style>
        .vdatetime-input {
            display: block;
            width: 100%;
            height: 31px;
            padding: 6px 12px;
            font-size: 12px;
            line-height: 1.42857;
            color: #555555;
            background-color: white;
            background-image: none;
            border: 1px solid #cccccc;
            border-radius: 4px;
            -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
            -webkit-transition: border-color ease-in-out 0.15s, box-shadow ease-in-out 0.15s;
            transition: border-color ease-in-out 0.15s, box-shadow ease-in-out 0.15s;
        }
    </style>
@endsection
