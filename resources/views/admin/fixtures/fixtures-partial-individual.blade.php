@if(isset($fixturesPool1))
    <?php $x = 1; ?>
    @if(!$fixturesPool1->isEmpty())
    <div class="row fixture ">
        <div style="background-color: #000;" class="col-md-12">
            <h3 class="pool-heading">Pool 1</h3>
        </div>
        <div id="pool1" class="btn button-primary open-close-operator">View/Close Pool 1</div>
        <div class="row slide-out-div-1">
        @foreach($fixturesPool1 as $fixture)
            <div class="col-md-4 card height-345 margin-t-25 padding-t-10">
                <div class="row fixture-num">Fixture {{ $x }}</div>
                <div class="row date-time-bar">
                    <div style="padding-left:0;" class="col-md-5 text-center date-time-fixture">
                        Date: {{ $fixture->date }}
                    </div>
                    <div class="col-md-2"></div>
                    <div class="col-md-5 text-center date-time-fixture">
                        Time: {{ $fixture->time }}
                    </div>
                </div>
                <div class="col-md-12">
                <div class="row date-time-bar-mobile">
                    <div class="col-xs-6 text-center">
                        Date: {{$fixture->date}}
                    </div>
                    <div class="col-xs-6 text-center">
                        Time: {{ $fixture->time }}
                    </div>
                </div>
                </div>
                <div class="col-md-12 top-bar-fixture padding-l-r-0">
                    <div style="background-color:#727272 !important;color:#fff !important;"
                         class="col-md-5 padding-l-r-0 padding-t-10 padding-b-mobile text-center padding-t-10 clan-name-fixture">
                        @if(isset($fixture->user))
                            @if($fixture->user->gamer_tag !== "")
                                <p class="player-gt"><b>{{ $fixture->user->gamer_tag }}</b></p>
                            @elseif($fixture->user->psn_id !== "")
                                <p class="player-gt"><b>{{ $fixture->user->psn_id }}</b></p>
                            @elseif($fixture->user->uplay_id !== "")
                                <p class="player-gt"><b>{{ $fixture->user->uplay_id }}</b></p>
                            @elseif($fixture->user->steam_id !== "")
                                <p class="player-gt"><b>{{ $fixture->user->steam_id }}</b></p>
                            @endif
                        @else
                            <p class="player-gt"><b>N/A</b></p>
                        @endif
                    </div>
                    <div class="wrap-vs-mobile col-md-2" style="color:#000 !important;font-size: 18.5px;text-align: center;"><b class="vs-mobile">VS</b>
                    </div>
                    <div style="background-color:#727272 !important;color:#fff !important;"
                         class="col-md-5 padding-l-r-0 padding-t-10 padding-b-mobile text-center clan-name-fixture">
                        @if(isset($fixture->user))
                            @if($fixture->user2->gamer_tag !== "")
                                <p class="player-gt"><b>{{ $fixture->user2->gamer_tag }}</b></p>
                            @elseif($fixture->user2->psn_id !== "")
                                <p class="player-gt"><b>{{ $fixture->user2->psn_id }}</b></p>
                            @elseif($fixture->user2->uplay_id !== "")
                                <p class="player-gt"><b>{{ $fixture->user2->uplay_id }}</b></p>
                            @elseif($fixture->user2->steam_id !== "")
                                <p class="player-gt"><b>{{ $fixture->user2->steam_id }}</b></p>
                            @endif
                        @else
                        <p class="player-gt"><b>N/A</b></p>
                        @endif
                    </div>
                </div>
                <div class="col-md-5 padding-l-r-0">
                    @if($fixture->user)
                    <img width="100%" height="auto" src="{{ $fixture->user->gamer_pic ? $fixture->user->gamer_pic : '/images/default-no-image-eax.jpeg' }}" class="clan1 user-1-fixture">
                    @endif
                </div>
                <div class="col-md-2 vs-fixture user-1-fixture">
                    VS
                </div>
                <div class="col-md-5 padding-l-r-0">
                    @if($fixture->user2)
                    <img width="100%" height="auto" src="{{ $fixture->user2->gamer_pic ? $fixture->user2->gamer_pic : '/images/default-no-image-eax.jpeg' }}" class="clan1 user-2-fixture">
                    @endif
                </div>
            </div>
            <?php $x++; ?>
        @endforeach
        </div>
    </div>
    @endif
@endif
@if(isset($fixturesPool2))
    <?php $x = 1; ?>
    @if(!$fixturesPool2->isEmpty())
        <div class="row fixture ">
            <div style="background-color: #000;" class="col-md-12">
                <h3 class="pool-heading">Pool 1</h3>
            </div>
            <div id="pool1" class="btn button-primary open-close-operator">View/Close Pool 1</div>
            <div class="row slide-out-div-1">
                @foreach($fixturesPool2 as $fixture)
                    <div class="col-md-4 card height-345 margin-t-25 padding-t-10">
                        <div class="row fixture-num">Fixture {{ $x }}</div>
                        <div class="row date-time-bar">
                            <div style="padding-left:0;" class="col-md-5 text-center date-time-fixture">
                                Date: {{ $fixture->date }}
                            </div>
                            <div class="col-md-2"></div>
                            <div class="col-md-5 text-center date-time-fixture">
                                Time: {{ $fixture->time }}
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="row date-time-bar-mobile">
                                <div class="col-xs-6 text-center">
                                    Date: {{$fixture->date}}
                                </div>
                                <div class="col-xs-6 text-center">
                                    Time: {{ $fixture->time }}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 top-bar-fixture padding-l-r-0">
                            <div style="background-color:#727272 !important;color:#fff !important;"
                                 class="col-md-5 padding-l-r-0 padding-t-10 padding-b-mobile text-center padding-t-10 clan-name-fixture">
                                @if(isset($fixture->user))
                                    <p class="player-gt"><b>{{ $fixture->user->gamer_tag }}</b></p>
                                @else
                                    <p class="player-gt"><b>N/A</b></p>
                                @endif
                            </div>
                            <div class="wrap-vs-mobile col-md-2" style="color:#000 !important;font-size: 18.5px;text-align: center;"
                            ><b class="vs-mobile">VS</b>
                            </div>
                            <div style="background-color:#727272 !important;color:#fff !important;"
                                 class="col-md-5 padding-l-r-0 padding-t-10 padding-b-mobile text-center clan-name-fixture">
                                @if(isset($fixture->user))
                                    <p class="player-gt"><b>{{ $fixture->user2->gamer_tag }}</b></p>
                                @else
                                    <p class="player-gt"><b>N/A</b></p>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-5 padding-l-r-0">
                            @if($fixture->user)
                                <img width="100%" height="auto" src="{{ $fixture->user->gamer_pic ? $fixture->user->gamer_pic : '/images/default-no-image-eax.jpeg' }}" class="clan1 user-1-fixture">
                            @endif
                        </div>
                        <div class="col-md-2 vs-fixture user-1-fixture">
                            VS
                        </div>
                        <div class="col-md-5 padding-l-r-0">
                            @if($fixture->user2)
                                <img width="100%" height="auto" src="{{ $fixture->user2->gamer_pic ? $fixture->user2->gamer_pic : '/images/default-no-image-eax.jpeg' }}" class="clan1 user-2-fixture">
                            @endif
                        </div>
                    </div>
                    <?php $x++; ?>
                @endforeach
            </div>
        </div>
    @endif
@endif