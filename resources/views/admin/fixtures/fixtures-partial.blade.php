@if(isset($fixturesPool1))
    <style>
        .vs {
            font-size: 30px;
            font-weight: bold;
            color: #000;
            margin-top: 65px;
            margin-left: 10px;
        }
        .result-fixture {
            height:36px;
        }
    </style>
    <?php $x = 1; ?>
    <div class="row fixture ">
        <div style="background-color: #000;" class="col-md-12">
            <h3 class="pool-heading">Pool 1</h3>
        </div>
        <div id="pool1" class="btn button-primary open-close-operator">View/Close Pool 1</div>
        <div class="row slide-out-div-1">
        @foreach($fixturesPool1 as $fixture)
            <div class="col-md-4 margin-t-25 padding-t-10">
                <div class="row fixture-num">Fixture {{ $x }}</div>
                <div class="col-md-12 top-bar-fixture padding-l-r-0">
                    <div class="mobile-date-time-fixtures col-md-4 date-time-bar" style="color:#fff !important;text-align: center;font-size: 12px;margin-top: 15px;">
                        <b>Date : {{ date('d-m-Y', strtotime($fixture->date)) }} Time : {{ date('H:ia', strtotime($fixture->time)) }}</b>
                    </div>
                    <div style="background-color:#727272 !important;color:#fff !important;margin-top: 0 !important;" class="col-md-6 padding-l-r-0 padding-t-10 padding-b-mobile text-center padding-t-10 clan-name-fixture">
                        <p><b>{{ strtoupper($fixture->clan_1) }}</b></p>
                    </div>
                    <div style="background-color:#727272 !important;color:#fff !important;"
                         class="col-md-6 padding-l-r-0 padding-t-10 padding-b-mobile text-center clan-name-fixture">
                        <p><b>{{ strtoupper($fixture->clan_2) }}</b></p>
                    </div>
                </div>
                <div class="col-md-4 padding-l-r-0 clan-image">
                    <img width="100%" height="125px" src="{{ $fixture->clan_1_image }}" class="clan1">
                </div>
                <div class="dt-date-time-fixtures col-md-4" style="color:#000 !important;text-align: center;margin-top:10%;font-size: 15px;">
                    <b>{{ date('d-m-Y', strtotime($fixture->date)) }}</b>
                    <br />
                    <b>{{ date('H:ia', strtotime($fixture->time)) }}</b>
                </div>
                <div class="col-md-4 padding-l-r-0 clan-image">
                    <img width="100%" height="125px" src="{{ $fixture->clan_2_image }}" class="clan2">
                </div>
                    <div class="col-md-12 margin-t-10">
                        @if(count($fixture->fixtureResult) > 0)
                            @foreach($fixture->fixtureResult as $result)
                                @if($result->clan_1_score == $result->clan_2_score)
                                    <p style="height: 36px;background-color: #4589f9;" class="btn btn-primary btn-xs">Draw</p>
                                @elseif($result->clan_1_score > $result->clan_2_score)
                                    <p style="height: 36px;background-color: #4589f9;" class="btn btn-primary btn-xs">Winner : {{ $result->clan_1 }}</p>
                                @else
                                    <p style="height: 36px;background-color: #4589f9;" class="btn btn-primary btn-xs">Winner : {{ $result->clan_2 }}</p>
                                @endif
                                @break
                            @endforeach
                        @elseif(Auth::user())
                            <a href="/{{ $fixture->id }}/{{ Auth::user()->clan_id }}/submit-fixture-result"><div class="btn btn-primary result-fixture">Submit Result</div></a>
                        @endif
                    </div>
            </div>
            <?php $x++; ?>
        @endforeach
        </div>
    </div>
@endif
@if(isset($fixturesPool2))
    <?php $x = 1; ?>
    <div class="row fixture">
        <div style="background-color: #000;" class="col-md-12">
            <h3 class="pool-heading">Pool 2</h3>
        </div>
        <div id="pool2" class="btn button-primary open-close-operator">View/Close Pool 2</div>
        <div class="row slide-out-div-2">
        @foreach($fixturesPool2 as $fixture)
                <div class="col-md-4 margin-t-25 padding-t-10">
                    <div class="row fixture-num">Fixture {{ $x }}</div>
                    <div class="col-md-12 top-bar-fixture padding-l-r-0">
                        <div class="mobile-date-time-fixtures col-md-4 date-time-bar" style="color:#fff !important;text-align: center;font-size: 12px;margin-top: 15px;">
                            <b>Date : {{ date('d-m-Y', strtotime($fixture->date)) }} Time : {{ date('H:ia', strtotime($fixture->time)) }}</b>
                        </div>
                        <div style="background-color:#727272 !important;color:#fff !important;margin-top: 0 !important;" class="col-md-6 padding-l-r-0 padding-t-10 padding-b-mobile text-center padding-t-10 clan-name-fixture">
                            <p><b>{{ strtoupper($fixture->clan_1) }}</b></p>
                        </div>
                        <div style="background-color:#727272 !important;color:#fff !important;"
                             class="col-md-6 padding-l-r-0 padding-t-10 padding-b-mobile text-center clan-name-fixture">
                            <p><b>{{ strtoupper($fixture->clan_2) }}</b></p>
                        </div>
                    </div>
                    <div class="col-md-4 padding-l-r-0 clan-image">
                        <img width="100%" height="125px" src="{{ $fixture->clan_1_image }}" class="clan1">
                    </div>
                    <div class="dt-date-time-fixtures col-md-4" style="color:#000 !important;text-align: center;margin-top:10%;font-size: 15px;">
                        <b>{{ date('d-m-Y', strtotime($fixture->date)) }}</b>
                        <br />
                        <b>{{ date('H:ia', strtotime($fixture->time)) }}</b>
                    </div>
                    <div class="col-md-4 padding-l-r-0 clan-image">
                        <img width="100%" height="125px" src="{{ $fixture->clan_2_image }}" class="clan2">
                    </div>
                    <div class="col-md-12 margin-t-10">
                        @if(count($fixture->fixtureResult) > 0)
                            @foreach($fixture->fixtureResult as $result)
                                @if($result->clan_1_score == $result->clan_2_score)
                                    <p style="height: 36px;background-color: #4589f9;" class="btn btn-primary btn-xs">Draw</p>
                                @elseif($result->clan_1_score > $result->clan_2_score)
                                    <p style="height: 36px;background-color: #4589f9;" class="btn btn-primary btn-xs">Winner : {{ $result->clan_1 }}</p>
                                @else
                                    <p style="height: 36px;background-color: #4589f9;" class="btn btn-primary btn-xs">Winner : {{ $result->clan_2 }}</p>
                                @endif
                                @break
                            @endforeach
                        @elseif(Auth::user())
                            <a href="/{{ $fixture->id }}/{{ Auth::user()->clan_id }}/submit-fixture-result"><div class="btn btn-primary result-fixture">Submit Result</div></a>
                        @endif
                    </div>
                </div>
            <?php $x++; ?>
        @endforeach
        </div>
    </div>
@endif