@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row margin-bottom-15-p">
            <div class="col-md-12 margin-top-10">
                <div class="margin-t-50 panel panel-default">
                    <div class="panel-heading">{{ request()->route('id') ? "Edit Individual Fixture" : "Create Individual Fixture" }}</div>
                    <div class="panel-body">
                        <form class="form-horizontal" method="POST" enctype="multipart/form-data"
                              action="{{ request()->route('id') ? "/admin/edit-individual-fixture".request()->route('id') : "/admin/create-individual-fixture" }}">
                            {{ csrf_field() }}
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="name" class="col-md-12 control-label">Select Tournament</label>
                                    <div class="col-md-12">
                                        <select name="tournament_id" class="fstdropdown-select">
                                            <option value="">Select a tournament</option>
                                            @foreach($tournaments as $tournament)
                                                <option value="{{ $tournament->id }}">{{ $tournament->tournament_title }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="pool" class="col-md-12 control-label">Pools</label>
                                    <div class="col-md-12">
                                        <select name="pool" class="fstdropdown-select">
                                            <option value="1">Pool 1</option>
                                            <option value="2">Pool 2</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div id="app2">
                                <input id="test12" class="test12" type="hidden" value="">
                                <div class="col-md-4">
                                    <label for="name" class="control-label">Select Date and Time</label>
                                    <datetime name="date_time" type="datetime" v-model="datetime"></datetime>
                                </div>
                                <div class="col-md-12 margin-t-10">
                                    <table id="myTable" class="table table-bordered dark-table scrollable-table">
                                        <thead>
                                        <tr>
                                            <th>Player 1</th>
                                            <th>Player 2</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <div onclick="addRow()" class="btn btn-primary">Add fixture row</div>
                                </div>
                            </div>

                            <div class="form-group col-md-12">
                                <div class="col-md-12 margin-t-10">
                                    <button type="submit" class="btn btn-primary pull-left">
                                        {{ request()->route('id') ? "Update" : "Create" }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>

        function addRow() {
            let users = <?php echo json_encode($usersObj) ?>;
            let data = JSON.parse(users);
            let newData = "";
            Object.keys(data).forEach(function(key) {
                newData += '<option value="' + data[key].id +'">' + data[key].name + '</option>';
            });

            // Get a reference to the table
            let tableRef = document.getElementById('myTable');

            // Insert a row at the end of the table
            let newRow = tableRef.insertRow(-1);

            // Insert a cell in the row at index 0
            let newCell = newRow.insertCell(0);
            let newCell1 = newRow.insertCell(1);

            // Append a text node to the cell
            var e = document.createElement('select');
            e.className = "fstdropdown-select";
            e.innerHTML = newData;
            e.name = "users_1[]";
            var t = document.createElement('select');
            t.className = "fstdropdown-select";
            t.innerHTML = newData;
            t.name = "users_2[]";
            newCell.appendChild(e);
            newCell1.appendChild(t);
            setFstDropdown().rebind();
        }
    </script>
    <style>
        .vdatetime-input {
            display: block;
            width: 100%;
            height: 31px;
            padding: 6px 12px;
            font-size: 12px;
            line-height: 1.42857;
            color: #555555;
            background-color: white;
            background-image: none;
            border: 1px solid #cccccc;
            border-radius: 4px;
            -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
            -webkit-transition: border-color ease-in-out 0.15s, box-shadow ease-in-out 0.15s;
            transition: border-color ease-in-out 0.15s, box-shadow ease-in-out 0.15s;
        }
    </style>
@endsection

