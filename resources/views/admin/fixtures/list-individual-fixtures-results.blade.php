@extends('layouts.app')

@section('content')
    <div class="container">
        <h2>List of all the {{ $tournament->tournament_title }} Fixture results in the system</h2>
        <table class="table table-bordered dark-table scrollable-table">
            <thead>
            <tr>
                <th>Player 1</th>
                <th>Player 1 Score</th>
                <th>Player 2</th>
                <th>Player 2 Score</th>
                <th>Screenshots</th>
            </tr>
            </thead>
            <tbody>
            @foreach($fixtureResults as $fixture)
                <tr>
                    <td>{{ $fixture->user->gamer_tag }}</td>
                    <td>{{ $fixture->player_1_score }}</td>
                    <td>{{ $fixture->user2->gamer_tag }}</td>
                    <td>{{ $fixture->player_2_score }}</td>
                    <td><a href="/admin/download-screenshots/individual-fixtures/{{ $fixture->fixture_id }}">Download Screenshots</a></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection