@extends('layouts.app')

@section('content')
    <div class="container">
        <h2>List of all the {{ $tournament->tournament_title }} Individual Fixtures in the system</h2>
        <table class="table table-bordered dark-table scrollable-table">
            <thead>
            <tr>
                <th>Player 1</th>
                <th>Player 2</th>
                <th>Date</th>
                <th>Time</th>
                <th>Submit Result</th>
                <th>Delete</th>
            </tr>
            </thead>
            <tbody>
            @foreach($fixtures as $fixture)
                <tr>
                    <td>{{ $fixture->user->gamer_tag }}</td>
                    <td>{{ $fixture->user2->gamer_tag }}</td>
                    <td>{{ $fixture->date }}</td>
                    <td>{{ $fixture->time }}</td>
                    <td><a href="/fixture/{{ $fixture->id }}/submit-individual-fixture-result">Submit</td>
                    <td><a href="/admin/delete-individual-fixture/{{ $fixture->id }}/">Delete</a></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection