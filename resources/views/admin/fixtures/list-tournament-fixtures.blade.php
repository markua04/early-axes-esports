@extends('layouts.app')

@section('content')
    <div class="container">
        <h2>List of all the {{ $tournament->tournament_title }} Fixtures in the system</h2>
        <table class="table table-bordered dark-table scrollable-table">
            <thead>
            <tr>
                <th>Clan 1</th>
                <th>Clan 2</th>
                <th>Date</th>
                <th>Time</th>
                <th>Submit Result</th>
                <th>Delete</th>
            </tr>
            </thead>
            <tbody>
            @foreach($fixtures as $fixture)
                <tr>
                    <td>{{ $fixture->clan_1 }}</td>
                    <td>{{ $fixture->clan_2 }}</td>
                    <td>{{ $fixture->date }}</td>
                    <td>{{ $fixture->time }}</td>
                    <td>
                    @if(checkIfClanResultSubmitted($fixture->id) == 'no')
                        <a href="/{{$fixture->id}}/{{$fixture->clan_1_id}}/submit-fixture-result" class="btn btn-primary btn-xs">
                            Submit Result
                        </a>
                    @else
                        {{ getFixtureResult($fixture->id) }}
                    @endif
                    </td>
                    <td><a href="/admin/delete-fixture/{{ $fixture->id }}/">Delete</a></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection