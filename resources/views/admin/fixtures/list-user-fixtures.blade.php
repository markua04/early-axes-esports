@extends('layouts.app')

@section('content')
    @include('frontend.fixtures.individual-tournament-standings-modal')
    <div class="container table-height">
        <h2>List of all {{ $user->gamer_tag }}'s Fixtures on the system</h2>
        <table class="table table-bordered dark-table scrollable-table">
            <thead>
            <tr>
                <th>Tournament</th>
                <th>Player 1</th>
                <th>Player 2</th>
                <th>Date</th>
                <th>Time</th>
                <th>Result</th>
                <th>Standings</th>
            </tr>
            </thead>
            <tbody>
            @foreach($fixtures as $fixture)
                <tr>
                    @if($fixture->tournament)
                        <td>{{ $fixture->tournament->tournament_title or '' }}</td>
                    @else
                        <td>N\A</td>
                    @endif
                        <td>{{ $fixture->user->gamer_tag }}</td>
                        <td>{{ $fixture->user2->gamer_tag }}</td>
                        <td>{{ $fixture->date }}</td>
                        <td>{{ $fixture->time }}</td>
                    @if($todaysDate < $fixture->date)
                        <td>
                            Not played yet.
                        </td>
                    @else
                        <td>
                            @if(checkIfUserResultSubmitted($fixture->id) == 'no')
                                <a href="/fixture/{{$fixture->id}}/submit-individual-fixture-result" class="btn btn-primary btn-xs">
                                    Submit Result
                                </a>
                            @else
                                {{ getIndividualFixtureResult($fixture->id) }}
                            @endif
                        </td>
                    @endif
                        <td>
                            <a href="#ex1" rel="modal:open" data-tournament_id="{{ $fixture->tournament_id }}" class="btn btn-primary btn-xs checkStandings">
                                Standings
                            </a>
                        </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    <script>
        $(document).ready(function() {
            let user = "{{ Auth::user()->id }}";
            let tournamentTitle = $("div#tournamentTitle h2");
            let noStandings = $("div#noStandings h2");
            tournamentTitle.hide();
            noStandings.hide();
            $("#ex1").hide();
            $('.checkStandings').on('click',function () {
                $("div#preload h2").html("");
                var tournamentId = $(this).data('tournament_id');
                $.ajax({
                    url: '/get-individual-standings/' + tournamentId,
                    type: 'get',
                    _method: 'GET',
                    headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                    data: { tournamentId: tournamentId},
                    success: function (response) {
                        tournamentTitle.show();
                        $("#ex1").show();
                        let x = 1;
                        tournamentTitle.html(response.tournament.tournament_title);
                        if(response.standings !== null){
                            $('#table1 tr').not(function(){ return !!$(this).has('th').length; }).remove();
                            $.each(response.standings, function(key, value) {
                                let playedGames = value.wins + value.losses + value.draws;
                                let highlight = "";
                                if(value.user_id.toString() === user.toString()){
                                    highlight = "class='highlight-standing'";
                                }
                                let row = $("<tr "+ highlight + " >" +
                                    "<td>" + x + "</td>" +
                                    "<td>" + value.gamer_name + "</td>" +
                                    "<td>" + playedGames + "</td>" +
                                    "<td>" + value.wins +"</td>" +
                                    "<td>" + value.losses +"</td>" +
                                    "<td>" + value.draws +"</td>" +
                                    "<td>" + value.goals_for +"</td>" +
                                    "<td>" + value.goals_against +"</td>" +
                                    "<td>" + value.difference +"</td>" +
                                    "<td>" + value.points +"</td>" +
                                    "</tr>");
                                $("#table1").append(row);
                                x++;
                            });
                        } else{
                            noStandings.show();
                            noStandings.html("No standings have been added for this tournament yet.");
                        }

                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            })
        });
    </script>
@endsection