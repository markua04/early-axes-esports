@extends('layouts.app')

@section('content')
    <div class="container">
        <h2>List of all the {{ $tournament->tournament_title }} Fixture results in the system</h2>
        <table class="table table-bordered dark-table scrollable-table">
            <thead>
            <tr>
                <th>Clan 1</th>
                <th>Clan 2</th>
                <th>Clan 1 Score</th>
                <th>Clan 2 Score</th>
                <th>Date</th>
                <th>Winner</th>
                <th>Screenshots</th>
            </tr>
            </thead>
            <tbody>
            @foreach($fixtureResults as $fixture)
                <tr>
                    <td>{{ $fixture->clan_1 }}</td>
                    <td>{{ $fixture->clan_2 }}</td>
                    <td>{{ $fixture->clan_1_score }}</td>
                    <td>{{ $fixture->clan_2_score }}</td>
                    <td>{{ isset($fixture->fixture->date) ? $fixture->fixture->date  : ''}}</td>
                    @if($fixture->clan_1_score > $fixture->clan_2_score)
                        <td>{{ $fixture->clan_1 }}</td>
                    @elseif($fixture->clan_1_score == $fixture->clan_2_score)
                        <td>Draw</td>
                    @elseif($fixture->clan_1_score < $fixture->clan_2_score)
                        <td>{{ $fixture->clan_2 }}</td>
                    @endif
                    <td><a href="/admin/download-screenshots/{{ $fixture->fixture_id }}">Download Screenshots</a></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection