@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row margin-bottom-15-p">
            <div class="col-md-12 margin-top-10">
                <div class="margin-t-50 panel panel-default">
                    <div class="panel-heading">{{ request()->route('id') ? "Edit Game Countdown" : "Create Game Countdown" }}</div>
                    <div class="panel-body">
                        <form class="form-horizontal" method="POST" enctype="multipart/form-data"
                              action="{{ request()->route('id') ? "/admin/edit-game-countdown/".request()->route('id') : "/admin/create-game-countdown" }}">
                            {{ csrf_field() }}

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name" class="col-md-12 control-label">Start Countdown Date</label>
                                    <div class="col-md-12">
                                        <input value="{{ isset($event->start_countdown_date) ? $event->start_countdown_date : "" }}" name="start_countdown_date" class="form-control" type="text" id="datepicker1">
                                        @include('alerts.error', ['error' => 'start_countdown_date'])
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group{{ $errors->has('start_countdown_time') ? ' has-error' : '' }}">
                                    <label for="name" class="col-md-12 control-label">Start Countdown Time</label>
                                    <div class="col-md-12">
                                        <input value="{{ isset($event->time) ? $event->start_countdown_time : "" }}" name="start_countdown_time" id="timepicker1" type="text" class="form-control input-small">
                                        @include('alerts.error', ['error' => 'start_countdown_time'])
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name" class="col-md-12 control-label">End Countdown Date</label>
                                    <div class="col-md-12">
                                        <input value="{{ isset($event->end_countdown_date) ? $event->end_countdown_date : "" }}" name="end_countdown_date" class="form-control" type="text" id="datepicker2">
                                        @include('alerts.error', ['error' => 'end_countdown_date'])
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group{{ $errors->has('end_countdown_time') ? ' has-error' : '' }}">
                                    <label for="name" class="col-md-12 control-label">End Countdown Time</label>
                                    <div class="col-md-12">
                                        <input value="{{ isset($event->end_countdown_time) ? $event->end_countdown_time : "" }}" name="end_countdown_time" id="timepicker2" type="text" class="form-control input-small">
                                        @include('alerts.error', ['error' => 'end_countdown_time'])
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name" class="col-md-12 control-label">Games</label>
                                    <div class="col-md-12">
                                        <select name="game" id="game" placeholder="None">
                                            @foreach($gamesList as $game)
                                                <option value="{{ $game->id }}">{{ $game->game_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="gamer_pic" class="col-md-12 control-label">Upload Logo</label>
                                <div class="col-md-12">
                                    @if(Session::has('success'))
                                        <div class="alert-box success">
                                            <h2>{!! Session::get('success') !!}</h2>
                                        </div>
                                    @endif
                                    <div class="control-group">
                                        {!! Form::file('image',['class' => 'upload-image','type' => 'file','data-max-size' => '2048']) !!}
                                        <p class="errors">{!!$errors->first('image')!!}</p>
                                        @if(Session::has('error'))
                                            <p class="errors">{!! Session::get('error') !!}</p>
                                        @endif
                                    </div>
                                    <div id="success"></div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-primary pull-left">
                                        {{ request()->route('id') ? "Update" : "Create" }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        var secondElement = new Choices('#game', { allowSearch: false }).setValue([]);
        $( function() {
            $( "#datepicker1" ).datepicker({ dateFormat: 'yy-mm-dd' }).val();
            $( "#datepicker2" ).datepicker({ dateFormat: 'yy-mm-dd' }).val();
            $('#timepicker1').timepicker();
            $('#timepicker2').timepicker();
        } );
    </script>

@endsection
