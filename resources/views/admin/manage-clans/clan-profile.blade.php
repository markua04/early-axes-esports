@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row" style="margin-top:5px;">
        <div class="main-profile">
            @if(isset($clan->clan_status) && $clan->clan_status == 0 && Auth::user()->user_level_id !== 1)
                <div class="margin-t-5">
                    <h3>Your clan has not been approved by the moderators yet.</h3>
                </div>
            @else
                @if(Auth::user() && Auth::user()->id == $clan->clan_owner_id )
                    <h3 class="top-bar-edit">
                        <a href="/edit-clan/{{ $clan->id }}">Edit Clan</a>
                    </h3>
                @elseif(Auth::user() && Auth::user()->user_level_id == 1)
                    <h3 class="top-bar-edit">
                        <a href="/edit-clan/{{ isset($clan->id) ? $clan->id  : '' }}">Edit Clan</a>
                    </h3>
                @endif
                    <div class="userprofile social ">
                        <div class="userpic"> <img src="{{ $clan->clan_logo }}" alt="" class="userpicimg"> </div>
                        <h3 class="username">{{ $clan->clan_name }}</h3>
                        {{--                    <div class="socials tex-center"> <a href="" class="btn btn-circle">--}}
                        {{--                            <i class="fa fa-facebook"></i></a> <a href="" class="btn btn-circle btn-danger ">--}}
                        {{--                            <i class="fa fa-google-plus"></i></a> <a href="" class="btn btn-circle btn-info ">--}}
                        {{--                            <i class="fa fa-twitter"></i></a> <a href="" class="btn btn-circle btn-warning "><i class="fa fa-envelope"></i></a>--}}
                        {{--                    </div>--}}
                    </div>
                    <div class="registered-clans-count text-center">
                        <span class="bold-content">CLAN OWNER : {{ getClanOwner($clan->id) }}</span>
                    </div>
{{--                    <div class="col-md-12 platform-bar xbox-bg-green">--}}
{{--                        <img class="logo-xbox" src="https://i.pinimg.com/originals/64/6e/41/646e41b261004f0605d63b6ef44b0be6.png">--}}
{{--                        <p>EAX SEASON 2 SIEGE CHAMPIONS</p>--}}
{{--                    </div>--}}

                <div class="btn-pref btn-group btn-group-justified btn-group-lg" role="group" aria-label="...">
                    <div class="btn-group" role="group">
                        <button type="button" id="stars" class="btn btn-primary selectables" href="#tab1" data-toggle="tab"><span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                            <div class="hidden-xs">Clan Description</div>
                        </button>
                    </div>
                    <div class="btn-group" role="group">
                        <button type="button" id="favorites" class="btn btn-default selectables" href="#tab2" data-toggle="tab"><span class="glyphicon glyphicon-heart" aria-hidden="true"></span>
                            <div class="hidden-xs">Clan Members</div>
                        </button>
                    </div>
                    <div class="btn-group" role="group">
                        <button type="button" id="following" class="btn btn-default selectables" href="#tab3" data-toggle="tab"><span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                            <div class="hidden-xs">Clan Results</div>
                        </button>
                    </div>
                </div>

                <div class="margin-t-5">
                    <div class="tab-content">
                        <div class="tab-pane fade in active" id="tab1" style="width:95%;">
                            <span style="color:black;">{!! isset($clan->clan_description) ? $clan->clan_description : '' !!}</span>
                        </div>
                        <div class="tab-pane fade in" id="tab2">
                            @include('frontend.partials.clans.clan-members')
                        </div>
                        <div class="tab-pane fade in" id="tab3">
                            @include('frontend.partials.clans.clan-results-feed')
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
</div>


<style>
    @media (max-width: 767px) {
        .userprofile.social {
            height: 320px !important;
        }
        .userprofile .userpic {
            height: 200px !important;
            width: 200px !important;
        }
        .userprofile .userpic .userpicimg {
            height: 210px !important;
        }
        .profile-wrap {
            margin-left:-15px !important;
            margin-right:-15px !important;
        }
        .username {
            font-size: 30px !important;
            line-height: 30px !important;
        }
    }
    .btn-pref .btn {
        -webkit-border-radius:0 !important;
    }
    .btn-group-justified {
        display: table;
        width: 100%;
        table-layout: fixed;
        border-collapse: separate;
    }
    .btn-group-justified>.btn, .btn-group-justified>.btn-group {
        display: table-cell;
        float: none;
        width: 1%;
    }
    .btn-primary {
        color: #fff;
        background-color: #428bca;
        border-color: #357ebd;
    }
    .btn {
        display: inline-block;
        padding: 6px 12px !important;
        margin-bottom: 0;
        font-size: 14px !important;
        font-weight: 400;
        line-height: 1.42857143;
        text-align: center;
        white-space: nowrap;
        vertical-align: middle;
        -ms-touch-action: manipulation;
        touch-action: manipulation;
        cursor: pointer;
        background-image: none;
        border: 1px solid transparent;
        border-radius: 4px;
        width: 100%;
        height: 55px !important;
    }
    .tab-pane, .nav-tabs {
        margin: 0 auto;
        max-width: 100%;
    }

    /*==============================*/
    /*====== siderbar user profile =====*/
    /*==============================*/
    .nav>li>a.userdd {
        padding: 5px 15px
    }
    .userprofile {
        width: 100%;
        float: left;
        clear: both;
        margin: 40px auto
    }
    .userprofile .userpic {
        height: 300px;
        width: 300px;
        clear: both;
        margin: 0 auto;
        display: block;
        border-radius: 100%;
        box-shadow: 0px 3px 10px 0 rgba(0, 0, 0, 0.15);
        -moz-box-shadow: 0px 3px 10px 0 rgba(0, 0, 0, 0.15);
        -webkit-box-shadow: 0px 3px 10px 0 rgba(0, 0, 0, 0.15);
        -ms-box-shadow: 0px 3px 10px 0 rgba(0, 0, 0, 0.15);
        position: relative;
    }
    .userprofile .userpic .userpicimg {
        height: 300px;
        width: 100%;
        border-radius: 100%;
    }
    .username {
        font-weight: 700;
        font-size: 35px;
        line-height: 35px;
        color: #000000;
        margin-top: 20px;
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
        text-align: center;
    }
    .username+p {
        color: #607d8b;
        font-size: 13px;
        line-height: 15px;
        white-space: nowrap;
        text-overflow: ellipsis;
        overflow: hidden;
    }
    .userprofile.small {
        width: auto;
        clear: both;
        margin: 0px auto;
    }
    .userprofile.small .userpic {
        height: 40px;
        width: 40px;
        margin: 0 10px 0 0;
        display: block;
        border-radius: 100%;
        box-shadow: 0px 3px 10px 0 rgba(0, 0, 0, 0.15);
        -moz-box-shadow: 0px 3px 10px 0 rgba(0, 0, 0, 0.15);
        -webkit-box-shadow: 0px 3px 10px 0 rgba(0, 0, 0, 0.15);
        -ms-box-shadow: 0px 3px 10px 0 rgba(0, 0, 0, 0.15);
        position: relative;
        float: left;
    }
    .userprofile.small .textcontainer {
        float: left;
        max-width: 100px;
        padding: 0
    }
    .userprofile.small .userpic .userpicimg {
        min-height: 100%;
        width: 100%;
        border-radius: 100%;
    }
    .userprofile.small .username {
        font-weight: 400;
        font-size: 16px;
        line-height: 21px;
        color: #000000;
        margin: 0px;
        float: left;
        width: 100%;
    }
    .userprofile.small .username+p {
        color: #607d8b;
        font-size: 13px;
        float: left;
        width: 100%;
        margin: 0;
    }
    /*==============================*/
    /*====== Social Profile css =====*/
    /*==============================*/
    .countlist h3 {
        margin: 0;
        font-weight: 400;
        line-height: 28px;
    }
    .countlist li {
        padding: 15px 30px 15px 0;
        font-size: 14px;
        text-align: left;
    }
    .countlist li small {
        font-size: 12px;
        margin: 0
    }
    .followbtn {
        float: right;
        margin: 22px;
    }
    .userprofile.social {
        background: url(/images/base-images/bg_2.jpg) no-repeat top center;
        background-size: cover;
        padding: 50px 0;
        margin: 0;
        height: 450px;
    }
    .userprofile.social .username {
        color: #ffffff
    }
    .userprofile.social .username+p {
        color: #ffffff;
        opacity: 0.8
    }
    .posttimeline .panel {
        margin-bottom: 15px
    }
    /*==============================*/
    /*====== Recently connected  heading =====*/
    /*==============================*/
    .member:hover .memmbername {
        bottom: 0
    }
    .member img {
        width: 100%;
        transition: 0.5s ease all;
    }
    .member:hover img {
        opacity: 0.8;
        transform: scale(1.2)
    }

    .panel-default>.panel-heading {
        color: #607D8B;
        background-color: #ffffff;
        font-weight: 400;
        font-size: 15px;
        border-radius: 0;
        border-color: #e1eaef;
    }

    .btn-circle {
        width: 30px !important;
        height: 30px !important;
        padding: 6px 0 !important;
        border-radius: 15px;
        text-align: center;
        font-size: 12px;
        line-height: 1.428571429;
    }

    .favorite i {
        color: #eb3147;
    }

    .btn i {
        font-size: 17px;
    }

    .panel {
        box-shadow: 0px 2px 10px 0 rgba(0, 0, 0, 0.05);
        -moz-box-shadow: 0px 2px 10px 0 rgba(0, 0, 0, 0.05);
        -webkit-box-shadow: 0px 2px 10px 0 rgba(0, 0, 0, 0.05);
        -ms-box-shadow: 0px 2px 10px 0 rgba(0, 0, 0, 0.05);
        transition: all ease 0.5s;
        -moz-transition: all ease 0.5s;
        -webkit-transition: all ease 0.5s;
        -ms-transition: all ease 0.5s;
        margin-bottom: 35px;
        border-radius: 0px;
        position: relative;
        border: 0;
        display: inline-block;
        width: 100%;
    }

    .panel-red.userlist .username, .panel-green.userlist .username, .panel-yellow.userlist .username, .panel-blue.userlist .username {
        color: #ffffff;
    }

    .panel-red.userlist p, .panel-green.userlist p, .panel-yellow.userlist p, .panel-blue.userlist p {
        color: rgba(255, 255, 255, 0.8);
    }

    .panel-red.userlist p a, .panel-green.userlist p a, .panel-yellow.userlist p a, .panel-blue.userlist p a {
        color: rgba(255, 255, 255, 0.8);
    }

    .photolist img {
        width: 100%;
    }

    .photolist {
        background: #e1eaef;
        padding-top: 15px;
        padding-bottom: 15px;
    }

    .profilegallery .grid-item a {
        height: 100%;
        display: block;
    }

    .grid a {
        width: 100%;
        display: block;
        float: left;
    }
    .registered-clans-count {
        background-color: #313131;
        color: #fff;
        font-size: 15px;
    }
</style>
@endsection