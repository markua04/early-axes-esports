@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12 margin-t-10">
                <div class="panel panel-default">
                    <div class="panel-heading">Edit Your Clan</div>
                    <div class="panel-body">
                        <form class="form-horizontal" method="POST" enctype="multipart/form-data" action="{{ isset($clan->id) ? "/edit-clan/$clan->id" : '' }}">
                            {{ csrf_field() }}
                            <div class="col-md-6">
                            <div class="form-group{{ $errors->has('clan_name') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-12 control-label">Clan Name</label>
                                <div class="col-md-12">
                                    <input id="name" type="text" class="form-control" name="clan_name" value="{{ isset( $clan['clan_name']) ? $clan['clan_name'] : '' }}" required autofocus>
                                    @if ($errors->has('clan_name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('clan_name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            </div>
                            <div class="col-md-6">
                            <div class="form-group{{ $errors->has('clan_email') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-12 control-label">Clan Email</label>
                                <div class="col-md-12">
                                    <input id="clan_email" type="text" class="form-control" name="clan_email" value="{{ isset($clan['clan_email']) ? $clan['clan_email'] : '' }}" required autofocus>
                                    @if ($errors->has('clan_email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('clan_email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            </div>

                            <div class="col-md-6">
                            <div class="form-group{{ $errors->has('clan_tel') ? ' has-error' : '' }}">
                                <label for="clan_tel" class="col-md-12 control-label">Clan Tel/Cell number</label>
                                <div class="col-md-12">
                                    <input id="clan_tel" class="form-control" name="clan_tel" value="{{ isset($clan['clan_tel']) ? $clan['clan_tel'] : '' }}" required>
                                    @if ($errors->has('clan_tel'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('clan_tel') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            </div>

                            <div class="col-md-12">
                            <div class="form-group{{ $errors->has('clan_description') ? ' has-error' : '' }}">
                                <label for="clan_tel" class="col-md-12 control-label">Clan Description</label>
                                <div class="col-md-12">
                                    <textarea id="editor" name='clan_description' class="form-control editor">{{ isset($clan['clan_description']) ? $clan['clan_description'] : '' }}</textarea>
                                    @if ($errors->has('clan_description'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('clan_description') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            </div>

                            <div class="col-md-6">
                            <div class="form-group">
                                <label for="secondary_captain" class="col-md-12 control-label">Change Secondary Captain</label>
                                <div class="col-md-12">
                                    <select name="secondary_captain">
                                        @foreach($clanMembers as $member)
                                            <option {{ isset($member->user->id) && $member->user->id == $secondaryCaptain ? "selected='selected'" : '' }} value="{{ isset($member->user->id) ? $member->user->id : '' }}">{{ isset($member->user->name) ? $member->user->name : '' }} {{ isset($member->user->surname) ? $member->user->surname : '' }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            </div>
                            <div class="col-md-6">
                            <div class="form-group">
                                <label for="clan_owner_id" class="col-md-12 control-label">Change Clan Owner</label>
                                <div class="col-md-12">
                                    <select name="clan_owner_id">
                                        @foreach($clanMembers as $member)
                                            <option {{ isset($member->user->id) && $member->user->id == $clanOwner ? "selected='selected'" : '' }} value="{{ isset($member->user->id) ? $member->user->id : '' }}">{{ isset($member->user->name) ? $member->user->name : '' }} {{ isset($member->user->surname) ? $member->user->surname : '' }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            </div>

                            <div class="col-md-12">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <p style="font-weight: bold;">Current Clan Logo</p>
                                    <p style="font-weight: bold;">Please make sure the image is either 600x600 or 1080x1080 or at least that aspect ratio so that the image will look great on your profile.</p>
                                    <img class="preview-logo" src="{{ isset($clan['clan_logo']) ? $clan['clan_logo'] : '' }}">
                                    @if(Session::has('success'))
                                        <div class="alert-box success">
                                            <h2>{!! Session::get('success') !!}</h2>
                                        </div>
                                    @endif
                                    <div class="secure">Upload Logo</div>
                                    <div class="control-group">
                                        <input onchange="ValidateSize(this)" class="upload-image" type="file" data-max-size="2048" name="image">
                                        <p class="errors">{!!$errors->first('image')!!}</p>
                                        @if(Session::has('error'))
                                            <p class="errors">{!! Session::get('error') !!}</p>
                                        @endif
                                    </div>
                                    <div id="success"></div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-12">
                                    <button id="upload-image" type="submit" class="btn btn-primary">
                                        Update
                                    </button>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <a onclick="return confirm('Are you sure?')" href="/delete-clan/{{ isset($clan->id) ? $clan->id : '' }}" class="btn btn-primary">
                                        Delete Clan
                                    </a>
                                </div>
                            </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="{{ asset('js/tinymce/js/tinymce/tinymce.min.js') }}"></script>
    <script src="{{ asset('js/tinymce/js/tinymce/plugins/jbimages/plugin.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('input[type="file"]').change(function(e){
                document.getElementById("clan_logo").value = e.target.files[0].name;
            });
        });
        tinymce.init({
            selector : "textarea",
            encoding: 'raw',
            height : "480",
            plugins : ["advlist autolink lists link image charmap print preview anchor", "searchreplace visualblocks code fullscreen", "insertdatetime media table contextmenu paste"],
            toolbar : "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image jbimages",
        });
        function ValidateSize(file) {
            var FileSize = file.files[0].size / 1024 / 1024; // in MB
            if (FileSize > 2) {
                alert('File size exceeds 2 MB');
                document.getElementById("upload-image").disabled = true;
                // $(file).val(''); //for clearing with Jquery
            } else {
                document.getElementById("upload-image").disabled = false;
            }
        }
    </script>
@endsection
