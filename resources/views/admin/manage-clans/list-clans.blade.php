@extends('layouts.app')

@section('content')
    <style>
        .card {
            margin-top: 0px;
            padding: 25px;
            background-color: rgba(214, 224, 226, 0.2);
            -webkit-border-top-left-radius: 5px;
            -moz-border-top-left-radius: 5px;
            border-top-left-radius: 5px;
            -webkit-border-top-right-radius: 5px;
            -moz-border-top-right-radius: 5px;
            border-top-right-radius: 5px;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
            height: 380px;
        }
    </style>
    <div class="container">
        <div class="row padding-t-10">
            <form action="/view-clans/search" method="POST" role="search">
                {{ csrf_field() }}
                <div style="padding-right: 0" class="input-group col-md-11 col-xs-9">
                    <input style="border-radius: 5px 0px 0px 5px;height: 35px;" type="text" class="form-control" name="q"
                           placeholder="Search clans"> <span class="input-group-btn">
                    </span>
                </div>
                <div style="padding-left:0" class="col-md-1 col-xs-3">
                    <button type="submit" class="btn btn-default">
                        <span class="glyphicon glyphicon-search">Search</span>
                    </button>
                </div>
            </form>
        </div>
        @if(!empty($check) && $check == 1)
            <h3 class="alert">No results found for that clan name.</h3>
        @endif
        <h1 class="tournament-head">Clans</h1>
        <table class="table table-bordered dark-table scrollable-table">
            <thead>
            <tr>
                <th>Clan Name</th>
                <th>Clan Owner</th>
                @if(Auth::user() && Auth::user()->getAttributes()['user_level_id'] == 1 || Auth::user() && isOwnerOfClan() !== 'No')
                <th>Contact Number</th>
                @endif
                @if(Auth::user() && Auth::user()->getAttributes()['user_level_id'] == 1)
                <th>Clan Fixtures</th>
                <th>Edit</th>
                <th>Delete</th>
                @endif
                @if(Auth::user())
                <th>Join Clan</th>
                @endif
            </tr>
            </thead>
            <tbody>
            @foreach($clans as $clan)
            <tr>
                <td style="width:70%;"><a style="color:#007af7;" href="/clan-profile/{{ $clan->id }}/{{ strtolower(str_replace(' ','-',$clan->clan_name)) }}">{{ $clan->clan_name }}</a></td>
                @if(Auth::user() && Auth::user()->getAttributes()['user_level_id'] == 1 || Auth::user() && isOwnerOfClan() !== 'No')
                    <td style="width:70%"><a style="color:#007af7;" href="/user-profile/{{ $clan->clan_owner_id }}/{{ getClanOwnerName($clan->id) }}">{{ getClanOwnerName($clan->id) }}</a></td>
                @else
                    <td style="width:70%"><a style="color:#007af7;" href="/user-profile/{{ $clan->clan_owner_id }}/{{ getClanOwnerName($clan->id) }}">{{ getClanOwnerGamerTag($clan->id) }}</a></td>
                @endif
                @if(Auth::user() && Auth::user()->getAttributes()['user_level_id'] == 1 || Auth::user() && Auth::user() && isOwnerOfClan() !== 'No')
                <td style="width:10%;">
                    {{ $clan->clan_tel }}
                </td>
                @endif
                @if(Auth::user() && Auth::user()->getAttributes()['user_level_id'] == 1)
                    <td style="width:10%;">
                        <a style="color:#007af7;" href="/clan-fixtures-by-tournament/{{ $clan->id }}">View</a>
                    </td>
                    <td style="width:10%">
                        <a style="color:#007af7;" href="/edit-clan/{{ $clan->id }}">Edit</a>
                    </td>
                    <td style="width:10%">
                        <a onclick="return confirm('Are you sure?')" style="color:#007af7;" href="/delete-clan/{{ $clan->id }}">Delete</a>
                    </td>
                @endif
                @if(Auth::user())
                    <td style="width:10%;">
                        <a onclick="return confirm('Are you sure you want to join the clan {{ $clan->clan_name }}?')" href="/join-clan/{{ $clan->clan_name }}"><div id="join-clan" class="btn btn-primary">Join Clan</div></a>
                    </td>
                @endif
            </tr>
            @endforeach
            </tbody>
        </table>
        {{ $clans->links() }}
    </div>
@endsection