@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row margin-bottom-15-p">
            <div class="col-md-12 margin-top-10">
                <div class="margin-t-50 panel panel-default">
                    <div class="panel-heading">Edit {{ $game->game_name }}</div>
                    <div class="panel-body">
                        <form class="form-horizontal" method="POST" enctype="multipart/form-data" action="/admin/edit-game/{{ $game->id }}">
                            {{ csrf_field() }}
                            <div class="form-group{{ $errors->has('game_name') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-12 control-label">Game Title</label>
                                <div class="col-md-12">
                                    <input id="name" type="text" class="form-control" name="game_name" value="{{ $game->game_name }}" >
                                    @if ($errors->has('game_name'))
                                        <span class="help-block">
                                    <strong>{{ $errors->first('game_name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="gamer_pic" class="col-md-12 control-label">Upload Game Banner</label>
                                <div class="col-md-12">
                                    @if(Session::has('success'))
                                        <div class="alert-box success">
                                            <h2>{!! Session::get('success') !!}</h2>
                                        </div>
                                    @endif
                                    <div class="control-group">
                                        {!! Form::file('image',['class' => 'upload-image','type' => 'file','data-max-size' => '2048']) !!}
                                        <p class="errors">{!!$errors->first('image')!!}</p>
                                        @if(Session::has('error'))
                                            <p class="errors">{!! Session::get('error') !!}</p>
                                        @endif
                                    </div>
                                    <div id="success"></div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-primary pull-left">
                                        Create
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
