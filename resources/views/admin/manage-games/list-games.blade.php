@extends('layouts.app')

@section('content')
    <div class="container">
        <h2>List of all the Games in the system</h2>
        <p>Take a look at all of the games below.</p>
        <table class="table table-bordered dark-table scrollable-table">
            <thead>
            <tr>
                <th>Name</th>
                <th>Banner</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>
            </thead>
            <tbody>
            @foreach($games as $game)
            <tr>
                <td>{{ $game->game_name }}</td>
                <td>{{ $game->game_banner }}</td>
                <td><a href="/admin/edit-game/{{ $game->id }}">Edit</a></td>
                <td><a onclick="return confirm('Are you sure?')" href="/admin/delete-game/{{ $game->id }}">Delete</a></td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection