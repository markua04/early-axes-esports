@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12 margin-t-30">
                <div class="panel panel-default">
                    <div class="panel-heading">Edit User {{ isset($user->name) ? $user->name : '' }}</div>
                    <div class="panel-body">
                        <form class="form-horizontal" method="POST" enctype="multipart/form-data"
                              action="{{ isset($user->id) ? "/edit-user/$user->id" : '' }}">
                            {{ csrf_field() }}
                            <div class="col-md-6 form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-12 control-label">Name</label>
                                <div class="col-md-12">
                                    <input id="name" type="text" class="form-control" name="name"
                                           value="{{ isset($user->name) ? $user->name : '' }}" required autofocus>
                                    @include('alerts.error', ['error' => 'name'])
                                </div>
                            </div>

                            <div class="col-md-6 form-group{{ $errors->has('surname') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-12 control-label">Surname</label>
                                <div class="col-md-12">
                                    <input id="surname" type="text" class="form-control" name="surname"
                                           value="{{ isset($user->surname) ? $user->surname : '' }}" required autofocus>
                                    @include('alerts.error', ['error' => 'surname'])
                                </div>
                            </div>

                            <div class="col-md-6 form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-12 control-label">Email</label>
                                <div class="col-md-12">
                                    <input id="email" class="form-control" name="email" value="{{ isset($user->email) ? $user->email : '' }}"
                                           required>
                                    @include('alerts.error', ['error' => 'email'])
                                </div>
                            </div>

                            <div class="col-md-6 form-group{{ $errors->has('gamer_tag') ? ' has-error' : '' }}">
                                <label for="gamer_tag" class="col-md-12 control-label">Xbox Gamer Tag</label>
                                <div class="col-md-12">
                                    <input id="gamer_tag" class="form-control" name="gamer_tag"
                                           value="{{ isset($user->gamer_tag) ? $user->gamer_tag : '' }}">
                                    @include('alerts.error', ['error' => 'gamer_tag'])
                                </div>
                            </div>

                            <div class="col-md-6 form-group{{ $errors->has('steam_id') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label">Steam ID</label>
                                <div class="col-md-12">
                                    <input id="gamer_tag" type="text" class="form-control" name="steam_id"
                                           value="{{ isset($user->steam_id) ? $user->steam_id : '' }}" autofocus>
                                    @include('alerts.error', ['error' => 'steam_id'])
                                </div>
                            </div>

                            <div class="col-md-6 form-group{{ $errors->has('psn_id') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label">PSN ID</label>
                                <div class="col-md-12">
                                    <input id="psn_id" type="text" class="form-control" name="psn_id"
                                           value="{{ isset($user->psn_id) ? $user->psn_id : '' }}" autofocus>
                                    @include('alerts.error', ['error' => 'psn_id'])
                                </div>
                            </div>

                            <div class="col-md-6 form-group{{ $errors->has('uplay_id') ? ' has-error' : '' }}">
                                <label for="uplay_id" class="col-md-4 control-label">UPlay ID</label>
                                <div class="col-md-12">
                                    <input id="uplay_id" type="text" class="form-control" name="uplay_id"
                                           value="{{ isset($user->uplay_id) ? $user->uplay_id : '' }}" autofocus>
                                    @include('alerts.error', ['error' => 'uplay_id'])
                                </div>
                            </div>

                            <div class="col-md-6 form-group{{ $errors->has('phone_number') ? ' has-error' : '' }}">
                                <label for="phone_number" class="col-md-12 control-label">Phone Number</label>
                                <div class="col-md-12">
                                    <input id="phone_number" class="form-control" name="phone_number" value="{{ isset($user->phone_number) ? $user->phone_number : '' }}">
                                    @include('alerts.error', ['error' => 'phone_number'])
                                </div>
                            </div>

                            <div class="col-md-6 form-group{{ $errors->has('clan_id') ? ' has-error' : '' }}" style="margin-bottom: 0px;">
                                <label for="clan_id" class="col-md-12 control-label">Change Clan</label>
                                <div class="col-md-12">
                                    <select class="fstdropdown-select" name="clan_id" id="clan_id">
                                        <option value="">select a clan</option>
                                        <option value="0">None</option>
                                        @foreach($clans as $clan)
                                            <option {{ isset($user->clan_id) && $user->clan_id == $clan->id ? 'selected' : '' }} value="{{ isset($clan->id) ? $clan->id : '' }}">{{ isset($clan->clan_name) ? $clan->clan_name : '' }}</option>
                                        @endforeach
                                    </select>
                                    @include('alerts.error', ['error' => 'clan_id'])
                                </div>
                            </div>

                            <div class="col-md-6 form-group{{ $errors->has('selected_source') ? ' has-error' : '' }}"
                                 style="margin-bottom: 0;">
                                <label for="selected_source" class="col-md-12 control-label">Select platform</label>
                                <div class="col-md-12">
                                    <select class="fstdropdown-select" data-searchdisable="true" name="selected_source">
                                        <option value="">select a platform</option>
                                        @foreach($platforms as $platform)
                                            @if($platform->platform !== "Cross-Platform")
                                            <option {{ isset($user->selected_source) && $user->selected_source == $platform->id ? "selected" : "" }} value="{{ $platform->id }}">{{ $platform->platform }}</option>
                                            @endif
                                                @endforeach
                                    </select>
                                    @include('alerts.error', ['error' => 'selected_source'])
                                </div>
                            </div>

                            <div class="col-md-6 form-group{{ $errors->has('select_image') ? ' has-error' : '' }}">
                                <label for="selected_source" class="col-md-12 control-label">Select image</label>
                                <div class="col-md-12">
                                    <select class="fstdropdown-select" data-searchdisable="true" name="select_image">
                                        <option value="">select an image</option>
                                        <option {{ isset($user->select_image) && $user->select_image == 1 ? 'selected' : '' }} value="1">Uploaded Image</option>
                                        <option {{ isset($user->select_image) && $user->select_image == 2 ? 'selected' : '' }} value="2">Platform Image</option>
                                    </select>
                                    @include('alerts.error', ['error' => 'select_image'])
                                </div>
                            </div>

{{--                            <div class="col-md-6 form-group">--}}
{{--                                <label for="gamer_pic_replacement" class="col-md-12 control-label">Select the games you play below</label>--}}
{{--                                <div class="col-md-12">--}}
{{--                                    <select name="games[]" id="games" placeholder="" multiple>--}}
{{--                                        @foreach($games as $game)--}}
{{--                                            <option value="{{ $game->id }}">{{ $game->game_name }}</option>--}}
{{--                                        @endforeach--}}
{{--                                    </select>--}}
{{--                                </div>--}}
{{--                                <div class="col-md-12">--}}
{{--                                    <label for="" class="control-label">Currently selected games : </label>--}}
{{--                                    @if($userGames !== null)--}}
{{--                                        @foreach($userGames as $game)--}}
{{--                                            {{ $game->gamesList['game_name'] }},--}}
{{--                                        @endforeach--}}
{{--                                    @endif--}}
{{--                                </div>--}}
{{--                            </div>--}}


                            <div class="form-group col-md-12">
                                <label for="gamer_pic" class="col-md-12 control-label">Gamer Pic</label>
                                <div class="col-md-12">
                                    <img src="{{ isset($user->gamer_pic) ? $user->gamer_pic : '' }}" width="170" height="auto">
                                </div>
                            </div>

                            <div class="form-group col-md-12">
                                <label for="gamer_pic_replacement" class="col-md-12 control-label">Replace Gamer Pic</label>
                                <p style="font-weight: bold;font-size:12px;margin-left: 15px;">Please make sure the image is either 600x600 or 1080x1080 or at least that aspect ratio so that the image will look great on your profile.</p>
                                <div class="col-md-12">
                                    @if(Session::has('success'))
                                        <div class="alert-box success">
                                            <h2>{!! Session::get('success') !!}</h2>
                                        </div>
                                        @include('alerts.error', ['error' => 'gamer_pic'])
                                    @endif
                                    <div class="control-group">
                                        {!! Form::file('image',['class' => 'upload-image','type' => 'file','data-max-size' => '2048']) !!}
                                        <p class="errors">{!!$errors->first('image')!!}</p>
                                        @if(Session::has('error'))
                                            <p class="errors">{!! Session::get('error') !!}</p>
                                        @endif
                                    </div>
                                    <div id="success"></div>
                                </div>
                            </div>

                            <div class="form-group col-md-6">
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-primary">
                                        Update
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        window.onload = function() {
            setFstDropdown();
        };
    </script>
@endsection
