@extends('layouts.app')

@section('content')
    <div class="container">
        <h2>List of all the gamers</h2>
        <p>Take a look at all of the gamers below or click on their profile links to see a full overview of the user</p>
        <table class="table table-bordered dark-table scrollable-table">
            <thead>
            <tr>
                <th>Name</th>
                <th>Gamer Tag</th>
                <th>Gamer Profile</th>
                <th>Clan</th>
            </tr>
            </thead>
            <tbody>
            @foreach($gamers as $gamer)
                <tr>
                    <td>{{ $gamer->name }}</td>
                    <td>{{ $gamer->gamer_tag }}</td>
                    <td>
                        <a style="color:blue;" href="/user-profile/{{ $gamer->id }}/{{ $gamer->gamer_tag}}">
                            Gamer Profile
                        </a>
                    </td>
                    <td>{{ $gamer->clan->clan_name }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection