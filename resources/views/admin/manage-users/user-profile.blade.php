@extends('layouts.app')

@section('content')
    <style>

        /* USER PROFILE PAGE */
        .card {
            margin-top: 0px;
            padding: 20px;
            background-color: rgba(214, 224, 226, 0.2);
            -webkit-border-top-left-radius: 5px;
            -moz-border-top-left-radius: 5px;
            border-top-left-radius: 0px;
            -webkit-border-top-right-radius: 5px;
            -moz-border-top-right-radius: 5px;
            border-top-right-radius: 5px;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
            height: 420px;
        }

        .card.hovercard {
            position: relative;
            padding-top: 0;
            height: 377px !important;
            overflow: hidden;
            text-align: center;
            background-color: #fff;
            background-color: rgba(255, 255, 255, 1);
        }

        .card.hovercard .card-background {
            height: 130px;
        }

        .card-background img {
            -webkit-filter: blur(25px);
            -moz-filter: blur(25px);
            -o-filter: blur(25px);
            -ms-filter: blur(25px);
            filter: blur(25px);
            margin-left: -100px;
            margin-top: -200px;
            min-width: 130%;
        }

        .card.hovercard .useravatar {
            position: absolute;
            top: 20px;
            left: 0;
            right: 0;
        }

        .card.hovercard .useravatar img {
            width: 310px;
            height: auto;
        }

        .card.hovercard .card-info {
            position: absolute;
            bottom: -25px;
            left: 0;
            right: 0;
        }

        .card.hovercard .card-info .card-title {
            padding: 10px 17px 10px 17px;
            font-size: 30px;
            font-weight: bold;
            line-height: 1;
            color: #FFFFFF;
            background-color: rgba(0, 0, 0, 0.55);
            -webkit-border-radius: 4px;
            -moz-border-radius: 4px;
            border-radius: 4px;
        }

        .card.hovercard .card-info {
            overflow: hidden;
            font-size: 12px;
            padding: 7px 7px 7px 7px;
            border-radius: 4px;
            line-height: 20px;
            color: #737373;
            margin-bottom: 20px;
            text-overflow: ellipsis;
        }

        .card.hovercard .bottom {
            padding: 0 20px;
            margin-bottom: 17px;
        }

        .btn-pref .btn {
            -webkit-border-radius: 0 !important;
        }

        .btn-group-justified {
            display: table;
            width: 100%;
            table-layout: fixed;
            border-collapse: separate;
        }

        .btn-group-justified > .btn, .btn-group-justified > .btn-group {
            display: table-cell;
            float: none;
            width: 1%;
        }

        .btn-primary {
            color: #fff;
            background-color: #428bca;
            border-color: #357ebd;
        }

        .btn {
            display: inline-block;
            padding: 6px 12px;
            margin-bottom: 0;
            font-size: 14px;
            font-weight: 400;
            line-height: 1.42857143;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            -ms-touch-action: manipulation;
            touch-action: manipulation;
            cursor: pointer;
            background-image: none;
            border: 1px solid transparent;
            border-radius: 4px;
            width: 100%;
            height: 60px;
        }

        .tab-pane, .nav-tabs {
            margin: 0 auto;
            max-width: 100%;
        }

        .card-title {
            padding: 10px 17px 10px 17px !important;
        }

        @media (max-width: 767px) {
            #pushContent {
                height: 266.5px;
                width: 100%;
                display: none;
                background-color: rgba(5, 49, 114, 0.95);
            }
        }
    </style>
    <div class="container">
        @if(Auth::user())
        @if(Auth::user()->id == $userData->id)
            <h3><a class="blue-text" href="/edit-user/{{ $userData->id }}">Edit Profile</a></h3>
        @endif
        @endif
        <div class="user-profile-head">
            @if($userData->selected_source == 1)
                {{ $userData->gamer_tag }}
            @elseif($userData->selected_source == 2)
                {{ $userData->psn_id }}
            @elseif($userData->selected_source == 3)
                {{ $userData->steam_id }}
            @else
                {{ $userData->uplay_id }}
            @endif
        </div>
        <div class="card hovercard">
            <div class="card-background">
                @if(($userData->selected_source == 2 || $userData->selected_source == 3) && $userData->select_image == 2)
                    <img class="card-bkimg" alt="" src="{{ URL::to('/') }}/images/default-no-image-eax.jpeg">
                @endif
                @if($userData->select_image == 2 && $userData->selected_source == 1)
                    @if($userData->selected_source == 1 && isset($profileData))
                        @if(isset($profileData->GameDisplayPicRaw))
                            <img class="card-bkimg" alt="" src="{{ $profileData->GameDisplayPicRaw }}">
                        @else
                            <img class="card-bkimg" alt=""
                                 src="{{ URL::to('/') }}/images/placeholders/profile-placeholder.png">
                        @endif
                    @else
                        @if(isset($profileData))
                            <img class="card-bkimg" alt="" src="{{ $profileData->avatarfull }}">
                        @endif
                    @endif
                @else
                    @if($userData->gamer_pic)
                        <img class="card-bkimg" alt="" src="{{ $userData->gamer_pic }}">
                    @elseif(isset($profileData))
                        <img class="card-bkimg" alt="" src="{{ $profileData->GameDisplayPicRaw }}">
                    @else
                        <img class="card-bkimg" alt="" src="{{ URL::to('/') }}/images/placeholders/bg-placeholder.png">
                    @endif
                @endif
            </div>
            <div class="useravatar">
                @if(($userData->selected_source == 2 || $userData->selected_source == 3 || $userData->selected_source == 4) && $userData->select_image == 2)
                    <img alt="" src="{{ URL::to('/') }}/images/default-no-image-eax.jpeg">
                @endif
                @if($userData->select_image == 2 && $userData->selected_source == 1)
                    @if($userData->selected_source == 1)
                        @if(isset($profileData->GameDisplayPicRaw))
                            <img alt="" src="{{ $profileData->GameDisplayPicRaw }}">
                        @endif
                    @elseif(isset($profileData->avatarfull))
                        <img alt="" src="{{ $profileData->avatarfull }}">
                    @else
                        <img class="card-bkimg" alt=""
                             src="{{ URL::to('/') }}/images/placeholders/profile-placeholder.png">
                    @endif
                @else
                    @if($userData->gamer_pic)
                        <img class="card-bkimg" alt="" src="{{ $userData->gamer_pic }}">
                    @elseif(isset($profileData->GameDisplayPicRaw))
                        <img class="card-bkimg" alt="" src="{{ $profileData->GameDisplayPicRaw }}">
                    @else
                        <img class="card-bkimg" alt=""
                             src="{{ URL::to('/') }}/images/placeholders/profile-placeholder.png">
                    @endif
                @endif
            </div>
            <div class="card-info">
                <br/>
                <br/>
                @if($userData->clan_id !== 0)
                    <span style="margin-top: 36%;background-color: rgba(0, 0, 0, 0.55);color:#fff;font-size:17px;font-weight: bold;">Clan : {{ $userData->clan->clan_name }}</span>
                @else
                    <span style="margin-top: 36%;background-color: rgba(0, 0, 0, 0.55);color:#fff;font-size:17px;font-weight: bold;">Clan : None</span>
                @endif
            </div>
            </div>
        </div>
        <div class="container">
        @if($userData->selected_source == 1)
            <div style="background-color:#0A246A;padding:6px;text-align:center;color:#fff;font-size: 18px;"
                 class="col-md-12 wrap-the-xbox-p main-xbox-data">
                @if(isset($profileData->XboxOneRep))
                    <div class="col-md-4">Xbox One Reputation <br/> {{ $profileData->XboxOneRep }}</div>
                    <div class="col-md-4">Gamer Score <br/> {{ $profileData->Gamerscore }}</div>
                    <div class="col-md-4">Account Tier <br/> {{ $profileData->AccountTier }}</div>
                @else
                    <div class="col-md-12" style="height:50px;padding-top:10px;">Xbox Gamer data not available right now</div>
                @endif
            </div>
            <div style="background-color:#0A246A;padding:6px;text-align:center;color:#fff;font-size: 18px;"
                 class="col-md-12 wrap-the-xbox-p mobile-score">
                @if(isset($profileData->Gamerscore))
                <div class="col-md-12">Gamer Score <br/> {{ $profileData->Gamerscore }}</div>
                @endif
            </div>
        @endif
        <div class="btn-pref btn-group btn-group-justified btn-group-lg" role="group" aria-label="...">
            <div class="btn-group" role="group">
                <button type="button" id="stars" class="btn btn-primary" href="#tab1" data-toggle="tab"><span
                            class="glyphicon glyphicon-star" aria-hidden="true"></span>
                    <div class="hidden-xs">Gamer Stats</div>
                </button>
            </div>
            @if($userData->selected_source == 1)
                <div class="btn-group" role="group">
                    <button type="button" id="favorites" class="btn btn-default" href="#tab2" data-toggle="tab"><span
                                class="glyphicon glyphicon-heart" aria-hidden="true"></span>
                        <div class="hidden-xs">Xbox Recent Achievements</div>
                    </button>
                </div>
            @elseif($userData->selected_source == 2)
                <div class="btn-group" role="group">
                    <button type="button" id="favorites" class="btn btn-default" href="#tab2" data-toggle="tab"><span class="glyphicon glyphicon-heart" aria-hidden="true"></span>
                        <div class="hidden-xs">PSN Trophies</div>
                    </button>
                </div>
            @elseif($userData->selected_source == 3)
                <div class="btn-group" role="group">
                    <button type="button" id="favorites" class="btn btn-default" href="#tab2" data-toggle="tab"><span
                                class="glyphicon glyphicon-heart" aria-hidden="true"></span>
                        <div class="hidden-xs">Steam Achievements</div>
                    </button>
                </div>
            @else
                <div class="btn-group" role="group">
                    <button type="button" id="favorites" class="btn btn-default" href="#tab2" data-toggle="tab"><span
                                class="glyphicon glyphicon-heart" aria-hidden="true"></span>
                        <div class="hidden-xs">Uplay Achievements</div>
                    </button>
                </div>
            @endif
            <div class="btn-group" role="group">
                <button type="button" id="favorites" class="btn btn-default" href="#tab3" data-toggle="tab"><span
                            class="glyphicon glyphicon-heart" aria-hidden="true"></span>
                    <div class="hidden-xs">Games</div>
                </button>
            </div>
        </div>

        <div class="well">
            <div class="tab-content">
                <div class="tab-pane fade in active" id="tab1">
                    <div class="col-md-12 game-select">
                        <p>Please select a platform before selecting a game.</p>
                    </div>
                    <div class="col-md-6">
                        <select id="platform" name="platform" class="platform select-stat">
                            <option selected value="">Select Platform</option>
                            {{--@if($userData->steam_id !== null)--}}
                                {{--<option value="steam">Steam</option>--}}
                            {{--@endif--}}
                            @if($userData->selected_source == 1)
                                <option value="xbl">Xbox Live</option>
                            @endif
                        </select>
                    </div>
                    <div class="col-md-6">
                        <select name="approval" class="updateMe">
                            <option selected value="">Select a game</option>
                            <option value="siege">Siege Stats</option>
                            <option value="rocket-league">Rocket League Stats</option>
                        </select>
                    </div>
                    <br/>
                    <div class="loading-rb6 col-md-12">
                        <div class="img center-block">
                            <img style="margin-left: 25px;" class="center" width="100"
                                 src="/images/base-images/rb6.gif">
                        </div>
                    </div>
                    @include('admin.api-stats.partials.siege-base-stats')
                    @include('admin.api-stats.partials.quick-competitive-stats')
                    <div class="loading-rl col-md-12">
                        <div class="img center-block">
                            <img style="margin-left: 25px;" class="center loading-rl" width="100"
                                 src="/images/rocket-league/loading-rl.gif">
                        </div>
                    </div>
                    @include('admin.api-stats.partials.rocket-league-stats')
                    @include('admin.api-stats.partials.rocket-league-quick-comp-stats')
                </div>
                @if($userData->selected_source == 1)
                    <div class="tab-pane fade in" id="tab2">
                        PSN Trophies coming soon!
                    </div>
{{--                    <div class="tab-pane fade in" id="tab2">--}}
{{--                        <div class="loading col-md-12">--}}
{{--                            <div class="img center-block">--}}
{{--                                <img style="margin-left: 25px;" class="center" width="100"--}}
{{--                                     src="/images/base-images/loading.gif">--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        @include('admin.user-profile.xbox-profile')--}}
{{--                    </div>--}}
                @elseif($userData->selected_source == 2)
                    <div class="tab-pane fade in" id="tab2">
                        PSN Trophies coming soon!
                    </div>
                @elseif($userData->selected_source == 3)
                    <div class="tab-pane fade in" id="tab2">
                        Steam achievements coming soon!
                   </div>
                @else
                    <div class="tab-pane fade in" id="tab2">
                        Uplay achievements coming soon!
                    </div>
                @endif
                <div class="tab-pane fade in" id="tab3">
                    <h2 class="tournament-head">Games this user plays</h2>
                    @if($userGames !== null)
                        @foreach($userGames as $game)
                            @if($game->gamesList)
                            <div class="col-md-4 margin-t-5">
                                <h3 class="text-center">{{ $game->gamesList['game_name'] }}</h3>
                                <img alt="{{ $game->gamesList['game_name'] }}" src="{{ $game->gamesList['game_banner'] }}">
                            </div>
                            @endif
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>
    <script src="{{ asset('js/api-stats/siege-stats.js') }}"></script>
    <script src="{{ asset('js/api-stats/rocket-league-stats.js') }}"></script>
<!--    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>-->
@endsection