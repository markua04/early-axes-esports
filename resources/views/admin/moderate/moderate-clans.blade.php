@extends('layouts.app')

@section('content')
    @include('alerts.status')
    <style>
        .site-wrapper {
            height: 85vh;
        }
    </style>
    <div class="container">
        <h2>Clans to be moderated and approved</h2>
        <div id="app2">
            <moderate-select-component :clans='{!! json_encode($clans) !!}'></moderate-select-component>
        </div>
    </div>
@endsection

