@extends('layouts.app')

@section('content')
    @include('alerts.status')
    <style>
        .site-wrapper {
            height: 85vh;
        }
    </style>
    <div class="container">
        <div class="row margin-t-10">
        <div class="col-md-12">
            <h2>Users that have changed gamer tags while in tournaments</h2>
        </div>
        <div id="app2">
            <moderate-users-gt :users='{!! json_encode($usersArray) !!}'></moderate-users-gt>
        </div>
        </div>
    </div>
@endsection

