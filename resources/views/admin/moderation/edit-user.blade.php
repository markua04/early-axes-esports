@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12 margin-t-30">
                <div class="panel panel-default">
                    <div class="panel-heading">Edit User {{ $user->name }}</div>
                    <div class="panel-body">
                        <form class="form-horizontal" method="POST" enctype="multipart/form-data"
                              action="{{ "/edit-user-privileges/$user->id" }}">
                            {{ csrf_field() }}
                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-2 control-label">Name</label>
                                <div class="col-md-10">
                                    <input id="name" type="text" class="form-control" name="name"
                                           value="{{ $user->name }}" required autofocus>
                                    @include('alerts.error', ['error' => 'name'])
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('surname') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-2 control-label">Surname</label>
                                <div class="col-md-10">
                                    <input id="surname" type="text" class="form-control" name="surname"
                                           value="{{ $user->surname }}" required autofocus>
                                    @include('alerts.error', ['error' => 'surname'])
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-2 control-label">Email</label>
                                <div class="col-md-10">
                                    <input id="email" class="form-control" name="email" value="{{ $user->email }}"
                                           required>
                                    @include('alerts.error', ['error' => 'email'])
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('gamer_tag') ? ' has-error' : '' }}">
                                <label for="gamer_tag" class="col-md-2 control-label">Xbox Gamer Tag</label>
                                <div class="col-md-10">
                                    <input id="gamer_tag" class="form-control" name="gamer_tag"
                                           value="{{ $user->gamer_tag }}">
                                    @include('alerts.error', ['error' => 'gamer_tag'])
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('psn_id') ? ' has-error' : '' }}">
                                <label for="psn_id" class="col-md-2 control-label">PSN ID</label>
                                <div class="col-md-10">
                                    <input id="psn_id" class="form-control" name="psn_id"
                                           value="{{ $user->psn_id }}">
                                    @include('alerts.error', ['error' => 'psn_id'])
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('uplay_id') ? ' has-error' : '' }}">
                                <label for="psn_id" class="col-md-2 control-label">Uplay ID</label>
                                <div class="col-md-10">
                                    <input id="uplay_id" class="form-control" name="uplay_id"
                                           value="{{ $user->uplay_id }}">
                                    @include('alerts.error', ['error' => 'uplay_id'])
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('steam_id') ? ' has-error' : '' }}">
                                <label for="steam_id" class="col-md-2 control-label">Steam ID</label>
                                <div class="col-md-10">
                                    <input id="steam_id" class="form-control" name="steam_id"
                                           value="{{ $user->steam_id }}">
                                    @include('alerts.error', ['error' => 'steam_id'])
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('phone_number') ? ' has-error' : '' }}">
                                <label for="phone_number" class="col-md-2 control-label">Phone Number</label>
                                <div class="col-md-10">
                                    <input id="phone_number" class="form-control" name="phone_number"
                                           value="{{ $user->phone_number }}">
                                    @include('alerts.error', ['error' => 'phone_number'])
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="clan_id" class="col-md-12 control-label">Clan</label>
                                    <div class="col-md-12">
                                        <select name="clan_id" id="clan_id" placeholder="None">
                                            <option value="0">None</option>
                                            @if($user->clan_id !== 0)
                                                @foreach($clans as $clan)
                                                    @if($user->clan->id == $clan->id)
                                                        <option value="{{ $clan->id }}"
                                                                selected>{{ $clan->clan_name }}</option>
                                                    @else
                                                        <option value="{{ $clan->id }}">{{ $clan->clan_name }}</option>
                                                    @endif
                                                @endforeach
                                            @else
                                                @foreach($clans as $clan)
                                                    <option value="{{ $clan->id }}">{{ $clan->clan_name }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name" class="col-md-12 control-label">User Level</label>
                                    <div class="col-md-12">
                                        <select name="user_level">
                                            @foreach($userLevels as $level)
                                                @if($level->id == $user->level->id)
                                                    <option value="{{ $level->id }}"
                                                            selected>{{ $level->name }}</option>
                                                @else
                                                    <option value="{{ $level->id }}">{{ $level->name }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="name" class="col-md-2 control-label">User Status</label>
                                    <div class="col-md-10">
                                        <select name="user_status_id">
                                            @foreach($userStatuses as $status)
                                                @if($status->id == $user->userStatuses->id)
                                                    <option value="{{ $status->id }}"
                                                            selected>{{ $status->title }}</option>
                                                @else
                                                    <option value="{{ $status->id }}">{{ $status->title }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                            <div class="form-group">
                                <label for="gamer_pic" class="col-md-2 control-label">Gamer Pic</label>
                                <div class="col-md-10">
                                    <p style="font-weight: bold;">{{ $user->gamer_pic }}</p>
                                </div>
                            </div>
                            </div>

                            <div class="col-md-12">
                            <div class="form-group">
                                <div class="col-md-10 col-md-offset-2">
                                    <button type="submit" class="btn btn-primary">
                                        Update
                                    </button>
                                </div>
                            </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
