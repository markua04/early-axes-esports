@extends('layouts.app')

@section('content')
    <div class="container">
        <h2>List of all the users</h2>
        <p>Take a look at all of the users below or click on their profile links to see a full overview of the user</p>
        <div class="row">
            <form action="/manage-users/search" method="POST" role="search">
                {{ csrf_field() }}
                <div style="padding-right: 0" class="input-group col-md-11 col-xs-9">
                    <input style="border-radius: 5px 0px 0px 5px;height: 35px;" type="text" class="form-control" name="q"
                           placeholder="Search users"> <span class="input-group-btn">
                    </span>
                </div>
                <div style="padding-left:0" class="col-md-1 col-xs-3">
                    <button type="submit" class="btn btn-default">
                        <span class="glyphicon glyphicon-search">Search</span>
                    </button>
                </div>
            </form>
        </div>
        <br />
        <table class="table table-bordered dark-table scrollable-table">
            <thead>
            <tr>
                <th>Gamer Tag</th>
                <th>First Name</th>
                <th>Surname</th>
                <th>Contact Number</th>
                <th>Clan Name</th>
                <th>Email</th>
                <th>User Level</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>
            </thead>
            <tbody>
            @foreach($users as $user)
                <tr>
                    <td>{{ $user->gamer_tag }}</td>
                    <td><a class="blue-text" href="/user-profile/{{ $user->id }}/{{ $user->name }}">{{ $user->name }}</a></td>
                    <td>{{ $user->surname }}</td>
                    <td>{{ $user->phone_number }}</td>
                    <td>{{ $user->clan['clan_name'] }}</td>
                    <td>{{ $user->email }}</td>
                    <td>{{ $user->level->name }}</td>
                    <td><a style="color:blue;" href="/edit-user-privileges/{{ $user->id }}">Edit</a></td>
                    <td><a style="color:blue;" href="/admin/delete-user/{{ $user->id }}">Delete</a></td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{ $users->links() }}
    </div>
@endsection