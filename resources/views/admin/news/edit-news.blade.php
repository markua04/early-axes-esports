@extends('layouts.app')

@section('content')
    <div class="container">
    <div class="row margin-bottom-15-p">
        <div class="col-md-12 margin-top-10">
            <div class="margin-t-50 panel panel-default">
                <div class="panel-heading">Update System News</div>
                <div class="panel-body">
                    <form class="form-horizontal" method="POST" enctype="multipart/form-data" action="/admin/edit-news/{{ $news->id }}">
                        {{ csrf_field() }}
                        <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-12 control-label">Title</label>
                            <div class="col-md-12">
                                <input id="name" type="text" class="form-control" name="title" value="{{ $news->title }}" >
                                @include('alerts.error', ['error' => 'title'])
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="type" class="col-md-12 control-label">Select Category</label>
                            <div class="col-md-12">
                                <select name="type">
                                    <option value="">Select a category</option>
                                    @foreach($categories as $category)
                                        <option value="{{ $category->id }}">{{ $category->title }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('content') ? ' has-error' : '' }}">
                            <label for="rules" class="col-md-12 control-label text-left-label">News Content</label>
                            <div class="col-md-12">
                                <textarea id="editor" name='content' class="form-control editor">{{ $news->content }}</textarea>
                                 @include('alerts.error', ['error' => 'content'])
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="gamer_pic" class="col-md-12 control-label">Upload News Banner</label>
                            <div class="col-md-12">
                                @if(Session::has('success'))
                                    <div class="alert-box success">
                                        <h2>{!! Session::get('success') !!}</h2>
                                    </div>
                                @endif
                                <div class="control-group">
                                    {!! Form::file('image',['class' => 'upload-image','type' => 'file','data-max-size' => '2048']) !!}
                                    <p class="errors">{!!$errors->first('image')!!}</p>
                                    @if(Session::has('error'))
                                        <p class="errors">{!! Session::get('error') !!}</p>
                                    @endif
                                </div>
                                <div id="success"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary pull-right">
                                    Update
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
