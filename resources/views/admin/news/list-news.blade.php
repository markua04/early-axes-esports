@extends('layouts.app')

@section('content')
    <div class="container">
        <h2>News</h2>
        <table class="table table-bordered dark-table">
            <thead>
            <tr>
                <th>Title</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>
            </thead>
            <tbody>
            @foreach($newsArticles as $news)
            <tr>
                <td style="font-size:15px;">{{ $news->title }}</td>
                <td><a href="/admin/edit-news/{{ $news->id }}">Edit</a></td>
                <td><a href="/admin/delete-news/{{ $news->id }}">Delete</a></td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection

