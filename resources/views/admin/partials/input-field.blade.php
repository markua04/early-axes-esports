<div class="form-group{{ $errors->has($name) ? ' has-error' : '' }}">
    <label for="name" class="col-md-12 control-label">{{ $label }}</label>
    <div class="{{ isset($sizeClass) ? $sizeClass : 'col-md-12' }}">
        <input id="name" type="text" class="form-control" name="{{ $name }}" value="{{ isset($value) ? $value : '' }}" {{ isset($required) && $required == 'yes' ? 'required autofocus' : '' }}>
        @include('alerts.error', ['error' => $name])
    </div>
</div>