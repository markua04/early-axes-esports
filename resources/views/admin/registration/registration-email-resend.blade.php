@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row margin-bottom-15-p">
            <div class="col-md-12 margin-t-30">
                <h2>Resend registration emails for users</h2>
                <div class="panel panel-default">
                    <div class="panel-heading">Registration Email Resend</div>
                    <div class="panel-body">
                        <form class="form-horizontal" method="POST" enctype="multipart/form-data" action="/admin/resend-registration-email">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                        <div class="col-md-12">
                                            @include('admin.partials.input-field',['name' => 'email','sizeClass' => 'col-md-12', 'label' => 'Email'])
                                        </div>
                                        </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary pull-left">
                                        Submit
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
