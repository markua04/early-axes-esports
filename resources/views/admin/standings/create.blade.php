@extends('layouts.app')

@section('content')
    <div class="container tournament-container">
        <div class="col-lg-12 col-md-12 margin-t-10">
            <h2><strong>Pool 1</strong></h2>
            <form class="form-horizontal" method="POST" enctype="multipart/form-data" action="/admin/create-custom-standings">
            {{ csrf_field() }}
            <div class="row margin-b-10">
                <div class="col-md-2">
                <select name="pool">
                    @foreach($pools as $pool)
                    <option value="{{ $pool->id }}">{{ $pool->pool_name }}</option>
                    @endforeach
                </select>
                </div>
                <div class="col-md-3">
                    <select name="tournament_id">
                        @foreach($tournaments as $tournament)
                            <option value="{{ $tournament->id }}">{{ $tournament->tournament_title }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <table id="pool-table" class="table scrollable-table">
                <thead>
                <tr style="height: 15px;">
                    <th style="height: 15px;">Team</th>
                    <th style="height: 15px;">Played</th>
                    <th style="height: 15px;">Matches Won</th>
                    <th style="height: 15px;">Matches Lost</th>
                    <th style="height: 15px;">Maps Won</th>
                    <th style="height: 15px;">Maps Lost</th>
                    <th style="height: 15px;">Rounds Won</th>
                    <th style="height: 15px;">Forfeit</th>
                </tr>
                </thead>
                <tbody>
                <tr style="height: 15px;"></tr>
                </tbody>
            </table>
            <div class="form-group">
                <div class="col-md-12">
                    <button type="submit" class="btn btn-primary pull-right">
                        Create
                    </button>
                </div>
            </div>
            </form>
            <a id="add-row" class="btn btn-primary" href="javascript:void(0);">Add Row</a>
            <a id="remove-row" class="btn btn-primary" href="javascript:void(0);">Remove Row</a>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            var x = 0;
            var addRow = document.getElementById('add-row');
            var removeRow = document.getElementById('remove-row');

            addRow.onclick = function() {
                $('#pool-table tbody').append('<tr style="height: 15px;">\n' +
                    '<td style="height: 15px;">' +
                    '<select name="standings['+ x +'][clan_id]">' +
                    '<option value="">Select Clan</option>' +
                    '<?php foreach($clans as $clan){ ?>' +
                    '<?php echo "<option value=".$clan->id.">$clan->clan_name</option>" ?>"' +
                    '<?php } ?>' +
                    '</select>' +
                    '</td>\n' +
                    '<td style="height: 15px;"><input id="name" type="text" class="form-control" name="standings['+ x +'][matches_played]" value=""></td>\n' +
                    '<td style="height: 15px;"><input id="name" type="text" class="form-control" name="standings['+ x +'][matches_won]" value=""></td>\n' +
                    '<td style="height: 15px;"><input id="name" type="text" class="form-control" name="standings['+ x +'][matches_lost]" value=""></td>\n' +
                    '<td style="height: 15px;"><input id="name" type="text" class="form-control" name="standings['+ x +'][maps_won]" value=""></td>\n' +
                    '<td style="height: 15px;"><input id="name" type="text" class="form-control" name="standings['+ x +'][maps_lost]" value=""></td>\n' +
                    '<td style="height: 15px;"><input id="name" type="text" class="form-control" name="standings['+ x +'][rounds_won]" value=""></td>\n' +
                    '<td style="height: 15px;"><input id="name" type="text" class="form-control" name="standings['+ x +'][forfeit]" value=""></td>\n' +
                    '</tr>');
                x++;
            };
            removeRow.onclick = function() {
                $('#pool-table tr:last').remove();
            };

        });
    </script>
@endsection