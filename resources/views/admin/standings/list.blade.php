@extends('layouts.app')

@section('content')
    <div class="container margin-b-30">
        <h1 class="tournament-head">Standings</h1>
        <?php $pool = 1; ?>
        @foreach($standings as $standing)
        @if(!$standing->isEmpty())
        <h2 class="tournament-head">Pool {{ $pool }}</h2>
        <table id="pool-table" class="table scrollable-table-fix standings-table">
                <thead>
                <tr style="height: 15px;">
                    <th style="height: 15px;">#</th>
                    <th style="height: 15px;">Team</th>
                    <th style="height: 15px;">Played</th>
                    <th style="height: 15px;">Won</th>
                    <th style="height: 15px;">Lost</th>
                    <th style="height: 15px;">Maps Won</th>
                    <th style="height: 15px;">Maps Lost</th>
                    <th style="height: 15px;">Rounds Won</th>
                    <th style="height: 15px;">Forfeit</th>
                    <th style="height: 15px;">Score</th>
                    <th style="height: 15px;">Update</th>
                    <th style="height: 15px;">Delete</th>
                </tr>
                </thead>
                <tbody>
                <?php $x = 0; ?>
                @foreach($standing as $standingData)
                    <?php $x++ ?>
                    <form class="form-horizontal" method="POST" enctype="multipart/form-data" action="/admin/update-standing/{{ $standingData->id }}">
                    {{ csrf_field() }}
                    <tr style="height: 15px;">
                        <td style="height: 15px;">{{ $x }}</td>
                        <td style="height: 15px;">{{ $standingData->clan->clan_name }}</td>
                        <td style="height: 15px;"><input id="matches_played" type="text" class="form-control" name="matches_played" value="{{ $standingData->matches_played }}"></td>
                        <td style="height: 15px;"><input id="matches_played" type="text" class="form-control" name="matches_won" value="{{ $standingData->matches_won }}"></td>
                        <td style="height: 15px;"><input id="matches_played" type="text" class="form-control" name="matches_lost" value="{{ $standingData->matches_lost }}"></td>
                        <td style="height: 15px;"><input id="matches_played" type="text" class="form-control" name="maps_won" value="{{ $standingData->maps_won }}"></td>
                        <td style="height: 15px;"><input id="matches_played" type="text" class="form-control" name="maps_lost" value="{{ $standingData->maps_lost }}"></td>
                        <td style="height: 15px;"><input id="matches_played" type="text" class="form-control" name="rounds_won" value="{{ $standingData->rounds_won }}"></td>
                        <td style="height: 15px;"><input id="matches_played" type="text" class="form-control" name="forfeit" value="{{ $standingData->forfeit }}"></td>
                        <td style="height: 15px;"><input id="matches_played" type="text" class="form-control" name="total_score" value="{{ $standingData->total_score }}"></td>
                        <td style="height: 15px;"><button class="btn btn-xs update-standing" data-standing-id="{{ $standingData->id }}" href="#">Update</button></td>
                        <td style="height: 15px;"><a class="btn btn-xs" onclick="return confirm('Are you sure you want to delete this standing?')" href="/admin/delete-custom-standing/{{ $standingData->id }}">Delete</a></td>
                    </tr>
                    </form>
                @endforeach
                </tbody>
            </table>
            @endif
            <?php $pool++; ?>
        @endforeach
    </div>
@endsection