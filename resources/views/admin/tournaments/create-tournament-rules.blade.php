@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="col-md-12 margin-top-10">
            <div class="margin-t-50 panel panel-default">
                <div class="panel-heading">Create Tournament Rules</div>
                <div class="panel-body">
                    <form class="form-horizontal" method="POST" enctype="multipart/form-data" action="/admin/create-tournament-rules">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="name" class="col-md-12 control-label">Select Tournament</label>
                            <div class="col-md-12">
                                <select name="tournament">
                                    @foreach($tournaments as $tournament)
                                        <option value="{{ $tournament->id }}">{{ $tournament->tournament_title }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('rules') ? ' has-error' : '' }}">
                            <label for="rules" class="col-md-12 control-label text-left-label">Tournament Rules</label>
                            <div class="col-md-12">
                                <textarea name='rules' class="form-control editor">{{ old('rules') }}</textarea>
                                @if ($errors->has('rules'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('rules') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary pull-right">
                                    Create
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script src="{{ asset('js/tinymce/js/tinymce/tinymce.min.js') }}"></script>
    <script src="{{ asset('js/tinymce/js/tinymce/plugins/jbimages/plugin.min.js') }}"></script>
    <script type="text/javascript">
        tinymce.init({
            selector : "textarea",
            encoding: 'raw',
            height : "480",
            plugins : ["advlist autolink lists link image charmap print preview anchor", "searchreplace visualblocks code fullscreen", "insertdatetime media table contextmenu paste"],
            toolbar : "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image jbimages",
        });
    </script>

@endsection
