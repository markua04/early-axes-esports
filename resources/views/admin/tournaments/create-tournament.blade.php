@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="col-md-12 margin-top-10">
            <div class="margin-t-50 panel panel-default">
                <div class="panel-heading">Create Tournament</div>
                <div class="panel-body">
                    <form class="form-horizontal" method="POST" enctype="multipart/form-data"
                          action="/admin/create-tournament">
                        {{ csrf_field() }}
                        <div class="col-md-3">
                            <div class="form-group{{ $errors->has('tournament_title') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-12 control-label">Tournament Title</label>
                                <div class="col-md-12">
                                    <input id="name" type="text" class="form-control" name="tournament_title"
                                           value="{{ old('tournament_title') }}">
                                    @include('alerts.error', ['error' => 'tournament_title'])
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group{{ $errors->has('tournament_prize') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-12 control-label">Tournament Prize</label>
                                <div class="col-md-12">
                                    <input id="name" type="text" class="form-control" name="tournament_prize"
                                           value="{{ old('tournament_prize') }}">
                                    @include('alerts.error', ['error' => 'tournament_prize'])
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group{{ $errors->has('toornament_standings') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-12 control-label">Toornament Standings</label>
                                <div class="col-md-12">
                                    <input id="name" type="text" class="form-control" name="toornament_standings"
                                           value="{{ old('toornament_standings') }}">
                                    @include('alerts.error', ['error' => 'toornament_standings'])
                                </div>
                            </div>
                        </div>


                        <div class="col-md-3">
                            @include('admin.partials.input-field',['name' => 'discord_link','label' => 'Discord Link', 'sizeClass' => 'col-md-12'])
                        </div>

                        <div class="col-md-3">
                            <div class="form-group{{ $errors->has('tournament_player_limit') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-12 control-label">Tournament player limit per clan</label>
                                <div class="col-md-12">
                                    <input id="name" type="text" class="form-control" name="tournament_player_limit"
                                           value="{{ old('tournament_player_limit') }}">
                                    @include('alerts.error', ['error' => 'tournament_player_limit'])
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="name" class="col-md-12 control-label">Genre</label>
                                <div class="col-md-12">
                                    <select name="genre">
                                        @foreach($genres as $genre)
                                            <option value="{{ $genre->id }}">{{ $genre->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="tournament_platform" class="col-md-12 control-label">Tournament Platform</label>
                                <div class="col-md-12">
                                    <select name="tournament_platform">
                                        @foreach($platforms as $platform)
                                            <option value="{{ $platform->id }}">{{ $platform->platform }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="standings_type" class="col-md-12 control-label">Standings Type</label>
                                <div class="col-md-12">
                                    <select name="standings_type">
                                        <option value="1">Auto Generated</option>
                                        <option value="2">Toornament</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="name" class="col-md-12 control-label">Tournament Game</label>
                                <div class="col-md-12">
                                    <select name="tournament_game">
                                        @foreach($games as $game)
                                            <option value="{{ $game->id }}">{{ $game->game_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="name" class="col-md-12 control-label">Tournament Status</label>
                                <div class="col-md-12">
                                    <select name="tournament_status">
                                            <option value="1">Active</option>
                                            <option value="2">Inactive</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="name" class="col-md-12 control-label">Tournament Type</label>
                                <div class="col-md-12">
                                    <select name="tournament_type_id">
                                        @foreach($tournamentTypes as $type)
                                            <option value="{{ $type->id }}">{{ $type->tournament_type_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="name" class="col-md-12 control-label">Tournament Registrations</label>
                                <div class="col-md-12">
                                    <select name="tournament_registrations">
                                        <option value="1">Open</option>
                                        <option value="0">Closed</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <h4>Points System</h4>
                        </div>
                        <div class="col-md-4">
                            @include('admin.partials.input-field',['name' => 'win_points','label' => 'Win Points', 'sizeClass' => 'col-md-12'])
                        </div>

                        <div class="col-md-4">
                            @include('admin.partials.input-field',['name' => 'loss_points','label' => 'Loss Points', 'sizeClass' => 'col-md-12'])
                        </div>

                        <div class="col-md-4">
                            @include('admin.partials.input-field',['name' => 'draw_points','label' => 'Draw Points', 'sizeClass' => 'col-md-12'])
                        </div>

                        <div class="col-md-12">
                            <div class="form-group{{ $errors->has('tournament_info') ? ' has-error' : '' }}">
                                <label for="clan_tel" class="col-md-12 control-label text-left-label">Tournament
                                    Info</label>
                                <div class="col-md-12">
                                    <textarea id="editor" name='tournament_info' class="form-control editor">{{ old('tournament_info') }}</textarea>
                                    @include('alerts.error', ['error' => 'tournament_info'])
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="gamer_pic" class="col-md-12 control-label">Upload Tournament Banner</label>
                                <div class="alert alert-danger error-msg" style="display:none"></div>
                                <div class="col-md-12">
                                    @if(Session::has('success'))
                                        <div class="alert-box success">
                                            <h2>{!! Session::get('success') !!}</h2>
                                        </div>
                                    @endif
                                    <div class="control-group">
                                        {!! Form::file('image',['class' => 'upload-image','type' => 'file','data-max-size' => '2048']) !!}
                                        <p class="errors">{!!$errors->first('image')!!}</p>
                                        @if(Session::has('error'))
                                            <p class="errors">{!! Session::get('error') !!}</p>
                                        @endif
                                    </div>
                                    <div id="success"></div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-primary pull-left">
                                        Create
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
