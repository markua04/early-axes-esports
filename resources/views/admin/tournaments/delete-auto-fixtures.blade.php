@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="col-md-12 margin-top-10">
            <div class="margin-t-50 panel panel-default">
                <div class="panel-heading">Create Tournament Rules</div>
                <div class="panel-body">
                    <form class="form-horizontal" method="POST" enctype="multipart/form-data" action="/admin/auto-fixtures-remove">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="name" class="col-md-12 control-label">Select Tournament</label>
                            <div class="col-md-12">
                                <select name="tournament_id">
                                    @foreach($tournaments as $tournament)
                                        <option value="{{ $tournament->id }}">{{ $tournament->tournament_title }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="pool" class="col-md-12 control-label">Select Pool</label>
                            <div class="col-md-12">
                                <select id="pool" name="pool">
                                    <option value="1">Pool 1</option>
                                    <option value="2">Pool 2</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary pull-right">
                                    Delete
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
