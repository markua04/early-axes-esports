@extends('layouts.app')

@section('content')
<div class="container">
    <div class="col-md-12 margin-top-10">
        <div class="row">
            <h3>Edit Standings for {{ $tournament->tournament_title }}</h3>
            <form class="form-horizontal" method="POST" enctype="multipart/form-data" action="/admin/edit-auto-user-standings/{{ $tournament->id }}">
                {{ csrf_field() }}
            <table id="pool-table" class="table scrollable-table">
                <thead>
                <tr style="height: 15px;">
                    <th style="height: 15px;">Standing</th>
                    <th style="height: 15px;">Team</th>
                    <th style="height: 15px;">Wins</th>
                    <th style="height: 15px;">Losses</th>
                    <th style="height: 15px;">Draws</th>
                    <th style="height: 15px;">Goals For</th>
                    <th style="height: 15px;">Goals Against</th>
                    <th style="height: 15px;">Difference</th>
                    <th style="height: 15px;">Points</th>
                </tr>
                </thead>
                <tbody>
                @foreach($autoGeneratedStandings as $standingData)
                    <tr style="height: 15px;">
                        <td style="height: 15px;"> {{ $x }} </td>
                        <td style="height: 15px;">{{ $standingData->gamer_name }}</td>
                        <td style="height: 15px;"><input required name="standings[{{ $standingData->id }}][wins]" class="form-control" value="{{ $standingData->wins }}"></td>
                        <td style="height: 15px;"><input required name="standings[{{ $standingData->id }}][losses]" class="form-control" value="{{ $standingData->losses }}"></td>
                        <td style="height: 15px;"><input required name="standings[{{ $standingData->id }}][draws]" class="form-control" value="{{ $standingData->draws }}"></td>
                        <td style="height: 15px;"><input name="standings[{{ $standingData->id }}][goals_for]" class="form-control" value="{{ $standingData->goals_for }}"></td>
                        <td style="height: 15px;"><input name="standings[{{ $standingData->id }}][goals_against]" class="form-control" value="{{ $standingData->goals_against }}"></td>
                        <td style="height: 15px;"><input name="standings[{{ $standingData->id }}][difference]" class="form-control" value="{{ $standingData->difference }}"></td>
                        <td style="height: 15px;"><input required name="standings[{{ $standingData->id }}][points]" class="form-control" value="{{ $standingData->points }}"></td>
                    </tr>
                    <?php $x++ ?>
                @endforeach
                </tbody>
            </table>
            <div class="form-group col-md-6">
                <div class="col-md-12">
                    <button type="submit" class="btn btn-primary">
                        Update
                    </button>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>
@endsection