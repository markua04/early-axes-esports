@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="col-md-12">
            <div class="margin-t-50 panel panel-default">
                <div class="panel-heading">Edit Tournament Rules for {{ $rulesEntity->tournament[0]->tournament_title }}</div>
                <div class="panel-body">
                    <form class="form-horizontal" method="POST" enctype="multipart/form-data" action="/admin/edit-rules/{{ $rulesEntity->id }}">
                        {{ csrf_field() }}
                        <div class="form-group{{ $errors->has('rules') ? ' has-error' : '' }}">
                            <label for="rules" class="col-md-12 control-label text-left-label">Tournament Rules</label>
                            <div class="col-md-12">
                                <textarea name='rules' id="editor" class="form-control editor">{{ $rulesEntity->rules }}</textarea>
                                @include('alerts.error', ['error' => 'rules'])
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary pull-right">
                                    Update
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
