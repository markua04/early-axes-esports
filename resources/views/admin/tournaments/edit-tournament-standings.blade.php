@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="col-md-12">
            <div class="margin-t-50 panel panel-default">
                <div class="panel-heading">Edit Tournament Standings for {{ $standingsEntity->tournament[0]->tournament_title }}</div>
                <div class="panel-body">
                    <form class="form-horizontal" method="POST" enctype="multipart/form-data" action="/admin/edit-standings/{{ $standingsEntity->id }}">
                        {{ csrf_field() }}
                        <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-12 control-label">Title</label>
                            <div class="col-md-12">
                                <input id="name" type="text" class="form-control" name="title" value="{{ $standingsEntity->title }}" >
                                @if ($errors->has('title'))
                                    <span class="help-block">
                                    <strong>{{ $errors->first('title') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('standings') ? ' has-error' : '' }}">
                            <label for="rules" class="col-md-12 control-label text-left-label">Tournament Standings</label>
                            <div class="col-md-12">
                                <textarea id="editor" name='standings' class="form-control editor">{{ $standingsEntity->standings }}</textarea>
                                @if ($errors->has('standings'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('standings') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary pull-right">
                                    Update
                                </button>
                                <a href="/admin/delete-fixed-standing/{{ $standingsEntity->tournament[0]->id  }}" style="margin-right:10px;" type="submit" class="btn btn-primary pull-right">
                                    Delete
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script src="{{ asset('js/tinymce/js/tinymce/tinymce.min.js') }}"></script>
    <script src="{{ asset('js/tinymce/js/tinymce/plugins/jbimages/plugin.min.js') }}"></script>
    <script type="text/javascript">
        tinymce.init({
            selector : "textarea",
            encoding: 'raw',
            height : "480",
            plugins : ["advlist autolink lists link image charmap print preview anchor", "searchreplace visualblocks code fullscreen", "insertdatetime media table contextmenu paste"],
            toolbar : "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image jbimages",
        });
    </script>

@endsection
