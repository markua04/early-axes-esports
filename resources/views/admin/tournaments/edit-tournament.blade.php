@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="col-md-12">
            <div class="margin-t-50 panel panel-default">
                <div class="panel-heading">Edit Tournament</div>
                <div class="panel-body">
                    <form class="form-horizontal" method="POST" enctype="multipart/form-data"
                          action="/admin/edit-tournament/{{ $tournament->id }}">
                        {{ csrf_field() }}
                        <div class="col-md-3">
                            @include('admin.partials.input-field',['name' => 'tournament_title','label' => 'Tournament Title', 'value' => $tournament->tournament_title, 'required' => 'yes'])
                        </div>

                        <div class="col-md-3">
                            @include('admin.partials.input-field',['name' => 'tournament_player_limit','label' => 'Tournament Player Limit', 'value' => $tournament->tournament_player_limit])
                        </div>

                        <div class="col-md-3">
                            @include('admin.partials.input-field',['name' => 'toornament_standings','label' => 'Toornament Bracket Url', 'value' => $tournament->toornament_standings])
                        </div>

                        <div class="col-md-3">
                            @include('admin.partials.input-field',['name' => 'discord_link','label' => 'Discord Link', 'sizeClass' => 'col-md-12', 'value' => $tournament->discord_link])
                        </div>

                        <div class="col-md-3">
                            @include('admin.partials.input-field',['name' => 'tournament_prize','label' => 'Toornament Prize', 'value' => $tournament->tournament_prize])
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="tournament_platform" class="col-md-12 control-label">Tournament Platform</label>
                                <div class="col-md-12">
                                    <select name="tournament_platform">
                                        @foreach($platforms as $platform)
                                            @if($tournament->tournament_platform == $platform->id)
                                                <option selected="selected" value="{{ $platform->id }}">{{ $platform->platform }}</option>
                                            @else
                                                <option value="{{ $platform->id }}">{{ $platform->platform }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="standings_type" class="col-md-12 control-label">Standings Type</label>
                                <div class="col-md-12">
                                    <select name="standings_type">
                                        <option {{ $tournament->standings_type == 1 ? 'selected' : '' }} value="1">Auto Generated</option>
                                        <option {{ $tournament->standings_type == 2 ? 'selected' : '' }} value="2">Toornament</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="standings_type" class="col-md-12 control-label">Tournament Winner</label>
                                <div class="col-md-12">
                                    <select name="winner">
                                        <option value="">Select a winner</option>
                                        @if($tournament->tournament_type_id == 2)
                                            @foreach($registeredPlayers as $team)
                                                <option value="{{ $team->clan_id }}">{{ isset($team->clan->clan_name) ? $team->clan->clan_name : '' }}</option>
                                            @endforeach
                                        @else
                                            @foreach($registeredPlayers as $team)
                                                <option value="{{ isset($team->users->id) ? $team->users->id : '' }}">{{ isset($team->users->name) && isset($team->users->surname) ? $team->users->name." ".$team->users->surname : '' }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="name" class="col-md-12 control-label">Tournament Game</label>
                                <div class="col-md-12">
                                    <select name="tournament_game">
                                        @foreach($games as $game)
                                            @if($tournament->tournament_game == $game->id)
                                                <option selected="selected" value="{{ $game->id }}">{{ $game->game_name }}</option>
                                            @else
                                                <option value="{{ $game->id }}">{{ $game->game_name }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="name" class="col-md-12 control-label">Tournament Status</label>
                                <div class="col-md-12">
                                    <select name="tournament_status">
                                        @if($tournament->tournament_status == 1)
                                            <option selected="selected" value="1">Active</option>
                                            <option value="2">Inactive</option>
                                        @else
                                            <option value="1">Active</option>
                                            <option selected="selected" value="2">Inactive</option>
                                        @endif
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="name" class="col-md-12 control-label">Tournament Type</label>
                                <div class="col-md-12">
                                    <select name="tournament_type_id">
                                        @foreach($tournamentTypes as $type)
                                            @if($tournament->tournament_type_id == $type->id)
                                                <option selected="selected" value="{{ $type->id }}">{{ $type->tournament_type_name }}</option>
                                            @else
                                                <option value="{{ $type->id }}">{{ $type->tournament_type_name }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="name" class="col-md-12 control-label">Tournament Registrations</label>
                                <div class="col-md-12">
                                    <select name="tournament_registrations">
                                        @if($tournament->tournament_registrations == 1)
                                            <option selected="selected" value="1">Open</option>
                                            <option value="0">Closed</option>
                                        @else
                                            <option value="1">Open</option>
                                            <option selected="selected" value="0">Closed</option>
                                        @endif
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <h4>Points System</h4>
                        </div>
                        <div class="col-md-4">
                            @include('admin.partials.input-field',['name' => 'win_points','label' => 'Win Points', 'sizeClass' => 'col-md-12', 'value' => $tournament->win_points])
                        </div>

                        <div class="col-md-4">
                            @include('admin.partials.input-field',['name' => 'loss_points','label' => 'Loss Points', 'sizeClass' => 'col-md-12', 'value' => $tournament->loss_points])
                        </div>

                        <div class="col-md-4">
                            @include('admin.partials.input-field',['name' => 'draw_points','label' => 'Draw Points', 'sizeClass' => 'col-md-12', 'value' => $tournament->draw_points])
                        </div>

                        <div class="col-md-12">
                            <div class="form-group{{ $errors->has('tournament_info') ? ' has-error' : '' }}">
                                <label for="clan_tel" class="col-md-12 control-label text-left-label">Tournament
                                    Info</label>
                                <div class="col-md-12">
                                    <textarea id="editor" name='tournament_info' class="form-control editor">{{ $tournament->tournament_info }}</textarea>
                                    @if ($errors->has('tournament_info'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('tournament_info') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="gamer_pic" class="col-md-12 control-label">Upload Tournament Banner</label>
                            <div class="col-md-12">
                                @if(Session::has('success'))
                                    <div class="alert-box success">
                                        <h2>{!! Session::get('success') !!}</h2>
                                    </div>
                                @endif
                                <div class="control-group">
                                    {!! Form::file('image',['class' => 'upload-image','type' => 'file','data-max-size' => '2048']) !!}
                                    <p class="errors">{!!$errors->first('image')!!}</p>
                                    @if(Session::has('error'))
                                        <p class="errors">{!! Session::get('error') !!}</p>
                                    @endif
                                </div>
                                <div id="success"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary pull-right">
                                    Update
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
