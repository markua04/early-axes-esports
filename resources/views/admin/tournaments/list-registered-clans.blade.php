@extends('layouts.app')

@section('content')
    @include('alerts.status')
    <div class="container">
        <h2>List of all the clans playing in {{ $tournament->tournament_title }}</h2>
        <table class="table table-bordered dark-table">
            <thead>
            <tr>
                <th>Clan</th>
                <th>Manage Players</th>
                <th>Forfeit</th>
                <th>Remove Clan</th>
            </tr>
            </thead>
            <tbody>
            @foreach($registeredClans as $clan)
                <tr>
                    <td>{{ $clan->clan->clan_name }}</td>
                    <td><a href="/admin/manage-tournament-players-by-clan/{{ isset($clan->clan->id) ? $clan->clan->id : '' }}/{{ $tournament->id }}">Manage</a></td>
                    <td>
                        <select name="approval" class="updateMe" data-clan={{ isset($clan->clan->id) ? $clan->clan->id : '' }} data-tournamentid={{ $tournament->id }}>
                            <option value="">Select</option>
                            <option {{ $clan->forfeit == 1 ? 'selected' : '' }} value="1">Forfeit...Nee Wat!</option>
                            <option {{ $clan->forfeit == null ? 'selected' : '' }} value="0">Still In! Nee maar mooi!</option>
                        </select>
                    </td>
                    <td><a onclick="return confirm('Are you sure?')" href="/remove-tournament-clan-registration/{{ $tournament->id }}/{{ isset($clan->clan->id) ? $clan->clan->id : '' }}">Remove</a></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <script>
        $(document).ready(function() {
            let player = $('.playerSelect').val();
            console.log(player);
            $('.updateMe').on('change',function () {
                let clanId = $(this).data('clan');
                let tournamentId = $(this).data('tournamentid');
                $.ajax({
                    url: '/admin/update-clan-registration-status/',
                    type: 'post',
                    _method: 'PUT',
                    headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                    data: { clanId: clanId, tournamentId: tournamentId, player : player} ,
                    success: function (response) {
                        alert(response.message);
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            });
        });

    </script>
@endsection