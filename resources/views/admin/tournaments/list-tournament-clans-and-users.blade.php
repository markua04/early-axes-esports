@extends('layouts.app')

@section('content')
    @include('alerts.status')
    <div class="container">
        <h2>List of all the clans playing in the Siege Tournament</h2>
        <p>Take a look at all of the gamers below or click on their profile links to see a full overview of the user</p>
        <table class="table table-bordered dark-table">
            <thead>
            <tr>
                <th>Name</th>
                <th>Gamer Tag</th>
                <th>Gamer Profile</th>
                <th>Clan</th>
            </tr>
            </thead>
            <tbody>
            @foreach($tournaments as $tournament)
                <tr>
                    <td>{{ $tournament->clan_member_id }}</td>
                    {{--<td>{{ $gamer->clan->clan_name }}</td>--}}
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection