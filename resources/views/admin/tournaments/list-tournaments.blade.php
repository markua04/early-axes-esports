@extends('layouts.app')

@section('content')
    <div class="container">
        <h2>Tournaments</h2>
        <p>Take a look at all of the tournaments below or click on the tournament to see a full overview</p>
        <div class="row margin-b-15">
            <div id="app2">
                <tournaments-list-component :games='{!! $gamesObj !!}' :platforms='{!! $platformsObj !!}'></tournaments-list-component>
                <manage-tournaments-component :tournaments='{!! $tournamentsObj !!}'></manage-tournaments-component>
            </div>
        </div>
    </div>
@endsection