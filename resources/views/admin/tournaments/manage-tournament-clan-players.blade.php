@extends('layouts.app')

@section('content')
    @include('alerts.status')
    <div class="container">
        <h2>List of all the clans playing in {{ $tournament->tournament_title }} sdfsdggd</h2>
        <table class="table table-bordered dark-table">
            <thead>
            <tr>
                <th>Name</th>
                <th>Remove</th>
            </tr>
            </thead>
            <tbody>
            @foreach($players as $player)
                <tr>
                    <td>{{ isset($player->gamer_name) ? $player->gamer_name : '' }}</td>
                    <td><a class="remove-player" onclick="return confirm('Are you sure?')" href="" data-clan={{ $clanId }} data-tournament={{ isset($tournament->id) ? $tournament->id : '' }} data-user={{ isset($player->users->id) ? $player->users->id : '' }}>Remove</a></td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <select name="playerSelect" class="playerSelect">
            <option value="">Select</option>
            @foreach($clanMembers as $player)
                <option value="{{ isset($player->user->id) ? $player->user->id : '' }}">{{ $player->gamer_name ? $player->gamer_name : ''}}</option>
            @endforeach
        </select>
        <a id="add-player" class="btn btn-primary" href="" data-clanid={{ $clanId }} data-tournamentid={{ isset($tournament->id) ? $tournament->id : '' }}>Add</a>
    </div>
    <script src="{{  asset('js/jquery-3.2.1.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('.remove-player').on('click', function (e) {
                e.preventDefault();
                let playerId = $(this).data('user');
                let clanId = $(this).data('clan');
                let tournamentId = $(this).data('tournament');
                $.ajax({
                    url: '/api/v1/remove-player-from-tournament',
                    type: 'post',
                    _method: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                        'Authorization': "<?php echo Auth::user()->api_token ?>"
                    },
                    data: {clanId: clanId, tournamentId: tournamentId, playerId: playerId},
                    success: function (response) {
                        if (response.status === true) {
                            alert('Player removed from tournament');
                        }
                        location.reload();
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            });
            let selected = null;
            $('.playerSelect').on('change', function() {
                selected = this.value;
            });
            $('#add-player').on('click',function (e) {
                e.preventDefault();
                if(selected == null){
                    alert("Please select a user to add.");
                }
                if(selected !== null){
                    let clanId = $(this).data('clanid');
                    let tournamentId = $(this).data('tournamentid');
                    $.ajax({
                        url: '/api/v1/add-player-to-tournament',
                        type: 'post',
                        _method: 'POST',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                            'Authorization': "<?php echo Auth::user()->api_token ?>"
                        },
                        data: { clanId: clanId, tournamentId: tournamentId, playerId : selected} ,
                        success: function (response) {
                            if(response.status === true){
                                alert('Player added to tournament');
                            }
                            location.reload();
                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });
                }
            });
        });

    </script>
@endsection