<form method="post">
<button class="button-user" id="loadActivity" name="but1">Show Recent Activity</button>
</form>
<div id="achievements"></div>
<script>
    $(document).ready(function() {
        $('#loadActivity').click(function(e){
            $('#loadActivity').hide();
            $('.loading').show().delay(5000).fadeOut(1000);
            $('#achievements').hide().delay(5000).fadeIn(3000);
            e.preventDefault();
            $.ajax({
                type: "POST",
                url: "/get-activity/" + '<?php echo $userData->id ?>',
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                success: function (response) {
                    console.log(response);
                    if(response){
                        var i;
                        for (i = 0; i < 21; i++) {
                            if(response[i].achievementName){
                                var row = $(
                                    '<div class="achievements-block" class="col-md-12">'+
                                    '<h4 style="font-size:19px;color:#fff;">'+ response[i].contentTitle +'</h4>'+
                                    '<p style="color:#fff;font-size:16px;">Achievement : ' + response[i].achievementName +'</p>' +
                                    response[i].achievementDescription +
                                    '<br/>'+
                                    '</div>'+
                                    '<img style="min-width: 100%" src="'+
                                    response[i].itemImage +
                                    '">'+
                                    '</div><br/><br/>'
                                );
                            } else {
//                                var row = $(
//                                    '<div style="background-color:#636468;height:90px;padding:8px 8px 8px 8px;color:#fff;" class="col-md-12">'+
//                                    '<h4 style="color:#fff;">Screenshot</h4>' +
//                                    '<p style="font-size:16px;">'+ response[i].contentTitle +'</p>'+
//                                    response[i].description +
//                                    '<br/>'+
//                                    '</div>'+
//                                    '<img style="min-width: 100%" src="'+
//                                    response[i].itemImage +
//                                    '">'+
//                                    '</div><br/><br/>'
//                                );
                            }

                            $("#achievements").append(row);
                        }
                    }
                },
                error: function (error) {
                    console.log(error);
                }
            })
        })
    });

</script>