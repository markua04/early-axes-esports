@extends('layouts.app')

@section('content')
<div class="wrapper col-md-12">
    <form class="form-signin" method="POST" action="{{ route('login') }}">
        <img class="eax-portait center-image margin-b-10" src="/images/base-images/eax-logo-portrait.png">
        <img class="eax-landscape center-image" src="/images/base-images/eax-logo-bw-landscape.png">
        <h2 class="form-signin-heading text-center">login</h2>
        {{ csrf_field() }}
        <input id="email" type="email" class="custom-inputs" name="email" placeholder="Email Address" value="{{ old('email') }}" required autofocus>
        @include('alerts.error', ['error' => 'email'])
        <br />
        <input id="password" type="password" class="custom-inputs" name="password" placeholder="Password" required>
        @include('alerts.error', ['error' => 'password'])
        <label class="checkbox">
            <input type="checkbox" value="remember-me" id="rememberMe" name="rememberMe" {{ old('remember') ? 'checked' : '' }}> Remember me
        </label>
        <button class="btn btn-primary btn-block" type="submit">Login</button>
        <a class="btn btn-primary btn-block" href="{{ route('password.request') }}">
            Reset Password
        </a>
    </form>
</div>

<style>
    .custom-inputs {
        border: 0;
        outline: 0;
        background: transparent;
        border-bottom: 1px solid #a7a7a7;
        width: 100%;
        font-size: 15px;
        height: 50px;
        margin-bottom: 25px !important;
    }
    .custom-inputs:focus {
        border-bottom: 1px solid #007af7;
    }
</style>
@endsection
