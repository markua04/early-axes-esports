@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12 margin-t-10">
                <div class="panel panel-default margin-t-15">
                    <div class="panel-heading">Create a new clan</div>
                    <div class="panel-body">
                        <form class="form-horizontal" method="POST" enctype="multipart/form-data" action="/register-clan">
                            {{ csrf_field() }}
                            <div class="form-group{{ $errors->has('clan_name') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-12 control-label text-left-label">Clan Name</label>
                                <div class="col-md-12">
                                    <input id="name" type="text" class="form-control" name="clan_name" value="{{ old('clan_name') }}" required autofocus>
                                    @if ($errors->has('clan_name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('clan_name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('clan_email') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-12 control-label text-left-label">Clan Email</label>
                                <div class="col-md-12">
                                    <input id="clan_email" type="text" class="form-control" name="clan_email" value="{{ old('clan_email') }}" required autofocus>
                                    @if ($errors->has('clan_email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('clan_email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('clan_tel') ? ' has-error' : '' }}">
                                <label for="clan_tel" class="col-md-12 control-label text-left-label">Clan Tel/Cell number</label>
                                <div class="col-md-12">
                                    <input id="clan_tel" class="form-control" name="clan_tel" value="{{ old('clan_tel') }}" required>
                                    @if ($errors->has('clan_tel'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('clan_tel') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('clan_description') ? ' has-error' : '' }}">
                                <label for="clan_description" class="col-md-12 control-label text-left-label">Clan Description</label>
                                <div class="col-md-12">
                                    @if ($errors->has('clan_description'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('clan_description') }}</strong>
                                    </span>
                                    @endif
                                    <textarea id="editor" id="clan_description" name='clan_description' class="form-control editor">{{ old('clan_description') }}</textarea>
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('clan_logo') ? ' has-error' : '' }}">
                                <input type="hidden" id="clan_logo" class="form-control" name="clan_logo" value="">
                                <div class="col-md-12">
                                @if(Session::has('success'))
                                    <div class="alert-box success">
                                        <h2>{!! Session::get('success') !!}</h2>
                                    </div>
                                @endif
                                <div class="secure">Upload Logo</div>
                                <div class="control-group">
                                    {!! Form::file('image',['class' => 'upload-image','id' => 'clan_logo','type' => 'file','data-max-size' => '2048']) !!}
                                    <p class="errors">{!!$errors->first('image')!!}</p>
                                </div>
                                <div id="success"></div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-12">
                                    <button class="btn btn-primary">
                                        Register
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
