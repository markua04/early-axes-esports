@extends('layouts.app')

@section('content')

    <div class="wrapper col-md-12">
        <form class="form-signin" method="POST" action="{{ route('register') }}" enctype="multipart/form-data">
            {{ csrf_field() }}
            <img class="eax-portait center-image margin-b-10" src="/images/base-images/eax-logo-portrait.png">
            <img class="eax-landscape center-image" src="/images/base-images/eax-logo-bw-landscape.png">
            <h2 class="form-signin-heading text-center">Register a free account</h2>
            <input class="custom-inputs"placeholder="Name" id="name" type="text" name="name" value="{{ old('name') }}" required autofocus>
            @include('alerts.error', ['error' => 'name'])
            <br />
            <input class="custom-inputs"placeholder="Surname" id="surname" type="text" name="surname" value="{{ old('surname') }}" required autofocus>
            @include('alerts.error', ['error' => 'surname'])
            <br />
            <input class="custom-inputs"placeholder="Email Address" id="email" type="email" name="email" value="{{ old('email') }}" required>
            @include('alerts.error', ['error' => 'email'])
            <br />
            <div>
            <select id="first" name="clan_id" class="fstdropdown-select">
                <option value="">Select a clan</option>
                <option value="0">None</option>
                @foreach($clans as $clan)
                    <option value="{{ $clan->id }}">{{ $clan->clan_name }}</option>
                @endforeach
            </select>
            </div>
            @include('alerts.error', ['error' => 'clan_id'])
            <br />
            <br />
            <div>
            <select id="second" name="selected_source" class="fstdropdown-select">
                <option value="">Select a platform</option>
                @foreach($platforms as $platform)
                    @if($platform->platform !== "Cross-Platform")
                        <option value="{{ $platform->id }}">{{ $platform->platform }}</option>
                    @endif
                @endforeach
            </select>
            </div>
            @include('alerts.error', ['error' => 'platform'])
            <br />
            <br />
            <input class="custom-inputs"placeholder="Xbox Gamer Tag" id="gamer_tag" type="text" name="gamer_tag" value="{{ old('gamer_tag') }}" autofocus>
            @include('alerts.error', ['error' => 'gamer_tag'])
            <br />
            <input class="custom-inputs"placeholder="PSN ID" id="psn_id" type="text" name="psn_id" value="{{ old('psn_id') }}" autofocus>
            @include('alerts.error', ['error' => 'psn_id'])
            <br />
            <input class="custom-inputs"placeholder="Steam ID" id="steam_id" type="text" name="steam_id" value="{{ old('steam_id') }}" autofocus>
            @include('alerts.error', ['error' => 'steam_id'])
            <br />
            <input class="custom-inputs"placeholder="Uplay ID" id="uplay_id" type="text" name="uplay_id" value="{{ old('uplay_id') }}" autofocus>
            @include('alerts.error', ['error' => 'uplay_id'])
            <br />
            <input class="custom-inputs"placeholder="Phone Number" id="phone_number" type="text" name="phone_number" value="{{ old('phone_number') }}">
            @include('alerts.error', ['error' => 'phone_number'])
            <br />
            <input class="custom-inputs"placeholder="Password" id="password" type="password" name="password" required>
            @include('alerts.error', ['error' => 'password'])
            <br />
            <input class="custom-inputs"placeholder="Password Confirmation" id="password-confirm" type="password" name="password_confirmation" required>
            @include('alerts.error', ['error' => 'password_confirmation'])
            <br />
            @if(Session::has('success'))
                <div class="alert-box success">
                    <h2>{!! Session::get('success') !!}</h2>
                </div>
            @endif
            <div class="secure">Upload Logo</div>
            <div class="control-group">
                {!! Form::file('image',['class' => 'upload-image','type' => 'file','data-max-size' => '2048']) !!}
                <p class="errors">{!!$errors->first('image')!!}</p>
                @if(Session::has('error'))
                    <p class="errors">{!! Session::get('error') !!}</p>
                @endif
            </div>
            <div id="success"></div>
            <button class="btn btn-primary btn-block" type="submit">Register</button>
        </form>
    </div>
<style>
    .form-signin {
        max-width: 600px;
    }
    .custom-inputs {
        border: 0;
        outline: 0;
        background: transparent;
        border-bottom: 1px solid #a7a7a7;
        width: 100%;
        font-size: 15px;
        height: 50px;
        margin-bottom: 25px !important;
    }
    .custom-inputs:focus {
        border-bottom: 1px solid #007af7;
    }
</style>
<script>
    window.onload = function() {
        setFstDropdown();
    };
    let $form = $('form');
    let $inputs = $('input[type="file"]:not([disabled])', $form);
    $inputs.each(function(_, input) {
        if (input.files.length > 0) return;
        $(input).prop('disabled', true)
    });
    let formData = new FormData($form[0]);
    $inputs.prop('disabled', false);
</script>

@endsection
