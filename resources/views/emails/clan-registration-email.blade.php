<!DOCTYPE html>
<html>
    <head>
        <title>Early Axes E-sports</title>
    </head>
    <body>
        <h2>New Clan registration</h2>
        Clan {{ $clanNameToModerate }} has just registered and needs moderation.
        <br />
        Please click the below link to moderate this clan on the admin system.
        <br />
        <a href="https://earlyaxes-esports.co.za/moderate-clans">Moderate</a>
    </body>
</html>