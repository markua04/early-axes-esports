<!DOCTYPE html>
<html>
    <head>
        <title>Early Axes E-sports Enquiry</title>
    </head>
    <body>
        <h2>Please see below enquiry from Early Axes E-sports:</h2>
        <br/>
        Name : {{ $request->name }}
        <br />
        @if($request->surname)
            Surname : {{ $request->surname }}
        @endif
        <br />
            Email : {{ $request->email }}
        <br />
        @if($request->contact)
            Contact Number : {{ $request->contact }}
        @endif
        <br />
        Description :
        <br />
        {{ $request->query }}
    </body>
</html>