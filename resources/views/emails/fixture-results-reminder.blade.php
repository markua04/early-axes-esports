<!DOCTYPE html>
<html>
    <head>
        <title>Early Axes E-sports Fixture Results Reminder</title>
    </head>
    <body>
        <h2>Good day clan leader!</h2>
        <p>This is a reminder to please submit your fixture results before the cut off dates specified</p>
        <p>
            Please follow the following steps and provide the results as well as scorecards for the applicable league and fixture
        </p>
        <p>
            1: login to www.earlyaxes-esports.co.za with your created account
        </p>
        <p>
            2: go to clans, navigate to clan fixtures.
        </p>
        <p>
            3: select the relevant tournament and click on view
        </p>
        <p>
            4: select the relevant fixture and click on submit
        </p>
        <p>
            5:  fill in the rounds won and submit the scorecards(screenshots)
        </p>
        <p>
            6:  select submit
        </p>
        <p>
            Thank you for being part of Early Axes and supporting our leagues
            Yours in gaming
        </p>
        <p>
            Early Axes Team
        </p>
        <br />
    </body>
</html>