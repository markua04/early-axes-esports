@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row margin-t-10">
            <div class="col-md-12">
                <div class="margin-t-50 panel panel-default">
                    <div class="panel-heading">Unsubscribe from mailing list</div>
                    <div class="panel-body">
                        <form class="form-horizontal" method="POST" enctype="multipart/form-data" action="/unsubscribe">
                            {{ csrf_field() }}
                            <p>Please enter your email address and the reason why you are unsubscribing below.</p>
                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-12 control-label">Email</label>
                                <div class="col-md-12">
                                    <input id="name" type="text" class="form-control" name="email" value="{{ old('email') }}" >
                                    @include('alerts.error', ['error' => 'email'])
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('reason') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-12 control-label">Reason</label>
                                <div class="col-md-12">
                                    <textarea id="name" class="form-control" name="reason" rows="15"></textarea>
                                    @include('alerts.error', ['error' => 'reason'])
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-primary pull-right">
                                        Unsubscribe
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
            </div>
        </div>
    </div>
@endsection