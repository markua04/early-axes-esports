<!DOCTYPE html>
<html>
    <head>
        <title>Early Axes E-sports</title>
    </head>
    <body>
        <h2>New Gamer Tag Change</h2>
        {{ $user->name . ' ' . $user->surname }} has changed his gamer tag but he is still part of a tournament.
        <br />
        <br />
        Please moderate this gamer tag change.
        <br />
        <br />
        <br />
        <a style="
            background-color: #0062c6;
            font-weight:bold;
            font-size:18px;
            padding:12px 22px 12px 20px;
            text-decoration:none;
            color:#fff;
            border-radius: 4px;
        " class="btn btn-primary" href="https://earlyaxes-esports.co.za/">Moderate Gamer Tag Change</a>
        <br />
        <br />
    </body>
</html>