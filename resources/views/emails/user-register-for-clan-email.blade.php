<!DOCTYPE html>
<html>
    <head>
        <title>Early Axes E-sports</title>
    </head>
    <body>
        <h2>New Clan registration</h2>
        {{ $user->gamer_tag }} has just registered for your clan and needs moderation.
        <br />
        <br />
        Please click the below link to moderate this user for your clan.
        <br />
        <br />
        <br />
        <a style="
            background-color: #0062c6;
            font-weight:bold;
            font-size:18px;
            padding:12px 22px 12px 20px;
            text-decoration:none;
            color:#fff;
            border-radius: 4px;
        " class="btn btn-primary" href="https://earlyaxes-esports.co.za/view-unapproved-members/{{$user->clan_id}}">Moderate Clan Registration</a>
        <br />
        <br />
    </body>
</html>