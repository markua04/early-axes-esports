<!DOCTYPE html>
<html>
    <head>
        <title>Welcome to Early Axes E-Sports</title>
    </head>
    <body>
        <h2>Welcome to Early Axes E-Sports {{ $user['name'] }}</h2>
        <p>Your registered email address is {{ $user['email'] }}</p>
        <p>Please click on the verify email button below to verify your account.</p>
        <br/>
        <a style="
            background-color: #0062c6;
            font-weight:bold;
            font-size:18px;
            padding:12px 22px 12px 20px;
            text-decoration:none;
            color:#fff;
            border-radius: 4px;
        " class="btn btn-primary" href="{{ url('user/verify', $user->verifyUser->token) }}">Verify Email</a>
        <br/>
        <br/>
        <br/>
        <a href="https://earlyaxes-esports.co.za">
            <img src="{{ $message->embed(public_path('images/emails/welcome.jpg')) }}">
        </a>
    </body>
</html>