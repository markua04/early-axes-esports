<!DOCTYPE html>
<html>
    <head>
        <title>Early Axes E-sports</title>
    </head>
    <body>
        <h2>Weekly Forfeited games</h2>
        <br />
        The forfeited games have been updated!
        <p>
            These teams games were forfeited this week as they did not submit their match results :
        </p>
        @foreach($clansMatchesForfeited as $match)
            <p>{{ $x." ".$match }}</p>
            <?php $x++ ?>
        @endforeach
    </body>
</html>