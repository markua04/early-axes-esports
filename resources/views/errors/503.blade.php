@extends('errors::layout')

@section('title', 'Service Unavailable')

@section('message', 'We are currently performing maintenance. We will be back soon!')