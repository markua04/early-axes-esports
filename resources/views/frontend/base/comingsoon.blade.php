@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="col-md-12 margin-t-50" style="min-height: 300px;">
            <h1 class="text-center">{{ $pageName }} coming soon!</h1>
        </div>
    </div>
@endsection