{{--title: json['title']['rendered'].toString(),--}}
{{--excerpt: json['excerpt']['rendered'].toString(),--}}
{{--body: json['content']['rendered'].toString(),--}}
{{--featuredImage: json['_embedded']['wp:featuredmedia'][0]['media_details']['sizes']['medium_large']['source_url'].toString(),--}}
{{--author: json['_embedded']['author'][0]['name'].toString(),--}}
{{--slug: json['slug'].toString(),--}}
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <title>Early Axes E-Sports | Xbox & PC Gaming</title>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-116930486-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'UA-116930486-1');
    </script>
    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <script>
        (adsbygoogle = window.adsbygoogle || []).push({
            google_ad_client: "ca-pub-1972342625386980",
            enable_page_level_ads: true
        });
    </script>
    <!-- Styles -->
    <link href="{{ asset('css/all.css?v=1.0') }}" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Titillium Web' rel='stylesheet'>
    <script src="https://use.fontawesome.com/e07165561c.js"></script>
</head>
<style>
    .container {
        margin-top: 0 !important;
    }
    h2 {
        font-weight: bold;
    }
    img {
        left: 0;
        top: 0;
        width: 100%;
        height: auto;
    }
    a {
        color:blue;
    }
    p {
        color:#000;
    }
    .site-wrapper {
        background-color: #007AF7;
    }
</style>
<div class="site-wrapper responsive padding-r-10 padding-l-10 margin-b-15">
    <div class="container">
        <div class="row margin-t-10">
            @if($registeredClanMembers == null)
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <h2 class="text-center">No clans have registered yet.</h2>
            @else
            @foreach($registeredClanMembers as $clan)
                <div class="col-md-3 col-xs-12 margin-b-20">
                    <div class="player-container">
                        @if(!empty($clan[0]->clan->clan_logo) )
                            <a target="_blank" href="/clan-profile/{{ $clan[0]->clan->id }}/{{ str_slug($clan[0]->clan->clan_name) }}">
                                @if($clan['forfeit'] == 1)
                                    <img class="clan-logo-resize clan-logo" width="100%" src="{{ $clan[0]->clan->clan_logo }}">
                                    <div class="middle-forfeit">
                                        <div class="text-forfeit">FORFEIT</div>
                                    </div>
                                @else
                                    <img class="clan-logo-resize" width="100%" src="{{ $clan[0]->clan->clan_logo }}">
                                @endif
                            </a>
                        @endif
                        @if(!empty($clan[0]->clan->clan_name))
                            <div style="color:#000;" class="bottom-left">{{ $clan[0]->clan->clan_name }}</div>
                        @endif
                    </div>
                    <div class="slide-out-div">
                        <div class="names text-center">
                            @foreach($clan as $clanMember)
                            @if($clanMember !== null)
                                @if($tournament->tournament_platform == 1)
                                    <p class="center">{{ $clanMember->users->gamer_tag or '' }}</p>
                                @endif
                                @if($tournament->tournament_platform == 2)
                                    <p class="center">{{ $clanMember->users->psn_id or '' }}</p>
                                @endif
                                @if($tournament->tournament_platform == 3)
                                    <p class="center">{{ $clanMember->users->steam_id or '' }}</p>
                                @endif
                                @if($tournament->tournament_platform == 4)
                                    @if($clanMember->users->uplay_id == null)
                                        <p class="center">{{ $clanMember->users->steam_id or '' }}</p>
                                    @else
                                        <p class="center">{{ $clanMember->users->uplay_id or '' }}</p>
                                    @endif
                                @endif
                                @endif
                            @endforeach
                            <br/>
                        </div>
                    </div>
                </div>
            @endforeach
            @endif
        </div>
    </div>
</div>