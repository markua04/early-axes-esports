@extends('layouts.app')

@section('content')
<div class="row margin-bottom-15-p">
    <div class="col-md-10 col-md-offset-1 margin-top-10">
        <div class="col-md-12">
        <h1>{{ $clanNewsArticle->title }}</h1>
        {!! $clanNewsArticle->content !!}
        </div>
    </div>
</div>

@endsection
