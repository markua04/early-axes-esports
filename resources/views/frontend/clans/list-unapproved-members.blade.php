@extends('layouts.app')

@section('content')
    <div class="container">
        <h2>List of all the members to be approved</h2>
        @if($clanMembersNeedingModeration->total() > 0)
            <div id="app2">
                <unapproved-members :users="{{ json_encode($clanMembersArray) }}"></unapproved-members>
            </div>
        @else
            <h3>There are currently no clan members awaiting approval.</h3>
        @endif
        <br />
        <h2>List members in the clan</h2>
        @if($clanMembers->count() > 0)
            <table class="table table-bordered dark-table scrollable-table">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Surname</th>
                    <th>Email</th>
                    <th>Gamer Tag</th>
                    <th>Remove</th>
                </tr>
                </thead>
                <tbody>
                @foreach($clanMembers as $member)
                    <tr>
                        <td>{{ $member->user->name }}</td>
                        <td>{{ $member->user->surname }}</td>
                        <td>{{ $member->user->email }}</td>
                        <td><a class="blue-text" href="/user-profile/{{ $member->user->id }}/{{ $member->user->name }}}">{{ $member->user->gamer_tag }}</a></td>
                        <td>
                            <a class="blue-text" href="/remove-user-from-clan/{{ $member->clan_id }}/{{ $member->user->id }}">Remove</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @else
            <h3>There are currently no clan members in your clan.</h3>
        @endif
    </div>

    <script>
        $(document).ready(function() {
            // Update clan member status
            $('.updateMe').on('change',function () {
                var selected = $(this).val();
                var userId = $(this).data('user');
                $.ajax({
                    url: '{{ url('update-user-clan-status') }}',
                    type: 'post',
                    _method: 'POST',
                    headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                    data: { userId: userId, selected : selected} ,
                    success: function (response) {
                        console.log(response);
                        alert('Clan member status updated');
                        location.reload();
                    },
                    error: function (error) {
                        console.log(response.status);
                    }
                });
            });
        });
    </script>
@endsection