@extends('layouts.app')

@section('content')
<script src='https://www.google.com/recaptcha/api.js'></script>
    <div class="container">
        <div class="row margin-bottom-15-p">
            <div class="col-md-12 margin-t-30">
                <h2>Get in touch with us with your queries.</h2>
                <p>If you have any queries please let us know below. We will try to get back to you as soon as possible!</p>
                <div class="panel panel-default">
                    <div class="panel-heading">Contact Us</div>
                    <div class="panel-body">
                        <form class="form-horizontal" method="POST" enctype="multipart/form-data" action="/contact">
                            {{ csrf_field() }}
                            <div class="row">
                            <div class="col-md-6">
                                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                    <label for="name" class="col-md-12 control-label">Name</label>
                                    <div class="col-md-12">
                                        <input id="name" type="text" class="form-control" name="name"
                                               value="">
                                        @include('alerts.error', ['error' => 'name'])
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group{{ $errors->has('surname') ? ' has-error' : '' }}">
                                    <label for="surname" class="col-md-12 control-label">Surname</label>
                                    <div class="col-md-12">
                                        <input id="surname" type="text" class="form-control" name="surname"
                                               value="">
                                    </div>
                                </div>
                            </div>
                            </div>
                            <div class="row">
                            <div class="col-md-6">
                                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                    <label for="email" class="col-md-12 control-label">Email</label>
                                    <div class="col-md-12">
                                        <input id="email" type="email" class="form-control" name="email"
                                               value="">
                                        @include('alerts.error', ['error' => 'email'])
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group{{ $errors->has('contact_number') ? ' has-error' : '' }}">
                                    <label for="contact_number" class="col-md-12 control-label">Contact Number</label>
                                    <div class="col-md-12">
                                        <input id="contact_number" type="text" class="form-control" name="contact_number"
                                               value="">
                                    </div>
                                </div>
                            </div>
                            </div>
                            <div class="row">
                            <div class="col-md-12">
                                <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                                    <label for="description" class="col-md-12 control-label text-left-label">Description of enquiry</label>
                                    <div class="col-md-12">
                                        <textarea rows="10" name='description'
                                                  class="form-control">{{ isset($event->description) ? $event->description : "" }}</textarea>
                                        @include('alerts.error', ['error' => 'description'])
                                    </div>
                                </div>
                            </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                 <div class="g-recaptcha" data-sitekey="6LeqBWQUAAAAAFY14lpTfJAxN9iBSTXbsbb2f2sT"></div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary pull-left">
                                        Submit
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
