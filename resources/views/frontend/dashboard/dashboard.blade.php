@extends('layouts.app')

@section('content')
<div class="container">
    @if($user->phone_number == null)
        <div class="alert alert-success">
            Please <a class="blue-text" href="/edit-user/{{ Auth::user()->id}}">Click Here</a> to add your contact number to your profile so we can keep you updated on all your tournaments.
        </div>
    @endif
        @if(isset($user->gamer_pic) && $user->gamer_pic !== null)
            <img class="center-block margin-t-10" width="200" height="auto" src="{{ $user->gamer_pic }}">
        @endif
        <p class="text-center welcome-mvp margin-t-15 margin-b-20">Welcome {{ $gamerTag }}</p>
        <p class="text-center margin-t-15 margin-b-20 xp-header">Current EAX XP : {{ $userXp }}</p>
        @if($clanJoinUrl !== "")
            <p class="text-center margin-b-20">Send this URL to your friends to easily join your clan.</p>
            <div class="col-md-12 text-center input-group margin-b-20">
            <input class="margin-t-5" id="joinUrl" value="{{ URL::to('/').$clanJoinUrl }}">
{{--            <a href="#ex2" rel="modal:open" id="copyToClipboard" class="btn btn-primary btn-xs" data-clipboard-target="#joinUrl">Copy Link</a>--}}
{{--            <div id="ex2" class="modal row">--}}
{{--                <a href="#" rel="modal:close">Close</a>--}}
{{--                <p>Clan join link copied</p>--}}
{{--            </div>--}}
            </div>
        @endif
        <div class="col-md-12 text-center margin-b-20">
        @if($clan !== null)
            <div class="col-md-6">
                @if($clanOwner == 'Yes' || $clan->secondary_captain == $user->id)
                    <a href="/clan-fixtures-by-tournament/{{ $user->clan_id  }}"><div class="btn btn-primary">Clan Fixture Results</div></a>
                @endif
            </div>
        @endif
        <div class="{{ $clan == null ? 'col-md-12' : 'col-md-6' }}">
            <a class="margin-b-15-mobi" href="/individual-fixtures/{{ $user->id  }}"><div class="btn btn-primary">Individual Fixture Results</div></a>
        </div>
        </div>
    <div class="row margin-b-20">
        <div class="col-md-12">
            <a href="admin/list-tournaments">
                <div class="col-md-4 card-wrap-dash">
                    <div style="height:103px" class="col-md-12 bg-blue-eax caret padding-t-10 card-height">
                        <p class="white-text text-center">Tournaments Played</p>
                        <div class="col-md-12 white-text">
                            <div class="col-md-5 col-xs-5 dash-icons">
                                <i class="fa fa-trophy fa-4x"></i>
                            </div>
                            <div class="col-md-7 col-xs-12 margin-t-0-mobi">
                                <p class="right-centre-user-dash" style="font-size:40px;">{{ $tournamentsPlayed or '0'}}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
            @if(isset($user->clan->clan_name))
            <a href="clan-profile/{{ $user->clan_id or '' }}/{{ $user->clan->clan_name or '' }}">
                @endif
                <div class="col-md-4 card-wrap-dash">
                    <div class="col-md-12 bg-blue-eax caret padding-t-10 card-height">
                        <p class="white-text text-center">Current Clan</p>
                        <div class="col-md-12 white-text">
                            <div class="col-md-3 col-xs-3 dash-icons">
                                <i class="fa fa-gamepad fa-4x"></i>
                            </div>
                            <div class="col-md-9 col-xs-12 margin-t-0-mobi">
                                <p class="margin-t-15 right-centre-user-dash">{{ $user->clan->clan_name or 'No Clan' }}</p>
                            </div>
                        </div>
                    </div>
                </div>
                @if(isset($user->clan->clan_name))
            </a>
            @endif

            <a href="/list-events">
                <div class="col-md-4 card-wrap-dash">
                    <div class="col-md-12 bg-blue-eax caret padding-t-10 card-height">
                        <p class="white-text text-center">Event Registrations</p>
                        <div class="col-md-12 white-text">
                            <div class="col-md-5 col-xs-5 dash-icons">
                                <i class="fa fa-flag-checkered fa-4x"></i>
                            </div>
                            <div class="col-md-7 col-xs-12 margin-t-0-mobi">
                                <p class="right-centre-user-dash" style="font-size:40px;">{{ $eventRegistrations or '0' }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>
{{--    <div class="col-md-12 margin-b-15 text-center">--}}
{{--        <h4>View and Submit your fixture results</h4>--}}
{{--        <p>Click the button below to view the fixtures!</p>--}}
{{--        @include('frontend.partials.fixtures.clan-fixtures')--}}
{{--    </div>--}}
{{--    <div class="col-md-12 margin-b-15">--}}
{{--        @include('frontend.partials.fixtures.individual-fixtures')--}}
{{--    </div>--}}
    <div class="col-md-6 col-xs-12">
        @include('frontend.partials.tournaments.latest-tournament')
    </div>
    <div class="col-md-6">
        <h3 class="">Latest Event</h3>
            <div class="col-md-12 event-wrap-dash">
                <div class="col-md-12 remove-padding-col-md-3" style="margin-top: 4px;">
                    <a class="normal-header-link" href="/view-event/{{ $event->id }}/{{ str_slug($event->title, '-') }}">
                        <img src="{{ $event->banner }}" class="">
                    </a>
                </div>
                <div class="col-md-12" style="font-size:14px;">
                    <a class="normal-header-link text-center" href="/view-event/{{ $event->id }}/{{ str_slug($event->title, '-') }}"><div class="normal-header-link">{{$event->title}}</div></a>
                    <div class="col-md-6 col-sm-6 text-center">Date : {{ $event->date  }}</div>
                    <div class="col-md-6 col-sm-6 text-center">Time : {{ $event->time  }}</div>
                    <br/>
                    <span>{!! strip_tags(str_limit($event->description,1500, '')) !!}<a style="font-size:13px;color:black;" class="read-more" href="/view-event/{{ $event->id }}/{{ str_slug($event->title, '-') }}">&nbsp;&nbsp;read more...</a></span>
                </div>
            </div>
    </div>
</div>
@endsection