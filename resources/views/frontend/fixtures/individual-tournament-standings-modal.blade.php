<style>
    .modal {
        max-width: 1000px;
    }
    .table-bordered>tbody>tr>th {
        border: 1px solid #ddd;
        color:#fff;
        background-color: #313131;
        height: 30px;
    }
    .highlight-standing {
        background-color: #0062c6 !important;
        color:#fff;
    }
    .highlight-standing:hover {
        background-color: #0062c6 !important;
        color:#fff !important;
    }
</style>
