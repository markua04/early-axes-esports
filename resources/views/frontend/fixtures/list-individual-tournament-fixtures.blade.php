@extends('layouts.app')

@section('content')
    <div class="container">
        <h2>LIST OF ALL THE FIXTURES</h2>
        <table class="table table-bordered dark-table scrollable-table">
            <thead>
            <tr>
                <th>Tournament</th>
                <th>Clan 1</th>
                <th>Clan 2</th>
                <th>Date</th>
                <th>Time</th>
                <th>Submit Fixture Result</th>
            </tr>
            </thead>
            <tbody>
            @foreach($fixtures as $fixture)
                <tr>
                    @if(getTournamentById($fixture->tournament_id))
                        <td>{{ getTournamentById($fixture->tournament_id)->tournament_title }}</td>
                    @else
                        <td>N/A</td>
                    @endif
                    <td>{{ $fixture->clan_1 }}</td>
                    <td>{{ $fixture->clan_2 }}</td>
                    <td>{{ $fixture->date }}</td>
                    <td>{{ $fixture->time }}</td>
                    @if($todaysDate < $fixture->date)
                        <td>
                            Not played yet.
                        </td>
                    @else
                        <td>
                            @if(checkIfClanResultSubmitted($fixture->id) == 'no')
                                <a href="/{{$fixture->id}}/{{$clanId}}/submit-fixture-result" class="btn btn-primary btn-xs">
                                    Submit Result
                                </a>
                            @else
                                {{ getFixtureResult($fixture->id) }}
                            @endif
                        </td>
                    @endif
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection