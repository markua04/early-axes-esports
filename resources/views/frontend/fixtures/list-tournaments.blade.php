@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Clan Fixtures</h1>
        <table class="table table-bordered dark-table scrollable-table">
            <thead>
            <tr>
                <th>Tournament Name</th>
                <th>View Tournament Fixtures</th>
            </tr>
            </thead>
            <tbody>
            @foreach($tournaments as $tournament)
                <tr>
                    <td>{{ $tournament->tournament_title }}</td>
                    <td><a href="/clan-fixtures/{{ $tournament->id }}/{{ $clanId }}">View</a></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection