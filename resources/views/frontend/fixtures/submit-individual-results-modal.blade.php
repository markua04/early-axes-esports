<div id="ex1" class="modal row">
    <a href="#" rel="modal:close">Close</a>
    <div class="row margin-bottom-15-p">
        <div class="col-md-12 margin-top-10">
            <form class="form-horizontal" method="POST" enctype="multipart/form-data" action="/fixture/{{ $fixture->id }}/submit-individual-fixture-result">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-md-12">Please enter the amount of wins for each player below.</div>
                    <div class="col-md-6">
                        <div class="form-group{{ $errors->has('player_1_score') ? ' has-error' : '' }}">
                            <label for="player_1_score" class="col-md-12 control-label">{{ $player1->gamer_tag }} Wins</label>
                            <div class="col-md-12">
                                <input value="" name="player_1_score" type="text" class="form-control input-small">
                                @include('alerts.error', ['error' => 'player_1_score'])
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group{{ $errors->has('player_2_score') ? ' has-error' : '' }}">
                            <label for="player_2_score" class="col-md-12 control-label">{{ $player2->gamer_tag }} Wins</label>
                            <div class="col-md-12">
                                <input value="" name="player_2_score" type="text" class="form-control input-small">
                                @include('alerts.error', ['error' => 'player_2_score'])
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="col-md-12">
                                <div class="input_fields_wrap">
                                    <div class="form-group">
                                        <label for="gamer_pic" class="col-md-12 control-label">Round 1</label>
                                        <div class="col-md-12">
                                            <div class="control-group">
                                                {!! Form::file('image',['name' => 'rounds[]','class' => 'upload-image','type' => 'file','data-max-size' => '2048']) !!}
                                            </div>
                                            <div id="success"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="col-md-12">
                                <button class="add_field_button btn btn-xs">Add round</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary pull-left">
                                    Submit
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>