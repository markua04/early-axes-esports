@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row margin-bottom-15-p">
            <div class="col-md-12 margin-top-10">
                <div class="margin-t-50 panel panel-default">
                    <div class="panel-heading">Fixture Results</div>
                    <div class="panel-body">
                        <form class="form-horizontal" method="POST" enctype="multipart/form-data" action="/{{ $fixture->id }}/{{$clanId}}/submit-fixture-result">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-md-12">Please enter the amount of wins for each clan below.</div>
                            <div class="col-md-6">
                                <div class="form-group{{ $errors->has('clan_1_score') ? ' has-error' : '' }}">
                                    <label for="clan_1_score" class="col-md-12 control-label">{{ isset($clan1->clan_name) ? $clan1->clan_name : '' }} Wins</label>
                                    <div class="col-md-12">
                                        <input value="" name="clan_1_score" type="text" class="form-control input-small">
                                        @include('alerts.error', ['error' => 'clan_1_score'])
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group{{ $errors->has('clan_2_score') ? ' has-error' : '' }}">
                                    <label for="clan_2_score" class="col-md-12 control-label">{{ isset($clan2->clan_name) ? $clan2->clan_name : '' }} Wins</label>
                                    <div class="col-md-12">
                                    <input value="" name="clan_2_score" type="text" class="form-control input-small">
                                    @include('alerts.error', ['error' => 'clan_2_score'])
                                    </div>
                                </div>
                            </div>
                            </div>
                            <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="col-md-12">
                                    <div class="input_fields_wrap">
                                        <div class="form-group">
                                            <label for="gamer_pic" class="col-md-12 control-label">Round 1</label>
                                            <div class="col-md-12">
                                                <div class="control-group">
                                                    {!! Form::file('image',['name' => 'rounds[]','class' => 'upload-image','type' => 'file','data-max-size' => '2048']) !!}
                                                </div>
                                                <div id="success"></div>
                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="col-md-12">
                                    <button class="add_field_button btn btn-xs">Add round</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="col-md-12">
                                    <button type="submit" class="btn btn-primary pull-left">
                                        Submit
                                    </button>
                                    </div>
                                </div>
                            </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            var max_fields = 10; //maximum input boxes allowed
            var wrapper = $(".input_fields_wrap"); //Fields wrapper
            var add_button = $(".add_field_button"); //Add button ID

            var x = 1; //initial text box count
            $(add_button).click(function (e) { //on add input button click
                e.preventDefault();
                if (x < max_fields) { //max input box allowed
                    x++; //text box increment
                    $(wrapper).append('<div class="control-group"><label>Round ' + x + '</label>' + '{!! Form::file('image',['name' => 'rounds[]','class' => 'upload-image','type' => 'file','data-max-size' => '2048']) !!}' + '<a href="#" class="remove_field">Remove</a></div>'); //add input box
                }
            });

            $(wrapper).on("click", ".remove_field", function (e) { //user click on remove text
                e.preventDefault();
                $(this).parent('div').remove();
                x--;
            })
        });
    </script>
@endsection
