@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="col-md-12 margin-b-10">
            @include('frontend.gamer_stats.partials.back-to-search')
            <img class="col-md-12" src="/images/apex/apex_banner.jpg">
            <div class="col-md-12 margin-t-5">
                <p class="margin-t-5">Please note if not many of your stats are showing you must have at least 20 games played and set one of your banner trackers to "Games Played".</p>
                <p>Still can't see all of your stats? <a class="blue-text" href="#ex1" rel="modal:open">Click here</a></p>
                @include('frontend.gamer_stats.partials.apex-instructions-modal')
            </div>
        </div>
        <div class="col-md-12 margin-b-15">
            <div class="col-md-2">
                @if($platform == '1')
                    @if(isset($userStats['xboxData']->GameDisplayPicRaw))
                        <img src="{{ $userStats['xboxData']->GameDisplayPicRaw }}">
                    @elseif(isset(Auth::user()->gamer_pic))
                        <img src="{{ Auth::user()->gamer_pic }}">
                    @else
                        <img src="/images/apex/apex_icon.png">
                    @endif
                @elseif(Auth::user())
                    @if(Auth::user()->gamer_pic !== null)
                        <img src="{{ Auth::user()->gamer_pic }}">
                    @else
                        <img src="/images/apex/apex_icon.png">
                    @endif
                @else
                    <img src="{{ $userStats['apex']->avatar }}">
                @endif
            </div>
            <div class="col-md-10 margin-t-15">
            <h2>{{ $userStats['apex']->name }}</h2>
            <h3>Global Rank : #{{ isset($userStats['apex']->globalrank) ? $userStats['apex']->globalrank : 'N/A' }}</h3>
            <h3>Level : {{ isset($userStats['apex']->level) ? $userStats['apex']->level : 'N/A' }}</h3>
            </div>
        </div>
        <div class="col-md-12">
        <div class="col-md-4 card-wrap-dash">
            <div style="height:103px" class="col-md-12 bg-blue-eax caret padding-t-10 card-height">
                <p class="white-text text-center">Total Kills</p>
                <div class="col-md-12 white-text">
                    <div class="col-md-5 col-xs-5 dash-icons">
                        <i class="fa fa-crosshairs fa-4x"></i>
                    </div>
                    <div class="col-md-7 col-xs-12 margin-t-0-mobi">
                        <p class="right-centre-user-dash" style="font-size:40px;">{{ isset($userStats['apex']->kills) ? $userStats['apex']->kills : 'N/A' }}</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-4 card-wrap-dash">
            <div style="height:103px" class="col-md-12 bg-blue-eax caret padding-t-10 card-height">
                <p class="white-text text-center">Total Wins</p>
                <div class="col-md-12 white-text">
                    <div class="col-md-5 col-xs-5 dash-icons">
                        <i class="fa fa-trophy fa-4x"></i>
                    </div>
                    <div class="col-md-7 col-xs-12 margin-t-0-mobi">
                        <p class="right-centre-user-dash" style="font-size:40px;">{{ isset($userStats['apex']->headshots) ? $userStats['apex']->headshots : 'N/A'}}</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-4 card-wrap-dash">
            <div style="height:103px" class="col-md-12 bg-blue-eax caret padding-t-10 card-height">
                <p class="white-text text-center">Total Damage</p>
                <div class="col-md-12 white-text">
                    <div class="col-md-5 col-xs-5 dash-icons">
                        <i class="fa fa-chain-broken fa-4x"></i>
                    </div>
                    <div class="col-md-7 col-xs-12 margin-t-0-mobi">
                        <p class="right-centre-user-dash" style="font-size:40px;">{{ isset($userStats['apex']->damage) ? $userStats['apex']->damage : 'N/A'  }}</p>
                    </div>
                </div>
            </div>
        </div>
        <br />
            <div class="col-md-12">
                <h2>Apex Legend Individual Stats</h2>
            </div>
        @foreach($userStats['specialist_stats'] as $key => $value)
        <div class="col-md-3 col-xs-12 margin-b-20">
            <div class="player-container bg-eax-dark-black">
                <img class="clan-logo-resize" width="auto" src="/images/apex/legends/{{ $key }}.png">
                <div style="color:#000;" class="bottom-left">{{ ucwords($key) }}</div>
            </div>
            <div class="slide-out-div">
                <div class="names">
                    @foreach($value as $name => $stat)
                    <p class="text-center bold-content">{{ ucfirst($name) }} : {{ $stat }}</p>
                    @endforeach
                    <br>
                </div>
            </div>
        </div>
        @endforeach
        </div>
    </div>
@endsection