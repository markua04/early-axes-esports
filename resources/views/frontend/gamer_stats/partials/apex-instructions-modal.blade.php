<div id="ex1" class="modal row">
    <a href="#" rel="modal:close">Close</a>
    <div class="row margin-bottom-15-p">
        <div class="col-md-12 margin-top-10">
            <div class="title"><h4>Why you cannot see your profile?</h4></div>
            <br />
            <div class="content">
                <h5>Cannot find your profile?</h5>
                <p>In order for you to see your stats, your banner tracker must include your kills.</p>
                <hr>
                <h5>Why only kills are displayed?</h5>
                <p>For us to track your headshots and matches, you must add them to your banner as well.</p>
            </div>
        </div>
    </div>
</div>