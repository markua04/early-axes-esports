<div class="row margin-b-15">
    <img class="col-md-12 margin-b-20" src="/images/banners/playerstats.png">
    <form class="form-horizontal" method="POST" enctype="multipart/form-data" action="/player-stats">
        {{ csrf_field() }}
        <div class="col-md-12 margin-b-15">
            <input id="gamertag" class="form-control" placeholder="Enter Gamer Tag" name="gamertag" value="{{ Auth::user() ? Auth::user()->gamer_tag : ''}}" required>
        </div>
        <div class="col-md-6">
            <select name="game" class="game-select">
                <option value="">Select a game</option>
                @foreach($games as $game)
                    <option value="{{ $game }}">{{ $game }}</option>
                @endforeach
            </select>
        </div>
        <div class="col-md-5">
            <select name="platform" class="platform-select">
                @foreach($platforms as $platform)
                    @if($platform->id !== 3)
                        <option value="{{ $platform->id }}">{{ $platform->platform }}</option>
                    @endif
                @endforeach
                    <option value="5">Origin</option>
            </select>
        </div>
        <div class="col-md-1">
            <button type="submit" class="btn btn-primary pull-right">Get Stats</button>
        </div>
    </form>
</div>
</div>