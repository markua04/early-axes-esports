@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row margin-b-10">
            @include('frontend.gamer_stats.partials.back-to-search')
            <img src="/images/siege/siege-stats.jpeg">
            @if($platform == 1 || isset($userStats['xboxData']->Gamerscore))
                <div class="platform-bar xbox-bg-green">
                    <img class="logo-xbox" src="/images/logos/xbox-logo.png">
                    <p>XBOX</p>
                </div>
            @elseif($platform == 2)
                <div class="platform-bar ps-bg-blue">
                    <img class="logo-ps" src="/images/logos/ps-logo.png">
                </div>
            @else
                <div class="col-md-12 platform-bar ps-bg-black">
                    <img class="logo-pc" src="/images/logos/pc-logo.png">
                </div>
            @endif
        </div>
        <div class="row gamer-stats-wrap">
            <div class="col-md-12 margin-b-10">
                <p class="text-center stats-header">Rainbow Six Siege Stats : {{ isset($userStats['siege']->p_name) ? $userStats['siege']->p_name : 'N/A' }}</p>
            </div>
            <div class="col-md-6">
                <div class="col-md-3 margin-b-15">
                    @if(isset($userStats['xboxData']->GameDisplayPicRaw))
                        <img class="gamer-pic" src="{{ isset($userStats['xboxData']->GameDisplayPicRaw) ? $userStats['xboxData']->GameDisplayPicRaw : '' }}">
                    @elseif(Auth::user() && Auth::user()->gamer_pic !== null)
                        <img class="gamer-pic" src="{{ Auth::user()->gamer_pic !== null ? Auth::user()->gamer_pic : '' }}">
                    @else
                        <img class="gamer-pic" src="/images/default-no-image-eax.jpeg">
                    @endif
                </div>
                <div class="col-md-9 margin-t-5">
                    <p class="stats-names">Level : {{ $userStats['siege']->p_level }}</p>
                    <p class="stats-names">Current MMR : {{ $userStats['siege']->p_currentmmr }}</p>
                    <p class="stats-names">Max MMR this season : {{ $userStats['siege']->p_maxmmr }}</p>
                </div>
            </div>
            <div class="col-md-6">
                <div class="col-md-3">
                    <img style="margin-top: -10px;" src="{{ isset($userStats['siege']->p_currentrank) ? "https://r6tab.com/images/pngranks/".$userStats['siege']->p_currentrank.".png" : '/images/default-no-image-eax.jpeg' }}">
                </div>
                <div class="col-md-9 margin-t-5">
                    <p class="stats-names">Ranked KD : {{  $userStats['siege']->KD }}</p>
                    <p class="stats-names">Favourite Attacker : {{  $userStats['operators'][$userStats['siege']->favattacker] !== 'NA' ? $userStats['operators'][$userStats['siege']->favattacker] : 'N/A' }}</p>
                    <p class="stats-names">Favourite Defender : {{  $userStats['operators'][$userStats['siege']->favdefender] !== 'NA' ? $userStats['operators'][$userStats['siege']->favdefender] : 'N/A' }}</p>
                </div>
            </div>
        </div>
        <br/>
        <div class="col-md-12">
            <h2>Ranked Stats this season</h2>
            @if($userStats['siege']->seasonal->total_rankedkills > 0 || $userStats['siege']->seasonal->total_rankeddeaths > 0)
            <table class="table table-bordered scrollable-table">
                <thead>
                <tr>
                    <th>Total Kills</th>
                    <th>Total Deaths</th>
                    <th>Total Wins</th>
                    <th>Total Losses</th>
                    <th>KD</th>
                </tr>
                </thead>
                <tbody>
                <td>{{ $userStats['siege']->seasonal->total_rankedkills }}</td>
                <td>{{ $userStats['siege']->seasonal->total_rankeddeaths }}</td>
                <td>{{ $userStats['siege']->seasonal->total_rankedwins }}</td>
                <td>{{ $userStats['siege']->seasonal->total_rankedlosses }}</td>
                <td>{{ $userStats['siege']->KD }}</td>
                </tbody>
            </table>
            @else
                <h3>No Stats for this season.</h3>
            @endif
        </div>
        <br />
        <div class="col-md-12 margin-t-15">
            <h2>Casual Stats this season</h2>
            @if($userStats['siege']->seasonal->total_casualkills > 0 || $userStats['siege']->seasonal->total_casualdeaths > 0)
            <table class="table table-bordered scrollable-table">
                <thead>
                <tr>
                    <th>Total Kills</th>
                    <th>Total Deaths</th>
                    <th>Total Wins</th>
                    <th>Total Losses</th>
                    <th>KD</th>
                </tr>
                </thead>
                <tbody>
                <td>{{ $userStats['siege']->seasonal->total_casualkills }}</td>
                <td>{{ $userStats['siege']->seasonal->total_casualdeaths }}</td>
                <td>{{ $userStats['siege']->seasonal->total_casualwins }}</td>
                <td>{{ $userStats['siege']->seasonal->total_casuallosses }}</td>
                <td>{{ $userStats['siege']->seasonal->total_casualdeaths > 0 ? number_format((float)$userStats['siege']->seasonal->total_casualkills / $userStats['siege']->seasonal->total_casualdeaths, 2, '.', '') : ''}}</td>
                </tbody>
            </table>
             @else
                <h3>No casual stats this season.</h3>
            @endif
        </div>
        <br />
        <div class="col-md-12 margin-t-15">
            <h2>Terrorist Hunt Stats</h2>
            @if($userStats['thunt']->kills > 0 || $userStats['thunt']->deaths > 0)
            <table class="table table-bordered scrollable-table">
                <thead>
                <tr>
                    <th>Total Kills</th>
                    <th>Total Deaths</th>
                    <th>Total Wins</th>
                    <th>Total Losses</th>
                    <th>KD</th>
                </tr>
                </thead>
                <tbody>
                <td>{{ $userStats['thunt']->kills > 0 ? $userStats['thunt']->kills : 0 }}</td>
                <td>{{ $userStats['thunt']->deaths > 0 ? $userStats['thunt']->deaths : 0 }}</td>
                <td>{{ $userStats['thunt']->wins > 0 ? $userStats['thunt']->wins : 0 }}</td>
                <td>{{ $userStats['thunt']->losses > 0 ? $userStats['thunt']->losses : 0}}</td>
                <td>{{ $userStats['thunt']->deaths > 0 ? number_format((float)$userStats['thunt']->kills / $userStats['thunt']->deaths, 2, '.', '') : 0 }}</td>
                </tbody>
            </table>
            @else
                <h3>No terrorist hunt stats available</h3>
            @endif
        </div>
    </div>
@endsection