@extends('layouts.app')

@section('content')
    <div class="container margin-b-30">
        <h2>Welcome to Early Axes E-sports</h2>
        <p>Here you can Create a personal profile and clan, manage a clan, enter for leagues and events and do much much
            more.</p>
        <p>By taking a look at how to navigate the Early Axes E-Sports website you will get a better understanding of
            how to find your way around.</p>
        <h3>Creating a Personal Profile (PC and Mobile)</h3>
        <p>When creating a personal profile you will need to do the following</p>
        <p>Click on register</p>
        <p>Fill out the registration page</p>
        <p>If part of a clan select the clan</p>
        <p>If not part of a clan select none</p>
        <p>If part of a clan but the clan has not been created select none</p>
        <p>If you are the clan captain and your clan has not been created, leave the tab &ldquo;None&rdquo;</p>
        <p>When you have completed your profile registration please click submit.</p>
        <h3>Creating a Clan (PC and mobile)</h3>
        <p>If you are a clan owner or the clan administrator:</p>
        <p>When you have completed your personal profile registration and have logged in to the website click on
            clans.</p>
        <p>Then click on create a clan</p>
        <p>Fill out the clan description</p>
        <p>Upload a Clan emblem (Please refrain from uploading any emblems or pictures that may be offensive or may be
            deemed offensive by others )</p>
        <p>When clan creation has been completed, click submit and wait for an Early Axes Admin for approval.</p>
        <p>Once approved, your clan will be visible to others</p>
        <h3>Getting Members into your clan (PC and mobile)</h3>
        <p>If you are a clan owner or the clan administrator:</p>
        <p>When clan members create their personal profiles, they must select the clan they belong to and wait for
            approval from the clan owner or clan captain.</p>
        <p>When a member has already registered a personal profile before the clan was created. The member must click on
            his/her own profile and click on edit profile and change the clan they belong to.</p>
        <h3>When entering a tournament</h3>
        <p>If you are a clan owner or the clan administrator:</p>
        <p>Early Axes has made the e-sports site that a clan only has to register once, even if you are a multi-gaming
            clan.</p>
        <p>Unless a clan has 2 separate teams for various games or for only one tournament they will need to register
            both.</p>
        <p>When clans are ready to take part in a league and have all team members in their clan, clan owners can click
            on list tournaments.</p>
        <p>Then select the tournament they would like to participate in.</p>
        <p>Then click on the black tab on the top right that says &ldquo;Enter tournament&rdquo;</p>
        <p>Clan owners will then need to select the players they wish to add to that tournament</p>
        <p>Then select submit. You will now be a participant in that league.</p>
        <p><br/></p>
        <h3>How to enter an event</h3>
        <p>Early Axes hosts plenty of community events, if you would like to participate in an event you will need to
            register for the event.</p>
        <p>Events are simpler and was created keeping no-community and community members in mind</p>
        <p>To enter an event you do not have to register a profile on the Early Axes E-sports Website</p>
        <p>To enter the event you simply click on the link that was posted on our social media pages and enter the
            registration form</p>
        <p>When you have completed the registration form select submit.</p>
        <p>By submitting your details you make it possible for us to add your GT to the list of participants and making
            it easier for the Early Axes admin to invite you to a certain event.</p>
        <p><br/></p>
        <h3>Checking gamer stats</h3>
        <p>All registered members</p>
        <p>Early Axes thought about each gamer and how important Xbox live stats and gaming stats are to you.</p>
        <p>When clicking on your own profile, you will see that your current Xbox Gamerscore will be visible</p>
        <p>You can also check your most recent Xbox live activity</p>
        <p>To check your gamer stats, select gamer stats</p>
        <p>Select platform</p>
        <p>Select your game</p>
        <p>And submit</p>
        <p>Early Axes is continuously improving and adding new and improved features to the Website to give all members
            a unique experience.</p>
        <p>If you have any queries or want to ask a question or report a fault please feel free to contact us via our
            social media channels</p>
    </div>
@endsection