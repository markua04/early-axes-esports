@extends('layouts.app')

@section('content')
    <div class="container">
        <h2 class="text-center">
            Tournament Excel/CSV Importer for individuals
        </h2>

        @if ( Session::has('success') )
            <div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <strong>{{ Session::get('success') }}</strong>
            </div>
        @endif

        @if ( Session::has('error') )
            <div class="alert alert-danger alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                    <span class="sr-o nly">Close</span>
                </button>
                <strong>{{ Session::get('error') }}</strong>
            </div>
        @endif

        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                <div>
                    @foreach ($errors->all() as $error)
                        <p>{{ $error }}</p>
                    @endforeach
                </div>
            </div>
        @endif

        <form action="/admin/import-individual-stats" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="col-md-6">
                <label for="tournament" class="col-md-12 control-label">Tournament</label>
                <select id="tournament" name="tournament" class="platform select-stat">
                    <option selected value="">Select Tournament</option>
                    @foreach($tournaments as $tournament)
                        <option value="{{ $tournament->id }}">{{ $tournament->tournament_title }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-6">
                <label for="pool" class="col-md-12 control-label">Pool</label>
                <select id="pool" name="pool" class="platform select-stat">
                    <option selected value="">Select Pool</option>
                    <option value="1">Pool 1</option>
                    <option value="2">Pool 2</option>
                </select>
            </div>
            <div class="col-md-12">
                Choose your xls/csv File : <input type="file" name="file" class="form-control">
                <input type="submit" class="btn btn-primary btn-lg" style="margin-top: 3%">
            </div>
        </form>

    </div>
@endsection