@extends('layouts.app')

@section('content')
    <div class="container">
        <h2>List of all the users</h2>
        <p>Take a look at all of the users below or click on their profile links to see a full overview of the user</p>
        <div class="row">
            <form action="/view-users/search" method="POST" role="search">
                {{ csrf_field() }}
                <div style="padding-right: 0" class="input-group col-md-11 col-xs-9">
                    <input style="border-radius: 5px 0px 0px 5px;height: 35px;" type="text" class="form-control" name="q"
                           placeholder="Search users"> <span class="input-group-btn">
                    </span>
                </div>
                <div style="padding-left:0" class="col-md-1 col-xs-3">
                    <button type="submit" class="btn btn-default">
                        <span class="glyphicon glyphicon-search">Search</span>
                    </button>
                </div>
            </form>
        </div>
        <br />
        <table class="table table-bordered dark-table scrollable-table">
            <thead>
            <tr>
                <th>Name</th>
                <th>Gamer Tag</th>
                <th>Clan Name</th>
                <th>Check Users Profile</th>
            </tr>
            </thead>
            <tbody>
            @foreach($users as $user)
                <tr>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->gamer_tag }}</td>
                    <td>{{ $user->clan['clan_name'] }}</td>
                    <td><a href="/user-profile/{{ $user->id }}/{{ $user->name }}">Profile</a></td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{ $users->links() }}
    </div>
@endsection