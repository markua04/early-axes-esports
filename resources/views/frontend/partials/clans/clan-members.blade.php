<table class="table table-bordered">
    <thead>
    <tr>
        <th>Gamer Tag</th>
        <th>User Profile</th>
    </tr>
    </thead>
    <tbody>
    @foreach($members as $member)
        <tr>
            <td>{{ getGamerTag($member->user) }}</td>
            <td><a style="color:blue;" href="/user-profile/{{ isset($member->user->id) ? $member->user->id : '' }}/{{ isset($member->user->name) ? $member->user->name : '' }}">View</a></td>
        </tr>
    @endforeach
    </tbody>
</table>
<div class="col-md-12">
{{ $members->links() }}
</div>
