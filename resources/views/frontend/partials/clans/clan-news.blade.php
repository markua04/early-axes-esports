@foreach($clanNews as $news)
    <div class="col-md-12 margin-b-10">
    <div class="header-news">
    {{ $news->title }}
    </div>
    <div class="col-md-12 margin-t-5" style="color:#2d2d2d;">
    {!! $news->content !!}
    </div>
    <br />
    <br />
    <div class="col-md-12">
    Posted : {{ isset($news->created_at) ? $news->created_at->format('d/m/Y') : "" }}
    <br />
    Posted By : {!! $news->user->gamer_tag !!}
    </div>
    </div>
@endforeach
<div class="col-md-12">
    {{ $clanNews->links() }}
</div>