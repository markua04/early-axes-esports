<div class="row">
    <div class="col-md-12">
        <div class="col-md-12">
            <table class="table table-bordered scrollable-table">
                <thead>
                <tr>
                    <th>Tournament</th>
                    <th>Date</th>
                    <th>Time</th>
                    <th>Clan 1</th>
                    <th>Clan 2</th>
                    <th>Winner</th>
                </tr>
                </thead>
                <tbody>
                @foreach($results as $result)
                @if(!empty($result->fixture->id))
                <tr>
                    <td>
                        @if(!empty($result->fixture->tournament_id))
                        {{ isset(getTournamentById($result->fixture->tournament_id)->tournament_title) ? getTournamentById($result->fixture->tournament_id)->tournament_title : '' }}
                        @endif
                    </td>
                    <td>{{ $result->fixture->date or '' }}</td>
                    <td>{{ $result->fixture->time or '' }}</td>
                    <td>{{ $result->fixture->clan_1 or '' }}</td>
                    <td>{{ $result->fixture->clan_2 or '' }}</td>
                    @if($result->clan_1_score > $result->clan_2_score)
                    <td>{{ $result->fixture->clan_1 or '' }}</td>
                    @else
                    <td>{{ $result->fixture->clan_2 or '' }}</td>
                    @endif
                </tr>
                @endif
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="col-md-12">
            {{ $results->links() }}
        </div>
    </div>
</div>
