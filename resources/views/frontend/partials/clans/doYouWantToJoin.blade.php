@extends('layouts.app')

@section('content')
    <div class="container">
        <h3 class="text-center">Do you want to join {{ $clanName }}?</h3>
        <div class="button-box col-md-12 text-center margin-t-30">
            <a class="btn btn-info" role="button" href="/join-clan-confirm/?clan={{ $clanName }}&confirm=true">
                Yes
            </a>
            <a class="btn btn-info" role="button" href="/join-clan-confirm/?clan={{ $clanName }}&confirm=false">
                No
            </a>
        </div>

{{--        <div class="row margin-bottom-15-p">--}}

{{--            <div class="row">--}}
{{--                <div class="col-md-6">--}}

{{--                </div>--}}
{{--                <div class="col-md-6">--}}
{{--                <a href="/join-clan-confirm/?clan={{ $clanName }}&confirm=false" type="submit"--}}
{{--                   class="btn btn-primary pull-right">--}}
{{--                    No--}}
{{--                </a>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
    </div>

@endsection