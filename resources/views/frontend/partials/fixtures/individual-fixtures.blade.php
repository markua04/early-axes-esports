@if(isset($individualFixtures))
    <div id="individualFixtures" class="btn button-primary open-close-operator col-md-12">View/Close Your Fixtures</div>
    <div class="row col-md-12 slide-out-div-2">
    @include('frontend.fixtures.submit-individual-results-modal')
    <table class="table table-bordered dark-table scrollable-table">
        <thead>
        <tr>
            <th>Tournament</th>
            <th>Player 1</th>
            <th>Player 2</th>
            <th>Date</th>
            <th>Time</th>
            <th>Submit Result</th>
        </tr>
        </thead>
        <tbody>
        @foreach($individualFixtures as $individualFixture)
            <tr>
                @if(getTournamentById($individualFixture->tournament_id))
                    <td>{{ getTournamentById($individualFixture->tournament_id)->tournament_title }}</td>
                @else
                    <td>N/A</td>
                @endif
                <td>{{ $individualFixture->player_1_id }}</td>
                <td>{{ $individualFixture->player_2_id }}</td>
                <td>{{ $individualFixture->date }}</td>
                <td>{{ $individualFixture->time }}</td>
                @if($todaysDate < $individualFixture->date)
                    <td>
                        Not played yet.
                    </td>
                @else
                    <td>
                        @if(checkIfClanResultSubmitted($individualFixture->id) == 'no')
                            <a href="#ex1" rel="modal:open" data-fixture="{{ $individualFixture->id }}" data-player1="{{ $individualFixture->clan_1 }}" data-player2="{{ $individualFixture->clan_2 }}" class="btn btn-primary btn-xs fixtureId">
                                Submit Result
                            </a>
                        @else
                            {{ getFixtureResult($individualFixture->id) }}
                        @endif
                    </td>
                @endif
            </tr>
        @endforeach
        </tbody>
    </table>
    </div>
    <script>
        $(document).ready(function() {
            $('.fixtureId').on('click',function () {
                let fixtureId = $(this).data('fixture');
                let player1 = $(this).data('player1');
                let player2 = $(this).data('player2');
                let subClan = '{{ Auth::user()->clan_id }}';
                $('.form-modal').attr('action', '/fixture/' + fixtureId + '/' + '/submit-individual-fixture-result?dynamic=true');
                $(".player_1_label").text(player1);
                $(".player_2_label").text(player2);
                var max_fields = 10; //maximum input boxes allowed
                var wrapper = $(".input_fields_wrap"); //Fields wrapper
                var add_button = $(".add_field_button"); //Add button ID

                var x = 1; //initial text box count
                $(add_button).click(function (e) { //on add input button click
                    e.preventDefault();
                    if (x < max_fields) { //max input box allowed
                        x++; //text box increment
                        $(wrapper).append('<div class="control-group"><label>Round ' + x + '</label>' + '{!! Form::file('image',['name' => 'rounds[]','class' => 'upload-image','type' => 'file','data-max-size' => '2048']) !!}' + '<a href="#" class="remove_field">Remove</a></div>'); //add input box
                    }
                });

                $(wrapper).on("click", ".remove_field", function (e) { //user click on remove text
                    e.preventDefault();
                    $(this).parent('div').remove();
                    x--;
                })
            })
        });
    </script>
@endif
