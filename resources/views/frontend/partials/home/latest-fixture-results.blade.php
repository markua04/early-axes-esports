<div class="row margin-t-10">
    <div class="col-md-12">
        <h4>Latest Fixture Results</h4>
    </div>
    <div class="col-md-12">
        <div id="app2">
            <fixture-results-ticker-component :fixtures="{{ json_encode($fixturesArray) }}" :count="{{ json_encode(count($fixturesArray)) }}"></fixture-results-ticker-component>
        </div>
    </div>
</div>