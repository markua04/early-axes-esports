<h3 class="underlined-h3">Take a look at some of our latest news</h3>
<h4>Early Axes E-Sports News</h4>
@foreach($systemNews as $news)
    <div class="row margin-b-10">
        <div class="col-md-3 margin-b-10" style="padding-right:10px">
            <div><img src="{{ $news->image }}"></div>
        </div>
        <div class="col-md-9 news-content-home">
            <div class="header-news-system">{{ $news->title }}</div>
            <div class="published-date">Published : {{ $news->created_at->format('d/m/Y') }}</div>
            <div class="more">{!! $news->content !!}</div>
        </div>
    </div>
@endforeach