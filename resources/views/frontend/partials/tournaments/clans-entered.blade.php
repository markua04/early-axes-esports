<div class="row">
@foreach($registeredClanMembers as $clan)
    <div class="col-md-3 col-xs-12 margin-b-20">
        <div class="player-container">
            @if(!empty($clan[0]->clan->clan_logo) )
            <a target="_blank" href="/clan-profile/{{ $clan[0]->clan->id }}/{{ str_slug($clan[0]->clan->clan_name) }}">
                @if($clan['forfeit'] == 1)
                <img class="clan-logo-resize clan-logo" width="100%" src="{{ $clan[0]->clan->clan_logo }}">
                <div class="middle-forfeit">
                    <div class="text-forfeit">FORFEIT</div>
                </div>
                @else
                    <img class="clan-logo-resize" width="100%" src="{{ $clan[0]->clan->clan_logo !== null ? $clan[0]->clan->clan_logo : '/images/default-no-image-eax.jpeg' }}">
                @endif
            </a>
            @endif
            @if(!empty($clan[0]->clan->clan_name))
            <div style="color:#000;" class="bottom-left">{{ $clan[0]->clan->clan_name }}</div>
            @endif
        </div>
        <div class="slide-out-div">
            <div class="names">
                @foreach($clan as $clanMember)
                    @if(isset($clanMember) && $clanMember->users->gt_change == null)
                    <a href="/user-profile/{{ $clanMember->users->id }}/{{ $clanMember->users->name }}">
                        @if($tournament->tournament_platform == 1)
                            <p class="center">{{ $clanMember->users->gamer_tag or '' }}</p>
                        @endif
                        @if($tournament->tournament_platform == 2)
                            <p class="center">{{ $clanMember->users->psn_id or '' }}</p>
                        @endif
                        @if($tournament->tournament_platform == 3)
                            <p class="center">{{ $clanMember->users->steam_id or '' }}</p>
                        @endif
                        @if($tournament->tournament_platform == 4)
                            @if($clanMember->users->uplay_id == null)
                                <p class="center">{{ $clanMember->users->steam_id or '' }}</p>
                            @else
                                <p class="center">{{ $clanMember->users->uplay_id or '' }}</p>
                            @endif
                        @endif
                        @if($tournament->tournament_platform == 5)
                            @if($clanMember->users->selected_source !== null)
                                @if($clanMember->users->selected_source !== null)
                                    @if($clanMember->users->selected_source == 1)
                                        <p class="center">{{ $clanMember->users->gamer_tag or '' }}</p>
                                    @elseif($clanMember->users->selected_source == 2)
                                        <p class="center">{{ $clanMember->users->psn_id or '' }}</p>
                                    @elseif($clanMember->users->selected_source == 3)
                                        <p class="center">{{ $clanMember->users->steam_id or '' }}</p>
                                    @elseif($clanMember->users->selected_source == 4)
                                        <p class="center">{{ $clanMember->users->uplay_id or '' }}</p>
                                    @endif
                                @endif
                            @else
                                @if( $clanMember->users->gamer_tag !== null)
                                    <p class="center">{{ $clanMember->users->gamer_tag or '' }}</p>
                                @elseif($clanMember->users->psn_id !== null)
                                    <p class="center">{{ $clanMember->users->psn_id or '' }}</p>
                                @elseif($clanMember->users->steam_id !== null)
                                    <p class="center">{{ $clanMember->users->steam_id or '' }}</p>
                                @elseif($clanMember->users->uplay_id !== null)
                                        <p class="center">{{ $clanMember->users->uplay_id or '' }}</p>
                                @endif
                            @endif
                        @endif
                    @endif
                    @if(isset($clanMember->users->gt_change) && $clanMember->users->gt_change == 1)
                        <p class="text-center gt-approval">{{ isset($clanMember->users->gamer_tag) ? $clanMember->users->gamer_tag : 'None' }} - GT Pending</p>
                    @endif
                    </a>
                @endforeach
                <br/>
            </div>
        </div>
    </div>
@endforeach
</div>