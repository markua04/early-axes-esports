@if(!empty($importedFpsStandingsPool1 !== null))
<h2 class="tournament-head">Pool 1</h2>
<table id="pool-table" class="table scrollable-table">
    <thead>
    <tr style="height: 15px;">
        <th style="height: 15px;">Clan</th>
        <th style="height: 15px;">Avg Score</th>
        <th style="height: 15px;">Kills</th>
        <th style="height: 15px;">Assists</th>
        <th style="height: 15px;">Deaths</th>
        <th style="height: 15px;">KD</th>
        <th style="height: 15px;">Rounds Won</th>
        <th style="height: 15px;">Rounds Lost</th>
        <th style="height: 15px;">Rounds Win/Loss</th>
        <th style="height: 15px;">Maps Win/Loss</th>
        <th style="height: 15px;">Points</th>
    </tr>
    </thead>
    <tbody>
    @foreach($importedFpsStandingsPool1 as $standingData)
        <tr style="height: 15px;">
            <td style="height: 15px;">{{ $standingData->clan }}</td>
            <td style="height: 15px;">{{ $standingData->avg_score }}</td>
            <td style="height: 15px;">{{ $standingData->kills }}</td>
            <td style="height: 15px;">{{ $standingData->assists }}</td>
            <td style="height: 15px;">{{ $standingData->deaths }}</td>
            <td style="height: 15px;">{{ $standingData->k_d_ratio }}</td>
            <td style="height: 15px;">{{ $standingData->round_w }}</td>
            <td style="height: 15px;">{{ $standingData->round_l }}</td>
            <td style="height: 15px;">{{ $standingData->round_win_loss_ratio }}</td>
            <td style="height: 15px;">{{ $standingData->map_win_loss }}</td>
            <td style="height: 15px;">{{ $standingData->points }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
@else
    Standings coming soon for Pool 1
@endif
@if(!empty($importedFpsStandingsPool2 !== null))
<h2 class="tournament-head">Pool 2</h2>
<table id="pool-table" class="table scrollable-table">
    <thead>
    <tr style="height: 15px;">
        <th style="height: 15px;">Clan</th>
        <th style="height: 15px;">Avg Score</th>
        <th style="height: 15px;">Kills</th>
        <th style="height: 15px;">Assists</th>
        <th style="height: 15px;">Deaths</th>
        <th style="height: 15px;">KD</th>
        <th style="height: 15px;">Rounds Won</th>
        <th style="height: 15px;">Rounds Lost</th>
        <th style="height: 15px;">Rounds Win/Loss</th>
        <th style="height: 15px;">Maps Win/Loss</th>
        <th style="height: 15px;">Points</th>
    </tr>
    </thead>
    <tbody>
    @foreach($importedFpsStandingsPool2 as $standingData)
        <tr style="height: 15px;">
            <td style="height: 15px;">{{ $standingData->clan }}</td>
            <td style="height: 15px;">{{ $standingData->avg_score }}</td>
            <td style="height: 15px;">{{ $standingData->kills }}</td>
            <td style="height: 15px;">{{ $standingData->assists }}</td>
            <td style="height: 15px;">{{ $standingData->deaths }}</td>
            <td style="height: 15px;">{{ $standingData->k_d_ratio }}</td>
            <td style="height: 15px;">{{ $standingData->round_w }}</td>
            <td style="height: 15px;">{{ $standingData->round_l }}</td>
            <td style="height: 15px;">{{ $standingData->round_win_loss_ratio }}</td>
            <td style="height: 15px;">{{ $standingData->map_win_loss }}</td>
            <td style="height: 15px;">{{ $standingData->points }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
@else
    Standings coming soon for pool 2
@endif
