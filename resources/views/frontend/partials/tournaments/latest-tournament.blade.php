
    @if($latestTournament)
    <h3 class="">Latest Tournament</h3>
    <div class="margin-b-30 tournament-wrapper caret">
        <div class="row padding-l-15 padding-r-15 padding-b-10" style="margin-top: 4px;">
            <a href="/view-tournament/{{$latestTournament->id}}/{{ str_slug($latestTournament->tournament_title) }}">
                <img class="tournament-banner-list" width="100%" src="{{ $latestTournament->tournament_banner }}">
            </a>
        </div>
        <div class="row tournament-content-list padding-r-25 padding-l-25">
            <a class="normal-header-link" href="/view-tournament/{{$latestTournament->id}}/{{ $latestTournament->tournament_title }}"><div style="font-size: 25px;" class="normal-header-link">{{$latestTournament->tournament_title}}</div></a>
            <span style="font-size: 16px;font-weight: bold;color:#262626;">Tournament Type : {{ getTournamentTypeNameById($latestTournament->tournament_type_id) }}</span>
            <br />
            <span style="font-size: 15px;">{!! strip_tags(str_limit($latestTournament->tournament_info,180, '')) !!}<a style="font-size:16px;" class="read-more" href="/view-tournament/{{$latestTournament->id}}/{{ $latestTournament->tournament_title }}">&nbsp;&nbsp;read more...</a></span>
        </div>
        @if(Auth::check() && Auth::user()->user_level_id == 1)
        @endif
    </div>
    @endif