<div class="row">
@foreach($tournamentRegisteredIndividuals as $player)
    <div class="col-md-3 col-xs-12 margin-b-10">
    <div class="player-container">
        @if($player->users)
            <a target="_blank" href="/user-profile/{{ $player->users->id }}/{{ $player->users->name }}">
        @else
            <a target="_blank" href="/user-profile/">
        @endif
        @if($player->users)
        @if(!empty($player->users->gamer_pic))
            <img class="user-logo-single" src="{{ $player->users->gamer_pic }}">
        @else
            <img class="user-logo-single" src="/images/default-no-image-eax.jpeg">
        @endif
        @else
            <img class="user-logo-single" src="/images/default-no-image-eax.jpeg">
        @endif
        </a>
        @if($player->users)
            @if($tournament->tournament_platform == 1)
                <div style="color:#000;" class="bottom-left">{{ $player->users->gamer_tag or '' }}</div>
            @endif
            @if($tournament->tournament_platform == 2)
                <div style="color:#000;" class="bottom-left">{{ $player->users->psn_id or '' }}</div>
            @endif
            @if($tournament->tournament_platform == 3)
                <div style="color:#000;" class="bottom-left">{{ $player->users->steam_id or '' }}</div>
            @endif
            @if($tournament->tournament_platform == 4)
                <div style="color:#000;" class="bottom-left">{{ $player->users->uplay_id or '' }}</div>
            @endif
        @endif
    </div>
    </div>
@endforeach
</div>