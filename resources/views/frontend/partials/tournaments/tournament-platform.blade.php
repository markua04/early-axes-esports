@if(!empty($tournament->tournament_platform))
    @if($tournament->tournament_platform == 1)
        <div class="col-md-12 platform-bar xbox-bg-green">
            <img class="logo-xbox" src="/images/logos/xbox-logo.png">
            <p>XBOX</p>
        </div>
    @endif
    @if($tournament->tournament_platform == 2)
        <div class="col-md-12 platform-bar ps-bg-blue">
            <img class="logo-ps" src="/images/logos/ps-logo.png">
        </div>
    @endif
    @if($tournament->tournament_platform == 3)
        <div class="col-md-12 platform-bar pc-bg-red">
            <img class="logo-pc" src="/images/logos/pc-logo.png">
        </div>
    @endif
    @if($tournament->tournament_platform == 4)
        <div class="col-md-12 platform-bar ps-bg-black">
            <img class="logo-pc" src="/images/logos/uplay-w.png">
        </div>
    @endif
    @if($tournament->tournament_platform == 5)
        <div class="col-md-12 platform-bar ps-bg-black">
            <p class="padding-t-10">MULTI-PLATFORM</p>
        </div>
    @endif
@endif