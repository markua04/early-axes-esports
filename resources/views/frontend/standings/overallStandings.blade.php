@extends('layouts.app')

@section('content')
    <div class="container">
        <h2 class="tournament-head">Overall Clan Standings for {{ $game->game_name }}</h2>
        <table id="pool-table" class="table scrollable-table">
            <thead>
            <tr style="height: 15px;">
                <th style="height: 15px;">Team</th>
                <th style="height: 15px;">Played</th>
                <th style="height: 15px;">Won</th>
                <th style="height: 15px;">Lost</th>
                <th style="height: 15px;">Draws</th>
                <th style="height: 15px;">Win Loss Ratio</th>
            </tr>
            </thead>
            <tbody>
            @foreach($clans as $clan)
                @if($clan['clan_logo'] && $clan['clan_name'])
                <tr style="height: 15px;">
                    <td><img class="zoom-img" width="45" src="{{ $clan['clan_logo'] }}" alt="{{ $clan['clan_name'] }}" /> {{ $clan['clan_name'] }}</td>
                    <td>{{ $clan['wins'] + $clan['losses'] + $clan['draws'] }}</td>
                    <td>{{ $clan['wins'] }}</td>
                    <td>{{ $clan['losses'] }}</td>
                    <td>{{ $clan['draws'] }}</td>
                    <td>{{ number_format((float)$clan['win_loss_ratio'], 2, '.', '') }}</td>
                </tr>
                @endif
            @endforeach
            </tbody>
        </table>
        @if($game->id == 6)
            <p class="margin-t-10 bold-content text-center">N.B. Please note that these stats are being tracked from the EAX Siege Xbox League Y4 S3 and EAX & PAGU PC Siege leagues and will be tracked from now on.</p>
        @endif
    </div>
@endsection
