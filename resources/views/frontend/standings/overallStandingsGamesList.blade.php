@extends('layouts.app')

@section('content')
    <div class="container">
        <h2 class="tournament-head">Overall Clan tournament standings for games</h2>
        <div class="col-md-6 text-center">
            <h3>{{ $games->game_name }}</h3>
            <img src="{{ $games->game_banner }}">
            @foreach($platforms as $platform)
                <div class="col-md-6 margin-t-10">
                    <a href="">
                        @if($platform['name'] == "Xbox")
                            <a href="/overall-clan-standings-by-game?game_id={{ $games->id }}&platform_id={{  $platform['id'] }}"><div class="btn btn-primary btn-lg"><img width="45" src="/images/logos/xbox-logo.png">&nbsp;&nbsp;{{ $platform['name'] }}</div></a>
                        @elseif($platform['name'] == "Uplay")
                            <a href="/overall-clan-standings-by-game?game_id={{ $games->id }}&platform_id={{  $platform['id'] }}"><div class="btn btn-primary btn-lg"><img width="48" src="/images/logos/uplay-w.png">&nbsp;&nbsp;{{ $platform['name'] }}</div></a>
                        @endif
                    </a>
                </div>
            @endforeach
        </div>
    </div>
@endsection