 @if($tournament->id == 10 && $tournamentRegisteredIndividualsStats !== null)
    <a href="/tournaments/individual-stats/{{ $tournament->id }}"><button style="font-size:12px;height:40px;" class="btn btn-primary">Check individual stats</button></a>
    @endif
    @if(!empty($tournament->tournamentStandings->standings))
        {!! $tournament->tournamentStandings->standings !!}
    @else
        <?php $pool = 1 ?>
        <?php $pool2 = 1 ?>
            {{--@if(!$standing->isEmpty())--}}
            <h2 class="tournament-head">Pool 1</h2>
        <table id="pool-table" class="table scrollable-table">
            <thead>
            <tr style="height: 15px;">
                <th style="height: 15px;">Standing</th>
                <th style="height: 15px;">Team</th>
                <th style="height: 15px;">Played</th>
                <th style="height: 15px;">Won</th>
                <th style="height: 15px;">Lost</th>
                @if($tournament->gamesList->game_name == 'Rainbow Six Siege')
                <th style="height: 15px;">Maps Won</th>
                <th style="height: 15px;">Maps Lost</th>
                <th style="height: 15px;">Rounds Won</th>
                @endif
                <th style="height: 15px;">Forfeit</th>
                <th style="height: 15px;">Total Score</th>
            </tr>
            </thead>
            <tbody>
            @foreach($standings1 as $standingData)
                <tr style="height: 15px;">
                    <td style="height: 15px;">{{ $pool }}</td>
                    <td style="height: 15px;">{{ $standingData->clan->clan_name }}</td>
                    <td style="height: 15px;">{{ $standingData->matches_played }}</td>
                    <td style="height: 15px;">{{ $standingData->matches_won }}</td>
                    <td style="height: 15px;">{{ $standingData->matches_lost }}</td>
                    @if($tournament->gamesList->game_name == 'Rainbow Six Siege')
                    <td style="height: 15px;">{{ $standingData->maps_won }}</td>
                    <td style="height: 15px;">{{ $standingData->maps_lost }}</td>
                    <td style="height: 15px;">{{ $standingData->rounds_won }}</td>
                    @endif
                    <td style="height: 15px;">{{ $standingData->forfeit }}</td>
                    <td style="height: 15px;">{{ $standingData->total_score }}</td>
                </tr>
                <?php $pool++; ?>
            @endforeach
            </tbody>
        </table>

            <h2 class="tournament-head">Pool 2</h2>
            <table id="pool-table" class="table scrollable-table">
                <thead>
                <tr style="height: 15px;">
                    <th style="height: 15px;">Standing</th>
                    <th style="height: 15px;">Team</th>
                    <th style="height: 15px;">Played</th>
                    <th style="height: 15px;">Won</th>
                    <th style="height: 15px;">Lost</th>
                    @if($tournament->gamesList->game_name == 'Rainbow Six Siege')
                        <th style="height: 15px;">Maps Won</th>
                        <th style="height: 15px;">Maps Lost</th>
                        <th style="height: 15px;">Rounds Won</th>
                    @endif
                    <th style="height: 15px;">Forfeit</th>
                    <th style="height: 15px;">Total Score</th>
                </tr>
                </thead>
                <tbody>
                @foreach($standings2 as $standingData)
                    <tr style="height: 15px;">
                        <td style="height: 15px;">{{ $pool2 }}</td>
                        <td style="height: 15px;">{{ $standingData->clan->clan_name }}</td>
                        <td style="height: 15px;">{{ $standingData->matches_played }}</td>
                        <td style="height: 15px;">{{ $standingData->matches_won }}</td>
                        <td style="height: 15px;">{{ $standingData->matches_lost }}</td>
                        @if($tournament->gamesList->game_name == 'Rainbow Six Siege')
                            <td style="height: 15px;">{{ $standingData->maps_won }}</td>
                            <td style="height: 15px;">{{ $standingData->maps_lost }}</td>
                            <td style="height: 15px;">{{ $standingData->rounds_won }}</td>
                        @endif
                        <td style="height: 15px;">{{ $standingData->forfeit }}</td>
                        <td style="height: 15px;">{{ $standingData->total_score }}</td>
                    </tr>
                    <?php $pool2++; ?>
                @endforeach
                </tbody>
            </table>
    {{--@endif--}}
@endif