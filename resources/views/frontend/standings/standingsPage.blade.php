<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <title>Early Axes E-Sports | Xbox & PC Gaming</title>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-116930486-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'UA-116930486-1');
    </script>
    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <script>
        (adsbygoogle = window.adsbygoogle || []).push({
            google_ad_client: "ca-pub-1972342625386980",
            enable_page_level_ads: true
        });
    </script>
    <!-- Styles -->
    <link href="{{ asset('css/all.css?v=1.4') }}" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Titillium Web' rel='stylesheet'>
    <script src="https://use.fontawesome.com/e07165561c.js"></script>
</head>
<style>
    .container {
        padding-left:0 !important;
        padding-right:0 !important;
        margin-top: 0 !important;
    }
    h2 {
        font-weight: bold;
    }
    a {
        color:blue;
    }
    p {
        color:#000;
    }
    @media (max-width: 767px) {
        .scrollable-table {
            display: block;
            overflow-x: auto;
            white-space: nowrap;
        }
    }

</style>
<div class="site-wrapper responsive padding-r-10 padding-l-10 margin-b-15">
    <div class="container">

        @if($tournament->standings_type == 1)
            <?php $x = 1 ?>
            <br/>
            <br/>
            @include('frontend.partials.tournaments.clan-standings')
        @elseif($tournament->standings_type == 2)
            <div class="row padding-l-10 padding-r-10">
                <iframe width="100%" height="800px" src="{{ $tournament->toornament_standings }}?_locale=en_US&theme=" scrolling="no" allowfullscreen frameborder="0"></iframe>
            </div>
        @else
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <h2 class="text-center">Standings have not been added yet.</h2>
        @endif
    </div>
</div>