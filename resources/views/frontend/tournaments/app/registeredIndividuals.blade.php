<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <title>Early Axes E-Sports | Xbox & PC Gaming</title>
    <!-- Styles -->
    <link href="{{ asset('css/all.css?v=1.0') }}" rel="stylesheet">
</head>
<style>
    .container {
        margin-top: 0 !important;
    }

    h2 {
        font-weight: bold;
    }

    img {
        left: 0;
        top: 0;
        width: 100%;
        height: auto;
    }
    a {
        color: blue;
    }
    p {
        color: #000;
    }
    .user-logo-single {
         height: auto !important;
    }
    .site-wrapper {
        background-color: #007AF7;
    }
</style>
<div class="site-wrapper responsive padding-r-10 padding-l-10 margin-b-15">
    <div class="container">
        <div class="row margin-t-15">
            @foreach($tournamentRegisteredIndividuals as $player)
                <div class="col-md-3 col-xs-12 margin-b-10">
                    <div class="player-container">
                        @if($player->users)
                            <a target="_blank" href="/user-profile/{{ $player->users->id }}/{{ $player->users->name }}">
                                @else
                                    <a target="_blank" href="/user-profile/">
                                        @endif
                                        @if($player->users)
                                            @if(!empty($player->users->gamer_pic))
                                                <img class="user-logo-single" src="{{ $player->users->gamer_pic }}">
                                            @else
                                                <img class="user-logo-single" src="/images/default-no-image-eax.jpeg">
                                            @endif
                                        @else
                                            <img class="user-logo-single" src="/images/default-no-image-eax.jpeg">
                                        @endif
                                    </a>
                                    @if($player->users)
                                        @if($tournament->tournament_platform == 1)
                                            <div style="color:#000;"
                                                 class="bottom-left">{{ $player->users->gamer_tag or '' }}</div>
                                        @endif
                                        @if($tournament->tournament_platform == 2)
                                            <div style="color:#000;"
                                                 class="bottom-left">{{ $player->users->psn_id or '' }}</div>
                                        @endif
                                        @if($tournament->tournament_platform == 3)
                                            <div style="color:#000;"
                                                 class="bottom-left">{{ $player->users->steam_id or '' }}</div>
                        @endif
                            </a>
                        @endif
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>