@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row padding-t-10">
            <div class="col-md-12">
                <img src="{{ $tournament ? $tournament->tournament_banner : '' }}">
                <h2 class="text-center">{{ $tournament ? $tournament->tournament_title : '' }}</h2>
                @include('frontend.tournaments.fixtures-display-partial')
            </div>
        </div>
    </div>
@endsection