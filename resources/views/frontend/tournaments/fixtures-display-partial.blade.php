@if(isset($results))
    <style>
        .vs {
            font-size: 30px;
            font-weight: bold;
            color: #000;
            margin-top: 65px;
            margin-left: 10px;
        }
        .result-fixture {
            height:36px;
        }
    </style>
    <?php $x = 1; ?>
    <div class="row fixture">
        <div class="row slide-out-div-1">
            @foreach($results as $fixture)
                <div class="col-md-4 card margin-t-25 padding-t-10">
                    <div class="row date-time-bar">
                        <div style="padding-left:0;" class="col-md-12 text-center date-time-fixture">
                            Date: {{ $fixture->fixture->date }}
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="row date-time-bar-mobile">
                            <div class="col-xs-6 text-center">
                                Date: {{$fixture->date}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5 padding-l-r-0 player-container">
                        <div class="player-container">
                            <a target="_blank" href="/clan-profile/{{ $fixture->clan_1_id }}/{{ $fixture->clan_1 }}">
                                <img width="100%" src="{{ $fixture->clan1->clan_logo }}">
                            </a>
                            <div style="color:#000;" class="bottom-left">{{ $fixture->clan_1 }}</div>
                        </div>
                    </div>
                    <div class="col-md-2 vs-fixture">
                        VS
                    </div>
                    <div class="col-md-5 padding-l-r-0 clan-image">
                        <div class="player-container">
                            <a target="_blank" href="/clan-profile/{{ $fixture->clan_2_id }}/{{ $fixture->clan_2 }}">
                                <img width="100%" src="{{ $fixture->clan2->clan_logo }}">
                            </a>
                            <div style="color:#000;" class="bottom-left">{{ $fixture->clan_2 }}</div>
                        </div>
                    </div>
                    <div class="col-md-12 margin-t-10">
                       @if($fixture->clan_1_score > $fixture->clan_2_score)
                            <p class="bold-content text-center">{{ $fixture->clan_1 }} beat {{ $fixture->clan_2 }} : {{ $fixture->clan_1_score }} - {{ $fixture->clan_2_score }}</p>
                       @elseif($fixture->clan_1_score < $fixture->clan_2_score)
                            <p class="bold-content text-center">{{ $fixture->clan_1 }} beat {{ $fixture->clan_2 }} : {{ $fixture->clan_1_score }} - {{ $fixture->clan_2_score }}</p>
                       @endif
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endif