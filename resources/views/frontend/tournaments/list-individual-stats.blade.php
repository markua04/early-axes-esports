@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row padding-t-10">
        <form action="/tournaments/individual-stats/search/{{$tournament->id}}" method="POST" role="search">
            {{ csrf_field() }}
            <div style="padding-right: 0" class="input-group col-md-11">
                <input style="border-radius: 5px 0px 0px 5px;height: 35px;" type="text" class="form-control" name="q"
                       placeholder="Search gamer tag or clan name"> <span class="input-group-btn">
                    </span>
            </div>
            <div style="padding-left:0" class="col-md-1">
                <button type="submit" class="btn btn-default search-btn">
                    <span class="glyphicon glyphicon-search">Search</span>
                </button>
            </div>
        </form>
    </div>
    @if(!empty($check) && $check == 1)
    <h3 class="alert">No results found for that gamer tag.</h3>
    @endif
    <h2>Individual Stats for {{ $tournament->tournament_title }} as of {{ $dateAdded->date_added }}</h2>

    <table class="table table-bordered dark-table scrollable-table">
        <thead>
        <tr>
            <th>Gamer</th>
            <th>Clan</th>
            <th>Matches Played</th>
            <th>Average Score</th>
            <th>Scores</th>
            <th>Kills</th>
            <th>Deaths</th>
            <th>K/D</th>
            <th>Assists</th>
        </tr>
        </thead>
        <tbody>
        @foreach($stats as $stat)
        <tr>
            <td>{{ $stat->gamer_tag }}</td>
            <td>{{ $stat->clan_name }}</td>
            <td>{{ $stat->matches_played }}</td>
            <td>{{ round($stat->avg_score) }}</td>
            <td>{{ $stat->scores }}</td>
            <td>{{ $stat->kills }}</td>
            <td>{{ $stat->deaths }}</td>
            @if($stat->kills == 0 || $stat->deaths == 0)
            <td>0</td>
            @else
            <td>{{ number_format($stat->kills/$stat->deaths, 2, ".", "") }}</td>
            @endif
            <td>{{ $stat->assists }}</td>
        </tr>
        @endforeach
        </tbody>
    </table>
</div>
@endsection