@extends('layouts.app')

@section('content')
    <div class="container">
        <h1 class="margin-b-15 tournament-head padding-t-15">Tournaments</h1>
        <div class="row margin-b-15">
        <div id="app2">
            <tournaments-list-component :games='{!! $gamesObj !!}' :platforms='{!! $platformsObj !!}'></tournaments-list-component>
        </div>
        @foreach($tournaments as $tournament)
            <div class="col-md-12 margin-b-30 tournament-wrapper caret margin-t-50-mobi">
                <div class="row" style="margin-top: 4px;">
                    <a href="/view-tournament/{{$tournament->id}}/{{ str_slug($tournament->tournament_title) }}">
                        <img class="tournament-banner-list" width="100%" src="{{ $tournament->tournament_banner }}">
                    </a>
                </div>
                <div class="row tournament-content-list">
                    <a class="normal-header-link"
                       href="/view-tournament/{{$tournament->id}}/{{ $tournament->tournament_title }}">
                        <div style="font-size: 25px;" class="normal-header-link">{{$tournament->tournament_title}}</div>
                    </a>
                    <span style="font-size: 16px;color:#262626;"><span class="bold-content">Game :</span> {{ isset($tournament->gamesList->game_name) ? $tournament->gamesList->game_name : '' }}</span>
                    <br/>
                    @if($tournament->platform)
                        <span style="font-size: 16px;color:#262626;"><span class="bold-content">Platform :</span> {{ isset($tournament->platform->platform) ? $tournament->platform->platform : '' }}</span>
                        <br/>
                    @endif
                    <span style="font-size: 16px;color:#262626;"><span class="bold-content">Tournament Type :</span> {{ getTournamentTypeNameById($tournament->tournament_type_id) }}</span>
                    <br/>
                    <span style="font-size: 15px;">{!! strip_tags(str_limit($tournament->tournament_info,1500, '')) !!}<a
                                style="font-size:16px;" class="read-more"
                                href="/view-tournament/{{$tournament->id}}/{{ $tournament->tournament_title }}">&nbsp;&nbsp;read more...</a></span>
                </div>
                @if(Auth::check() && Auth::user()->user_level_id == 1)
                    <div onclick="return confirm('Are you sure?')" class="edit-link"><a
                                href="/admin/delete-tournament/{{ $tournament->id }}">Delete</a></div>
                    <div class="edit-link"><a href="/admin/edit-tournament/{{ $tournament->id }}">Edit</a></div>
                @endif
            </div>
        @endforeach
        @if(count($tournaments) > 3)
            {{ $tournaments->links() }}
        @endif
    </div>
@endsection