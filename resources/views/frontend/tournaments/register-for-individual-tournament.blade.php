@extends('layouts.app')

@section('content')
    <div class="container">
        <form class="form-horizontal" method="POST" action="/tournaments/individual-register-for-tournament/{{ $user->id }}">
            {{ csrf_field() }}
            <h2>Select the tournament below to register.</h2>
            <div class="player-amount" style="font-size:1.2em;"></div>
            <div class="form-group">
                <label for="tournament" class="col-md-12 control-label">Select Tournament</label>
                <div class="col-md-12">
                    <select name="tournament" class="tournament">
                        @foreach($tournaments as $tournament)
                            <option value="{{ $tournament->id }}">{{ $tournament->tournament_title }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12">
                    <button type="submit" class="btn btn-primary pull-right">
                        Register
                    </button>
                </div>
            </div>
        </form>
    </div>
@endsection
