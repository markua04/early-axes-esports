@extends('layouts.app')

@section('content')
    <div class="container">
        <form class="form-horizontal" method="POST" action="/tournaments/clan-register-for-tournament/{{ $clan->id }}/{{ $tournament->id }}">
            {{ csrf_field() }}
            <h2>Select members from your clan to add to the {{ $tournament->tournament_title }} tournament</h2>
            <div class="player-amount" style="font-size:1.2em;"></div>
            <div class="form-check">
                @foreach($users as $user)
                    <div class="col-md-3">
                    <label class="form-check-label">
                        <input name="users[]" class="form-check-input" type="checkbox" value="{{ $user->id }}">
                        {{ $user->gamer_tag }}
                    </label>
                    </div>
                @endforeach
            </div>

            <div class="form-group">
                <div class="col-md-12">
                    <button type="submit" class="btn btn-primary pull-right">
                        Register Clan
                    </button>
                </div>
            </div>
        </form>
    </div>
    </div>
@endsection
