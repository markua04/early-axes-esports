@extends('layouts.app')

@section('content')
    <style>
        /* Tournament PAGE */
        .card {
            margin-top: 0px;
            padding: 30px;
            -webkit-border-top-left-radius: 5px;
            -moz-border-top-left-radius: 5px;
            border-top-left-radius: 5px;
            -webkit-border-top-right-radius: 5px;
            -moz-border-top-right-radius: 5px;
            border-top-right-radius: 5px;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
            height: 450px;
        }

        .card.hovercard {
            position: relative;
            padding-top: 0;
            overflow: hidden;
            text-align: center;
            background-color: #fff;
            background-color: rgba(255, 255, 255, 1);
        }

        .card.hovercard .card-background {
            height: 130px;
        }

        .card-background img {
            -webkit-filter: blur(25px);
            -moz-filter: blur(25px);
            -o-filter: blur(25px);
            -ms-filter: blur(25px);
            filter: blur(25px);
            margin-left: -100px;
            margin-top: -200px;
            min-width: 130%;
        }

        .card.hovercard .useravatar {
            position: absolute;
            top: 50px;
            left: 0;
            right: 0;
        }

        .card.hovercard .useravatar img {
            width: 300px;
            height: auto;
            border-radius: 4px;
            -moz-border-radius: 50%;
            border: 5px solid rgba(255, 255, 255, 0.5);
        }

        .card.hovercard .card-info {
            position: absolute;
            bottom: 14px;
            left: 0;
            right: 0;
        }

        .card.hovercard .card-info .card-title {
            padding: 10px 17px 10px 17px;
            font-size: 38px;
            font-weight: bold;
            line-height: 1;
            color: #FFFFFF;
            background-color: rgba(0, 0, 0, 0.55);
            -webkit-border-radius: 4px;
            -moz-border-radius: 4px;
            border-radius: 4px;
        }

        .card.hovercard .card-info {
            overflow: hidden;
            font-size: 12px;
            padding: 7px 7px 7px 7px;
            border-radius: 4px;
            line-height: 30px;
            color: #737373;
            text-overflow: ellipsis;
        }

        .card.hovercard .bottom {
            padding: 0 20px;
            margin-bottom: 17px;
        }

        .btn-pref .btn {
            -webkit-border-radius: 0 !important;
        }

        .btn-group-justified {
            display: table;
            width: 100%;
            table-layout: fixed;
            border-collapse: separate;
        }

        .btn-group-justified > .btn, .btn-group-justified > .btn-group {
            display: table-cell;
            float: none;
            width: 1%;
        }

        .btn-primary {
            color: #fff;
            background-color: #428bca;
            border-color: #357ebd;
        }

        .btn {
            display: inline-block;
            padding: 6px 12px;
            margin-bottom: 0;
            font-size: 14px;
            font-weight: 400;
            line-height: 1.42857143;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            -ms-touch-action: manipulation;
            touch-action: manipulation;
            cursor: pointer;
            background-image: none;
            border: 1px solid transparent;
            border-radius: 4px;
            width: 100%;
            height: 60px;
        }

        .tab-pane, .nav-tabs {
            margin: 0 auto;
            max-width: 100%;
        }

        .card-title {
            padding: 10px 17px 10px 17px !important;
        }

        .btn-primary {
            color: #fff;
            background-color: #2e2e2e;
            border-color: #ffffff;
        }

        .open-close-operator {
            height: 32px;
            margin-top: 5px;
        }

        @media (max-width: 768px) {
            .open-close-operator {
                height: 32px;
                margin-top: 0 !important;
            }

            .fixture .card {
                padding: 2px 16px 0px 16px !important;
            }

            .btn {
                display: inline-block;
                padding: 8px 12px;
                margin-bottom: 0;
                font-size: 14px;
                font-weight: 400;
                line-height: 1.42857143;
                text-align: center;
                white-space: nowrap;
                vertical-align: middle;
                -ms-touch-action: manipulation;
                touch-action: manipulation;
                cursor: pointer;
                background-image: none;
                border: 1px solid transparent;
                border-radius: 4px;
                width: 100%;
                height: 36px !important;
            }

            .tournamentMobi {
                padding-left: 3%;
                padding-right: 3%;
                margin-top: 7px;
            }

            .well {
                margin-top: 5px;
            }
        }
        .video-container {
            position: relative;
            padding-bottom: 56.25%;
            padding-top: 30px; height: 0; overflow: hidden;
        }

        .video-container iframe,
        .video-container object,
        .video-container embed {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
        }
        .btn.btn-primary {
            color: #fff;
            background-color: #2e2e2e;
            border-color: #2e2e2e;
            box-shadow: 0 2px 2px 0 rgba(46, 46, 46, 0.14), 0 3px 1px -2px rgba(46, 46, 46, 0.2), 0 1px 5px 0 rgba(46, 46, 46, 0.12);
        }
    </style>
    <div class="container tournament-container">
        <div class="row margin-t-5">
            @if(Auth::user() && userIsAdmin(Auth::user()->id) == "yes")
                <h3 class="top-bar-edit">
                    <a href="/admin/edit-tournament/{{ $tournament->id }}">Edit Tournament</a>
                </h3>
            @endif
            @if($tournament->tournament_type_id == 2)
                <div class="tournament-head">
                    <span class="tournament-title">{{ $tournament->tournament_title }}</span>
                    @if($tournament->tournament_status == 1 && $tournament->tournament_registrations == 1)
                        @if(isOwnerOfClan() == "Yes" || isSecondaryCaptain() == 'Yes')
                            <a href="/tournaments/clan-register-for-tournament/{{ Auth::user()->clan_id }}/{{ $tournament->id }}"
                               class="btn btn-primary pull-right enter-clan">
                                Register
                            </a>
                            <a href="/tournaments/clan-register-for-tournament/{{ Auth::user()->clan_id }}/{{ $tournament->id }}"
                               class="btn btn-primary pull-right enter-clan-mobile">
                                Register
                            </a>
                        @else
                            <a href="/clan-registration"
                               class="btn btn-primary pull-right enter-clan">
                                Register
                            </a>
                            <a href="/clan-registration"
                               class="btn btn-primary pull-right enter-clan-mobile">
                                Register
                            </a>
                        @endif
                    @else
                        @if($tournament->tournament_registrations == 0)
                            <div class="col-md-6 pull-right registrations-closed"><span
                                        style="font-size:15px;margin-top:9px;"
                                        class="pull-right">Registrations Closed</span></div>
                            <div style="padding:0;" class="registration-closed-mobile col-md-12 pull-left"><span
                                        class="pull-right">Registrations Closed</span></div>
                        @endif
                    @endif
                </div>
            @else
                <div class="tournament-head">
                    <span class="tournament-title">{{ $tournament->tournament_title }}</span>
                    @if($tournament->tournament_status == 1 && $tournament->tournament_registrations == 1)
                        @if(Auth::check() && $tournament->tournament_type_id == 3)
                            <a href="/tournaments/individual-register-for-tournament/{{ Auth::user()->getAttributes()['id'] }}"
                               class="btn btn-primary pull-right enter-clan">
                                Register
                            </a>
                            <a href="/tournaments/individual-register-for-tournament/{{ Auth::user()->getAttributes()['id'] }}"
                               class="btn btn-primary pull-right enter-clan-mobile">
                                Register
                            </a>
                        @else
                            <a href="/register"
                               class="btn btn-primary pull-right enter-clan">Register</a>
                            <a href="/register"
                               class="btn btn-primary pull-right enter-clan-mobile">Register</a>
                        @endif
                    @else
                        @if($tournament->tournament_registrations == 0)
                            <div class="col-md-6 pull-right registrations-closed"><span
                                        style="font-size:15px;margin-top:9px;"
                                        class="pull-right">Registrations Closed</span></div>
                        @endif
                    @endif
                </div>
                <div style="background-color:#fff">
                    @if($tournament->tournament_status !== 1 && $tournament->tournament_registrations !== 1)
                        <div class="tournament-head">
                            <div class="col-md-6">{{ $tournament->tournament_title }}</div>
                            @if($tournament->tournament_registrations == 0)
                                <div class="col-md-6 pull-right registrations-closed"><span
                                            style="font-size:15px;margin-top:9px;" class="pull-right">Registrations Closed</span>
                                </div>
                                <div style="padding:0;" class="registration-closed-mobile col-md-12 pull-left"><span
                                            class="pull-right">Registrations Closed</span></div>
                            @endif
                        </div>
                    @endif

                    @endif
                    <img src="{{ $tournament->tournament_banner }}" class="tournament-banner">
                    @if($tournament->winner !== null && $tournament->tournament_type_id == 2)
                        <div style="background-color: #2e2e2e;height:85px;padding-top: 10px;" class="col-md-12 registered-clans-count text-center">
                            <img width="50px" src="/images/base-images/winnerscup.png">
                            <p>TOURNAMENT WINNER : {{ isset($tournament->clan->clan_name) ? strtoupper($tournament->clan->clan_name) : '' }}</p>
                        </div>
                    @endif
                    <div class="col-md-12 registered-clans-count text-center">
                        @if($tournament->tournament_type_id == 2)
                            REGISTERED CLAN COUNT : {{ $tournamentRegisteredClansCount }}
                        @else
                            REGISTERED PLAYER COUNT : {{ $tournamentPlayerCount }}
                        @endif
                    </div>
                    @include('frontend.partials.tournaments.tournament-platform')
                    @if($tournament->tournament_registrations == 0)
                        <div style="color:#fff" class="bg-black registration-closed-mobile col-md-12 text-center">
                            Registrations Closed
                        </div>
                    @endif
                    <div class="btn-pref btn-group btn-group-justified btn-group-lg" role="group" aria-label="...">
                        <div class="btn-group" role="group">
                            <button type="button" id="stars" class="btn mobile-btn-tournament btn-primary" href="#tab1"
                                    data-toggle="tab"><span
                                        class="glyphicon glyphicon-star" aria-hidden="true"></span>
                                <div class="hidden-xs">Description</div>
                            </button>
                        </div>
                        @if(!empty($tournament->tournamentRules->rules))
                            <div class="btn-group" role="group">
                                <button type="button" id="favorites" class="btn mobile-btn-tournament btn-default"
                                        href="#tab2" data-toggle="tab"><span
                                            class="glyphicon glyphicon-heart" aria-hidden="true"></span>
                                    <div class="hidden-xs">Rules</div>
                                </button>
                            </div>
                        @endif
                        @if($tournament->tournament_type_id !== 3)
                            <div class="btn-group" role="group">
                                <button type="button" id="following" class="btn mobile-btn-tournament btn-default"
                                        href="#tab3" data-toggle="tab"><span
                                            class="glyphicon glyphicon-user" aria-hidden="true"></span>
                                    <div class="hidden-xs clans-entered">Clans</div>
                                </button>
                            </div>
                        @else
                            <div class="btn-group" role="group">
                                <button type="button" id="following" class="btn mobile-btn-tournament btn-default"
                                        href="#tab3" data-toggle="tab"><span
                                            class="glyphicon glyphicon-user" aria-hidden="true"></span>
                                    <div class="hidden-xs clans-entered">Players</div>
                                </button>
                            </div>
                        @endif

                        <div class="btn-group" role="group">
                            <button type="button" id="following" class="btn mobile-btn-tournament btn-default"
                                    href="#tab4" data-toggle="tab"><span
                                        class="glyphicon glyphicon-user" aria-hidden="true"></span>
                                <div class="hidden-xs">Standings</div>
                            </button>
                        </div>

                        @if($fixturesPool1 && count($fixturesPool1) !== 0)
                            <div class="btn-group" role="group">
                                <button type="button" id="following" class="btn mobile-btn-tournament btn-default"
                                        href="#tab5" data-toggle="tab"><span
                                            class="glyphicon glyphicon-user" aria-hidden="true"></span>
                                    <div class="hidden-xs">Fixtures</div>
                                </button>
                            </div>
                        @endif
                        @if(isset($tournament->tournamentStreams))
                            <div class="btn-group" role="group">
                                <button type="button" id="following" class="btn mobile-btn-tournament btn-default"
                                        href="#tab6" data-toggle="tab"><span
                                            class="glyphicon glyphicon-user" aria-hidden="true"></span>
                                    <div class="hidden-xs">Streams</div>
                                </button>
                            </div>
                        @endif
                    </div>

                    <div class="well">
                        <div class="tab-content col-md-12">
                            <div class="tab-pane fade in active info" id="tab1">
                                {!! $tournament->tournament_info !!}
                            </div>
                            @if(!empty($tournament->tournamentRules->rules))
                                <div class="tab-pane fade in" id="tab2">
                                    <div class="tournament-rules">
                                        {!! $tournament->tournamentRules->rules !!}
                                    </div>
                                </div>
                            @endif
                            <div class="tab-pane fade in" id="tab3">
                                <br/>
                                @if($tournament->tournament_type_id !== 3)
                                    @include('frontend.partials.tournaments.clans-entered')
                                @else
                                    @include('frontend.partials.tournaments.registered-individuals')
                                @endif
                            </div>
                            <div class="tab-pane fade in" id="tab4">
                                <?php $x = 1 ?>
                                @if($tournament->standings_type == 1)
                                @if($tournament->tournament_type_id == 2)
                                    @include('frontend.partials.tournaments.clan-standings')
                                @else
                                    @include('frontend.partials.tournaments.individual-standings')
                                @endif
                                @elseif($tournament->standings_type == 2)
                                    <div class="row padding-l-10 padding-r-10">
                                        <iframe width="100%" height="800px"
                                                src="{{ $tournament->toornament_standings }}?_locale=en_US&theme="
                                                scrolling="no" allowfullscreen frameborder="0"></iframe>
                                    </div>
                                @else
                                    No standings available at the moment.
                                @endif
                            </div>
                            <div class="tab-pane fade in" id="tab5">
                                @if($tournament->challonge_bracket)
                                    <iframe src="{{ $tournament->challonge_bracket }}" width="100%" height="500"
                                            frameborder="0" scrolling="auto" allowtransparency="true"></iframe>
                                @else
                                    <div class="col-md-12">
                                        @if($tournament->tournament_type_id == 2)
                                            @include('admin.fixtures.fixtures-partial')
                                        @else
                                            @include('admin.fixtures.fixtures-partial-individual')
                                        @endif
                                    </div>
                                @endif
                            </div>
                            <div class="tab-pane fade in" id="tab6">
                                <div class="col-md-12">
                                    <div id="app2">
                                    {!! $tournament->tournamentStreams ? $tournament->tournamentStreams->streams : '' !!}
{{--                                    <tournament-bracket :brackets='{!! json_encode($bracketsData['gamesArray']) !!}'></tournament-bracket>--}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
@endsection