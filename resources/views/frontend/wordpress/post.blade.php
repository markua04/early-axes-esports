<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <title>Early Axes E-Sports | Xbox & PC Gaming</title>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-116930486-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'UA-116930486-1');
    </script>
    <!-- Styles -->
    <link href="{{ asset('css/all.css?v=0.9') }}" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Titillium Web' rel='stylesheet'>
    <script src="https://use.fontawesome.com/e07165561c.js"></script>
</head>
<style>
    .container {
        margin-top: 0 !important;
    }
    h2 {
        font-weight: bold;
    }
    img {
        left: 0;
        top: 0;
        width: 100%;
        height: auto;
    }
    .video-container {
        position: relative;
        padding-bottom: 56.25%;
        padding-top: 30px; height: 0; overflow: hidden;
    }
    .video-container iframe,
    .video-container object,
    .video-container embed {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
    }
    a {
        color:blue;
    }
    p {
        color:#000;
    }
</style>
<img src="{{ $post->_embedded->{'wp:featuredmedia'}[0]->media_details->sizes->medium_large->source_url }}">
<div class="site-wrapper responsive padding-r-10 padding-l-10 margin-b-15">
    <div class="container">
        <h2>{!! $post->title->rendered !!}</h2>
        <p class="bold-content">{!! strtoupper('Author: '.$post->_embedded->author[0]->name) !!} - DATE: {{ date('m/d/Y', strtotime($post->date)) }}</p>
        {!! $post->content->rendered !!}
    </div>
    <br />
    <div class="full-article-footer">
        <div class="article-footer">
            <div class="avatar-module">
                <img class="avatar" height="100px" src="{{ $post->_embedded->author[0]->avatar_urls->{'96'}  }}">
            </div>
            <br>
            <p class="bold-content">Written by : {{ $post->_embedded->author[0]->name }}</p>
            <p>{{ $post->_embedded->author[0]->description }}</p>
        </div>
    </div>
</div>