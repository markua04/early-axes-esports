@extends('layouts.app')

@section('content')
    <style>
        h4 {
            font-weight: bold;
            font-size: 23px;
        }
        .home-page-banner {
            display: block;
            margin-bottom: -30px;
            margin-top: 5px;
        }
        .center-image .img-responsive {
            margin: 0 auto;
        }
    </style>
    <div class="container">
        <div class="row margin-t-5 home-page">
            <div id="myCarousel" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarousel" data-slide-to="1"></li>
                    <li data-target="#myCarousel" data-slide-to="2"></li>
                </ol>
                <!-- Wrapper for slides -->
                <div class="carousel-inner">
                    <div class="item active">
                        <img src="/images/base-images/slides/slide1.jpeg" alt="Early Axes Esports" style="width:100%;">
                    </div>

                    <div class="item">
                        <a href="/view-tournaments">
                            <img src="/images/base-images/slides/slide2.jpeg" alt="Early Axes Tournaments" style="width:100%;">
                        </a>
                    </div>

                    <div class="item">
                        <a target="_blank" href="https://earlyaxes.co.za/category/game-reviews/">
                            <img src="/images/base-images/slides/slide3.jpeg" alt="Early Axes Articles" style="width:100%;">
                        </a>
                    </div>
                </div>

                <!-- Left and right controls -->
                <a class="left carousel-control white-text" href="#myCarousel" data-slide="prev">
                    <i class="fa fa-angle-left white-text"></i>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control white-text" href="#myCarousel" data-slide="next">
                    <i class="fa fa-angle-right white-text"></i>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>

            <h3>Welcome to Early Axes
                E-Sports</h3>
            <p>
                We started our journey in 2017 by hosting small leagues and tournaments and with an ever growing Xbox
                community and fantastic response we have created this platform for local Xbox gamers.
                The Early Axes E-sports division was created for the passion of competitive gaming on Xbox one. We
                strive to bring our community the best Xbox competitive scene that south Africa has ever seen.
                The Early Axes E-sports division allows us to further our vision of becoming the e-sports leaders
                in Southern Africa by providing the community with an one stop hub to create, maintain, register for
                upcoming leagues and even get the latest early axes e-sports news.
                We believe that the community drives our passion for competitive gaming and we will be adding more great
                competitive games to our ever growing list of leagues.
            </p>
            <br />
{{--            <div id="countdown"></div>--}}

            @include('frontend.partials.home.latest-news')

            @include('frontend.partials.home.latest-tournaments')

            @include('frontend.partials.home.latest-fixture-results')

            <div class="row">
                <div class="col-md-12">
                <div class="col-md-6 articles-twitter">
                    <h4 style="margin-top:15px;">Early Axes Latest Articles</h4>
                    <div id="articles-container"></div>
                </div>
                <div class="col-md-6 articles-twitter">
                    <h4 style="margin-top:15px;">Early Axes Tweets</h4>
                    <a data-tweet-limit=1 data-chrome="nofooter noborders transparent" class="twitter-timeline"
                       href="https://twitter.com/EarlyAxes?ref_src=twsrc%5Etfw">Tweets by EarlyAxes</a>
                    <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
                </div>
                </div>
            </div>
            <br/><br/>
            <div id="matchesblank">
                <div class="demo">
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <br />
                    <a href="https://play.google.com/store/apps/details?id=za.co.earlyaxesesports.earlyaxes"><img src="/images/google-play.png" class="img-responsive center-block"></a>
                </div>
            </div>
    </div>
    <script>

        $(document).ready(function () {
            $('.carousel').carousel({
                interval: 5000
            });
            var showChar = 450;
            var ellipsestext = "...";
            var moretext = "Read more";
            var lesstext = "Read less";
            $('.more').each(function () {
                var content = $(this).html();
                var textcontent = $(this).text();
                if (textcontent.length > showChar) {
                    var c = textcontent.substr(0, showChar);
                    var html = '<span class="container"><span>' + c + '</span>' + '<span class="moreelipses">' + ellipsestext + '</span></span><span class="morecontent">' + content + '</span>';
                    $(this).html(html);
                    $(this).after('<a href="" class="morelink">' + moretext + '</a>');
                }

            });

            $(".morelink").click(function () {
                if ($(this).hasClass("less")) {
                    $(this).removeClass("less");
                    $(this).html(moretext);
                    $(this).prev().children('.morecontent').fadeToggle(300, function () {
                        $(this).prev().fadeToggle(300);
                    });

                } else {
                    $(this).addClass("less");
                    $(this).html(lesstext);
                    $(this).prev().children('.container').fadeToggle(300, function () {
                        $(this).next().fadeToggle(300);
                    });
                }
                return false;
            });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $(function () {
                $('#vertical-ticker').totemticker({
                    row_height: '167px',
                    next: '#ticker-next',
                    previous: '#ticker-previous',
                    stop: '#stop',
                    start: '#start',
                    mousestop: true,
                    speed: 900,
                    interval: 6000
                });
            });
        });
    </script>

    <script src="https://unpkg.com/react@16.0.0/umd/react.production.min.js" crossorigin></script>
    <script src="https://unpkg.com/react-dom@16.0.0/umd/react-dom.production.min.js" crossorigin></script>
    <script src="{{ 'js/react/babel-6.26.0.min.js' }}"></script>

    <!-- Load our React component. -->
    <script src="{{ 'js/react/modules/home.js' }}" type="text/babel"></script>
{{--    <script src="{{ 'js/react/modules/countdown.js' }}" type="text/babel"></script>--}}

@endsection
