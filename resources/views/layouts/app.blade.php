<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta property="og:image" content="https://earlyaxes-esports.co.za/images/base-images/eaxbanner-new-opt.jpg">
    <meta property="og:image:type" content="image/png">
    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <script>
        (adsbygoogle = window.adsbygoogle || []).push({
            google_ad_client: "ca-pub-1972342625386980",
            enable_page_level_ads: true
        });
    </script>
    <title>Early Axes E-Sports | Xbox & PC Gaming</title>
    <!-- Styles -->
    <link href="{{ mix('css/all.css') }}" rel="stylesheet">
    <link href="{{ asset('css/fstdropdown.min.css') }}" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Titillium Web' rel='stylesheet'>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-116930486-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'UA-116930486-1');
    </script>
    <script src="https://use.fontawesome.com/e07165561c.js"></script>
    <script src="{{ asset('js/choices.min.js') }}"></script>
</head>
<body>
<div id="swipe" class="site-wrapper responsive">
    @include('layouts.nav')
    <div id="app">
        @if(Auth::check() && isOwnerOfClan() == "Yes")
            <sidebar-menu-component :clan_owner="true" :user='{!! json_encode(Auth::user()) !!}'></sidebar-menu-component>
        @elseif(Auth::check() && isOwnerOfClan() == "No")
            <sidebar-menu-component :clan_owner="false" :user='{!! json_encode(Auth::user()) !!}'></sidebar-menu-component>
        @else
            <sidebar-menu-component :clan_owner="false"></sidebar-menu-component>
        @endif
    </div>
    @yield('content')
</div>
@include('layouts.footer')
    <!-- Scripts -->
<script src="{{ mix('/js/app.js') }}"></script>
<script src="{{ asset('js/ckeditor/ckeditor.js')  }}"></script>
<script src="{{ asset('js/fstdropdown.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/3.0.5/daterangepicker.js"></script>
@if(Auth::user()['user_level_id'] == 1)
    <script>
        CKEDITOR.replace('editor');
    </script>
@else
    <script>
        CKEDITOR.replace('editor', {
            removeButtons: 'Source'
        });
    </script>
@endif
</body>
</html>
