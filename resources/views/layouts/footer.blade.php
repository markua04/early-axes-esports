<footer class="site-footer">
    <div class="container footer-container">
        <div class="row">
            <div class="col-md-3 margin-b-10">
                <ul>
                    <li>
                        <h3 class="widget-title">CONNECT WITH US</h3>
                    </li>
                    <li>
                        <a class="fa fa-gamepad fa-lg" href="https://earlyaxes.co.za" target="_blank"> EARLY AXES</a>
                    </li>
                    <li>
                        <a class="fa fa-facebook fa-lg" href="https://www.facebook.com/groups/earlyaxes" target="_blank"> FACEBOOK</a>
                    </li>
                    <li>
                        <a class="fa fa-youtube fa-lg" href="https://www.youtube.com/channel/UCHuUNUqGWWP2p8a7oZQnxdQ" target="_blank"> YOUTUBE</a>
                    </li>
                    <li>
                        <a class="fa fa-twitter fa-lg" href="https://twitter.com/EarlyAxes" target="_blank"> TWITTER</a>
                    </li>
                </ul>
            </div>
            <div class="col-md-3 margin-b-10">
                <h3 class="widget-title">LATEST TOURNAMENTS</h3>

                <ul class="recent-posts">
                    @foreach(getLatestTournaments() as $tournament)
                        <li>
                            <a class="tournament-footer-title" href="{{ URL::to('/') }}/view-tournament/{{$tournament->id}}/{{ str_slug($tournament->tournament_title) }}">{{ $tournament->tournament_title }}</a>
                        </li>
                    @endforeach
                </ul>
            </div>
            <div class="col-md-3 margin-b-10">
                <h3 class="widget-title">Navigation</h3>

                <ul class="menu">
                    <li><a href="/">Home</a></li>
                    <li><a href="/view-tournaments">Tournaments</a></li>
                    <li><a href="/view-clans">Clans</a></li>
                    <li><a href="/list-events">Events</a></li>
                </ul>
            </div>
            <div class="col-md-3 margin-b-10">
                <h3 class="widget-title">THANK YOU!</h3>
                <p>For visiting the home of Early Axes! If you have any enquiries or suggestions please email us on: info@earlyaxes.co.za</p>
            </div>
        </div>
    </div>
    <div class="copyright-footer text-center">
        <div class="container">
            <div class="row">
                <div class="col-md-12">COPYRIGHT EARLY AXES 2019</div>
            </div>
        </div>
    </div>
</footer>