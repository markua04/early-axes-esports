<nav class="site-navigation">
    <ul>
        @guest
        <li class="menu-item-has-children">
            <a href="/login">Login</a>
            <a href="/register">Register</a>
        </li>
        @endguest
    </ul>
</nav>