<div class="nav-wrap main-menu">
    <div class="container">
        <div class="site-logo retina">
            <a href="/">
                <img src="{{ asset('images/logos/blue-white.png') }}" alt="Early Axes logo">
            </a>
        </div>
        <nav class="site-navigation">
            <ul>
                <li class="menu-item-has-children">
                @guest
                <a href="/view-tournaments">Tournaments</a>
                <a href="/view-clans">Clans</a>
                <a href="/list-events">Events</a>
                <a href="/player-stats">Player Stats</a>
                <a href="/overall-clan-standings">Clan Standings</a>
                <a href="/contact">Contact Us</a>
                <a href="{{ route('login') }}">Login</a>
                <a href="{{ route('register') }}">Register</a>
                </li>
                @else
                    @if(userIsAdmin(Auth::user()->id) == 'yes')
                        @if(Auth::user()->user_level_id == 1)
                            <li class="menu-item-has-children current_page_item">
                                <a style="font-size:1.2em" class="top-level" href="/dashboard">Dashboard<span class="caret"></span></a>
                            </li>
                            <li class="menu-item-has-children current_page_item">
                                <div class="top-level">USERS<span class="caret"></span></div>
                                <ul class="sub-menu">
                                    <li class="dropdown">
                                        <ul role="menu">
                                            <li>
                                                <a href="/manage-users">Manage users</a>
                                            </li>
                                            <li>
                                                <a href="/manage-users">List Users<span class="caret"></span></a>
                                            </li>
                                            <li>
                                                <a href="/admin/resend-registration-email">Resend User Emails<span class="caret"></span></a>
                                            </li>
                                            <li>
                                                <a href="/update-user-gt-approval">Moderate GT Changes<span class="caret"></span></a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li class="menu-item-has-children current_page_item">
                                <div class="top-level">GAMES<span class="caret"></span></div>
                                <ul class="sub-menu">
                                    <li class="dropdown">
                                        <ul role="menu">
                                            <li>
                                                <a href="/admin/create-games">Create Games</a>
                                            </li>
                                            <li>
                                                <a href="/admin/list-games">List Games<span class="caret"></span></a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li class="menu-item-has-children current_page_item">
                                <div class="top-level">NEWS<span class="caret"></span></div>
                                <ul class="sub-menu">
                                    <li class="dropdown">
                                        <ul role="menu">
                                            <li>
                                                <a href="/admin/create-news">Create System News</a>
                                            </li>
                                            <li>
                                                <a href="/admin/list-news">List News<span class="caret"></span></a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                        @endif
                    @endif
                    @if(Auth::user()->getAttributes()['user_level_id'] == 3)
                        <li class="menu-item-has-children current_page_item">
                            <a style="font-size:1.2em" class="top-level" href="/dashboard">Dashboard<span class="caret"></span></a>
                        </li>
                        <li class="menu-item-has-children current_page_item">
                            <div class="top-level">USERS<span class="caret"></span></div>
                            <ul class="sub-menu">
                                <li class="dropdown">
                                    <ul role="menu">
                                        <li>
                                            <a href="/view-users">View Users</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                    @endif
                    <li class="menu-item-has-children current_page_item">
                        <div class="top-level">TOURNAMENTS<span class="caret"></span></div>
                        <ul class="sub-menu">
                            <li class="dropdown">
                                <ul role="menu">
                                    @if(Auth::user()->getAttributes()['user_level_id'] == 1)
                                        <li>
                                            <a href="/admin/create-tournament">Create Tournaments</a>
                                        </li>
                                        <li>
                                            <a href="/admin/manage-tournaments">Manage Tournaments</a>
                                        </li>
                                    @endif
                                    <li>
                                        <a href="/admin/list-tournaments">List Tournaments</a>
                                    </li>
                                    @if(Auth::user()->getAttributes()['user_level_id'] == 1)
                                        <li>
                                            <a href="/admin/create-tournament-rules">Create Tournament Rules</a>
                                        </li>
                                    @endif
                                    @if(Auth::user()->getAttributes()['user_level_id'] == 1)
                                        <li>
                                            <a href="/admin/create-custom-standings">Create Custom Standings</a>
                                        </li>
                                        <li>
                                            <a href="/admin/create-fixed-standings">Create Fixed Standings</a>
                                        </li>
                                    @endif
                                    @if(Auth::user()->getAttributes()['user_level_id'] == 1)
                                        <li>
                                            <a href="/admin/export-tournament-data/">Export Tournament Data</a>
                                        </li>
                                    @endif
                                    @if(Auth::user()->getAttributes()['user_level_id'] == 1)
                                        <li>
                                            <a href="/admin/create-fixture">Create Fixture</a>
                                        </li>
                                        <li>
                                            <a href="/admin/create-individual-fixture/">Create Individual Fixture</a>
                                        </li>
                                        <li>
                                            <a href="/admin/importer">Import Stats</a>
                                        </li>
                                        <li>
                                            <a href="/admin/importer-clan-fixtures">Import Clan Fixtures</a>
                                        </li>
                                        <li>
                                            <a href="/admin/importer-individuals">Import Individual Fixtures</a>
                                        </li>
                                        <li>
                                            <a href="/admin/create-tournament-streams">Create Streams</a>
                                        </li>
                                    @endif
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li class="menu-item-has-children current_page_item">
                        <div class="top-level">EVENTS<span class="caret"></span></div>
                        <ul class="sub-menu">
                            <li class="dropdown">
                                <ul class="dropdown-menu" role="menu">
                                    @if(Auth::user()->getAttributes()['user_level_id'] == 1)
                                        <li>
                                            <a href="/admin/create-event">Create Events</a>
                                        </li>
                                        <li>
                                            <a href="/admin/export-event-data/">Export Event Data</a>
                                        </li>
                                    @endif
                                    <li>
                                        <a href="/list-events">List Events</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li class="menu-item-has-children current_page_item">
                        <a style="font-size:1.2em" class="top-level" href="/player-stats">Stats<span
                                    class="caret"></span></a>
                    </li>
                    <li class="menu-item-has-children current_page_item">
                        <a style="font-size:1.2em" class="top-level" href="/overall-clan-standings">Standings<span
                                    class="caret"></span></a>
                    </li>
                    @if(Auth::user() && Auth::user()->user_level_id == 3)
                        <li class="menu-item-has-children current_page_item">
                            <div class="top-level">CLANS<span class="caret"></span></div>
                            <ul class="sub-menu">
                                <li class="dropdown">
                                    <ul class="dropdown-menu" role="menu">
                                        @if(Auth::user()->clan_id != 0)
                                            <li>
                                                <a href="{{ '/clan-profile/'.Auth::user()->clan_id.'/'.getClanSlug('clan_name') }}">View
                                                    Clan</a>
                                            </li>
                                        @endif
                                        @if(isOwnerOfClan() !== "Yes" && Auth::user()->clan_id == 0)
                                            <li>
                                                <a href="/clan-registration">Create a clan</a>
                                            </li>
                                        @endif
                                        @if(isOwnerOfClan() == "Yes" && Auth::user()->clan_id !== 0)
                                            <li>
                                                <a href="/manage-clan-members/{{ Auth::user()->clan_id }}">Manage Clan Members</a>
                                            </li>
                                            <li>
                                                <a href="/edit-clan/{{ Auth::user()->clan_id }}">Edit clan</a>
                                            </li>
                                        @endif
                                        @if(isSecondaryCaptain() == 'Yes'|| isOwnerOfClan() == "Yes" && Auth::user()->clan_id !== 0)
                                            <li>
                                                <a href="/clan-fixtures-by-tournament/{{ Auth::user()->clan_id }}">Clan Fixtures</a>
                                            </li>
                                        @endif
                                        <li>
                                            <a href="/view-clans">List Clans<span class="caret"></span></a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li class="menu-item-has-children current_page_item">
                            <a style="font-size:1.2em" class="top-level" href="/contact">Contact Us<span
                                        class="caret"></span></a>
                        </li>
                        <li class="menu-item-has-children current_page_item">
                            <a style="font-size:1.2em" class="top-level" href="/help">Help<span
                                        class="caret"></span></a>
                        </li>
                    @else
                        <li class="menu-item-has-children current_page_item">
                            <div class="top-level">CLANS<span class="caret"></span></div>
                            <ul class="sub-menu">
                                <li class="dropdown">
                                    <ul class="dropdown-menu" role="menu">
                                        @if(Auth::user()->clan_id != 0)
                                            <li>
                                                <a href=/clan-profile/{{ Auth::user()->clan_id.'/'.getClanSlug() }}">View Clan</a>
                                            </li>
                                        @endif
                                        @if(Auth::user()['user_level_id'] == 1)
                                            <li>
                                                <a href="/moderate-clans">Moderate Clans</a>
                                            </li>
                                        @endif
                                        <li>
                                            <a href="/clan-registration">Create a clan</a>
                                        </li>
                                        @if(isOwnerOfClan() == "Yes" && Auth::user()->clan_id !== 0)
                                            {{--
                                            <li>--}}
                                            {{--<a href="/create-clan-news">Create clan news</a>--}}
                                            {{--
                                        </li>
                                        --}}
                                            <li>
                                                <a href="/clan-fixtures-by-tournament/{{ Auth::user()->clan_id }}">Clan Fixtures</a>
                                            </li>
                                            <li>
                                                <a href="/edit-clan/{{ Auth::user()->clan_id }}">Edit clan</a>
                                            </li>
                                            <li>
                                                <a href="/manage-clan-members/{{ Auth::user()->clan_id }}">Manage Clan Members</a>
                                            </li>
                                        @endif
                                        <li>
                                            <a href="/view-clans">List Clans<span class="caret"></span></a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                    @endif
                    @if(userIsAdmin(Auth::user()->id) == "no")
                        <li class="menu-item-has-children current_page_item">
                            <div class="top-level"><img class="gamer_pic_nav" src="{{ Auth::user()->gamer_pic ? Auth::user()->gamer_pic : '/images/default-no-image-eax.jpeg' }}"><span class="caret"></span></div>
                            <ul class="sub-menu">
                                <li class="dropdown">
                                    <ul class="dropdown-menu" role="menu">
                                        <li>
                                            <a href="/edit-user/{{ Auth::user()->id }}">Edit Profile<span class="caret"></span></a>
                                        </li>
                                        <li>
                                            <a href="/individual-fixtures/{{ Auth::user()->id }}">My Fixtures<span class="caret"></span></a>
                                        </li>
                                        <li>
                                            <a href="/user-profile/{{ Auth::user()->id }}/{{ Auth::user()->name }}">View Profile<span class="caret"></span></a>
                                        </li>
                                        <li>
                                            <a href="{{ route('logout') }}"
                                               onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                                Logout
                                            </a>
                                            <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                                  style="display: none;">
                                                {{ csrf_field() }}
                                            </form>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                    @else
                        <li class="menu-item-has-children current_page_item">
                            <div class="top-level"><img class="gamer_pic_nav" src="{{ Auth::user()->gamer_pic ? Auth::user()->gamer_pic : '/images/default-no-image-eax.jpeg' }}"></div>
                            <ul class="sub-menu">
                                <li class="dropdown">
                                    <ul class="dropdown-menu" role="menu">
                                        <li>
                                            <a href="/edit-user/{{ Auth::user()->id}}">Edit Profile<span class="caret"></span></a>
                                        </li>
                                        <li>
                                            <a href="/individual-fixtures/{{ Auth::user()->id }}">My Fixtures<span class="caret"></span></a>
                                        </li>
                                        <li>
                                            <a href="/user-profile/{{ Auth::user()->id }}/{{ Auth::user()->name }}">View Profile<span class="caret"></span></a>
                                        </li>
                                        <li>
                                            <a href="{{ route('logout') }}"
                                               onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                                Logout
                                            </a>
                                            <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                                  style="display: none;">
                                                {{ csrf_field() }}
                                            </form>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                    @endif
                @endguest
            </ul>
        </nav>
    </div>
</div>