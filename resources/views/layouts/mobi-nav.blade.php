<style>
    .site-wrapper {
        background-color: #fff;
        overflow: hidden;
        margin-top: -4px;
    }
</style>
<div class="mobi-menu">
    <div class="site-logo retina">
        <a class="padding-t-5" href="/">
            <img src="{{ asset('images/logos/blue-white.png') }}" alt="Early Axes logo">
        </a>
    </div>
    <div class="sidebar">
        <div class="sidenav">
            <div class="sidenav-scrollable">
            @if(Auth::check())
                <a href="/user-profile/{{ Auth::user()->id }}/{{ Auth::user()->name }}">
                    <img class="gamer_pic_nav"
                         src="{{ Auth::user()->gamer_pic ? Auth::user()->gamer_pic : '/images/default-no-image-eax.jpeg' }}">
                </a>
                <a href="/user-profile/{{ Auth::user()->id }}/{{ Auth::user()->name }}">{{Auth::user()->name }} </a>
                <a href="/individual-fixtures/{{ Auth::user()->id }}">My Fixtures</a>
                <a href="/edit-user/{{ Auth::user()->id}}">Edit Profile</a>
                <hr/>
            @endif
            @guest
                <a href="/">Home</a>
                <a href="/view-tournaments">Tournaments</a>
                <a style="padding-bottom: 2px;" href="/view-clans">Clans</a>
                <a style="padding-bottom: 2px;" href="/list-events">Events</a>
                <a style="padding-bottom: 2px;" href="/player-stats">Player Stats</a>
                <a style="padding-bottom: 2px;" href="/overall-clan-standings">Clan Standings</a>
                <a href="/contact">Contact Us</a>
                <hr style="color:#fff;">
                <a href="{{ route('login') }}">Login</a>
                <a href="{{ route('register') }}">Register</a>
            @else
                <a href="/">Home</a>
                @if(Auth::check())
                    @if(Auth::user() && Auth::user()->user_level_id == 3)
                        <a href="/dashboard">Dashboard</a>
                        <button class="dropdown-btn">Users
                            <i class="fa fa-caret-down"></i>
                        </button>
                        <div class="dropdown-container">
                            <a href="/view-users">View Users</a>
                        </div>
                    @else
                        @if(Auth::user()->getAttributes()['user_level_id'] == 1)
                            <a href="/dashboard">Dashboard</a>
                            <a href="/player-stats">Player Stats</a>
                            <a href="/overall-clan-standings">Clan Standings</a>
                            <button class="dropdown-btn">Users<i class="fa fa-caret-down"></i></button>
                            <div class="dropdown-container">
                                <a href="/manage-users">Manage Users</a>
                                <a href="/view-users">View Users</a>
                                <a href="/admin/resend-registration-email">Resend User Emails</a>
                            </div>
                            <button class="dropdown-btn">News<i class="fa fa-caret-down"></i></button>
                            <div class="dropdown-container">
                                <a href="/admin/create-news">Create News</a>
                                <a href="/admin/list-news">List News</a>
                            </div>
                            <button class="dropdown-btn">Games<i class="fa fa-caret-down"></i></button>
                            <div class="dropdown-container">
                                <a href="/admin/create-games">Create Games</a>
                                <a href="/admin/list-games">List Games</a>
                            </div>
                        @endif
                    @endif
                @endif
                <button class="dropdown-btn">Tournaments<i class="fa fa-caret-down"></i></button>
                <div class="dropdown-container">
                    <a href="/view-tournaments">View Tournaments</a>
                    @if(Auth::user()->getAttributes()['user_level_id'] == 1)
                        <a href="/admin/create-tournament">Create Tournaments</a>
                        <a href="/admin/manage-tournaments">Manage Tournaments</a>
                        <a href="/admin/list-tournaments">List Tournaments</a>
                        <a href="/admin/create-tournament-rules">Create Tournament Rules</a>
                        <a href="/admin/create-custom-standings">Create Custom Standings</a>
                        <a href="/admin/create-fixed-standings">Create Fixed Standings</a>
                        <a href="/admin/export-tournament-data/">Export Tournament Data</a>
                        <a href="/admin/create-fixture">Create Fixture</a>
                        <a href="/admin/create-individual-fixture/">Create Individual Fixture</a>
                        <a href="/admin/importer">Import Stats</a>
                        <a href="/admin/importer-clan-fixtures">Import Clan Fixtures</a>
                        <a href="/admin/importer-individuals">Import Individual Fixtures</a>
                        <a href="/admin/create-tournament-streams">Create Streams</a>
                    @endif
                </div>
                @if(Auth::user()->getAttributes()['user_level_id'] == 1)
                    <button class="dropdown-btn">Events<i class="fa fa-caret-down"></i></button>
                    <div class="dropdown-container">
                        <a href="/admin/create-event">Create Events</a>
                        <a href="/admin/export-event-data/">Export Event Data</a>
                    </div>
                @else
                    <a href="/list-events">Events</a>
                @endif
                @if(Auth::user()->user_level_id == 1)
                    <button class="dropdown-btn">Clans<i class="fa fa-caret-down"></i></button>
                    <div class="dropdown-container">
                        @if(Auth::user()->getAttributes()['user_level_id'] == 1)
                            @if(Auth::user()->clan_id !== 0)
                                <a href="{{ '/clan-profile/'.Auth::user()->clan_id.'/'.getClanSlug() }}">View Clan</a>
                            @endif
                            @if(Auth::user()['user_level_id'] == 1)
                                <a href="/moderate-clans">Moderate Clans</a>
                            @endif
                            @if(isOwnerOfClan() == "No")
                                <a href="/clan-registration">Create a clan</a>
                            @endif
                            @if(isOwnerOfClan() == "Yes")
                                <a href="/manage-clan-members/{{ Auth::user()->clan_id }}">Manage Clan Members</a>
                                <a href="/clan-fixtures-by-tournament/{{ Auth::user()->clan_id }}">Clan Fixtures</a>
                            @endif
                            <a href="/view-clans">List Clans</a>
                        @endif
                    </div>
                @else
                    <button class="dropdown-btn">Clans<i class="fa fa-caret-down"></i></button>
                    <div class="dropdown-container">
                        <a href="/view-clans">List Clans</a>
                        @if(Auth::user()->clan_id !== 0)
                            <a href="/clan-profile/{{ Auth::user()->clan_id.'/'.getClanSlug() }}">View Clan</a>
                        @endif
                        @if(isOwnerOfClan() == "No")
                            <a href="/clan-registration">Create a clan</a>
                        @endif
                        @if(isOwnerOfClan() == "Yes")
                            <a href="/clan-fixtures-by-tournament/{{ Auth::user()->clan_id }}">Clan Fixtures</a>
                            <a href="/manage-clan-members/{{ Auth::user()->clan_id }}">Manage Clan Members</a>
                            <a href="/edit-clan/{{ Auth::user()->clan_id }}">Edit clan</a>
                        @endif
                    </div>
                    <a href="/player-stats">Player Stats</a>
                    <a href="/contact">Contact Us</a>
                    <a href="/help">Help</a>
                @endif
                @if(userIsAdmin(Auth::user()->id) == "no")
                    <a href="{{ route('logout') }}"
                       onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                        Logout
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST"
                          style="display: none;">
                        {{ csrf_field() }}
                    </form>
                @else
                    <a href="{{ route('logout') }}"
                       onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                        Logout
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST"
                          style="display: none;">
                        {{ csrf_field() }}
                    </form>
                @endif
            @endif
            </div>
        </div>
    </div>
</div>
