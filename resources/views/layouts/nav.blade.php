<style>
    .navbar, .dropdown-menu {
        background: rgba(255, 255, 255, 0.25);
        border: none;

    }

    .nav > li > a, .dropdown-menu > li > a:focus, .dropdown-menu > li > a:hover, .dropdown-menu > li > a, .dropdown-menu > li {
        border-bottom: 3px solid transparent;
    }

    .nav > li > a:focus, .nav > li > a:hover, .nav .open > a, .nav .open > a:focus, .nav .open > a:hover, .dropdown-menu > li > a:focus, .dropdown-menu > li > a:hover {
        border-bottom: 3px solid transparent;
        background: none;
    }

    .navbar a, .dropdown-menu > li > a, .dropdown-menu > li > a:focus, .dropdown-menu > li > a:hover, .navbar-toggle {
        color: #fff;
    }

    .dropdown-menu {
        -webkit-box-shadow: none;
        box-shadow: none;
    }

    .nav li:hover:nth-child(8n+1), .nav li.active:nth-child(8n+1) {
        border-bottom: #C4E17F 3px solid;
    }

    .nav li:hover:nth-child(8n+2), .nav li.active:nth-child(8n+2) {
        border-bottom: #F7FDCA 3px solid;
    }

    .nav li:hover:nth-child(8n+3), .nav li.active:nth-child(8n+3) {
        border-bottom: #FECF71 3px solid;
    }

    .nav li:hover:nth-child(8n+4), .nav li.active:nth-child(8n+4) {
        border-bottom: #F0776C 3px solid;
    }

    .nav li:hover:nth-child(8n+5), .nav li.active:nth-child(8n+5) {
        border-bottom: #DB9DBE 3px solid;
    }

    .nav li:hover:nth-child(8n+6), .nav li.active:nth-child(8n+6) {
        border-bottom: #C49CDE 3px solid;
    }

    .nav li:hover:nth-child(8n+7), .nav li.active:nth-child(8n+7) {
        border-bottom: #669AE1 3px solid;
    }

    .nav li:hover:nth-child(8n+8), .nav li.active:nth-child(8n+8) {
        border-bottom: #62C2E4 3px solid;
    }

    .mobile-menu .navbar-toggle .icon-bar {
        color: #fff;
        background: #fff;
    }

    .mobile-menu .dropdown-menu {
        list-style-type: none !important;
    }

    .mobile-menu .dropdown {
        list-style-type: none !important;
    }

    .sub-menu .dropdown li {
        list-style-type: none !important;
    }

    .sub-menu .dropdown li a {
        margin-left: 0;
        padding-left: 0;
    }

    .sub-menu .dropdown ul {
        margin-left: 0;
        padding-left: 0;
    }

    .parent-link {
        font-size: 15px;
        font-weight: bold;
    }

    ul {
        margin-left: 0;
        padding-left: 0;
    }

    .navbar, .dropdown-menu {
        background: rgba(255, 255, 255, 0);
        border: none;
    }

    @media (max-width: 767px) {
        .responsive .site-navigation {
            background-color: transparent;
            display: none;
        }

        .mobile-menu {
            display: block;
        }

        .main-menu {
            display: none;
        }

        .scrollable-table {
            display: block;
            overflow-x: auto;
            white-space: nowrap;
        }
    }

    @media (min-width: 767px) {
        .responsive .site-navigation {
            display: block;
        }

        .mobile-menu {
            display: none;
        }
    }

    @media (min-width: 768px) {
        .responsive .site-navigation .sub-menu {
            background-color: #fff;
            border-left: 1px solid #f6f6f6;
            border-right: 1px solid #f6f6f6;
            height: 0;
            overflow: hidden;
            padding: 0;
            color: black;
            margin-left: -102px;
            position: absolute;
            left: 50%;
            top: 100%;
            width: 204px;
            -webkit-transition: height 0.3s;
            transition: height 0.3s;
        }

        .responsive .site-navigation .sub-menu a {
            display: block;
            color: black;
            padding: 6px 24px;
        }
    }

</style>
@include('layouts.mobi-nav')
@include('layouts.main-nav')
@include('alerts.status')