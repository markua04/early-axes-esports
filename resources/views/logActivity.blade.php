@extends('layouts.app')

@section('content')

<div class="container">
    <h1>Log Activity Lists</h1>
    <table style="white-space: nowrap;display: block;overflow-x: auto;" class="table table-bordered dark-table scrollable-table">
        <tr>
            <th>No</th>
            <th>Subject</th>
            <th>URL</th>
            <th>Method</th>
            <th>Ip</th>
            <th width="300px">User Agent</th>
            <th>User Id</th>
            <th>Name</th>
            <th>Surname</th>
            <th>Users Registered</th>
            <th>Date</th>
        </tr>
        @if($logs->count())
            @foreach($logs as $key => $log)
                <tr>
                    <td>{{ ++$key }}</td>
                    <td>{{ $log->subject }}</td>
                    <td class="text-success">{{ $log->url }}</td>
                    <td><label class="label label-info">{{ $log->method }}</label></td>
                    <td class="text-warning">{{ $log->ip }}</td>
                    <td class="text-danger">{{ $log->agent }}</td>
                    <td>{{ $log->user_id }}</td>
                    <td>{{ $log->user_name }}</td>
                    <td>{{ $log->user_surname }}</td>
                    <td>{{ $log->data }}</td>
                    <td>{{ $log->created_at }}</td>
                </tr>
            @endforeach
        @endif
    </table>
    {{ $logs->links() }}
</div>

@endsection