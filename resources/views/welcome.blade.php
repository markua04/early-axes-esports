@extends('layouts.app')

@section('content')
<style>
    .site-footer {
        background-color: #313131;
        color: white;
        line-height: 1.9em;
        margin-top: 0;
    }
    .nav-wrap {
        display:none;
    }
    .site-footer {
        background-color: #313131;
        color: white;
        display: none;
        line-height: 1.9em;
        margin-top: 0;
    }
    .margin-top-22 {
        margin-top: 22%;
    }
    @media (min-width: 768px)
    {
        .site-navigation .sub-menu li, .site-navigation > ul > li > a, .site-navigation > ul a {
            color: #fff;
        }
    }
    .site-navigation {
        border: none;
        display: block;
        margin-right: 30px;
        float: right;
    }
</style>
<div class="background-welcome row margin-bottom-15-p">
    @include('layouts.frontend-nav')
    <div class="col-md-6 col-md-offset-3 margin-top-22">
    {{--<span class="main-header-welcome">Early Axes E-Sports</span>--}}
    </div>
</div>

@endsection
