<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('/get-standings-fps/{id}', 'ApiController@getTournamentFpsStandings');

Route::post('/login', 'API\AuthController@login');
Route::middleware(['auth'])->group(function () {
    Route::get('/get-test', 'ApiController@getTest');
});

Route::post('/contact-us-mobi/', 'ApiController@mobiappContact');
Route::post('/submit-clan-fixture-result', 'API\FixturesController@createFixtureResult');
Route::prefix('v1')->namespace('API')->group(function () {
    Route::post('/remove-player-from-tournament','TournamentController@removePlayerFromTournament');
    Route::post('/add-player-to-tournament','TournamentController@addPlayerToTournament');
    // Login
    Route::post('/login','AuthController@postLogin');
    // Register
    Route::post('/register','AuthController@postRegister');
    Route::get('/get-user','AuthController@getUserByToken');
    // Protected with APIToken Middleware
    Route::middleware('APIToken')->group(function () {
        Route::post('/logout','AuthController@postLogout');
        Route::post('/submit-individual-fixture-result', 'IndividualFixturesController@createIndividualFixtureResult');
        Route::get('/get-clan-fixtures','FixturesController@getClanFixtures');
        Route::get('/get-individual-fixtures','IndividualFixturesController@getUserFixtures');
    });
//    Route::middleware('VerifyAdminToken')->group(function () {
//    });
});

