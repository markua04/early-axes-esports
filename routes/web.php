<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('headquarters');
Route::any('/unsubscribe', 'EmailController@unsubscribe');
Route::match(array('GET', 'POST'),'/contact', 'HomeController@contact');
Route::get('/help', 'HomeController@help');
Route::get('/view-tournaments', 'TournamentController@listTournaments');
Route::get('/view-tournament/{id}/{slug}', 'TournamentController@viewTournament');
Route::get('/view-clans', 'ClanController@listClans');
Route::post('/view-clans/search', 'SearchController@searchClans');
Route::post('/tournaments/individual-stats/search/{id}', 'SearchController@searchIndividualStats');
Route::get('/clan-profile/{id}/{slug}', 'HomeController@getClanById');
Route::get('/list-events/', 'EventController@listEvents');
Route::get('/view-event/{id}/{slug}/', 'EventController@viewEvent');
Route::get('/user/verify/{token}', 'Auth\RegisterController@verifyUser');
Route::get('/cache-posts/', 'HomeController@cachePosts');
Route::get('/view-users/', 'ModerateController@listUsers');
Route::post('/view-users/search', 'ModerateController@searchUsers');
Route::any('/get-rl-stats/{platform}/{gamerTag}', 'ApiController@displayRocketLeagueStats');
Route::any('/{id}/list-fixtures', 'FixturesController@listFixtures');
Route::any('/clan-fixtures/{id}/{clanId}', 'ClanController@listFixtures');
Route::any('/fixture-results-list/{id}', 'TournamentController@getFixtureResultsData');

// API for App
Route::get('/get-tournaments', 'TournamentController@getTournaments');
Route::get('/get-tournament/{id}', 'TournamentController@getTournament');
Route::get('/get-rules/{id}', 'ApiController@getTournamentRules');
Route::get('/get-standings/{id}', 'ApiController@getTournamentStandings');
Route::get('/get-fixtures/{id}', 'ApiController@getTournamentFixtures');
Route::get('/get-wp-post/{id}', 'WordpressPostsController@displayPost');
Route::get('/get-custom-post/{id}', 'WordpressPostsController@displayCustomPost');
Route::get('/get-standings-tournament/{id}', 'ApiController@getStandingsPage');
Route::get('/get-registered-clans/{id}', 'ApiController@getClansEntered');
Route::get('/get-registered-players/{id}', 'ApiController@getPlayersEntered');
Route::get('/get-latest-news/{count}', 'ApiController@getLatestNews');
Route::any('/player-stats', 'GameApiStatsController@playerStats');
Route::get('/get-apex-user', 'GameApiStatsController@getApexStatsByUser');
Route::get('/get-siege-user', 'GameApiStatsController@getSiegeStatsByUser');
Route::get('/get-articles/{count}', 'WordpressPostsController@processArticles');
Route::get('/get-individual-standings/{tournamentId}', 'IndividualFixturesController@getUserStandingsByTournament');
Route::get('/overall-clan-standings', 'OverallClanStandingsController@getOverallTournamentGames');
Route::get('/overall-clan-standings-by-game', 'OverallClanStandingsController@getOverallClanStandingsByGameAndPlatform');

Auth::routes();

// Authenticated Routes
Route::middleware(['checkloggedinstatus'])->group(function () {
// User routes
    Route::any('/register-clan', 'ClanController@create');
    Route::match(array('GET', 'POST'),'/clan-registration', 'ClanController@create');
    Route::any('user-profile/{id}/{slug}', 'UserController@viewClanUserProfile');
    Route::any('edit-clan/{id}', 'ClanController@updateClan');
    Route::any('/edit-user/{id}', 'UserController@editUser');
    Route::any('/view-gamers', 'UserController@listUsers');
    Route::match(array('GET', 'POST'),'/manage-clan-members/{id}', 'ModerateController@listUnapprovedClanMembers');
    Route::any('/remove-user-from-clan/{clanId}/{userId}', 'ClanController@removeClanMember');
    Route::any('/create-clan-news', 'ClanController@createClanNews');
    Route::any('/view-clan-news/{slug}', 'HomeController@getClanNewsArticle');
    Route::post('/get-activity/{id}', 'PlatformController@getXboxRecentActivity');
    Route::any('/{id}/list-fixtures', 'FixturesController@listFixtures');
    Route::any('/{id}/{clanId}/submit-fixture-result', 'FixturesController@createFixtureResult');
    Route::any('/clan-fixtures-by-tournament/{clanId}', 'ClanController@listFixturesByTournament');
    Route::any('/clan-fixtures/{id}/{clanId}', 'ClanController@listFixtures');
    Route::any('/join-clan/{clanName}', 'ClanController@joinClanQuestion');
    Route::any('/join-clan-confirm/', 'ClanController@joinClan');

// Admin routes
    Route::any('/manage-users', 'ModerateController@listUsers');
    Route::any('/update-clan', 'ModerateController@updateClanStatus');
    Route::any('/update-user-clan-status', 'ModerateController@updateUserClanStatus');
    Route::any('delete-clan/{id}', 'ClanController@deleteClan');
    Route::any('/moderate-clans', 'ModerateController@listClans');
    Route::any('/get-stats/{game}/{platform}/{gamerTag}', 'UserController@getSiegeStats');

// Tournament Admin Routes
    Route::any('/admin/view-tournament-clans/{id}', 'TournamentController@getClanRegistrationsByClanId');

// System News Routes
    Route::any('/admin/create-news/', 'NewsController@create');
    Route::any('/admin/edit-news/{id}', 'NewsController@edit');
    Route::any('/admin/list-news/', 'NewsController@listNews');
    Route::any('/admin/delete-news/{id}', 'NewsController@delete');

// Tournament Frontend Routes
    Route::any('/tournaments/clan-register-for-tournament/{clanId}/{tournamentId}', 'TournamentController@clanRegisterForTournament');
    Route::any('/tournaments/individual-register-for-tournament/{id}', 'TournamentIndividualRegistrationsController@userRegisterForIndividualTournament');
    Route::any('/clan-register-players-for-tournament/', 'TournamentController@clanRegisterPlayersForTournament');
    Route::get('get-image', 'ImageController@getImage');
    Route::post('ajax-upload-image', ['as' => 'ajax.upload-image', 'uses' => 'ImageController@ajaxUploadImage']);
    Route::get('/get-tournament-player-count/', 'TournamentController@getPlayerCount');
    Route::get('/get-eax-posts/', 'ApiController@getLatestPostsEarlyAxes');
    Route::any('/admin/list-tournaments', 'TournamentController@listTournaments');

// Events Routes
    Route::any('/admin/create-event/', 'EventController@create');
    Route::any('/admin/edit-event/{id}', 'EventController@editEvent');
    Route::any('/admin/delete-event/{id}', 'EventController@deleteEvent');
    Route::any('/event-registration/', 'EventRegistrationController@create');
    Route::any('/admin/export-event-data/', 'EventRegistrationController@generateEventEntries');

// Fixture Routes
    Route::any('/admin/create-fixture/', 'FixturesController@create');
    Route::any('/admin/edit-fixture/{id}', 'FixturesController@edit');
    Route::any('/admin/delete-individual-fixture/{id}', 'IndividualFixturesController@deleteIndividualFixture');
    Route::any('/admin/list-fixture-results/{id}', 'IndividualFixturesController@listFixtureResults');
    Route::any('/admin/list-fixture-results-clan/{id}', 'FixturesController@listFixtureResults');
    Route::any('/admin/list-fixtures/{id}', 'FixturesController@listFixtures');
    Route::any('/admin/delete-fixture/{id}/', 'FixturesController@deleteFixture');
    Route::any('/admin/download-screenshots/{id}/', 'FixturesController@downloadAllScreenshots');
    Route::any('/admin/create-individual-fixture/', 'IndividualFixturesController@create');
    Route::any('/individual-fixtures/{tournamentId}/{userId}', 'IndividualFixturesController@listFixtures');
    Route::any('/individual-fixtures/{userId}', 'IndividualFixturesController@listUserFixtures');
    Route::any('/fixture/{fixtureId}/submit-individual-fixture-result', 'IndividualFixturesController@createIndividualFixtureResult');

// Individual Fixtures
    Route::any('/admin/individual-fixtures/list-fixture-results/{tournamentId}', 'IndividualFixturesController@listFixtureResults');
    Route::any('/admin/download-screenshots/individual-fixtures/{id}', 'IndividualFixturesController@downloadAllScreenshots');

// Admin Routes
    Route::any('/admin', 'HomeController@dashboard');
    Route::any('/dashboard', 'HomeController@dashboard');
    Route::any('/manage-users/search', 'ModerateController@searchUsers');
    Route::any('/admin/create-fixed-standings', 'TournamentController@createTournamentStandings');
    Route::any('/admin/create-custom-standings', 'StandingsController@createStandings');
    Route::any('/admin/list-standings/{id}', 'StandingsController@listStandings');
    Route::any('/admin/list-standings-by-tournament/', 'StandingsController@standingsByTournament');
    Route::any('/admin/delete-fixed-standing/{id}', 'TournamentController@deleteTournamentStandings');
    Route::any('/admin/delete-custom-standing/{id}', 'StandingsController@deleteStanding');
    Route::any('/admin/update-standing/{id}', 'StandingsController@updateStanding');

    Route::any('/admin/importer', 'ExcelImporterController@index');
    Route::any('/admin/importer-individuals', 'ExcelImporterController@individual');
    Route::any('/admin/import-individual-stats', 'ExcelImporterController@importIndividualFixtures');
    Route::any('/admin/importer-clan-fixtures', 'ExcelImporterController@clansFixtureImporter');
    Route::any('/admin/import-clan-fixtures', 'ExcelImporterController@importClanFixtures');
    Route::any('/admin/import', 'ExcelImporterController@import');
    Route::any('/tournaments/individual-stats/{id}', 'TournamentController@getIndividualStats');
    Route::any('/admin/registered-clans/{id}', 'TournamentController@getRegisteredClans');

    // get latest xbox image for user
    Route::any('/latest-xbox-image/{id}', 'UserController@getLatestXboxImage');

    Route::post('/admin/update-clan-registration-status/', 'TournamentController@updateClanForfeitStatus');
    Route::any('/admin/reminder-fixture/{id}', 'FixturesController@remindFixtureResults');
    Route::match(array('GET', 'POST'),'/view-unapproved-members/{id}', 'ModerateController@listUnapprovedClanMembers');
    Route::middleware(['adminusercheck'])->group(function () {
        // Tournament Routes
        Route::any('/admin/create-tournament', 'TournamentController@create');
        Route::any('/admin/manage-tournaments', 'TournamentController@manageTournaments');
        Route::any('/admin/create-tournament-rules', 'TournamentController@createRules');
        Route::any('/admin/create-tournament-standings', 'TournamentController@createTournamentStandings');
        Route::any('/admin/edit-tournament/{id}', 'TournamentController@editTournament');
        Route::any('/admin/delete-tournament/{id}', 'TournamentController@deleteTournament');
        Route::any('/admin/edit-rules/{id}', 'TournamentController@editRules');
        Route::any('/admin/edit-standings/{id}', 'TournamentController@editStandings');
        Route::any('/admin/export-tournament-data/', 'TournamentController@generateTournamentEntries');
        Route::any('/admin/edit-auto-clan-standings/{tournamentId}', 'StandingsController@editStandings');
        Route::any('/admin/edit-auto-user-standings/{tournamentId}', 'StandingsController@editIndividualStandings');
        Route::any('/remove-tournament-clan-registration/{tournamentId}/{clanId}', 'TournamentController@removeClanFromTournament');
        Route::get('admin/log-activity', 'HomeController@logActivity');
        Route::any('/admin/resend-registration-email', 'ModerateController@resendRegistrationEmail');
        Route::any('/edit-user-privileges/{id}', 'ModerateController@manageUser');
        Route::any('/admin/delete-user/{id}', 'ModerateController@deleteUser');
        // Game Routes
        Route::any('/admin/create-games', 'GameController@create');
        Route::any('/admin/list-games', 'GameController@listGames');
        Route::any('/admin/edit-game/{id}', 'GameController@edit');
        Route::any('/admin/delete-game/{id}', 'GameController@delete');
        Route::any('/admin/create-game-countdown', 'ReleaseTimerController@create');

        // Tournament Streams
        Route::any('/admin/create-tournament-streams', 'TournamentController@createStreams');
        Route::any('/admin/edit-tournament-streams/{id}', 'TournamentController@editStreams');
        Route::get('/get-registered-clans-for-tournament/{id}', 'TournamentController@getTournamentClans');
        Route::get('/get-registered-players-for-tournament/{id}', 'TournamentController@getTournamentPlayers');
        Route::any('/admin/manage-tournament-players-by-clan/{clanId}/{tournamentId}', 'TournamentController@getTournamentPlayersByClan');

        // Brackets
        Route::any('/admin/create-brackets', 'BracketsController@create');

        //Auto Fixtures
        Route::any('/admin/create-auto-fixtures/', 'TournamentController@createRoundRobinFixtures');
        Route::any('/admin/auto-fixtures-remove', 'FixturesController@deleteAutoFixtures');
        Route::any('/update-user-gt-approval', 'ModerateController@userGamerTagApprovals');
    });

});